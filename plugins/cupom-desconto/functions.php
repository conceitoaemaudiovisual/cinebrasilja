<?php
	add_action('init', 'registrar_cupom_desconto');
	function registrar_cupom_desconto(){
		$labels = array(
			'name' => 'Cupons de Desconto',
			'singular_name' => 'Cupom de Desconto',
			'add_new' => 'Novo Cupom',
			'add_new_item' => 'Adicionar Novo Cupom',
			'edit_item' => 'Editar Cupom',
			'new_item' => 'Novo Cupom de Desconto',
			'all_items' => 'Visualizar Cupons',
			'view_item' => 'Visualizar Cupom',
			'search_items' => 'Localizar Cupom',
			'not_found' =>  'Nenhum Cupom  Localizado',
			'not_found_in_trash' => 'Nenhum Cupom na Lixeira',
			'parent_item_colon' => '',
			'menu_name' => 'Cupons Desconto'
		);

		$args = array(
			'labels'             => $labels,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => true,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => null,
			'supports'           => array('title')
		);
		register_post_type('cupons_desconto' , $args );
		flush_rewrite_rules();
	}

	add_action( 'admin_init', 'metabox_cupons_detalhes' );
	function metabox_cupons_detalhes() {
		wp_enqueue_script('cupom-js',get_bloginfo('wpurl')."/wp-content/plugins/cupom-desconto/javascript/functions.js");
		wp_enqueue_style('cupom-css',get_bloginfo('wpurl')."/wp-content/plugins/cupom-desconto/css/estilos.css");
		add_meta_box( 'metaboxCupomDetalhes', 'Detalhes do Cupom', 'conteudoCupomDetalhes', 'cupons_desconto', 'normal', 'high' );
		add_meta_box( 'metaboxCupomUtilizacao', 'Utiliza&ccedil;&otilde;es do Cupom', 'conteudoCupomUtilizacao', 'cupons_desconto', 'normal', 'high' );
	}

	function conteudoCupomDetalhes(){
		global $post;
		$dadosCupom = get_post_meta($post->ID, 'cupom-detalhes', true);
 		$seleciona[$dadosCupom[tipo]] = "selected";
		echo "  <link rel='stylesheet' href='//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css'>
				<script src='//code.jquery.com/jquery-1.10.2.js'></script>
 				<script src='//code.jquery.com/ui/1.11.3/jquery-ui.js'></script>
				<table width='100%' cellpadding='0' cellspacing='0'>
					<tr>
						<td width='25%'>Codigo</td>
						<td width='15%'>Valor Desconto</td>
						<td width='13%'>Tipo Desconto</td>
						<td width='13%'>Utiliza&ccedil;&otilde;es</td>
						<td>Periodo de Validade</td>
					</tr>
					<tr>
						<td><input type='text' name='cupom[codigo]' id='codigo' Style='width:95%' value='$dadosCupom[codigo]' maxlength='30' onkeypress='mascaraCampo(this);' ></td>
						<td><input type='text' name='cupom[valor]'  id='valor'  Style='width:90%' value='$dadosCupom[valor]'  maxlength='5'  onkeypress='mascaraVal(this);' onkeyup='formataValor(this)'></td>
						<td>
							<select name='cupom[tipo]' id='tipo' Style='width:90%'>
								<option value='p' $seleciona[p]>Porcentagem</option>
								<option value='v' $seleciona[v]>Valor R$</option>
							</select>
						</td>
						<td><input type='text' name='cupom[quantidade]' id='quantidade' Style='width:90%' value='$dadosCupom[quantidade]' maxlength='4'onkeypress='mascaraVal(this);'></td>
						<td align='left'>
							<table width='100%' cellpadding='0' cellspacing='0'>
								<tr>
									<td><input type='text' name='cupom[datainicio]' id='data-ini' Style='width:100%' value='$dadosCupom[datainicio]' class='formata-data' maxlength='10'onkeypress='mascaraVal(this);' onkeyup='acertadata(this)'></td>
									<td align='center' width='30'>at&eacute;</td>
									<td><input type='text' name='cupom[datafim]' id='data-fim' Style='width:100%' value='$dadosCupom[datafim]' class='formata-data' maxlength='10'onkeypress='mascaraVal(this);' onkeyup='acertadata(this)'></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>";
	}
	function conteudoCupomUtilizacao(){
		global $wpdb, $post;
		$pedidosCupom = $wpdb->get_results("select concat(c.Nome_Completo,' ',c.Sobrenome) Nome,DATE_FORMAT(p.Data_Cadastro, '%d/%m/%Y %H:%i') Data_Cadastro,p.Valor_Plano, pc.Valor_Desconto, pc.Produto_Cadastro_Pedido_ID
											from wp_planos_cupons pc
											inner join wp_planos_pedidos p on p.Plano_Pedido_ID = pc.Produto_Cadastro_Pedido_ID
											inner join wp_produtos_cadastros c on c.Produto_Cadastro_ID = p.Produto_Cadastro_ID
											where pc.Post_ID = $post->ID
											order by p.Data_Cadastro desc");
		foreach($pedidosCupom as $index=>$pedidoCupom){
			if($index==0)
				echo "	<table width='100%' border='0'>
							<tr>
								<td width='080' align='center'><b>N. Pedido</td>
								<td width='150' align='center'><b>Data Pedido</td>
								<td><b>Nome Cliente</td>
								<td width='120' align='center'><b>Valor Plano</td>
								<td width='120' align='center'><b>Valor Desconto</td>
							</tr>
						</table>";
			echo "		<table width='100%' border='0'>
							<tr>
								<td width='080' align='center'>$pedidoCupom->Produto_Cadastro_Pedido_ID</td>
								<td width='150' align='center'>$pedidoCupom->Data_Cadastro</td>
								<td>$pedidoCupom->Nome</td>
								<td width='120' align='center'>R$ $pedidoCupom->Valor_Plano</td>
								<td width='120' align='center'>R$ $pedidoCupom->Valor_Desconto</td>
							</tr>
						</table>";
		}
	}


	add_action( 'save_post', 'saveConteudoCupomDetalhes' );
	function saveConteudoCupomDetalhes(){
		global $post;
		update_post_meta($post->ID, 'codigo-cupom', $_POST['cupom'][codigo]);
		update_post_meta($post->ID, 'cupom-detalhes', $_POST['cupom']);
	}
?>
