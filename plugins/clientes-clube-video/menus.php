<?php
	add_action('init', 'cinebrasilplay_clients_register');
	function cinebrasilplay_clients_register() {
		$labels = array(
			'name' => 'Igrejas',
			'singular_name' => 'Igreja',
			'add_new' => 'Adicionar Nova',
			'add_new_item' => 'Adicionar nova',
			'edit_item' => 'Editar Igreja',
			'new_item' => 'Adicionar nova',
			'all_items' => 'Gerenciar Igrejas',
			'view_item' => 'Visualizar Igreja',
			'search_items' => 'Localizar Igrejas',
			'not_found' =>  'Nenhuma Igreja Localizada',
			'not_found_in_trash' => 'Nenhuma Igreja na Lixeira',
			'parent_item_colon' => '',
			'menu_name' => 'Clientes'
		);
		$args = array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'menu_position' 		=> 3,
			'hierarchical' 			=> true,
			'menu_icon'   			=> 'dashicons-id-alt',
			'supports' 				=> array('title','thumbnail'),
		);
		register_post_type('igrejas' , $args );
		$labels = array(
			'name' => 'Clientes',
			'singular_name' => 'Cliente',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar novo',
			'edit_item' => 'Editar Cliente',
			'new_item' => 'Adicionar novo',
			'all_items' => 'Gerenciar Clientes',
			'view_item' => 'Visualizar Cliente',
			'search_items' => 'Localizar Clientes',
			'not_found' =>  'Nenhum Cliente Localizado',
			'not_found_in_trash' => 'Nenhum Cliente na Lixeira',
			'parent_item_colon' => '',
			'menu_name' => 'Clientes'
		);
		$args = array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'menu_position' 		=> 3,
			'hierarchical' 			=> true,
			'menu_icon'   			=> 'dashicons-id-alt',
			'supports' 				=> array('title'),
			'show_in_menu'			=> 'edit.php?post_type=igrejas',
		);
		register_post_type('clientes' , $args );

		flush_rewrite_rules();
	}
?>