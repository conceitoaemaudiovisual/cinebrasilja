<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	session_start();
	global $wpdb;
	require_once('../../../../wp-config.php');

	global $current_user;
	get_currentuserinfo();
	$dadosUser = get_user_meta($current_user->data->ID);

	$email 		= "operacao@cinebrasil.tv";
	$token 		= "A184D628FD8248B19E408D8F9122DB06";
	$url 		= "https://ws.pagseguro.uol.com.br/v2/pre-approvals/request?email=$email&token=$token";
	$anoFinal 	= date('Y')+2;
	$dataFinal  = $anoFinal."-".date('m-d');

	$nome		= $current_user->data->user_login;
	$email 		= $current_user->data->user_email;

	$plano = get_post(get_user_meta($current_user->data->ID,'plano-selecionado',true),'detalhes-plano',true);
	$planoDetalhes = get_post_meta(get_user_meta($current_user->data->ID,'plano-selecionado',true),'detalhes-plano',true);

	$planoDetalhes[mensal] = '1.00';

	$xmlVenda = utf8_encode("<?xml version='1.0' encoding='UTF-8'?>
								<preApprovalRequest>
									<redirectURL>https://www.cinebrasilja.com/confirmacao-pagamento/</redirectURL>
									<reviewURL>https://www.cinebrasilja.com/wp-content/plugins/clientes-clube-video/pagseguro/revisao.php</reviewURL>
									<reference>".$plano->ID."</reference>
									<sender>
										<name>$nome</name>
										<email>$email</email>
										<phone>
											<areaCode>$ddd</areaCode>
											<number>$telefone</number>
										</phone>
										$endereco
									</sender>
									<preApproval>
										<charge>auto</charge>
										<name>".utf8_encode($plano->post_title)."</name>
										<amountPerPayment>".str_replace(',','.',$planoDetalhes[mensal])."</amountPerPayment>
										<period>Monthly</period>
									</preApproval>
								</preApprovalRequest>");

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, Array('Content-Type: application/xml; charset=ISO-8859-1'));
	curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlVenda);

	$xml = curl_exec($curl);

	$erro = curl_error($curl);
	if($erro != ""){
		echo "<br><br>&nbsp;&nbsp;Ocorreu um erro na transa&ccedil;&atilde;o: $erro";
		exit;
	}

	if($xml == 'Unauthorized'){
		echo "<br><br><br><center>Unauthorized Connection'<br><br>";
		exit;
		header('Location: erro.php?tipo=autenticacao');
		exit;
	}
	curl_close($curl);

	$xml= simplexml_load_string($xml);

	if(count($xml -> error) > 0){
		print_r($xml->error->message);
		exit;
	}else{
		$codigoPreAprovacao = $xml->code;
	}

	header("location:https://pagseguro.uol.com.br/v2/pre-approvals/request.html?code=$codigoPreAprovacao");
?>
<!--
<br>
Link Dirteto:https://pagseguro.uol.com.br/v2/pre-approvals/request.html?code=<?php echo $codigoPreAprovacao;?>
<br>
<a href="https://pagseguro.uol.com.br/v2/pre-approvals/request.html?code=<?php echo $codigoPreAprovacao;?>" target='_blank'>Envia requisicao</a>
-->