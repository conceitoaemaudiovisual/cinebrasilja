jQuery(document).ready(function($){
	jQuery("#cpf").mask("999.999.999-99");
	jQuery("#cnpj").mask("99.999/9999-99");
	jQuery("#telefone, #telefone-responsavel").mask("(99) 9999-9999?9");
	jQuery("#cep").mask("99999-999");
	jQuery('.input-texto').live('keypress',function(e){mascaraCampo($(this));});
	jQuery('.input-numero').live('keypress',function(e){mascaraVal($(this));});
	jQuery('#cep').live('blur',function(e){
		jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clientes-clube-video/localiza-endereco.php?cep='+jQuery(this).val(), data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
			success: function(retorno){
				var campo = retorno.split(',');
				if(campo[0] != ''){
					jQuery('#rua').val(campo[1]+' '+campo[2]);
					jQuery('#bairro').val(campo[3]);
					jQuery('#cidade').val(campo[4]);
					jQuery('#uf').val(campo[5]);
					jQuery('#numero').focus();
				}else{
					jQuery('#rua').val('');
					jQuery('#bairro').val('');
					jQuery('#cidade').val('');
					jQuery('#uf').val('');
					jQuery('#endereco').focus();
				}
			}
		});
	});
})