<?php
	wp_enqueue_style('cliente-video',plugin_dir_url(__FILE__)."css/style.css");
	wp_enqueue_script('cliente-video',plugin_dir_url(__FILE__)."javascript/funcoes.js",array('jquery'));


	add_action( 'admin_init', 'metabox_clube_clientes' );
	function metabox_clube_clientes() {
		add_meta_box('clube-cliente-cliente', 'Dados Cadastrais do Cliente', 'clienteDadosCliente', array (  'clientes' ), 'normal', 'core' );
		add_meta_box('clube-cliente-endereco', 'Endere&ccedil;o', 'clienteEndereco', array ( 'clientes' ), 'normal', 'core' );
		add_meta_box('clube-igreja-planos', 'Detalhe do Plano', 'clientePlanos', array ( 'clientes' ), 'side', 'default' );
		add_meta_box('clube-igreja-extrato', 'Extrato Pagamento', 'clienteExtratoPagamento', array ( 'clientes' ), 'normal', 'default' );
		add_meta_box('clube-igreja-historico', 'Hist&oacute;rico de Contatos', 'clienteHistorico', array ( 'clientes' ), 'normal', 'default' );
	}

	function clienteDados(){
		global $post, $wpdb;
		$dados = get_post_meta($post->ID, 'dados-cliente', true);
		echo "	<table width='100%'>
					<tr>
						<td>DDD+Telefone</td>
						<td>E-mail</td>
						<td>Login</td>
						<td>Senha</td>
					</tr>
					<tr>
						<td><input type='text' name='dados[telefone]' 	class='input-cadastro' id='telefone'	value='$dados[telefone]' Style='width:95%' ></td>
						<td><input type='text' name='dados[email]' 		class='input-cadastro' id='email'		value='$dados[email]'	 Style='width:95%' ></td>
						<td><input type='text' name='dados[login]' 		class='input-cadastro' id='login'		value='$dados[login]'	 Style='width:95%' ></td>
						<td><input type='text' name='dados[senha]' 		class='input-cadastro' id='senha'		value='$dados[senha]'	 Style='width:100%' ></td>
					</tr>
					<tr>
						<td>CNPJ</td>
						<td>Inscri&ccedil;&atilde;o Municipal</td>
						<td>Inscri&ccedil;&atilde;o Estadual</td>
					</tr>
					<tr>
						<td><input type='text' name='dados[cnpj]' 			class='input-cadastro' id='cnpj'			value='$dados[cnpj]'	 			 Style='width:95%' ></td>
						<td><input type='text' name='dados[insc-municipal]'	class='input-cadastro' id='insc-municipal'	value='".$dados['insc-municipal']."' Style='width:95%' placeholder='Isento'></td>
						<td><input type='text' name='dados[insc-estadual]' 	class='input-cadastro' id='insc-estadual'	value='".$dados['insc-estadual']."'  Style='width:95%' placeholder='Isento' ></td>
					</tr>
				</table>";
	}
	function clienteDadosCliente(){
		global $post, $wpdb;
		$dados = get_post_meta($post->ID, 'dados-cliente', true);
		echo "	<table width='100%'>
					<tr>
						<td>CPF</td>
						<td>DDD+Telefone</td>
						<td>E-mail</td>
						<td>Login</td>
						<td>Senha</td>
					</tr>
					<tr>
						<td><input type='text' name='dados[cpf]' 		class='input-cadastro' id='cpf'			value='$dados[cpf]'	 	 Style='width:95%' ></td>
						<td><input type='text' name='dados[telefone]' 	class='input-cadastro' id='telefone'	value='$dados[telefone]' Style='width:95%' ></td>
						<td><input type='text' name='dados[email]' 		class='input-cadastro' id='email'		value='$dados[email]'	 Style='width:95%' ></td>
						<td><input type='text' name='dados[login]' 		class='input-cadastro' id='login'		value='$dados[login]'	 Style='width:95%' ></td>
						<td><input type='text' name='dados[senha]' 		class='input-cadastro' id='senha'		value='$dados[senha]'	 Style='width:100%' ></td>
					</tr>
				</table>";
	}

	function clienteEndereco(){
		global $post, $wpdb;
		$dados = get_post_meta($post->ID, 'endereco-cliente', true);
		echo "	<table width='100%'>
					<tr>
						<td>Cep</td>
						<td>Rua</td>
						<td>Numero</td>
					</tr>
					<tr>
						<td><input type='text' name='endereco[cep]' 		id='cep'			value='$dados[cep]'		Style='width:95%' ></td>
						<td><input type='text' name='endereco[rua]' 		id='rua'			value='$dados[rua]'		Style='width:95%' ></td>
						<td><input type='text' name='endereco[numero]' 		id='numero'			value='$dados[numero]'	Style='width:100%' ></td>
					</tr>
					<tr>
						<td>Complemento</td>
						<td>Bairro</td>
						<td>Cidade</td>
						<td>UF</td>
					</tr>
					<tr>
						<td><input type='text' name='endereco[complemento]' id='complemento'	value='$dados[complemento]'	Style='width:95%' ></td>
						<td><input type='text' name='endereco[bairro]' 		id='bairro'			value='$dados[bairro]'		Style='width:95%' ></td>
						<td><input type='text' name='endereco[cidade]' 		id='cidade'			value='$dados[cidade]'		Style='width:95%' ></td>
						<td><input type='text' name='endereco[uf]' 			id='uf'				value='$dados[uf]'			Style='width:100%' ></td>
					</tr>
				</table>";
	}
	function clienteDadosResponsavel(){
		global $post, $wpdb;
		$dados = get_post_meta($post->ID, 'dados-responsavel', true);
		echo "	<table width='100%'>
					<tr>
						<td>Nome</td>
						<td>DDD+Telefone</td>
						<td>E-mail</td>
						<td>Cargo/Fun&ccedil;ao</td>
					</tr>
					<tr>
						<td><input type='text' name='responsavel[nome]' 		class='input-cadastro' id='nome-responsavel'	 value='$dados[nome]'		Style='width:95%' ></td>
						<td><input type='text' name='responsavel[telefone]' 	class='input-cadastro' id='telefone-responsavel' value='$dados[telefone]'	Style='width:95%' ></td>
						<td><input type='text' name='responsavel[email]' 		class='input-cadastro' id='email-responsavel'	 value='$dados[email]'		Style='width:95%' ></td>
						<td><input type='text' name='responsavel[cargo]' 		class='input-cadastro' id='cargo-responsavel'	 value='$dados[cargo]'		Style='width:100%' ></td>
					</tr>
				</table>";
	}


	function clientePlanos(){
		global $post, $wpdb;
		$dados 		= get_post_meta($post->ID, 'dados-cliente', true);
		$endereco 	= get_post_meta($post->ID, 'endereco-cliente', true);
		$planoDados	= get_post(get_post_meta($post->ID, 'plano-cliente', true));
		$planoSelecionado[get_post_meta($post->ID, 'plano-cliente', true)] = "selected";

		$args = array(
			'posts_per_page'   => -1,
			'orderby'          => 'post_title',
			'order'            => 'asc',
			'post_type'        => 'planos',
			'post_status'      => 'publish'
		);
		$planos = get_posts( $args );
		echo "	<select name='slc-plano' id='slc-plano' Style='width:100%'>
					<option value=''>Cliente Inativo</option>";
		foreach($planos as $plano)
			echo "	<option value='$plano->ID' ".$planoSelecionado[$plano->ID].">$plano->post_title</option>";
		echo "	</select>";

		echo "<textarea Style='width:100%;margin-top:10px;height:90px;'>".geraLinkPagamento($dados,$endereco,$planoDados)."</textarea>";

		echo "<b>Status Pagamento: </b>".statusPagamento();
	}

	function clienteExtratoPagamento(){
//		echo statusPagamento();

	}

	function clienteCodigos(){
		global $post, $wpdb;
		$plano		= get_post_meta($post->ID, 'plano-cliente', true);
		$detalhes 	= get_post_meta($plano,'detalhes-plano',true);
		$membros 	= get_post_meta($post->ID,'membros-plano',true);

		$utilizados = $wpdb->get_results("  select u1.user_id, u2.meta_value, u.display_name
											from wp_usermeta u1
											inner join wp_usermeta u2 on u2.meta_key ='codigo-vinculo' and u2.user_id = u1.user_id
											inner join wp_users u on u.ID = u1.user_id
											where u1.meta_key = 'vinculo' and u1.meta_value = $post->ID");
		foreach($utilizados as $utilizado){
			$codVinculo[$utilizado->meta_value][user] = $utilizado->user_id;
			$codVinculo[$utilizado->meta_value][nome] = $utilizado->display_name;
			$codVinculo[$utilizado->meta_value][cor]  = "red";
		}
		if(is_array($membros)){
			foreach($membros as $indice=>$membro){
				$m++;
				if($indice<$detalhes['membros'])
					echo $m." - <span Style='color:".$codVinculo[$membro][cor]."' title='".$codVinculo[$membro][nome]."'>".$membro."</span><br>";
			}
		}else{
			for($i=1;$i<=$detalhes['membros'];$i++){
				$membros[] = md5($post->ID.$i);
			}
			update_post_meta($post->ID,'membros-plano',$membros);
		}
	}




	add_action('save_post','salvaDetalhesCliente');
	function salvaDetalhesCliente(){
		global $post, $wpdb;

		$user_name 	= $_POST[dados]['login'];
		$password	= $_POST[dados]['senha'];
		$user_email	= $_POST[dados]['email'];

		$user_id = username_exists( $user_name );
		if( !$user_id) {
			$user_id = wp_create_user( $user_name, $password, $user_email );
		}
//		$user_id = wp_update_user( array( 'ID' => $user_id, 'user_pass' => $password));
//		$user_id = wp_update_user( array( 'ID' => $user_id, 'user_login' => $user_name,));
//		$user_id = wp_update_user( array( 'ID' => $user_id, 'user_email' => $user_email));
//		$user_id = wp_update_user( array( 'ID' => $user_id, 'display_name' => $post->post_title));
//		$user_id = wp_update_user( array( 'ID' => $user_id, 'user_nicename' =>  $post->post_name));

		update_user_meta($user_id, 'telefone-cliente', $_POST['dados']['telefone']);
		update_user_meta($user_id, 'plano-cliente', $_POST['slc-plano']);
		update_post_meta($post->ID, 'plano-cliente', $_POST['slc-plano']);
		update_post_meta($post->ID, 'id-cliente', $user_id);
		update_post_meta($post->ID, 'dados-cliente', $_POST['dados']);
		update_post_meta($post->ID, 'dados-responsavel', $_POST['responsavel']);

		update_post_meta($post->ID, 'endereco-cliente', $_POST['endereco']);
	}

	function statusPagamento(){
		global $post;
		$codigo = get_post_meta($post->ID, 'codigo-pagamento',true);
		if($codigo != ""){
			$url = "https://ws.pagseguro.uol.com.br/pre-approvals/$codigo/payment-orders";
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$xml = curl_exec($curl);
			return $xml;
		}else{
			return "Nenhuma transa&ccedil;&atilde;o.";
		}
	}

	add_shortcode('dados-minha-conta', 'dadosMinhaConta');
	function dadosMinhaConta(){
		global $wpdb, $current_user, $post_ID;
		get_currentuserinfo();
		dadosAdicionaisInstituicao($current_user);
	}


	function dadosAdicionaisInstituicao($current_user){
		global $wpdb;
		$email 				= $current_user->data->user_email;
		$dados	 			= get_user_meta($current_user->data->ID);
		$user				= $current_user->data->user_login;
		$plano				= get_post($dados['plano-selecionado'][0]);
		$planos 			= get_planos('select','s');
		$xmlPlano   		= unserialize($dados['dados-plano-contatado'][0]);
		$dataPlano			= substr($xmlPlano['date'], 8,2);
		$dataCancelamento	= $dados['data-final-assinatura'][0];

		if($dados['plano-selecionado'] == '') $plano->post_title = "Plano Livre";

		$assistidosConta = '<table  width="90%" Style="margin:0 auto;float: right;">
								<tr class="bordaInferiorConta">
									<td class="txt-gestao-membros"  height="40">Nome do V&iacute;deo</td>
									<td class="txt-gestao-membros" width="120px" align="center" height="40">Data</td>
									<td class="txt-gestao-membros" width="170px" height="40">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>';
		$videosAssistidosConta = $wpdb->get_results(" select DATE_FORMAT(data_cadastro,'%d/%m/%Y') data_cadastro, p.post_title, post_name
												from aux_users_videos v
												inner join wp_posts p on p.ID = v.Video_ID
												where user_id = ".$current_user->data->ID."
												order by data_cadastro desc");
		foreach($videosAssistidosConta as $videoAssistidoConta){
			$assistidosConta .= '<tr class="bordaInferiorConta">
									<td class="txt-lista-membros" height="40">'.$videoAssistidoConta->post_title.'</td>
									<td class="txt-lista-membros"  align="center" height="40">'.$videoAssistidoConta->data_cadastro.'</td>
									<td class="txt-lista-right"  height="40"><a href="'.$videoAssistidoConta->post_name.'" class="txt-lista-right">Assistir Novamente</td>
									<td>&nbsp;</td>
								</tr>';
		}
		$assistidosConta .= '</table>';
		echo "<div id='assistindo-conta' Style='display:none;'>$assistidosConta;</div>";

		$userID 	= $current_user->data->ID;
		$codRetorno = get_user_meta($userID,'codido-retorno-pagamento',true);

		$tracker 	= get_user_meta($userID,'identificador-pagseguro',true);
		if($userID != ""){
			if($codRetorno != ""){
				$retorno = "https://ws.pagseguro.uol.com.br/v2/pre-approvals/$codRetorno?email=valim@comev.org.br&token=4E9B064D59574CA7B6BC22F7D1EFCC34";
				$ch = curl_init($retorno);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				$data = utf8_decode(curl_exec($ch));
				curl_close($ch);
				$result = simplexml_load_string ($data, 'SimpleXmlElement', LIBXML_NOERROR+LIBXML_ERR_FATAL+LIBXML_ERR_NONE);
				$xml = new SimpleXmlElement($data, LIBXML_NOCDATA);
				$dadosRetorno = json_decode( json_encode($xml) , 1);
			}
		}

		$dadosRetorno['date'] = substr($dadosRetorno['date'],0,10);

		$pagamentoConta = '<table  width="70%" Style="margin:0 auto;float: right;">
								<tr class="bordaInferiorConta">
									<td class="txt-gestao-membros"  height="40">Data</td>
									<td class="txt-gestao-membros" width="210px" align="center" height="40">Cod Transa&ccedil;&atilde;o</td>
									<td class="txt-gestao-membros" width="170px" align="center"height="40">Status</td>
									<td>&nbsp;</td>
								</tr>
								<tr class="bordaInferiorConta">
									<td class="txt-lista-membros" height="40">'.$dadosRetorno['date'].'</td>
									<td class="txt-lista-membros"  align="center" height="40">'.$dadosRetorno['tracker'].'</td>
									<td class="txt-lista-right"  align="center" height="40">'.$dadosRetorno['status'].'</td>
									<td>&nbsp;</td>
								</tr></table>';

		echo "<div id='pagamento-conta' Style='display:none;'>$pagamentoConta</div>";


		if($dataCancelamento != ""){
			$textoCancela = "$('#cancela-assinatura').html('Reativar Assinatura<br><span Style=font-size:10px;>Disponivel at&eacute; $dataCancelamento</span>');jQuery('#alterar-assinatura').remove();";
			$dataPlano = "";
		}

		echo "	<script>
					jQuery(document).ready(function($){
						$('#alterar-email-view').html('$email');
						$('#alterar-telefone-view').html('".$dados['text-telefone-cadastro'][0]."');
						$('#alterar-assinatura-view').html('$plano->post_title');
						$('#alterar-user-view').html('$user');
						$('#cancela-assinatura').attr('href','#');
						$('#videos-assistidos,#cobrancas-extrato').hide();
						$('#videos-assistidos').append($('#assistindo-conta').html());
						$('#cobrancas-extrato').append($('#pagamento-conta').html());

						$textoCancela
					});
					jQuery('#visualiza-assistidos-link').live('click',function(e){
						jQuery('#videos-assistidos').show();
					});
					jQuery('#extrato-cobranca').live('click',function(e){
						jQuery('#cobrancas-extrato').show();
					});

					jQuery('#alterar-user').live('click',function(e){
						jQuery('#alterar-user span, .txtMinhaContaCor').hide();
						jQuery('#alterar-user-view').html(\"<input type='text' placeholder='$user' name='alterar-user-text' id='alterar-user-text' Style='padding: 3px 0px 3px 3px;float:left;'><i class='confirma-alteracao iconPlanosCinzaC' attr='alterar-user-view' value='$email'></i><i class='cancela-alteracao iconPlanosCinzaR' attr='alterar-user-view' value='$user'></i>\");
						jQuery('#alterar-user-text').focus();
					});
					jQuery('#alterar-email').live('click',function(e){
						jQuery('#alterar-email span, .txtMinhaContaCor').hide();
						jQuery('#alterar-email-view').html(\"<input type='text' placeholder='$email' name='alterar-email-text' id='alterar-email-text' Style='padding: 3px 0px 3px 3px;float:left;'><i class='confirma-alteracao iconPlanosCinzaC' attr='alterar-email-view' value='$email'></i><i class='cancela-alteracao iconPlanosCinzaR' attr='alterar-email-view' value='$email'></i>\");
						jQuery('#alterar-email-text').focus();
					});
					jQuery('#alterar-senha').live('click',function(e){
						jQuery('#alterar-senha span, .txtMinhaContaCor').hide();
						jQuery('#alterar-senha-view').html(\"<input type='password' placeholder='Senha: ******' name='alterar-senha-text' id='alterar-senha-text' Style='padding: 3px 0px 3px 3px;float:left;'><i class='confirma-alteracao iconPlanosCinzaC' attr='alterar-senha-view' value='Senha: ******'></i><i class='cancela-alteracao iconPlanosCinzaR' attr='alterar-senha-view' value='Senha: ******'></i>\");
						jQuery('#alterar-senha-text').focus();
					});
					jQuery('#alterar-telefone').live('click',function(e){
						jQuery('#alterar-telefone span, .txtMinhaContaCor').hide();
						jQuery('#alterar-telefone-view').html(\"<input type='text' placeholder='".$dados['telefone-cliente'][0]."' name='alterar-telefone-text' id='alterar-telefone-text' Style='padding: 3px 0px 3px 3px;float:left;'><i class='confirma-alteracao iconPlanosCinzaC' attr='alterar-telefone-view' value='".$dados['telefone-cliente'][0]."'></i><i class='cancela-alteracao iconPlanosCinzaR' attr='alterar-telefone-view' value='".$dados['telefone-cliente'][0]."'></i>\");
						jQuery('#alterar-telefone-text').focus();
					});
					jQuery('#alterar-assinatura').live('click',function(e){
						jQuery('#alterar-assinatura span, .txtMinhaContaCor').hide();
						jQuery('#alterar-assinatura-view').html(\"$planos<i class='confirma-alteracao iconPlanosCinzaC' attr='alterar-assinatura-view'></i><i class='cancela-alteracao iconPlanosCinzaR' attr='alterar-assinatura-view'></i>\");
						jQuery('#alterar-assinatura-text').focus();
					});

					jQuery('#cancela-assinatura').live('click',function(e){
						if(confirm('Confirma o cancelamento da sua assinatura a partir do dia ".$dataPlano."/".date('m')."?')){
							jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clientes-clube-video/cancela-assinatura.php?id=".$current_user->data->ID."&dia=".$dataPlano."', data: dados, dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
								success: function(retorno){
									location.reload();
								}
							});
						}
					});


					jQuery('.cancela-alteracao').live('click',function(e){
						jQuery('#alterar-email span,#alterar-user span, .txtMinhaContaCor').show();
						jQuery('#'+jQuery(this).attr('attr')).html(jQuery(this).attr('value'));
					});
					jQuery('.confirma-alteracao').live('click',function(e){
						idCadastro 	= jQuery('#gerencia-dados-instituicao-view').attr('attr');
						dados = jQuery('#frm-minha-conta-dados').serialize();
						jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clientes-clube-video/atualiza-dados.php?id=".$current_user->data->ID."', data: dados, dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
							success: function(retorno){
								if(retorno.trim()==1){
									open('/home/', '_top');
								}else{
									location.reload();
								}
							}
						});
					});
				</script>";
	}

	function get_planos($tipo, $restrito){
		global $wpdb;
		$optPlano .= "<option value=''>Plano Livre</option>";
		$planos = $wpdb->get_results("select distinct ID, post_title
									  from wp_posts p
									  inner join wp_postmeta pm on pm.post_id = p.ID
									  where post_type = 'planos' and post_status ='publish'
									  order by post_title");
		if($tipo=='select'){
			foreach($planos as $plano)
				$optPlano .= "<option value='$plano->ID'>$plano->post_title</option>";
			return "<select name='planos-disponiveis' id='planos-disponiveis' Style='padding:2px 0px 2px 10px;float:left;'><option value=''>Selecione o plano desejado</option>$optPlano</select>";
		}
	}

?>