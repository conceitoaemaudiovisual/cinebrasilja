<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	global $wpdb;
	require_once('../../../wp-config.php');

	$descricao = $wpdb->get_row("SELECT p.Post_Title, p.Post_Excerpt, pm.meta_value Description,month(post_date) Mes, year(post_date) Ano
								 FROM wp_posts p
								 INNER JOIN wp_postmeta pm ON pm.post_id = p.ID AND pm.meta_key = 'video-detalhes'
								 WHERE p.id = ".$_POST['attr']);
	$arrayDescription = unserialize(unserialize($descricao->Description));
	$tempoDuracao =  $arrayDescription[tempo];
	echo "	<div class='box-video-description' Style='position:absolute;width:265px;height:206px;background:url(".get_bloginfo('wpurl')."/wp-content/images/layout/bg-box-desc.png) top left no-repeat;font-family:arial;'>
				<div Style='width:240px;background-color:#e0b568;text-align:center;border-top-left-radius:2px;border-top-right-radius:2px;margin-top:3px;margin-left:22px;height:20px;'>
					<p Style='font-size:13px;color:#ffffff;text-align:center;'><b>$descricao->Post_Title</b></p>
				</div>
				<div Style='width:225px;margin-top:10px;margin-left:32px;height:140px;margin-bottom:10px;'>
					<p Style='font-size:13px;color:#000000;text-align:left;'>$descricao->Post_Excerpt</p>
				</div>
				<div Style='width:240px;background-color:#620000;text-align:center;border-bottom-left-radius:2px;border-bottom-right-radius:2px;margin-top:3px;margin-left:22px;height:20px;'>
					<span Style='font-size:12px;color:#ffffff;text-align:left;margin-right:80px;'><b>".mostraMeses($descricao->Mes)."/$descricao->Ano</b></span>
					<span Style='font-size:12px;color:#ffffff;text-align:right;'><b>$tempoDuracao</b></span>
				</div>
			</div>";
/*
	function mostraMeses($entrada){
		$mes['1'] = 'JANEIRO';
		$mes['2'] = 'FEVEREIRO';
		$mes['3'] = 'MAR&Ccedil;O';
		$mes['4'] = 'ABRIL';
		$mes['5'] = 'MAIO';
		$mes['6'] = 'JUNHO';
		$mes['7'] = 'JULHO';
		$mes['8'] = 'AGOSTO';
		$mes['9'] = 'SETEMBRO';
		$mes['10'] = 'OUTUBRO';
		$mes['11'] = 'NOVEMBRO';
		$mes['12'] = 'DEZEMBRO';
		return $mes[$entrada];
	}
*/

?>
