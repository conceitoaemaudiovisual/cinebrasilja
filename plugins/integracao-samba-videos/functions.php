<?php
	wp_enqueue_style('clube-video',plugin_dir_url(__FILE__)."css/style.css");
	wp_enqueue_style('clube-video-bxslider',plugin_dir_url(__FILE__)."css/jquery.bxslider.min.css");
	wp_enqueue_script('clube-video',plugin_dir_url(__FILE__)."javascript/funcoes.js",array('jquery'));
	wp_enqueue_script('clube-video-jcar',plugin_dir_url(__FILE__)."javascript/jquery.bxslider.min.js",array('jquery'));
	wp_enqueue_script('clube-video-mask',plugin_dir_url(__FILE__)."javascript/jquery.maskedinput.min.js",array('jquery'));
	wp_enqueue_script('loading',plugin_dir_url(__FILE__)."javascript/async.min.js",array('jquery'));
	wp_enqueue_script('recaptcha',"https://www.google.com/recaptcha/api.js",array('jquery'));

	wp_enqueue_script('cast',"https://www.gstatic.com/cv/js/sender/v1/cast_sender.js?loadCastFramework=1",array('jquery'));
	wp_enqueue_script('clube-video-mask',plugin_dir_url(__FILE__)."javascript/CastVideos.js",array('jquery'));


	add_action( 'admin_init', 'metabox_clube_video' );
	function metabox_clube_video() {
		add_meta_box('clube-anexar-video', 'Anexar V&iacute;deo', 'anexaVideo', 'post', 'side', 'core' );
		add_meta_box('clube-video-destaque', 'Video Destaque', 'destacaVideo', 'post', 'side', 'default' );
		add_meta_box('clube-video-sinopse', 'Sinopse', 'sinopseVideo', 'post', 'normal', 'high' );
		add_meta_box('clube-video-ficha', 'Ficha T&eacute;cnica', 'fichaVideo', 'post', 'normal', 'high' );
		add_meta_box('clube-temporada', 'Videos da Temporada', 'videosTemporada', 'series', 'normal', 'high' );
		add_meta_box('clube-video-destaque', 'Video Destaque', 'destacaVideo', 'serie', 'side', 'default' );
		add_meta_box('clube-video-sinopse', 'Sinopse', 'sinopseVideo', 'series', 'normal', 'high' );
		add_meta_box('clube-video-ficha', 'Ficha T&eacute;cnica', 'fichaVideo', 'series', 'normal', 'high' );
//		add_meta_box('clube-video-detalhes', 'Detalhes do V&iacute;deo', 'detalhesVideo', 'post', 'normal', 'default' );
//		add_meta_box('clube-video-detalhes', 'Detalhes do V&iacute;deo', 'detalhesVideo', 'series', 'normal', 'default' );
		add_meta_box('clube-video-destaque', 'Video Destaque', 'destacaVideo', 'series', 'side', 'default' );
		add_meta_box('clube-autores', 'Videos do Especial', 'videosAutor', 'especiais', 'normal', 'high' );

		add_meta_box('clube-video-especiais', 'Especiais', 'palestranteVideo', 'post', 'side', 'default' );
		add_meta_box('clube-video-espsciais', 'Especiais', 'palestranteVideo', 'series', 'side', 'default' );
	}

	function destacaVideo(){
		global $post;
		$site 			= get_post_meta($post->ID,'video_destaque',true);
		$kids 			= get_post_meta($post->ID,'video_kids',true);
		$textoDestaque 	= get_post_meta($post->ID,'video_destaque_texto',true);
		echo "	<table width='100%' Style='margin-bottom:10px;'>
					<tr>
						<td align='left'>Texto Destaque Banner Mobile</td>
					</tr>
					<tr>
						<td align='left'><input type='text' name='video-destaque-texto' value='$textoDestaque' Style='width:100%;' placeholder='Assista J&aacute;'></td>
					</tr>
				</table>";
		echo "	<table>
					<tr>
						<td width='25' align='right'><input type='checkbox' name='video-destaque' value='checked' $site></td>
						<td width='70' align='left'>Home Site</td>
						<td width='25' align='right'><input type='checkbox' name='video-kids' value='checked' $kids></td>
						<td>Home Kids</td>
					</tr>
				</table>";
	}

	add_action('save_post','video_clube_video_save');
	function video_clube_video_save(){
		global $post;
		if($_POST){
			if(isset($_POST['sinopse-video'])){
				update_post_meta($post->ID,'video-anexado',$_POST['select-video']);
				update_post_meta($post->ID,'select-legenda',$_POST['select-legenda']);

				update_post_meta($post->ID,'video-detalhes',$_POST['video-detalhes']);
				update_post_meta($post->ID,'video_destaque',$_POST['video-destaque']);
				update_post_meta($post->ID,'video_kids',$_POST['video-kids']);
				update_post_meta($post->ID,'video_destaque_texto',$_POST['video-destaque-texto']);

				update_post_meta($post->ID,'sinopse-video',$_POST['sinopse-video']);
				update_post_meta($post->ID,'ficha-video',$_POST['ficha-video']);
				update_post_meta($post->ID,'detalhes-video',$_POST['detalhes-video']);

				update_post_meta($post->ID,'video-serie',$_POST['serie']);
				update_post_meta($post->ID,'video-palestra',$_POST['palestra']);

				update_post_meta($post->ID,'resumo_mobile',$_POST['resumo-mobile']);
				update_post_meta($post->ID,'titulo-episodios',$_POST['titulo-episodios']);

				update_post_meta($post->ID,'video-palestrante',$_POST['video-palestrante']);
				wp_set_object_terms($post->ID, 'series', 'Temporada_video', true );
			}
		}
	}


	function sinopseVideo(){
		global $post,$wpdb;
		$sinopse = get_post_meta($post->ID,'sinopse-video',true);
		$settings = array('editor_height'=>100,'tinymce'=> true ,'media_buttons'=>false);
		wp_editor( $sinopse, 'sinopse-video',$settings);
	}

	function fichaVideo(){
		global $post,$wpdb;
		$ficha = get_post_meta($post->ID,'ficha-video',true);
		$settings = array('editor_height'=>100,'tinymce'=> true ,'media_buttons'=>false);
		wp_editor($ficha,'ficha-video',$settings);
	}

	function detalhesVideo(){
		global $post,$wpdb;
		$ficha = get_post_meta($post->ID,'detalhes-video',true);
		$settings = array('editor_height'=>100,'tinymce'=> true ,'media_buttons'=>false);
		wp_editor($ficha,'detalhes-video',$settings);
	}
	function resumoMobile(){
		global $post,$wpdb;
		$resumo = get_post_meta($post->ID,'resumo_mobile',true);
		$settings = array('editor_height'=>100,'tinymce'=> false ,'media_buttons'=>false);
		wp_editor( $resumo, 'resumo-mobile',$settings);
		echo "<style>#qt_resumo-mobile_toolbar{display:none;}</style>";
	}


	add_action( 'admin_init', 'metabox_planos_detalhes' );
	function metabox_planos_detalhes() {
		add_meta_box( 'metaboxPlanosDetalhes', 'Detalhes do Plano', 'conteudoMetaboxPlanosDetalhes', 'planos', 'side', 'core' );
		function conteudoMetaboxPlanosDetalhes(){
			global $post,$wpdb;
			$detalhesPlano 	= get_post_meta($post->ID,'detalhes-plano',true);
			$monetizacaoPlano[get_post_meta($post->ID,'monetiza-plano',true)] = 'checked';
			$videoHsPlano[get_post_meta($post->ID,'videoHD-plano',true)] = 'checked';
			$planoRestrito[get_post_meta($post->ID,'plano-restrito',true)] = 'checked';

			echo "	<table width='100%'>
						<tr>
							<td>Valor Mensal:</td>
						</tr>
						<tr>
							<td><input type='text' Style='width:095%' name='detalhes[mensal]' 	onkeypress='mascaraVal(this)' onkeyup='formataValor(this)' maxlength='8' value='".$detalhesPlano[mensal]."'></td>
						</tr>
						<tr>
							<td>Acessos Simult&atilde;neos:</td>
						</tr>
						<tr>
							<td><input type='text' Style='width:095%' name='detalhes[simultaneo]' 	onkeypress='mascaraVal(this)' maxlength='2' value='".$detalhesPlano[simultaneo]."'></td>
						</tr>
					</table>
					<table width='100%' height='60'>
						<tr>
							<td width='90'>
								Monetiza&ccedil;&atilde;o:
							</td>
							<td>
								<input type='radio' name='monetiza-video' value='s' ".$monetizacaoPlano['s'].">Sim
								&nbsp;&nbsp;
								<input type='radio' name='monetiza-video' value='n' ".$monetizacaoPlano['']." ".$monetizacaoPlano['n'].">N&atilde;o
							</td>
						</tr>
						<tr>
							<td>
								Utiliza HD:
							</td>
							<td>
								<input type='radio' name='hd-video' value='s' ".$videoHsPlano['s'].">Sim
								&nbsp;&nbsp;
								<input type='radio' name='hd-video' value='n' ".$videoHsPlano['']." ".$videoHsPlano['n'].">N&atilde;o
							</td>
						</tr>
					</table>";
		}
	}
	add_action( 'save_post', 'metabox_planos_detalhes_principais_Save' );
	function metabox_planos_detalhes_principais_Save(){
		global $post;
		update_post_meta($post->ID,'detalhes-plano',$_POST['detalhes']);
		update_post_meta($post->ID,'monetiza-plano',$_POST['monetiza-video']);
		update_post_meta($post->ID,'videoHD-plano',$_POST['hd-video']);
	}

	add_filter( 'wp_mail_content_type', 'set_content_type' );
	function set_content_type( $content_type ) {
		return 'text/html';
	}

	function anexaVideo(){
		global $post;
		$videosSamba = localizaVideosSamba();
		$videoKey = get_post_meta($post->ID, 'video-anexado', true);
		foreach($videosSamba as $indice=>$video){
			$videoSelected[$video[media_key]] = $indice;
			$arrayVideo[$indice][catID] = $video[categoriaID];
			$arrayVideo[$indice][title] = $video[postTitle];
			$arrayVideo[$indice][key] 	= $video[media_key];
		}
		$videosSambaArray = json_encode($arrayVideo);
		$texto .= "	<script>var videosSamba = JSON.stringify($videosSambaArray);</script>";

		$selecionadoCat[$arrayVideo[$videoSelected[$videoKey]][catID]] = "selected";
		$selecionadoVideo[$arrayVideo[$videoSelected[$videoKey]][key]] = "selected";
		foreach($videosSamba as $videos){
			$dadosVideo .=  "<option value='$videos[media_key]' ".$selecionadoVideo[$videos[media_key]]." >$videos[postTitle]</option>";
			$categorias[$videos[categoriaID]] = "<option value='$videos[categoriaID]' ".$selecionadoCat[$videos[categoriaID]].">$videos[categoria]</option>";
		}

		$texto .= "	<table width='100%' class='customize-pane-parent'>
						<tr>
							<td>
								<select name='select-categoria' id='select-categoria' Style='width:100%;'>
									<option value=''>Selecione a Categoria</option>";
		foreach($categorias as $categoria){
			$texto .= $categoria;
		}
		$texto .= "				</select>
							</td>
						</tr>
						<tr>
							<td>
								<select name='select-video' id='select-video' Style='width:100%;' class='chosen-select'>
									<option value=''>Selecione o Video</option>
									$dadosVideo
								</select>
							</td>
						</tr>
					</table>
					<table width='100%' id='player-video-samba'>
						<tr>
							<td colspan='2'>
								<iframe allowfullscreen webkitallowfullscreen mozallowfullscreen
										width='100%'
										height='150'
										src='//fast.player.liquidplatform.com/pApiv2/embed/e416100f0c85f292fbc2ad198c88ae11/$videoKey'
										scrolling='no'
										frameborder='0'>
								</iframe>
							</td>
						</tr>
					</table>
		";
		echo $texto;
	}

	function localizaVideosSamba(){
		$apiKey = "263ce2e49f293385e58604b3875d070f";
		$categorias = retornaCurlNovo("channels?key=$apiKey&filter=id;name;");
		foreach($categorias as $categoria){
			$categoriaID = $categoria->id;
			for($i=1;$i<=2;$i++){
				$paginador = ($i*50)-50;
				$artigos = retornaCurlNovo("medias?key=$apiKey&search=channelId:$categoriaID&sort=asc&first=$paginador");
				foreach($artigos as $artigo){
					$categoria  = json_decode(json_encode($categoria),TRUE);
					$artigo 	= json_decode(json_encode($artigo),TRUE);
					$dados['categoriaID']	= $categoria['id'];
					$dados['categoria']		= $categoria['name'];
					$dados['media_key']		= $artigo['id'];
					$dados['postTitle'] 	= $artigo['title'];

					$videoSamba[] = $dados;
				}
			}
		}
		return $videoSamba;
	}

	function retornaCurlNovo($urlComplemento){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://fast.api.liquidplatform.com/2.0/".$urlComplemento);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        return simplexml_load_string($output);
	}

	function videosTemporada(){
		global $wpdb, $post, $videos, $temporadas,$episodios;
		$videos = $wpdb->get_results("select ID, Post_Title from wp_posts where post_type = 'post' and post_status = 'publish' order by post_title");
		foreach($videos as $video)
			$optionVideo .= "<option value='$video->ID'>$video->Post_Title</option>";

		$temporadas = $wpdb->get_results("select t.term_id, t.name from wp_terms t inner join wp_term_taxonomy tx on tx.term_id = t.term_id where taxonomy = 'Temporada_video' and parent = 65 order by name");
		foreach($temporadas as $temporada)
			$optionTemporada .= "<option value='$temporada->term_id'>$temporada->name</option>";

		$episodios = $wpdb->get_results("select t.term_id, t.name from wp_terms t inner join wp_term_taxonomy tx on tx.term_id = t.term_id where taxonomy = 'Temporada_video' order by name");
		$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
		$texto = "	<table width='100%' class='customize-pane-parent'>
						<tr>
							<td width='120'>T&iacute;tulo dos Epis&oacute;dios</td>
							<td><input type='text' name='titulo-episodios' id='titulo-episodios' value='$tituloEpisodios' Style='width:100%'></td>
						</tr>
					</table>

					<table width='100%' class='customize-pane-parent'>
						<tr>
							<td>Video</td>
							<td width='150'>Temporada</td>
							<td width='150'>Episodio</td>
							<td width='90'>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<select Style='width:100%;padding:10px;' name='serie[0][video]' id='videos-serie' class='required'>
									<option value=''>selecione</option>
									$optionVideo
								</select>
							</td>
							<td>
								<select Style='width:100%;' name='serie[0][temporada]' id='temporada-serie' class='required'>
									<option value=''>selecione</option>
									$optionTemporada
								</select>
							</td>
							<td>
								<select Style='width:100%;' name='serie[0][episodio]' id='episodio-serie' class='required'>
									<option value=''>selecione</option>
								</select>
							</td>
							<td>
								<input type='button' class='button button-primary button-large' value='Adicionar' id='adiciona-video-serie' Style='width:89px;'>
							</td>
						</tr>
					</table>
					<table width='100%' Style='margin-top:0px;'>";
		$videosCadastrados = get_post_meta($post->ID,'video-serie',true);
		foreach($videosCadastrados as $videoCadastrado){
			if($videoCadastrado[video] != ""){
				$indice++;
				$detalhesVideo = localizaDetalhesVideo($videoCadastrado);
				$texto .= "	<tr>
								<td>&nbsp;".$detalhesVideo[titulo][video]."</td>
								<td width='150'>&nbsp;".$detalhesVideo[titulo][temporada]."</td>
								<td width='150'>&nbsp;".$detalhesVideo[titulo][episodio]."</td>
								<td width='90' align='center' class='submitbox'>
									<a class='submitdelete remove-video-serie' attr='$indice' Style='cursor:pointer;'>remover video</a>
									<input type='hidden' class='detalhes-video-$indice' name='serie[$indice][video]' 		value='".$detalhesVideo[video]."'>
									<input type='hidden' class='detalhes-video-$indice' name='serie[$indice][temporada]' 	value='".$detalhesVideo[temporada]."'>
									<input type='hidden' class='detalhes-video-$indice' name='serie[$indice][episodio]' 	value='".$detalhesVideo[episodio]."'>
								</td>
							</tr>";
			}
		}
		$texto .= "	</table>";
		echo $texto;
	}


	function videosAutor(){
		global $wpdb, $post, $videos;
		$videos = $wpdb->get_results("select ID, Post_Title from wp_posts where post_type = 'post' and post_status = 'publish' order by post_title");
		foreach($videos as $video)
			$optionVideo .= "<option value='$video->ID'>$video->Post_Title</option>";

		$texto = "	<table width='100%' class='customize-pane-parent'>
						<tr>
							<td>
								<select Style='width:100%;padding:10px;' name='palestra[0][video]' id='videos-palestra' class='required'>
									<option value=''>Selecione</option>
									$optionVideo
								</select>
							</td>
							<td width='100'>
								<input type='button' class='button button-primary button-large' value='Adicionar' id='adiciona-video-palestra' Style='width:89px;'>
							</td>
						</tr>
					</table>
					<table width='100%' Style='margin-top:0px;'>";
		$videosCadastrados = get_post_meta($post->ID,'video-palestra',true);
		foreach($videosCadastrados as $videoCadastrado){
			if($videoCadastrado[video] != ""){
				$indice++;
				$detalhesVideo = localizaDetalhesVideo($videoCadastrado);
				$texto .= "	<tr>
								<td>&nbsp;".$detalhesVideo[titulo][video]."</td>
								<td width='97' align='left' class='submitbox'>
									<a class='submitdelete remove-video-palestra' attr='$indice' Style='cursor:pointer;'>remover video</a>
									<input type='hidden' class='detalhes-video-$indice' name='palestra[$indice][video]' value='".$detalhesVideo[video]."'>
								</td>
							</tr>";
			}
		}
		$texto .= "	</table>";
		echo $texto;
	}



	function localizaDetalhesVideo($dados){
		global $videos, $temporadas,$episodios;

		foreach($videos as $video)
			if($video->ID==$dados[video])
				$dados[titulo][video] = $video->Post_Title;
		foreach($temporadas as $temporada)
			if($temporada->term_id==$dados[temporada])
				$dados[titulo][temporada] = $temporada->name;
		foreach($episodios as $episodio)
			if($episodio->term_id==$dados[episodio])
				$dados[titulo][episodio] = $episodio->name;

		return $dados;
	}


	function validaLogado(){

	}
















	add_shortcode('video-slider', 'videoSlider');
	function videoSlider($atributos){
		global $wpdb;
		$texto .= "<script src='/wp-content/plugins/integracao-samba-videos/javascript/jquery.bxslider.min.js'></script>
				<link rel='stylesheet' href='/wp-content/plugins/integracao-samba-videos/css/jquery.bxslider.min.css' type='text/css' media='all'>
				<script>
					jQuery('.video-list-new').live('mouseover', function(){
						jQuery('.video-thumbnail-capa').hide();
						jQuery(this).children().show();
					});
					jQuery('.video-list-new').live('mouseleave', function(){
						jQuery('.video-thumbnail-capa').hide();
						jQuery('.box-video-description').remove();
					});
					jQuery('.video-list-new').live('click', function(){
						jQuery('.video-list-close').click();
						jQuery('html, body').animate({scrollTop: $($(this).attr('attrtop')).offset().top-40}, 600);
						jQuery($(this).attr('attrid')).html('');
						jQuery($(this).attr('attrid')).load('/wp-content/plugins/integracao-samba-videos/functions/video-description.php?id='+jQuery(this).attr('attr'))
						jQuery('.setinha-carrossel-'+jQuery(this).attr('attr')).show().css('z-index', 9999);
						jQuery($(this).attr('attrid')).slideUp(200).delay(800).fadeIn();
					})
					jQuery('.video-list-close').live('click', function(){
						jQuery('#video_principal').remove();
						jQuery('.description-carrossel').hide();
					})

				</script>";

			$videos = $wpdb->get_results("select p.ID,p.Post_Title, p.Post_Content, p.Post_Excerpt, p.Post_name, pm4.meta_value Description,
										  month(post_date) Mes, year(post_date) Ano
										  from wp_posts p
										  inner join wp_postmeta pm3 on pm3.post_id = p.ID and pm3.meta_key = 'video_destaque'
										  inner join wp_postmeta pm4 on pm4.post_id = p.ID and pm4.meta_key = 'video-detalhes'
										  where post_type = 'video' and post_status = 'publish' $strConsulta $ordem");
			foreach($videos as $video){
				if($atributos[type]=='H'){
					$thumb_id = get_post_meta($video->ID, 'video_thumb-landscape_thumbnail_id', true );
					$thumb = wp_get_attachment_image_src($thumb_id,'imagem-horizontal');
				}
				if($atributos[type]=='V'){
					$thumb_id = get_post_meta($video->ID, 'video_thumb-portrait_thumbnail_id', true );
					$thumb = wp_get_attachment_image_src($thumb_id,'imagem-vertical');
				}


				$imagesCarrousel .= " 	<li class='video-list-new' attr='$video->ID' Style='cursor:pointer' link='$video->Post_name' attrid='.".$argumentos['widget_id']."-desc' attrtop='.".$argumentos['widget_id']."'>
											<div Style='position:fixed;display:none;' class='video-thumbnail-capa'>
												<img src='/wp-content/uploads/img-hover-produto.png'>
											</div>
											<img src='$thumb[0]' class='video-thumbnail' alt='$video->Post_Title'/>
										</li>";
				$sinopse   	= get_post_meta($video->ID,'sinopse-video',true);
				$ficha	 	= get_post_meta($video->ID,'ficha-video',true);
				$amostra	= str_replace('.mp4','-amostra_x264.mp4',$upload_dir['url']."/amostra/".get_post_meta($video->ID,'video_principal',true));
			}
			$texto .= "<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('.bxslider').bxSlider({
								minSlides: 3,
								maxSlides: 12,
								slideWidth: ".$atributos[width].",
								slideMargin: 5,
								pager:false
							});
						});
					</script>";
			$texto .= "<p class='tituloLogadaCinza'>$atributos[title]</p>";
			$texto .= "<div class='videos-destaques'>
						<ul class='bxslider' Style='float:left;'>
							$imagesCarrousel
							$imagesCarrousel
							$imagesCarrousel
						</ul>
					</div>
					<div Style='float:left;width:100%;position:relative;margin-bottom:10px;display:none;text-align:center;' id='faixa-carrossel' class='".$argumentos['widget_id']."-desc description-carrossel'></div>";
			return $texto;
		}

add_action('category_edit_form_fields','category_edit_form_fields');
add_action('category_add_form_fields','category_edit_form_fields');

function category_edit_form_fields ( $term ) {
	$posicao = get_term_meta( $term->term_id, 'posicao_home', true );
	$modulosel[get_term_meta( $term->term_id, 'posicao_home_modulo', true )] = "selected";
	$estiloHome[get_term_meta( $term->term_id, 'estilo_home', true )] = "selected";
	$posicaoMenu = get_term_meta( $term->term_id, 'modulo_menu', true );
	$menusel[get_term_meta( $term->term_id, 'modulo_menu', true )] = "selected";
	$titulosel[get_term_meta( $term->term_id, 'titulo_home', true )] = "selected";
?>
	<input name='modulo_home' type='hidden' value='1'>
    <tr class="form-field">
		<th valign="top" scope="row">
			<label for="catpic"><?php _e('Posi&ccedil;&atilde;o na Home', ''); ?></label>
		</th>
		<td>
			<input type="text" id="posicao_home" name="posicao_home" Style='width:100%;' value='<?php echo $posicao;?>'/>
		</td>
	</tr>
    <tr class="form-field">
		<th valign="top" scope="row">
			<label for="catpic"><?php _e('Mostrar T&iacute;tulo na Home', ''); ?></label>
		</th>
		<td>
			<select name='titulo_home'>
				<option value='0' <?php echo $titulosel[0];?><?php echo $titulosel[''];?> >Sim</option>
				<option value='1' <?php echo $titulosel[1];?> >N&atilde;o</option>
			</select>
		</td>
	</tr>

    <tr class="form-field">
		<th valign="top" scope="row">
			<label for="catpic"><?php _e('Estilo na Home', ''); ?></label>
		</th>
		<td>
			<select name='estilo_home'>
				<option value=''>selecione o estilo desejado</option>
				<option value='carrosel-vertical' <?php echo $estiloHome['carrosel-vertical'];?> >Carrosel Vertical</option>
				<option value='carrosel-horizontal' <?php echo $estiloHome['carrosel-horizontal'];?> >Carrosel Horizontal</option>
			</select>
		</td>
	</tr>
    <tr class="form-field">
		<th valign="top" scope="row">
			<label for="catpic"><?php _e('Posi&ccedil;&atilde;o no Menu', ''); ?></label>
		</th>
		<td>
			<select name='modulo_menu'>
				<option value=''>selecione a posi&ccedil;&atilde;o desejada</option>
				<option value='1' <?php echo $menusel[1];?> >Bloco 1</option>
				<option value='2' <?php echo $menusel[2];?> >Bloco 2</option>
				<option value='3' <?php echo $menusel[3];?> >Bloco 3</option>
			</select>
		</td>
	</tr>
    <tr class="form-field">
		<th valign="top" scope="row">
			<label for="catpic"><?php _e('Posi&ccedil;&atilde;o no Menu', ''); ?></label>
		</th>
		<td>
			<input type="text" id="posicao_menu" name="posicao_menu" Style='width:100%;' value='<?php echo $posicaoMenu;?>'/>
			<br><br>
		</td>
	</tr>
<?php
    }

	function category_save_posicao( $term_id ) {

		if ( isset( $_POST['posicao_home'] ) ) {
			 update_term_meta( $term_id, 'posicao_home', $_POST['posicao_home'] );
		}
		if ( isset( $_POST['modulo_home'] ) ) {
			 update_term_meta( $term_id, 'posicao_home_modulo', $_POST['modulo_home']);
		}
		if ( isset( $_POST['estilo_home'] ) ) {
			 update_term_meta( $term_id, 'estilo_home', $_POST['estilo_home']);
		}
		if ( isset( $_POST['modulo_menu'] ) ) {
			 update_term_meta( $term_id, 'modulo_menu', $_POST['modulo_menu']);
		}
		if ( isset( $_POST['posicao_menu'] ) ) {
			 update_term_meta( $term_id, 'posicao_menu', $_POST['posicao_menu']);
		}
		if ( isset( $_POST['titulo_home'] ) ) {
			 update_term_meta( $term_id, 'titulo_home', $_POST['titulo_home']);
		}
	}
	add_action( 'edited_category', 'category_save_posicao' );
	add_action( 'create_category', 'category_save_posicao' );



	function palestranteVideo(){
		global $wpdb, $post;
		$palestranteSel[get_post_meta($post->ID, 'video-palestrante', true)] = "selected";
		$palestrantes = $wpdb->get_results("select ID, post_title from wp_posts where post_type = 'especiais' order by post_title");
		foreach($palestrantes as $palestrante)
			$optionPalestrante .= "<option value='$palestrante->ID' ".$palestranteSel[$palestrante->ID].">$palestrante->post_title</option>";
		$texto = "	<table width='100%' class='customize-pane-parent'>
						<tr>
							<td>
								<select Style='width:100%;padding:10px;' name='video-palestrante' id='video-palestrante' class='required'>
									<option value=''>Selecione</option>
									$optionPalestrante
								</select>
							</td>
						</tr>
					</table>";
		echo $texto;
	}



	add_action('init', 'AvisosUsuarioRegister');
	function AvisosUsuarioRegister() {
		$labels = array(
			'name' => 'Avisos',
			'singular_name' => 'Avisos',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar novo',
			'edit_item' => 'Editar Avisos',
			'new_item' => 'Adicionar novo',
			'all_items' => 'Gerenciar Avisos',
			'view_item' => 'Visualizar Avisos',
			'search_items' => 'Localizar Avisos',
			'not_found' =>  'Nenhum Avisos Localizada',
			'not_found_in_trash' => 'Nenhum Aviso na Lixeira',
			'parent_item_colon' => '',
			'menu_name' => 'Avisos'
		);
		$args = array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'menu_position' 		=> '35',
			'hierarchical' 			=> true,
			'menu_icon'   			=> 'dashicons-format-aside',
			'supports' 				=> array('title','editor')
		  );
		register_post_type('avisos' , $args );
	}

	add_action( 'admin_init', 'metabox_aviso_detalhes' );
	function metabox_aviso_detalhes() {
		add_meta_box('aviso-detalhes-extras' , 'Detalhes Adicionais', 'detalhesExtrasAviso', 'avisos', 'side', 'core' );
	}
	function detalhesExtrasAviso(){
		echo "	<table>
					<tr>
						<td>
							<input type='checkbox' value='s' name='envia-aviso-app'>Enviar para App
						</td>
					</tr>
				</table>";
	}

	add_action('save_post','enviaMensagemAviso');
	function enviaMensagemAviso(){
		global $post;
		update_post_meta($post->ID,'envia-aviso-app', $_POST['envia-aviso-app']);

		if($post->post_type=='avisos'){
			$headings = array(
				"en" => $post->post_title
			);
			$content = array(
				"en" => strip_tags($post->post_content)
			);
			$fields = array(
				'app_id' => "046e85d7-8845-42a4-ba03-f867029fb288",
				'included_segments' => array('All'),
				'headings' => $headings,
				'contents' => $content
			);
			$fields = json_encode($fields);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic OGE1ODJmZTEtZDY0Zi00M2NlLWE1YzAtMzdhMjRkMzQzZjg4'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$response = curl_exec($ch);
			curl_close($ch);
		}

	}

	add_shortcode('login-clube-video','login_clube_video');
	function login_clube_video(){
		$args = array(
			'echo'           => false,
			'remember'       => true,
			'redirect'       => "/",
			'form_id'        => 'loginform',
			'id_username'    => 'user_login',
			'id_password'    => 'user_pass',
			'id_remember'    => 'rememberme',
			'id_submit'      => 'wp-submit',
			'label_username' => __( 'E-mail' ),
			'label_password' => __( 'Password' ),
			'label_remember' => __( 'Lembrar de mim neste computador.' ),
			'label_log_in'   => __( 'Entrar' ),
			'value_username' => '',
			'value_remember' => true
		);
		return wp_login_form( $args );
	}

	add_filter( 'authenticate', 'custom_authenticate_username_password', 30, 3);
	function custom_authenticate_username_password( $user, $username, $password ){
		if ( is_a($user, 'WP_User') ) { return $user; }
		if ( empty($username) || empty($password) ){
			$error = new WP_Error();
			$user  = new WP_Error('authentication_failed', __('<strong>ERROR</strong>: Invalid username or incorrect password.'));
			wp_redirect("/login/#errorlogin");
		}
	}

	add_shortcode('validate-user-login','validateUserLogin');
	function validateUserLogin(){
		if(!is_user_logged_in() ) {
			echo "<script>window.location='/login/'</script>";
			exit;
		}
	}
	add_shortcode('planos-disponiveis-padrao','planosDisponiveisPadrao');
	function planosDisponiveisPadrao($parametros){
		session_start();
		global $planos, $current_user;
		get_currentuserinfo();
		$_SESSION['user_cadasro'] = $current_user;
		$args = array(
			'posts_per_page'   => 5,
			'orderby'          => 'post_title',
			'order'            => 'ASC',
			'post_type'        => 'planos',
			'post_status'      => 'publish',
		);
		$planos = get_posts( $args );
		foreach($planos as $indice=>$plano){
			$selPlano[count($planos)-1] = "Selected";
			$selVal[count($planos)-1] = "Sel";
			$detalhesPlano .= "		<td>
										<div class='selBorderPlanos'>
											<div class='cellPlanos$selPlano[$indice]' attr='$plano->ID'>$plano->post_title</div>
										</div>
									</td>";
			$valorPlano = get_post_meta($plano->ID,'detalhes-plano',true);
			$valoresMensais .= "	<td>
										<div class=''>
											<div class='txtPlanos13$selVal[$indice] valorPlanos' attr='$plano->ID'>R$ $valorPlano[mensal]</div>
										</div>
									</td>";
		}

		$argsItens = array(
			'taxonomy'   => 'item_plano',
			'hide_empty' => false
		);

		$itensPlanos = get_terms($argsItens);
		foreach($itensPlanos as $itensPlano){
			global $planos, $wpdb;
			$itemPlano  .= "	<tr class='borderBPlanos'>
									<td class='txtPlanos13Esq'>$itensPlano->name</td>";
			foreach($planos as $indice=>$plano){
				$selPlano[$indice] = "Cinza";
				$selPlano[count($planos)-1] = "Laranja";
				$itemSelecionados = $wpdb->get_var("select object_id from wp_term_relationships where term_taxonomy_id = $itensPlano->term_id and object_id = $plano->ID");
				if($itemSelecionados == "")
					$itemPlano  .= "	<td class='iconPlanos$selPlano[$indice]R iconPlanos' attr='$plano->ID' attr-sel='R'></td>";
				else
					$itemPlano  .= "	<td class='iconPlanos$selPlano[$indice]C iconPlanos' attr='$plano->ID' attr-sel='C'></td>";
			}
			$itemPlano  .= "	</tr>";
		}
		if($parametros['ativo']=='nao')
			$dados = "	<div class='col-sm-12' style='background-color: #f3f3f3; height: 100%;'>
							<div class='col-sm-3'></div>
							<div class='col-sm-6 topContainerPassos'>
							<div>
							<table style='width: 100%;'>
								<tbody>
									<tr>
										<td class='separadorPlanos'></td>
										$detalhesPlano
									</tr>
									<tr class='borderBPlanos'>
										<td class='txtPlanos13Esq'>Valores mensais</td>
										$valoresMensais
									</tr>
									$itemPlano
								</tbody>
							</table>
						</div>
					</div>
					<div class='col-sm-3'></div>
				</div>";
		else
			$dados = "	<div class='col-sm-12' style='background-color: #f3f3f3; height: 100%;'>
							<div class='col-sm-3'></div>
							<div class='col-sm-6 topContainerPassos'>
								<p class='txtPlanosPassos'>PASSO 2</p>
								<p class='titlePassos'>Escolha o melhor plano para voc&ecirc;.</p>
							<div>
							<table style='width: 100%;'>
								<tbody>
									<tr>
										<td class='separadorPlanos'></td>
										$detalhesPlano
									</tr>
									<tr class='borderBPlanos'>
										<td class='txtPlanos13Esq'>Valores mensais</td>
										$valoresMensais
									</tr>
									$itemPlano
								</tbody>
							</table>
						</div>
						<div class='containerBotaoPassos topBtnPlanos'><a class='btn-Passos-planos' target='_top'>CONTINUAR</a></div>
					</div>
					<div class='col-sm-3'></div>
				</div>";

		return $dados;
	}

	add_shortcode('retorno-pagamento-pagseguro-cine-brasil','retornoPagamentoPagseguro');
	function retornoPagamentoPagseguro(){
		global $wpdb, $current_user, $post_ID;
		get_currentuserinfo();

		$userID = $current_user->data->ID;
		$codRetorno = $_GET['code'];

		if($codRetorno != ""){
			$retorno = "https://ws.pagseguro.uol.com.br/v2/pre-approvals/$codRetorno?email=operacao@cinebrasil.tv&token=A184D628FD8248B19E408D8F9122DB06";
			$ch = curl_init($retorno);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$data = utf8_decode(curl_exec($ch));
			curl_close($ch);
			$xml = new SimpleXmlElement($data, LIBXML_NOCDATA);

			$plano = get_post_meta(get_user_meta($userID,'plano-selecionado',true),'detalhes-plano',true);


			$dadosRetorno = json_decode( json_encode($xml) , 1);
			$dadosRetorno[valor] = $plano[mensal];
			update_user_meta($userID,'xml-pagamento',$data);
			update_user_meta($userID,'dados-plano-contatado',$dadosRetorno);
			update_user_meta($userID,'codido-retorno-pagamento',$codRetorno);
			update_user_meta($userID,'identificador-pagseguro',$dadosRetorno['tracker']);

			if($dadosRetorno['status']=='CANCELLED'){
				echo autorizacaoNegada($dadosRetorno);
			}else{
				echo pagamentoAprovado($dadosRetorno);
			}
		}
	}



	function autorizacaoNegada($dados){
		return "	<div class='col-sm-12'>
						<div class='col-sm-1'></div>
						<div class='col-sm-10 topContainerPassos'>
							<table width='100%' cellspacing='0' cellpadding='0'>
								<tbody>
									<tr>
										<td style='font-family: Arial, Helvetica, Tahoma, Verdana, sans-serif; font-weight: normal; font-size: 15px; color: #333333;' align='center' height='50'>
											<table width='80%' cellspacing='0' cellpadding='0'>
												<tbody>
													<tr>
														<td class='txtTransacao' align='center'>Seu cadastro foi efetuado com sucesso, mas informamos que seu cart&atilde;o n&atilde;o foi autorizado, solicitamos que tente novamente ou utilize outro cart&atilde;o.</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td align='center' height='201'>
											<table style='padding-top: 25px; padding-bottom: 25px;' width='80%' cellspacing='0' cellpadding='0' bgcolor='#f2f2f2'>
												<tbody>
													<tr>
														<td rowspan='7' width='30%'><img style='display: block; margin: 0 auto;' src='/wp-content/uploads/transacao-nao-autorizada.png' /></td>
														<td height='30'></td>
														<td align='left'></td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' width='25%' height='30'>Status da Transa&ccedil;&atilde;o:</td>
														<td class='txtTransacaoColuna2' align='left' width='45%'>5 - Autoriza&ccedil;&atilde;o negada</td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' height='30'>Forma de pagamento:</td>
														<td class='txtTransacaoColuna3' align='left'>Cart&atilde;o de Cr&eacute;dito</td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' height='30'>N&uacute;mero do pedido:</td>
														<td class='txtTransacaoColuna3' align='left'>$dados[tracker]</td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' height='30'>TID da Transa&ccedil;&atilde;o:</td>
														<td class='txtTransacaoColuna3' align='left'>$dados[code]</td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' height='30'>Valor total do pedido:</td>
														<td class='txtTransacaoColuna3' align='left'>R$ $dados[valor]</td>
													</tr>
												</tbody>
											</table>
											<p>&nbsp;</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>";
	}

	function pagamentoAprovado($dados){
		return "	<div class='col-sm-12'>
						<div class='col-sm-1'></div>
						<div class='col-sm-10 topContainerPassos'>
							<table width='100%' cellspacing='0' cellpadding='0'>
								<tbody>
									<tr>
										<td align='center' height='46'>
											<table width='80%' cellspacing='0' cellpadding='0'>
												<tbody>
													<tr>
														<td class='txtBemvindo'>Bem-vindo(a)</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td align='center'>
											<table style='padding-top: 25px; padding-bottom: 25px;' width='80%' cellspacing='0' cellpadding='0' bgcolor='#f2f2f2'>
												<tbody>
													<tr>
														<td rowspan='7' width='30%'><img style='display: block; margin: 0 auto;' src='/wp-content/uploads/transacao-autorizacao.png' /></td>
														<td height='30'></td>
														<td align='left'></td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' width='25%' height='30'>Status da Transa&ccedil;&atilde;o:</td>
														<td class='txtTransacaoColuna2' align='left' width='45%'>5 - Autoriza&ccedil;&atilde;o aprovada</td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' height='30'>Forma de pagamento:</td>
														<td class='txtTransacaoColuna3' align='left'>Cart&atilde;o de Cr&eacute;dito</td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' height='30'>N&uacute;mero do pedido:</td>
														<td class='txtTransacaoColuna3' align='left'>$dados[tracker]</td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' height='30'>TID da Transa&ccedil;&atilde;o:</td>
														<td class='txtTransacaoColuna3' align='left'>$dados[code]</td>
													</tr>
													<tr>
														<td class='txtTransacaoColuna' height='30'>Valor total do pedido:</td>
														<td class='txtTransacaoColuna3' align='left'>R$ $dados[valor]</td>
													</tr>
													<tr>
														<td height='30'></td>
														<td align='left'></td>
													</tr>
													<tr>
														<td colspan='3'><br><br>
															<a href='/'>
																<div class='btn-notificacao-orange' Style='width:340px;'>Comece a assistir</div>
															</a>
														</td>
													</tr>
												</tbody>

											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class='col-sm-1'> </div>
					</div>";
	}


?>
