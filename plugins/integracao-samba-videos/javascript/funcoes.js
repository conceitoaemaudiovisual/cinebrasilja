jQuery(document).ready(function($){
/*
	jQuery('#wp-submit').live('click', function(e){
		e.preventDefault();
		jQuery.ajax({type: 'POST',url: 'https://www.cinebrasilplay.com.br/api/core/validaLogin/?acao=1', data: jQuery('#loginform #user_login').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
			success: function(retorno){
				if(retorno > 30){
					$('.txt-dados-invalidos').html('Maximo de logins atigidos!').show();
					jQuery.ajax({type: 'POST',url: 'https://www.cinebrasilplay.com.br/api/core/validaLogin/?acao=-1', data: jQuery('#loginform #user_login').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
						success: function(retorno){
							console.log('login fail:'+retorno);
						}
					})
				}else{
					$('#loginform').submit();
				}
			}
		});
	});

	jQuery('#logOut').live('click', function(e){
		jQuery.ajax({type: 'POST',url: 'https://www.cinebrasilplay.com.br/api/core/validaLogin/?acao=-1', data: jQuery('.logoutid').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
			success: function(retorno){
				console.log('logOut:'+retorno);
			}
		});
	});
*/
	jQuery('#select-categoria').live('change', function(){
		var arrayVid = JSON.parse(videosSamba);
		jQuery('#select-video').empty().trigger('chosen:updated');
	    jQuery('#select-video').append("<option value=''>Selecione o Video</option>");
		$.each(arrayVid,function(index, value){
			if(jQuery('#select-categoria').val()==value.catID){
			    jQuery('#select-video').append("<option value='"+value.key+"'>"+value.title+"</option>");
			}
		})
		jQuery('#select-video').trigger('chosen:updated');
	});
	jQuery('#select-categoria-2').live('change', function(){
		var arrayVid = JSON.parse(videosSamba1);
		jQuery('#select-video-2').empty().trigger('chosen:updated');
	    jQuery('#select-video-2').append("<option value=''>Selecione o Video</option>");
		$.each(arrayVid,function(index, value){
			if(jQuery('#select-categoria-2').val()==value.catID){
			    jQuery('#select-video-2').append("<option value='"+value.key+"'>"+value.title+"</option>");
			}
		})
		jQuery('#select-video-2').trigger('chosen:updated');
	});


	jQuery('#adiciona-video-serie').live('click', function(){
		var erro = 0;
		jQuery('.required').each(function(){
			if($(this).val()==''){
				$('#'+$(this).attr('id').replace('-','_')+'_chosen a').css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD');
				erro = 1;
			}
		});
		if(erro==0)
			$('#post').submit();
	});

	jQuery('.required').live('change', function(){
		$('#'+$(this).attr('id').replace('-','_')+'_chosen a').css('background-color', '').css('border', '');
	});
	jQuery('#temporada-serie').live('change', function(){
       	var xmlhttp = new XMLHttpRequest();
       	xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
				jQuery('#episodio-serie').empty();
				jQuery('#episodio-serie').append(this.responseText);
				$('#episodio-serie').trigger( 'chosen:updated' );
				jQuery('.s3bubble-details-full-overlay').addClass('s3bubble-details-full-overlay-select');
            }
        };
        xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/localiza-episodios.php?temporada="+jQuery(this).val(), true);
        xmlhttp.send();

	});
	jQuery('.remove-video-serie').live('click', function(){
		jQuery('.detalhes-video-'+jQuery(this).attr('attr')).remove();
		jQuery('#post').submit();
	});


	jQuery('#adiciona-video-palestra').live('click', function(){
		var erro = 0;
		jQuery('.required').each(function(){
			if($(this).val()==''){
				$('#'+$(this).attr('id').replace('-','_')+'_chosen a').css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD');
				erro = 1;
			}
		});
		if(erro==0)
			$('#post').submit();
	});
	jQuery('.required').live('change', function(){
		$('#'+$(this).attr('id').replace('-','_')+'_chosen a').css('background-color', '').css('border', '');
	});
	jQuery('.remove-video-palestra').live('click', function(){
		jQuery('.detalhes-video-'+jQuery(this).attr('attr')).remove();
		jQuery('#post').submit();
	});









	jQuery('.input-login').live('keypress', function(){
		mascaraCampo($(this));
	});
	jQuery('.botao-login-normal').live('click',function(e){
		var erro = 0;
		jQuery('.input-login').each(function(){
			if(jQuery(this).val()==''){
				erro = erro + 1;
				jQuery(this).css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD');
				if(erro==1)
					jQuery(this).focus();

			}
		});
		if(erro==0){
			if(!validaEmailCadastro(document.getElementById('text-login'))){
				jQuery('#text-login').css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD').focus();
				jQuery('#erro-login').html('Endere\u00e7o de e-mail inv\u00e1lido');
				erro = erro + 1;
			}
		}
		if(erro==0){
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/valida-login.php', data: jQuery('.input-login').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					if(retorno.trim()==0){
						jQuery('#erro-login').html('Login ou Senha inv\u00e1lida.');
					}else{
						window.location.href = '/';
					}
				}
			});
		}
	});

	jQuery('#menu-topo-navegar').live('hover',function(e){
		jQuery('#menu-topo-navegar-interno').show();
	});
	jQuery('#menu-topo-navegar').live('mouseleave',function(e){
		jQuery('#menu-topo-navegar-interno').hide();
	});

	jQuery('#menu-topo-buscar').live('hover',function(e){
		jQuery('#menu-topo-buscar-interno').show();
		jQuery('#txt-menu-buscar').val('').focus();
	});
	jQuery('#menu-topo-buscar').live('mouseleave',function(e){
		jQuery('#menu-topo-buscar-interno').hide();
	});

	jQuery('#menu-topo-usuario').live('hover',function(e){
		jQuery('#menu-topo-usuario-interno').show();
	});
	jQuery('#menu-topo-usuario').live('mouseleave',function(e){
		jQuery('#menu-topo-usuario-interno').hide();
	});
	jQuery('#menu-usuario-navegar-interno-sair').live('click',function(e){
		jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/logout.php', data: jQuery('form').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
			success: function(retorno){
				window.location.href = '/';
			}
		});
	});
	jQuery('#menu-topo-navegar-interno-inicio').live('click',function(e){
		window.location.href = '/';
	});
	jQuery('#menu-topo-navegar-interno-lista').live('click',function(e){
		window.location.href = '/lista/';
	});
	jQuery('#menu-topo-navegar-interno-contato').live('click',function(e){
		window.location.href = '/contato/';
	});
	jQuery('#menu-buscar-botao').live('click',function(e){
		if(jQuery('#txt-menu-buscar').val()==''){
			jQuery('#txt-menu-buscar').css('background-color', '#feefef').focus();
			return false;
		}
		jQuery('#txt-menu-buscar').css('background-color', '');
		jQuery('#busca-geral').attr('action','/busca/').attr('method','post').submit();
	});
	jQuery('#menu-usuario-navegar-interno-cadastro').live('click',function(e){
		window.location.href = '/homolog/inscricao/';
	});

	jQuery('.botao-atualiza-cadastro').live('click',function(e){
		var erro = 0;
		jQuery('.required').each(function(){
			if(jQuery(this).val()==''){
				erro = erro + 1;
				jQuery(this).css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
				if(erro==1)
					jQuery(this).focus();
			}
		});
		if(erro==0){
			if(!validaEmailCadastro(document.getElementById('email-cadastro'))){
				jQuery('#email-cadastro').css('background-color', '#feefef').css('border', '1px solid #FFCDCD').focus();
				jQuery('#erro-cadastro').html('Endere\u00e7o de e-mail inv\u00e1lido');
				erro = erro + 1;
			}
		}
		if(erro==0){
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/atualiza-cadastro.php', data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					jQuery('#erro-cadastro').html('Cadastro Atualizado com sucesso.');
				}
			});
		}
	});

	jQuery('.required, .li-tipo-cartao').live('click change keypress keyup',function(e){
		jQuery('.required, .input-cadastro').css('background-color', '').css('border', '');
		jQuery('.li-tipo-cartao').css('background-color', '').css('border', '1px solid #ffffff');
	});

	jQuery('.botao-finaliza-cadastro').live('click',function(e){
		var erro = 0;
		jQuery('.required').each(function(){
			if(jQuery(this).val()==''){
				erro = eval(erro) + 1;
				jQuery(this).css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
			}
		});
		if(jQuery('#pais').val()=='Brasil'){
			if(jQuery('#cpf').val()==''){
				jQuery('#cpf').css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
				erro = eval(erro) + 1;
				if(erro==1)
					jQuery('#cpf').focus();
			}
		}
		jQuery('.tipo-cartao').each(function(){
			if($( "input:checked" ).length==0){
				jQuery('.li-tipo-cartao').css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
				erro = eval(erro) + 1;
			}
		});
		if(erro==0){
			if(!validaEmailCadastro(document.getElementById('email-cadastro'))){
				jQuery('#email-cadastro').css('background-color', '#feefef').css('border', '1px solid #FFCDCD').focus();
				jQuery('#erro-cadastro').html('Endere\u00e7o de e-mail inv\u00e1lido');
				erro = eval(erro) + 1;
			}
		}
		if(erro==0){
			jQuery(this).removeClass('botao-finaliza-cadastro').addClass('botao-atualiza-cadastro-aguarde');
			jQuery('.required').css('background-color', '').css('border', '');
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/gateway-cielo/novo-cliente.php', data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					jQuery('#form-pgto').attr('target','_self').submit();
				}
			});
		}
	});
});






























function validaEmailCadastro(email){
	valor = email.value
	Arroba= valor.indexOf("@");
	Ponto= valor.lastIndexOf(".");
	espaco= valor.indexOf(" ");
	if (valor == '')
		return false;
	if  ((Arroba != -1) && (Ponto > Arroba +1) && (espaco==-1)){
		return true;
	}else{
		return false;
	}
}
function mascaraVal(formato, keypress, objeto){
	campo = eval (objeto);
	if((event.keyCode >= 48)&&(event.keyCode <= 57)){
		event.returnValue = true;
	}else{
	  event.returnValue = false;
	}
}

function mascaraCampo(formato, keypress, objeto){
    campo = eval (objeto);
	if(((event.keyCode >= 65)&&(event.keyCode <= 90))
	 ||((event.keyCode >= 97)&&(event.keyCode <= 122))
	 ||((event.keyCode >= 192)&&(event.keyCode <= 255))
	 ||((event.keyCode >= 32)&&(event.keyCode <= 38))
	 ||((event.keyCode >= 40)&&(event.keyCode <= 47))
	 ||((event.keyCode >= 58)&&(event.keyCode <= 64))
	 ||((event.keyCode >= 91)&&(event.keyCode <= 95))
	 ||((event.keyCode >= 123)&&(event.keyCode <= 255))
	 ||((event.keyCode <= 57)&&(event.keyCode >= 48))
	 ||((event.keyCode >= 44)&&(event.keyCode <= 47))
	 ||((event.keyCode == 38))||((event.keyCode == 95))){
	 		event.returnValue = true;
	 	}else{
	 		event.returnValue = false;}
}
function formataValor(campo){
  valor = campo.value;
  tamanho =valor.length;
  decimal = tamanho - 2;

  antes= valor.indexOf(",");
  depois = antes+1

  inteiro = valor.substring(0,antes)+valor.substring(tamanho,depois);
  tamanho = inteiro.length;

  if(tamanho>=3)
  	campo.value = inteiro.substring(0,tamanho-2)+','+inteiro.substring(tamanho-2,tamanho);

  if(tamanho<3)
  	campo.value = inteiro.substring(0,tamanho-2)+inteiro.substring(tamanho-2,tamanho);

}


jQuery(document).ready(function(){
	jQuery('#loop-interna').css('padding','');
	jQuery('.input-login').live('click keypress',function(e){
		jQuery('.input-login').css('background-color', '').css('border', '');
		jQuery('#erro-login').html('');
	});
	jQuery('.esqueceu-senha').live('click',function(e){
		jQuery('.texto-superior-entrar').html('Recuperar Senha').css('width','65%');
		jQuery('#erro-login').css('width','35%');
		jQuery('.campo-login-inicio').hide();
		jQuery('.campo-login-recupera').show();
		jQuery('#erro-login').html('');
	});
	jQuery('.cancelar-senha').live('click',function(e){
		jQuery('.texto-superior-entrar').html('Entrar').css('width','40%');
		jQuery('#erro-login').css('width','60%');
		jQuery('.campo-login-inicio').show();
		jQuery('.campo-login-recupera').hide();
	});

	jQuery('.localizar-senha').live('click',function(e){
		if($('#text-login').val()==''){
			jQuery('#text-login').css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD');
			return false;
		}
		jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/localiza-senha.php', data: $('#text-login').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
			success: function(retorno){
				console.log(retorno)
				if(retorno=='')
					jQuery('#erro-login').html('E-mail inv\u00e1lido');
				else{
					$('.cancelar-senha').click();
					jQuery('#erro-login').html('Senha enviada com sucesso!');
				}
			}
		});
	});
	jQuery('.tipo-cartao').live('click', function(){
		if(jQuery(this).val()=='aura'){
			jQuery('#numero_cartao').attr('maxlength','19');
		}else
			jQuery('#numero_cartao').attr('maxlength','16');
	});
});




	jQuery('.botao-finaliza-cadastro-antigo').live('click',function(e){
		var erro = 0;
		jQuery('.required').each(function(){
			if(jQuery(this).val()==''){
				erro = erro + 1;
				jQuery(this).css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
				if(erro==1)
					jQuery(this).focus();
			}
		});
		if(erro==0){
			if(!validaEmailCadastro(document.getElementById('email-cadastro'))){
				jQuery('#email-cadastro').css('background-color', '#feefef').css('border', '1px solid #FFCDCD').focus();
				jQuery('#erro-cadastro').html('Endere\u00e7o de e-mail inv\u00e1lido');
				erro = erro + 1;
			}
		}
		if(erro==0){
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/valida-cadastro.php', data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					console.log('envia pgto');
					jQuery('.required').css('background-color', '').css('border', '');
					jQuery.ajax({type: 'POST',url: '/wp-content/plugins/gateway/pagseguro/', data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
						success: function(retorno){
							jQuery('#finaliza-pagamento').attr('href','/wp-content/plugins/gateway/finaliza-pagamento.php?cod='+retorno).trigger('click');
						}
					});
				}
			});
		}
	});


jQuery(document).ready(function($){
	jQuery('.botao-minha-lista-home').live('click', function(){
		pID = jQuery(this).attr('attr');
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				$('.botao-minha-lista-home').removeClass('botao-minha-lista-home').addClass('botao-minha-lista-del');
				location.reload();
			}
		};
		xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/atualiza-minha-lista.php?userID="+jQuery('#list-perfil-id').attr('userid')+"&type=1&pID="+jQuery(this).attr('attr'), true);
		xmlhttp.send();
	})
	jQuery('.botao-minha-lista-del').live('click', function(){
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				$('.botao-minha-lista-del').removeClass('botao-minha-lista-del').addClass('botao-minha-lista-home');
				location.reload();
			}
		};
		xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/atualiza-minha-lista.php?userID="+jQuery('#list-perfil-id').attr('userid')+"&type=0&pID="+jQuery('.slide-banner').attr('attr'), true);
		xmlhttp.send();
	})

	jQuery('.botao-minha-lista-home-interna').live('click', function(){
		idVideo = $(this).attr('attr');
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				$('.botao-minha-lista-home-interna').removeClass('botao-minha-lista-home-interna').addClass('botao-minha-lista-del-interna');
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						location.reload();
					}
				}
				xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/atualiza-minha-lista-home.php?userID="+jQuery('#list-perfil-id').attr('userid')+"&pID="+idVideo, true);
				xmlhttp.send();
			};
		}
		xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/atualiza-minha-lista.php?userID="+jQuery('#list-perfil-id').attr('userid')+"&type=1&pID="+jQuery(this).attr('attr'), true);
		xmlhttp.send();
	})

	jQuery('.botao-minha-lista-del-interna').live('click', function(){
		idVideo = $(this).attr('attr');
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				$('.botao-minha-lista-del-interna').removeClass('botao-minha-lista-del-interna').addClass('botao-minha-lista-home-interna');
				$('.my-list-carousels-container .tile').each(function(){
					if($(this).attr('data-id')==idVideo){
						$('.my-list-carousels-container').slick('slickRemove',$(this).attr('data-slick-index'));
						$('.s3bubble-details-inner-close').click();
					}
				});
			}
		};
		xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/atualiza-minha-lista.php?userID="+jQuery('#list-perfil-id').attr('userid')+"&type=0&pID="+jQuery(this).attr('attr'), true);
		xmlhttp.send();
	})

	jQuery('.tablist-item-episodio').live('click', function(){
		$(this).addClass('tablist-item-episodio-selecionado');
		$('.tablist-item-semelhante').removeClass('tablist-item-semelhante-selecionado');
		$('.tablist-item-detalhes').removeClass('tablist-item-detalhes-selecionado');
		$('.tablist-ficha-tecnica').removeClass('tablist-ficha-tecnica-selecionado');
		jQuery('.synopis.content .col-sm-5.col-xs-5.rel, .synopis-outer.synopis-outer-ajax h2.synopis.hidden-xs').hide();
		jQuery('.detalhes-video-view, .banner-ficha-tecnica, .container-banner-botoes-ajax, .banner-ficha-tecnica, .container-banner-botoes-ajax').hide();
		jQuery('.s3bubble-details-full-overlay').addClass('s3bubble-details-full-overlay-select');
		jQuery('.play-icon-wrap-rel').hide();

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				jQuery('.episodios-serie-view').html(this.responseText).fadeIn();
				jQuery('.bxslider-series').bxSlider({
					minSlides: 2,
					maxSlides: 12,
					slideWidth: 270	,
					slideMargin: 10,
					pager:false
				});
		}
		};
		xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/mostra-episodios-serie.php?pID="+jQuery(this).attr('attr'), true);
		xmlhttp.send();
	})
	jQuery('.tablist-item-semelhante').live('click', function(){
		$(this).addClass('tablist-item-semelhante-selecionado');
		$('.tablist-item-episodio').removeClass('tablist-item-episodio-selecionado');
		$('.tablist-item-detalhes').removeClass('tablist-item-detalhes-selecionado');
		$('.tablist-ficha-tecnica').removeClass('tablist-ficha-tecnica-selecionado');
		jQuery('.detalhes-video-view, .episodios-serie-view, .banner-ficha-tecnica, .container-banner-botoes-ajax').hide();
		jQuery('.synopis.content .col-sm-5.col-xs-5.rel, .synopis-outer.synopis-outer-ajax h2.synopis.hidden-xs').hide();
		jQuery('.s3bubble-details-full-overlay').addClass('s3bubble-details-full-overlay-select');
		jQuery('.play-icon-wrap-rel').hide();
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				jQuery('.episodios-serie-view').html(this.responseText).fadeIn();
				jQuery('.bxslider-series').bxSlider({
					minSlides: 2,
					maxSlides: 12,
					slideWidth: 270	,
					slideMargin: 10,
					pager:false
				});
			}
		};
		xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/mostra-titulos-semelhantes.php?pID="+jQuery(this).attr('attr'), true);
		xmlhttp.send();
	})
	jQuery('.tile_meta_more_info').live('click', function(){
		$('.tablist-ficha-tecnica').addClass('tablist-ficha-tecnica-selecionado');
		$('.tablist-item-semelhante').removeClass('tablist-item-semelhante-selecionado');
		$('.tablist-item-episodio').removeClass('tablist-item-detalhes-selecionado');
		$('.tablist-item-detalhes').removeClass('tablist-item-detalhes-selecionado');
		$('.tablist-item-episodio').removeClass('tablist-item-episodio-selecionado');
		jQuery('.play-icon-wrap-rel').show();
		jQuery('.episodios-serie-view, .banner-ficha-tecnica, .container-banner-botoes-ajax').hide();
		jQuery('.synopis.content .col-sm-5.col-xs-5.rel, .synopis-outer.synopis-outer-ajax h2.synopis.hidden-xs').show();
		jQuery('.detalhes-video-view').hide();
		jQuery('.s3bubble-details-full-overlay').addClass('s3bubble-details-full-overlay-select');
	})
	jQuery('.tablist-item-detalhes').live('click', function(){
		$(this).addClass('tablist-item-detalhes-selecionado');
		$('.tablist-item-semelhante').removeClass('tablist-item-semelhante-selecionado');
		$('.tablist-item-episodio').removeClass('tablist-item-detalhes-selecionado');
		$('.tablist-ficha-tecnica').removeClass('tablist-ficha-tecnica-selecionado');
		jQuery('.play-icon-wrap-rel').hide();
		jQuery('.episodios-serie-view, .banner-ficha-tecnica, .container-banner-botoes-ajax').hide();
		jQuery('.synopis.content .col-sm-5.col-xs-5.rel, .synopis-outer.synopis-outer-ajax h2.synopis.hidden-xs').hide();
		jQuery('.detalhes-video-view').show();
		jQuery('.s3bubble-details-full-overlay').addClass('s3bubble-details-full-overlay-select');
	})
	jQuery('.tablist-ficha-tecnica').live('click', function(){
		$(this).addClass('tablist-ficha-tecnica-selecionado');
		$('.tablist-item-semelhante').removeClass('tablist-item-semelhante-selecionado');
		$('.tablist-item-episodio').removeClass('tablist-item-detalhes-selecionado');
		$('.tablist-item-detalhes').removeClass('tablist-item-detalhes-selecionado');
		$('.tablist-item-episodio').removeClass('tablist-item-episodio-selecionado');
		jQuery('.play-icon-wrap-rel').show();
		jQuery('.episodios-serie-view, .banner-ficha-tecnica, .container-banner-botoes-ajax').hide();
		jQuery('.synopis.content .col-sm-5.col-xs-5.rel, .synopis-outer.synopis-outer-ajax h2.synopis.hidden-xs').show();
		jQuery('.detalhes-video-view').hide();
		jQuery('.s3bubble-details-full-overlay').addClass('s3bubble-details-full-overlay-select');
	})
	jQuery('.slc-temporadas-videos').live('change',function(e){
		$(this).addClass('tablist-item-episodio-selecionado');
		$('.tablist-item-semelhante').removeClass('tablist-item-semelhante-selecionado');
		$('.tablist-item-detalhes').removeClass('tablist-item-detalhes-selecionado');
		jQuery('.synopis.content .col-sm-5.col-xs-5.rel, .synopis-outer.synopis-outer-ajax h2.synopis.hidden-xs').hide();
		jQuery('.detalhes-video-view .banner-ficha-tecnica, .container-banner-botoes-ajax, .banner-ficha-tecnica, .container-banner-botoes-ajax').hide();

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				jQuery('.episodios-serie-view').html(this.responseText).fadeIn();
				jQuery('.bxslider-series').bxSlider({
					minSlides: 2,
					maxSlides: 12,
					slideWidth: 270	,
					slideMargin: 10,
					pager:false
				});
		}
		};
		xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/mostra-episodios-serie.php?pID="+jQuery(this).attr('attr')+"&selTemporada="+jQuery(this).val(), true);
		xmlhttp.send();
	})

	jQuery('.cellPlanos').live('click', function(){
		jQuery('.cellPlanosSelected').removeClass('cellPlanosSelected').addClass('cellPlanos');
		jQuery(this).removeClass('cellPlanos').addClass('cellPlanosSelected');
		idPlano = jQuery(this).attr('attr');
		jQuery('.valorPlanos').each(function(){
			if(jQuery(this).attr('attr')==idPlano)
				jQuery(this).removeClass('txtPlanos13').addClass('txtPlanos13Sel')
			else
				jQuery(this).removeClass('txtPlanos13Sel').addClass('txtPlanos13')
		});
		jQuery('.iconPlanos').each(function(){
			if(jQuery(this).attr('attr')==idPlano){
				if(jQuery(this).attr('attr-sel')=='R'){
					jQuery(this).removeClass('iconPlanosCinzaR').addClass('iconPlanosLaranjaR')
				}
				if(jQuery(this).attr('attr-sel')=='C'){
					jQuery(this).removeClass('iconPlanosCinzaC').addClass('iconPlanosLaranjaC')
				}
			}else{
				if(jQuery(this).attr('attr-sel')=='R'){
					jQuery(this).addClass('iconPlanosCinzaR').removeClass('iconPlanosLaranjaR')
				}
				if(jQuery(this).attr('attr-sel')=='C'){
					jQuery(this).addClass('iconPlanosCinzaC').removeClass('iconPlanosLaranjaC')
				}
			}
		});
	});

	jQuery('.btn-Passos-planos').live('click', function(){
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				window.location.href = '/cadastro/';
			}
		};
		xmlhttp.open("POST", "https://"+jQuery(location).attr('hostname')+"/integracoes/atualiza-plano-user.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("plano="+jQuery('.cellPlanosSelected').attr('attr'));
	});

	jQuery('.btn-finaliza-cadastro').live('click', function(){
		var erro = 0;
		jQuery('.input-required').each(function(){
			if(jQuery(this).val()==''){
				jQuery(this).css('background-color','rgba(251, 79, 20, 0.13)').css('border','1px solid red');
				console.log(jQuery(this))
				erro = 1;
			}
		});
		if(erro==0){
			jQuery('#frm-finaliza-cadastro').attr('method','post').attr('action',"https://"+jQuery(location).attr('hostname')+"/integracoes/atualiza-cadastro-user.php").submit();
		}
	});
	jQuery('.input-required').live('click keypress keyup', function(){
		jQuery('.input-required').css('background-color','').css('border','');
	});

	jQuery('.cd-main-content').live('click', function(){
		if(jQuery(".cd-search").hasClass('is-visible')){
			jQuery(".cd-search").hide();
			jQuery(".cd-search-trigger").removeClass("search-is-visible");
			jQuery(".cd-search").removeClass("is-visible");
			jQuery(".menu-buscar").css('color','#ffffff');
//			jQuery(".cd-search").show();
		}
	});
})



/* Instru��es Chrome Cast */

var castAtivo = 0;
window['__onGCastApiAvailable'] = function(isAvailable) {
  if (isAvailable) {
    initializeCastApi();
  }
};
initializeCastApi = function() {
  cast.framework.CastContext.getInstance().setOptions({
    receiverApplicationId: '4D4BE8D5',
    autoJoinPolicy: chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED
 });


	var castSession = cast.framework.CastContext.getInstance().getCurrentSession();
	var context = cast.framework.CastContext.getInstance();
	context.addEventListener(
  		cast.framework.CastContextEventType.SESSION_STATE_CHANGED,
  		function(event) {
    		switch (event.sessionState) {
      			case cast.framework.SessionState.SESSION_STARTED:
       				castAtivo = 1;
       				fotterCast(castAtivo);
       		 		break;
      			case cast.framework.SessionState.SESSION_RESUMED:
       				castAtivo = 1;
       				fotterCast(castAtivo);
       		 		break;
      			case cast.framework.SessionState.SESSION_ENDED:
       				castAtivo = 0;
       				fotterCast(castAtivo);
			        break;
    		}
  		})
};


var btnPlayerAtual;
var player;
var playerController;
var tempoAtual;
var request;
var englishSubtitle;
var dados;
var playLoad = null;

jQuery(document).ready(function($){
	setTimeout(function(){
		btnPlayerAtual = "";
		player = new cast.framework.RemotePlayer();
		playerController = new cast.framework.RemotePlayerController(player);
	}, 1000);

	var castDuracao;
	var pause = 0;

	setTimeout(function(){
		if(player['isConnected']){
			if(player['isMediaLoaded']){
				tempoAtual = player['currentTime'];
				jQuery('.player-footer-cast').show();
				jQuery('.content-footer-cast').hide();
				jQuery('.title-footer-cast').html("Assistindo: "+player['mediaInfo']['metadata']['subtitle']);
				jQuery('.cast-duracao').html(toHHMMSS(player['duration']));
				jQuery('#cast-duracao').attr('max',player['duration']).val(player['currentTime']);
				jQuery('#cast-volume').val(player['volumeLevel']);
				clearInterval(castDuracao);
				castDuracao = setInterval(function(){
											tempoAtual = eval(tempoAtual)+1;
											jQuery('#cast-duracao').val(tempoAtual);
											jQuery('.cast-tempo-atual').html(toHHMMSS(tempoAtual));
										  },
							   1000);
			}
		}
	}, 2500);


	jQuery('.play-icon-wrap, .play-icon-wrap-serie').live('click', function(){
		var idPost;
		$(".fa-stop").removeClass('fa-stop').addClass('fa-play');

		if($(this).hasClass("cast-player")){
			event.preventDefault();
			playerController.stop();
			fotterCast(2);
			return false;
		}
		if(castAtivo==1){
			$(this).addClass('cast-play');
			btnPlayerAtual = $(this);
			btnPlayerAtual.find(".fa-play").removeClass('fa-play').addClass('fa-refresh').addClass('fa-spin');
			idPost = $(this).parents(".tile").attr('data-id');
			if (typeof idPost === "undefined") {
				idPost = $(this).parent().children().attr('data-id');
			}
			if (typeof idPost === "undefined") {
				idPost = $(this).attr('data-id');
			}
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					dados = this.responseText.split('|');
					jQuery('.title-footer-cast').html("<i class='fa fa-refresh fa-spin' ></i>&nbsp;&nbsp;Aguarde, carregando o video "+dados[3]);
					jQuery('.content-footer-cast').hide();
					playCastApi(dados);
					return false;
				}
			};
			xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/dados-samba-video.php?vID="+idPost, true);
			xmlhttp.send();
			return false;
		}
	})

	function playCastApi(dados){
		castSession = cast.framework.CastContext.getInstance().getCurrentSession();
		if(dados[5] != "")
			mediaInfo = new chrome.cast.media.MediaInfo(dados[5]);
		else
			mediaInfo = new chrome.cast.media.MediaInfo(dados[0]);
		mediaInfo.contentType = 'video/mp4';
		mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
		mediaInfo.metadata.subtitle = dados[3];
		var englishSubtitle = new chrome.cast.media.Track(1, chrome.cast.media.TrackType.TEXT);
		englishSubtitle.trackContentId = dados[4];
		englishSubtitle.trackContentType = 'text/vtt';
		englishSubtitle.subtype = chrome.cast.media.TextTrackType.SUBTITLES;
		englishSubtitle.name = 'English Subtitles';
		englishSubtitle.language = 'en-US';
		englishSubtitle.customData = null;

		var tracks = [englishSubtitle];
		mediaInfo.customData = null;
		mediaInfo.streamType = chrome.cast.media.StreamType.BUFFERED;
		mediaInfo.textTrackStyle = new chrome.cast.media.TextTrackStyle();
		mediaInfo.duration = null;
		mediaInfo.tracks = tracks;
		var activeTrackIds = [1];
		request = new chrome.cast.media.LoadRequest(mediaInfo);
		request.activeTrackIds = activeTrackIds;

		if(dados[4] != "")
			jQuery('.fa-cc').show().addClass('track-active');
		else
			jQuery('.fa-cc').hide();

		if(dados[5] != "")
			jQuery('.fa-audio-description').show().addClass('audio-legendado');
		else
			jQuery('.fa-audio-description').hide();

		if(dados[5] == dados[0]){
			jQuery('.fa-audio-description').hide();
		}

		castSession.loadMedia(request).then(
		  	function(){
				btnPlayerAtual.find(".fa-play").removeClass('fa-play').addClass('fa-stop');
			    btnPlayerAtual.find(".fa-refresh").removeClass('fa-refresh').removeClass('fa-spin').addClass('fa-stop');
				jQuery('.player-footer-cast').show();
				jQuery('.fa-stop-fotter').addClass('fa-stop');
				jQuery('.cast-play').addClass('cast-player');
				jQuery('.title-footer-cast').html("Assistindo: "+dados[3]);
				jQuery('.cast-duracao').html(dados[1]);
				jQuery('#cast-duracao').attr('max',player['duration']).val(player['currentTime']);
				jQuery('#cast-volume').val(player['volumeLevel']);
				tempoAtual = 0;
				clearInterval(castDuracao);
				castDuracao = setInterval(function(){
												tempoAtual = eval(tempoAtual)+1;
												jQuery('#cast-duracao').val(tempoAtual);
												jQuery('.cast-tempo-atual').html(toHHMMSS(tempoAtual));
											  },
								   1000);
			},
		  	function(errorCode){
//				fotterCast(4);
//				jQuery('.title-footer-cast').html("Erro ao conectar com o v&iacute;deo "+dados[3]+"<br>Aguarde alguns minutos ou tente mais tarde.");
//			    btnPlayerAtual.find(".fa-refresh").removeClass('fa-refresh').removeClass('fa-spin').addClass('fa-play');
//			  	jQuery('.content-footer-cast').hide();
//			  	console.log('Error code: ' + errorCode+ ' - ' + dados[0]);
//				return false;
		  	}
		);
	}


	function playCastApiLoad(dados){
		castSession = cast.framework.CastContext.getInstance().getCurrentSession();
		mediaInfo = new chrome.cast.media.MediaInfo(dados[7]);
		mediaInfo.contentType = 'video/mp4';
		mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
		mediaInfo.metadata.subtitle = dados[3];
		var englishSubtitle = new chrome.cast.media.Track(1, chrome.cast.media.TrackType.TEXT);
		englishSubtitle.trackContentId = dados[4];
		englishSubtitle.trackContentType = 'text/vtt';
		englishSubtitle.subtype = chrome.cast.media.TextTrackType.SUBTITLES;
		englishSubtitle.name = 'English Subtitles';
		englishSubtitle.language = 'en-US';
		englishSubtitle.customData = null;

		var tracks = [englishSubtitle];
		mediaInfo.customData = null;
		mediaInfo.streamType = chrome.cast.media.StreamType.BUFFERED;
		mediaInfo.textTrackStyle = new chrome.cast.media.TextTrackStyle();
		mediaInfo.duration = null;
		mediaInfo.tracks = tracks;
		var activeTrackIds = [dados[10]];
		request = new chrome.cast.media.LoadRequest(mediaInfo);
		playCastApiLoad.activeTrackIds = activeTrackIds;

		if(dados[4] != "")
			jQuery('.fa-cc').show().addClass('track-active');
		else
			jQuery('.fa-cc').hide();

		if(dados[5] != ""){
//			jQuery('.fa-audio-description').show().addClass('audio-legendado');
		}else
			jQuery('.fa-audio-description').hide();

		if(dados[5] == dados[0]){
			jQuery('.fa-audio-description').hide();
		}
		castSession.loadMedia(request).then(
		  	function(){
				btnPlayerAtual.find(".fa-play").removeClass('fa-play').addClass('fa-stop');
			    btnPlayerAtual.find(".fa-refresh").removeClass('fa-refresh').removeClass('fa-spin').addClass('fa-stop');
				jQuery('.player-footer-cast').show();
				jQuery('.fa-stop-fotter').addClass('fa-stop');
				jQuery('.cast-play').addClass('cast-player');
				jQuery('.title-footer-cast').html("Assistindo: "+dados[3]);
//				jQuery('.cast-duracao').html(dados[1]);
//				jQuery('#cast-duracao').attr('max',player['duration']).val(player['currentTime']);
				jQuery('#cast-volume').val(player['volumeLevel']);
				tempoAtual = jQuery('#cast-duracao').val();;
				clearInterval(castDuracao);

				var media = cast.framework.CastContext.getInstance().getCurrentSession().getMediaSession()
				seekRequest = new chrome.cast.media.SeekRequest();
					seekRequest.currentTime = parseInt(tempoAtual);
					media.seek(seekRequest);
					media.play();


				castDuracao = setInterval(function(){
												tempoAtual = eval(tempoAtual)+1;
												jQuery('#cast-duracao').val(tempoAtual);
												jQuery('.cast-tempo-atual').html(toHHMMSS(tempoAtual));
											  },
								   1000);


			},
		  	function(errorCode){
//				fotterCast(4);
//				jQuery('.title-footer-cast').html("Erro ao conectar com o v&iacute;deo "+dados[3]+"<br>Aguarde alguns minutos ou tente mais tarde.");
//			    btnPlayerAtual.find(".fa-refresh").removeClass('fa-refresh').removeClass('fa-spin').addClass('fa-play');
//			  	jQuery('.content-footer-cast').hide();
//			  	console.log('Error code: ' + errorCode+ ' - ' + dados[0]);
//				return false;
		  	}
		);

	}

	function successCallback(dados){
		console.log('sucesso: '+dados);
	}

	function errorCallback(dados){
		console.log('error: '+dados);
	}

	jQuery('.player-footer-cast .fa-stop-fotter').live('click', function(){
		playerController.stop();
		fotterCast(2);
		return false;
	});
	jQuery('.player-footer-cast .fa-pause').live('click', function(){
		playerController.playOrPause();
		jQuery('.player-footer-cast .fa-play').show();
		$(this).hide();
		pause = 1;
		clearInterval(castDuracao);
	});
	jQuery('.player-footer-cast .fa-play').live('click', function(){
		playerController.playOrPause();
		$(this).hide();
		jQuery('.player-footer-cast .fa-pause').show();
		tempoAtual = jQuery('#cast-duracao').val();
		pause = 0;
		castDuracao = setInterval(function(){
									tempoAtual = eval(tempoAtual)+1;
									jQuery('#cast-duracao').val(tempoAtual);
									jQuery('.cast-tempo-atual').html(toHHMMSS(tempoAtual));
								  },
					   1000);
		return false;
	});

	jQuery('.fa-volume-up').live('click', function(){
		jQuery('#cast-volume').show();
		jQuery('.fa-volume-up').hide();
	});
	jQuery('#cast-volume').live('mouseup', function(){
	  	player.volumeLevel = eval(jQuery(this).val());
	  	playerController.setVolumeLevel();
		jQuery('#cast-volume').hide();
		jQuery('.fa-volume-up').show();
	});


	jQuery('#cast-duracao').live('input', function(){
		clearInterval(castDuracao);
		tempoAtual = jQuery(this).val();
		jQuery('.cast-tempo-atual').html(toHHMMSS(tempoAtual));
		jQuery('#cast-duracao').val(tempoAtual)
		var media = cast.framework.CastContext.getInstance().getCurrentSession().getMediaSession()
		seekRequest = new chrome.cast.media.SeekRequest();
		seekRequest.currentTime = parseInt(tempoAtual);
		media.seek(seekRequest);
		if(pause==0){
			media.play();
			castDuracao = setInterval(function(){
										tempoAtual = eval(tempoAtual)+1;
										jQuery('#cast-duracao').val(tempoAtual);
										jQuery('.cast-tempo-atual').html(toHHMMSS(tempoAtual));
									  },
						   1000);
		}
	});

	jQuery('.track-active').live('click', function(){
		var media = cast.framework.CastContext.getInstance().getCurrentSession().getMediaSession()
		var activeTrackIds = [];
        var tracksInfoRequest = new chrome.cast.media.EditTracksInfoRequest(activeTrackIds);
        	media.editTracksInfo(tracksInfoRequest, function succCB(){}, function errorCallback(err){
          	console.log(err)
        });
		jQuery('.fa-cc').removeClass('track-active').addClass('track-inactive');
	});
	jQuery('.track-inactive').live('click', function(){
		var media = cast.framework.CastContext.getInstance().getCurrentSession().getMediaSession()
		var activeTrackIds = [1];
        var tracksInfoRequest = new chrome.cast.media.EditTracksInfoRequest(activeTrackIds);
        	media.editTracksInfo(tracksInfoRequest, function succCB(){}, function errorCallback(err){
          	console.log(err)
        });
		jQuery('.fa-cc').addClass('track-active').removeClass('track-inactive');
	});

	jQuery('.audio-dublado').live('click', function(){
		jQuery('.fa-audio-description').removeClass('audio-dublado').addClass('audio-legendado');
		playLoad = null;
		playLoad = dados;
		playLoad[6] = jQuery('#cast-duracao').val();
		playLoad[7] = dados[5];
		playLoad[10] = 1;
		playCastApiLoad(playLoad);
	})
	jQuery('.audio-legendado').live('click', function(){
		tempoAtual = jQuery('#cast-duracao').val();
		jQuery('.fa-audio-description').removeClass('audio-legendado').addClass('audio-dublado');
		jQuery('.fa-cc').addClass('track-inactive').removeClass('track-active');
		playLoad = null;
		playLoad = dados;
		playLoad[6] = jQuery('#cast-duracao').val();
		playLoad[7] = dados[0];
		playLoad[10] = null;
		playCastApiLoad(playLoad);
	})
});


function fotterCast(status){
	if(status==1){
		jQuery('.footer-cast').show();
	}else if(status==2){
		jQuery('.title-footer-cast').html('Pronto para transmitir')
		jQuery('.cast-player').removeClass('cast-player');
		jQuery('.content-footer-cast').show();
		jQuery('.player-footer-cast').hide();
		jQuery('.fa-stop').removeClass('fa-stop').addClass('fa-play');
	}else if(status==4){
		jQuery('.cast-player').removeClass('cast-player');
		jQuery('.content-footer-cast').show();
		jQuery('.player-footer-cast').hide();
	}else{
		jQuery('.title-footer-cast').html('Pronto para transmitir')
		jQuery('.footer-cast').hide();
		jQuery('.content-footer-cast').show();
		clearInterval(castDuracao);
	}
}

jQuery(document).ready(function($){
	var largura = jQuery(document).width();
	var altura = eval(largura) * 0.25;
	jQuery('.page-palestrantes-interna').height(altura);

	$(".imgIcones").eq(3).remove();
});

jQuery( window ).resize(function() {
	var largura = jQuery(document).width();
	var altura = eval(largura) * 0.25;
	jQuery('.page-palestrantes-interna').height(altura);
});

function toHHMMSS(secs){
	var sec_num = parseInt(secs, 10)
	var hours   = Math.floor(sec_num / 3600) % 24
	var minutes = Math.floor(sec_num / 60) % 60
	var seconds = sec_num % 60
	return [hours,minutes,seconds]
		.map(v => v < 10 ? "0" + v : v).filter((v,i) => v !== "00" || i > 0).join(":")
}