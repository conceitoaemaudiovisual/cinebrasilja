<?php
	class videosDestaqueClube extends WP_Widget {
		public function videosDestaqueClube() {
			parent::WP_Widget(false, "Videos Destaque");
		}
		public function widget($argumentos, $instancia) {
			global $wpdb;
			$upload_dir = wp_upload_dir();
			if(wp_is_mobile()==""){
				if(($argumentos['widget_id']=='videosdestaqueclube-2')||($argumentos['widget_id']=='videosdestaqueclube-6')){
					echo "	<script>
								jQuery('.video-list-new').live('mouseover', function(){
									jQuery('.video-thumbnail-capa').hide();
									jQuery(this).children().show();
								});
								jQuery('.video-list-new').live('mouseleave', function(){
									jQuery('.video-thumbnail-capa').hide();
									jQuery('.box-video-description').remove();
								});
								jQuery('.video-list-new').live('click', function(){
									$('.video-list-close').click();
									$('html, body').animate({scrollTop: $($(this).attr('attrtop')).offset().top-40}, 600);
									jQuery($(this).attr('attrid')).html('');
									jQuery($(this).attr('attrid')).load('/wp-content/plugins/clube-kardec/functions/video-description.php?id='+jQuery(this).attr('attr'))
									jQuery('.setinha-carrossel-'+jQuery(this).attr('attr')).show().css('z-index', 9999);
									jQuery($(this).attr('attrid')).slideUp(200).delay(800).fadeIn();
								})
								jQuery('.video-list-close').live('click', function(){
									jQuery('#video_principal').remove();
									jQuery('.description-carrossel').hide();
								})

							</script>
							<style>
								.faixa-carrossel span{
									font-family:Arial, Helvetica, Tahoma, Verdana, sans-serif !important;
									font-weight:bold !important;
									font-size:19px !important;
									color:#ffffff !important;
									text-decoration:none !important;
								}
								.faixa-carrossel p{
									font-family:Arial, Helvetica, Tahoma, Verdana, sans-serif;
									font-weight:normal;
									font-size:13px;
									color:#ffffff;
									text-decoration:none;
								}
								#faixa-carrossel-ficha p{
									font-family:Arial, Helvetica, Tahoma, Verdana, sans-serif; font-weight:normal; font-size:13px; color:#ffffff; text-decoration:none;
								}
								.bx-wrapper .bx-viewport{
									border: 5px solid transparent
								}
								.bx-wrapper .bx-next{
								    margin-right: -9px;
								}
								.bx-wrapper .bx-prev {
								    margin-left: -17px !important;
								}
								#video-description-container p{
									color:#ffffff !important;
									font-size: 14px !important;
								}
							</style>";
				}
			}else{
				if(($argumentos['widget_id']=='videosdestaqueclube-2')||($argumentos['widget_id']=='videosdestaqueclube-6')){
					echo "	<script>
								jQuery('.video-list-new').live('click', function(){
									open('/video/'+jQuery(this).attr('link'),'_self');
								});
							</script>";

				}
			}
			echo "			<style>
								.".$argumentos['widget_id'].", .".$argumentos['widget_id']." .bx-viewport,".$argumentos['widget_id']."  .bx-wrapper{
									background-color:".$instancia[cor]." !important;
								}

							</style>";

			$ordem = " order by p.post_title";

			if($instancia['categoria'] == -1){
				$videosID = $wpdb->get_results("select Video_ID from wp_clube_cadastros_lista  group by Video_ID order by count(*) desc limit 20");
				foreach($videosID as $videoID){
					$video .= $virgula.$videoID->Video_ID;
					$virgula = ",";
				}
				$strConsulta = " and p.id in ($video) ";
				$ordem 		 = " order by field(p.id, $video) limit 20 ";
			}else if($instancia['categoria'] == -2){
				$ordem = " order by post_date desc limit 20 ";
			}else{
				$video = "";
				$virgula = "";
				$videosID = $wpdb->get_results("select object_id Video_ID from wp_term_relationships tr inner join wp_term_taxonomy tx on tx.term_taxonomy_id = tr.term_taxonomy_id where term_id = ".$instancia['categoria']);
				foreach($videosID as $videoID){
					$video .= $virgula.$videoID->Video_ID;
					$virgula = ",";
				}
				$strConsulta = " and p.id in ($video) ";
				$ordem = " order by post_date desc limit 20 ";
			}
			if($instancia['categoria'] == 6)
				$ordem = " order by post_name";

			$videos = $wpdb->get_results("select p.ID,p.Post_Title, p.Post_Content, p.Post_Excerpt, p.Post_name, pm2.meta_value Thumb, pm4.meta_value Description,
										  month(post_date) Mes, year(post_date) Ano
										  from wp_posts p
										  inner join wp_postmeta pm1 on pm1.post_id = p.ID and pm1.meta_key = '_thumbnail_id'
										  inner join wp_postmeta pm2 on pm2.post_id = pm1.meta_value and pm2.meta_key = '_wp_attachment_metadata'
										  inner join wp_postmeta pm3 on pm3.post_id = p.ID and pm3.meta_key = 'video_destaque'
										  inner join wp_postmeta pm4 on pm4.post_id = p.ID and pm4.meta_key = 'video-detalhes'
										  where post_type = 'video' and post_status = 'publish' $strConsulta $ordem");
			foreach($videos as $video){
				$arrayThumb = unserialize($video->Thumb);
				if($arrayThumb[sizes][thumbnail][file] != ""){
					$thumb = $upload_dir[url]."/".$arrayThumb[sizes][thumbnail][file];
					$imagesCarrousel .= " 	<li class='video-list-new' attr='$video->ID' Style='cursor:pointer' link='$video->Post_name' attrid='.".$argumentos['widget_id']."-desc' attrtop='.".$argumentos['widget_id']."'>
												<div Style='position:fixed;display:none;' class='video-thumbnail-capa'>
													<img src='http://clubekardec.com.br/wp-content/images/layout/img-hover-produtos.png'>
												</div>
												<img src='$thumb' class='video-thumbnail' alt='$video->Post_Title'/>
											</li>";
					$sinopse   	= get_post_meta($video->ID,'sinopse-video',true);
					$ficha	 	= get_post_meta($video->ID,'ficha-video',true);
					$amostra	= str_replace('.mp4','-amostra_x264.mp4',$upload_dir['url']."/amostra/".get_post_meta($video->ID,'video_principal',true));
				}
			}
			if(($argumentos['widget_id']=='videosdestaqueclube-2')||($argumentos['widget_id']=='videosdestaqueclube-6'))
				echo "	<script type='text/javascript'>
							$(document).ready(function(){
								$('.bxslider').bxSlider({
									minSlides: 3,
									maxSlides: 12,
									slideWidth: 170,
									slideMargin: 10,
									pager:false
								});
							});
						</script>";
			echo "	<div class='videos-destaques ".$argumentos['widget_id']."' Style='float:left;width:99%;padding:3px 0.5% 4px 0.5%;'>
						<ul class='bxslider' Style='float:left;'>
							$imagesCarrousel
						</ul>
					</div>
					<div Style='float:left;width:100%;position:relative;margin-bottom:10px;display:none;text-align:center;' id='faixa-carrossel' class='".$argumentos['widget_id']."-desc description-carrossel'></div>";

		}
		public function update($nova_instancia, $instancia_antiga) {
			$instance[categoria] = $_POST['categorias'];
			$instance[cor] = $_POST['cor'];
			return $instance;
		}
		public function form($instancia) {
			$selecionado[$instancia[categoria]] = 'selected';
			echo "	<select name='categorias' style='width:100%;margin-top:5px;margin-bottom:10px;'>
						<option value='-1' ".$selecionado[-1].">Mais assistidos</option>
						<option value='-2' ".$selecionado[-2].">Lan&ccedil;amentos</option>
					</select>
					Cor de fundo: <input type='text' name='cor' value='".$instancia[cor]."'>";
		}
	}
	add_action('widgets_init', create_function('', 'return register_widget("videosDestaqueClube");'));

	function mostraMeses($entrada){
		$mes['1'] = 'JANEIRO';
		$mes['2'] = 'FEVEREIRO';
		$mes['3'] = 'MAR&Ccedil;O';
		$mes['4'] = 'ABRIL';
		$mes['5'] = 'MAIO';
		$mes['6'] = 'JUNHO';
		$mes['7'] = 'JULHO';
		$mes['8'] = 'AGOSTO';
		$mes['9'] = 'SETEMBRO';
		$mes['10'] = 'OUTUBRO';
		$mes['11'] = 'NOVEMBRO';
		$mes['12'] = 'DEZEMBRO';
		return $mes[$entrada];
	}
?>
