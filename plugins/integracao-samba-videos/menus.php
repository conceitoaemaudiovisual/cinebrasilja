<?php
	add_action('init', 'cinebrasilplay_register');
	function cinebrasilplay_register() {
		$labels = array(
			'name' 					=> __( 'Generos'),
			'singular_name' 		=> __( 'Genero'),
			'search_items' 			=> __( 'Buscar' ),
			'popular_items' 		=> __( 'Mais usadas' ),
			'all_items' 			=> __( 'Todas as Generos' ),
			'parent_item' 			=> null,
			'parent_item_colon' 	=> null,
			'edit_item' 			=> __( 'Adicionar nova Genero' ),
			'update_item' 			=> __( 'Atualizar' ),
			'add_new_item' 			=> __( 'Cadastrar Genero' ),
			'new_item_name' 		=> __( 'Nova' )
		  );
		$rewrite = array(
			'slug'                       => 'genero',
			'with_front'                 => true,
			'hierarchical'               => true,
		);
		register_taxonomy('genero_video', array('post','series'),
			array(
			'labels' 			=> $labels,
			'singular_label' 	=> 'Genero',
			'hierarchical' 		=> true,
			'all_items' 		=> 'Todas as Generos',
			'query_var' 		=> true,
			'show_admin_column' => true,
			'sort'				=> true,
			'rewrite' 			=> $rewrite)
		);
		$labels = array(
			'name' => 'Series',
			'singular_name' => 'Serie',
			'add_new' => 'Adicionar Nova',
			'add_new_item' => 'Adicionar nova',
			'edit_item' => 'Editar Serie',
			'new_item' => 'Adicionar nova',
			'all_items' => 'Gerenciar Series',
			'view_item' => 'Visualizar Serie',
			'search_items' => 'Localizar Series',
			'not_found' =>  'Nenhuma Serie Localizada',
			'not_found_in_trash' => 'Nenhuma Serie na Lixeira',
			'parent_item_colon' => '',
			'menu_name' => 'Series'
		);
		$args = array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'menu_position' 		=> 4,
			'hierarchical' 			=> true,
			'menu_icon'   			=> 'dashicons-format-aside',
			'supports' 				=> array('title','editor','thumbnail'),
			'taxonomies' 			=> array('post_tag','category','kids')
		);
		register_post_type('series' , $args );

		$labels = array(
			'name' 					=> __( 'Temporadas'),
			'singular_name' 		=> __( 'Temporada'),
			'search_items' 			=> __( 'Buscar' ),
			'popular_items' 		=> __( 'Mais usadas' ),
			'all_items' 			=> __( 'Todas as Temporadas' ),
			'parent_item' 			=> null,
			'parent_item_colon' 	=> null,
			'edit_item' 			=> __( 'Adicionar nova Temporada' ),
			'update_item' 			=> __( 'Atualizar' ),
			'add_new_item' 			=> __( 'Cadastrar Temporada' ),
			'new_item_name' 		=> __( 'Nova' )
		  );
		$rewrite = array(
			'slug'                       => 'temporada',
			'with_front'                 => true,
			'hierarchical'               => true,
		);
		register_taxonomy('Temporada_video', array('series'),
			array(
			'labels' 			=> $labels,
			'singular_label' 	=> 'Temporada',
			'hierarchical' 		=> true,
			'all_items' 		=> 'Todas as Temporadas',
			'query_var' 		=> true,
			'show_admin_column' => true,
			'sort'				=> true,
			'rewrite' 			=> $rewrite,
			'meta_box_cb'		=> false)
		);

		$labels = array(
			'name' => 'Planos',
			'singular_name' => 'Planos',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar novo',
			'edit_item' => 'Editar Plano',
			'new_item' => 'Adicionar novo',
			'all_items' => 'Gerenciar planos',
			'view_item' => 'Visualizar planos',
			'search_items' => 'Localizar planos',
			'not_found' =>  'Nenhum plano Localizado',
			'not_found_in_trash' => 'Nenhum plano na Lixeira',
			'parent_item_colon' => '',
			'menu_name' => 'Planos'
		);
		$args = array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'menu_position' 		=> '15',
			'hierarchical' 			=> true,
			'menu_icon'   			=> 'dashicons-cart',
			'supports' 				=> array('title','editor')
		  );
		register_post_type('planos' , $args );

		$labels = array(
			'name' 					=> __( 'Itens'),
			'singular_name' 		=> __( 'Item'),
			'search_items' 			=> __( 'Buscar' ),
			'popular_items' 		=> __( 'Mais usados' ),
			'all_items' 			=> __( 'Todos os Itens' ),
			'parent_item' 			=> null,
			'parent_item_colon' 	=> null,
			'edit_item' 			=> __( 'Adicionar novo Item' ),
			'update_item' 			=> __( 'Atualizar' ),
			'add_new_item' 			=> __( 'Cadastrar Item' ),
			'new_item_name' 		=> __( 'Novo' )
		  );
		$rewrite = array(
			'slug'                       => 'Item',
			'with_front'                 => true,
			'hierarchical'               => true,
		);
		register_taxonomy('item_plano', array('planos'),
			array(
			'labels' 			=> $labels,
			'singular_label' 	=> 'Item',
			'hierarchical' 		=> true,
			'all_items' 		=> 'Todos as Itens',
			'query_var' 		=> true,
			'show_admin_column' => true,
			'sort'				=> true,
			'rewrite' 			=> $rewrite)
		);


		$labels = array(
			'name' => 'Especiais',
			'singular_name' => 'Especiais',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar novo',
			'edit_item' => 'Editar Especial',
			'new_item' => 'Adicionar novo',
			'all_items' => 'Gerenciar Especiais',
			'view_item' => 'Visualizar Especiais',
			'search_items' => 'Localizar Especiais',
			'not_found' =>  'Nenhum Especial Localizado',
			'not_found_in_trash' => 'Nenhum Especial na Lixeira',
			'parent_item_colon' => '',
			'menu_name' => 'Especiais'
		);
		$args = array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'menu_position' 		=> 4,
			'hierarchical' 			=> true,
			'menu_icon'   			=> 'dashicons-groups',
			'supports' 				=> array('title','editor','thumbnail','tags'),
			'taxonomies' 			=> array('post_tag','category')
		  );
		register_post_type('especial' , $args );
		flush_rewrite_rules();
	}
?>