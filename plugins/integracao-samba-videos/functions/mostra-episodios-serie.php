<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	session_start();
	global $wpdb, $post;
	require_once('../../../../wp-config.php');

	$postID = $_GET['pID'];
	$post = get_post($postID);
	$tituloSerie = $post->post_title;

	$terms = $wpdb->get_results("select term_id, name from wp_terms");
	foreach($terms as $term){
		$categorias[$term->term_id] = $term->name;
	}
	if($post->post_type=='palestrante'){
		$mostraTemporadaEpisodio = " Style='display:none;' ";
		$destaqueTitulo = " Style='font-weight:bold;margin-top:3px;' ";
		$videosSerie = get_post_meta($post->ID,'video-palestra',true);
		foreach($videosSerie as $indice=>$episodios){
			if($episodios['video'] != ""){
				$videos[] =  $episodios['video'];
				$videoDetalhes[$episodios[video]] = $episodios;
			}
		}
		if($indice <= 0) $videos[] = -1000;
	}else{
		$videosSerie = get_post_meta($postID,'video-serie',true);
		foreach($videosSerie as $indice=>$episodios){
			if($episodios['episodio'] != ""){
				$episodioNumero = explode(' ',$categorias[$episodios['episodio']]);
				$temporadaNumero = explode(' ',$categorias[$episodios['temporada']]);
				$videosSerie[$indice]['temporada-titulo'] = $categorias[$episodios['temporada']];
				$videosSerie[$indice]['temporada-numero'] = $temporadaNumero[1];
				$videosSerie[$indice]['episodio-titulo'] = $categorias[$episodios['episodio']];
				$videosSerie[$indice]['episodio-numero'] = $episodioNumero[1];
				$videos[] =  $episodios['video'];
				$videoDetalhes[$episodios[video]] = $episodios;
				if($episodios['temporada'] != ""){
					$isSerie = 1;
					$temporadasSerie[$temporadaNumero[1]] = $categorias[$episodios['temporada']];
				}
			}
		}
	}
	if($isSerie==1)
		ksort($temporadasSerie);

?>
<?php if($isSerie==1){?>
<select name='selTemporada' class='slc-temporadas-videos' attr='<?php echo $postID;?>'>
	<?php
		$temporadaSelecionada[$_GET['selTemporada']] = "selected";
		foreach($temporadasSerie as $indiceTemporada=>$temporada)
			echo "<option value='$indiceTemporada' ".$temporadaSelecionada[$indiceTemporada].">$temporada</option>";
	?>
</select>
<?php }?>
<ul class="bxslider-series carousels overlay">
<?php
	$temporadaSel = $_GET['selTemporada'];
	if($temporadaSel == '')
		$temporadaSel = 1;

	$args = array(
			'posts_per_page' => (int)get_theme_mod( 'streamium_global_options_homepage_desktop' ),
			'post__in' => $videos
	);
	$loop = new WP_Query( $args );
	if($loop->have_posts()):
		while ( $loop->have_posts() ) : $loop->the_post();
		if ( has_post_thumbnail() ) : // thumbnail check
		$imgMetaPadraoID = get_post_meta($post->ID,'post_thumb-padrao_thumbnail_id',true);
		$imgMetaHover1ID = get_post_meta($post->ID,'post_thumb-hover-1_thumbnail_id',true);
		$imgMetaHover2ID = get_post_meta($post->ID,'post_thumb-hover-2_thumbnail_id',true);
		$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
		$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
		$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
		$nonce = wp_create_nonce( 'streamium_likes_nonce' );

		$temporadaNumero = explode(' ',$categorias[$videoDetalhes[$post->ID][temporada]]);
		$episodioNumero = explode(' ',$categorias[$videoDetalhes[$post->ID][episodio]]);
		$categorias[$videoDetalhes[$post->ID][episodio]] = $episodioNumero[1];

		if($isSerie==1){
			if($temporadaSel == $temporadaNumero[1])
				$listaSeries[$temporadaNumero[1].$episodioNumero[1]] = "<li class='temporada-$temporadaNumero[1] temporadas-episodios-container'>
																			<img src='$image[0]'/>
																			<a class='play-icon-wrap-serie hidden-xs'  href='".get_permalink($post->ID)."' data-id='".$post->ID."'>
																				<div class='play-icon-wrap-rel'>
																					<div class='play-icon-wrap-rel-ring' Style='border: 0.27vw solid #fff !important;'></div>
																					<span class='play-icon-wrap-rel-play'>
																						<i class='fa fa-play fa-1x' aria-hidden='true' Style='margin-left: 6px !important;'></i>
																					</span>
																				</div>
																			</a>
																			<span class='carrossel-titulo-episodios-temporada' $mostraTemporadaEpisodio>".$categorias[$videoDetalhes[$post->ID][episodio]]."</span>
																			<span class='carrossel-titulo-episodios-video' $destaqueTitulo>$post->post_title</span>
																			<span class='carrossel-sinopse-episodios-video'>".get_post_meta($post->ID, 'sinopse-video', true)."</span>
																		</li>";
		}else{
			echo "<li>
					<img src='$image[0]'/>
					<a class='play-icon-wrap-serie hidden-xs'  href='".get_permalink($post->ID)."'>
						<div class='play-icon-wrap-rel'>
							<div class='play-icon-wrap-rel-ring' Style='border: 0.27vw solid #fff !important;'></div>
							<span class='play-icon-wrap-rel-play'>
								<i class='fa fa-play fa-1x' aria-hidden='true' Style='margin-left: 6px !important;'></i>
							</span>
						</div>
					</a>
					<span class='carrossel-titulo-episodios-temporada' $mostraTemporadaEpisodio>".$categorias[$videoDetalhes[$post->ID][episodio]]."</span>
					<span class='carrossel-titulo-episodios-video' $destaqueTitulo>$post->post_title</span>
					<span class='carrossel-sinopse-episodios-video'>".get_post_meta($post->ID, 'sinopse-video', true)."</span>
				</li>";
		}
	endif;
	endwhile;
	endif;
	wp_reset_query();
	ksort($listaSeries);
	foreach($listaSeries as $dadosSeries)
		echo $dadosSeries;

?>
</ul>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.bxslider').bxSlider({
			minSlides: 2,
			maxSlides: 12,
			slideWidth: 270	,
			slideMargin: 10,
			pager:false
		});
	});
</script>
