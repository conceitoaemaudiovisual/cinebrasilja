<?php

	wp_enqueue_style('clube-video',plugin_dir_url(__FILE__)."css/style.css");
	wp_enqueue_script('clube-video',plugin_dir_url(__FILE__)."javascript/funcoes.js",array('jquery'));
	wp_enqueue_script('clube-video-masc',plugin_dir_url(__FILE__)."javascript/jquery.maskedinput.min.js",array('jquery'));

	add_action( 'admin_init', 'metabox_clube_video' );
	function metabox_clube_video() {
		add_meta_box('clube-anexar-video', 'Anexar V&iacute;deo', 'anexaVideo', 'post', 'side', 'core' );
		add_meta_box('clube-video-destaque', 'Video Destaque / Lan&ccedil;amento', 'destacaVideo', 'post', 'side', 'default' );
		add_meta_box('clube-video-sinopse', 'Sinopse', 'sinopseVideo', 'post', 'normal', 'high' );
		add_meta_box('clube-video-ficha', 'Ficha T&eacute;cnica', 'fichaVideo', 'post', 'normal', 'high' );
		add_meta_box('clube-temporada', 'Videos da Temporada', 'videosTemporada', 'series', 'normal', 'high' );
	}

	function destacaVideo(){
		global $post;
		$destaque 	= get_post_meta($post->ID,'video_destaque',true);
		$lancamento = get_post_meta($post->ID,'video_lancamento',true);
		echo "	<table>
					<tr>
						<td width='25' align='right'><input type='checkbox' name='video-destaque' value='checked' $destaque></td>
						<td width='70' align='left'>Destaque</td>
						<td width='25' align='right'><input type='checkbox' name='video-lancamento' value='checked' $lancamento></td>
						<td>Lan&ccedil;amento</td>
					</tr>
				</table>";
	}

	add_action('save_post','video_clube_video_save');
	function video_clube_video_save(){
		global $post;
		update_post_meta($post->ID,'video-anexado',$_POST['select-video']);
		update_post_meta($post->ID,'video-detalhes',$_POST['video-detalhes']);
		update_post_meta($post->ID,'video_destaque',$_POST['video-destaque']);
		update_post_meta($post->ID,'video_lancamento',$_POST['video-lancamento']);

		update_post_meta($post->ID,'sinopse-video',$_POST['sinopse-video']);
		update_post_meta($post->ID,'ficha-video',$_POST['ficha-video']);

		update_post_meta($post->ID,'video-serie',$_POST['serie']);
	}


	function sinopseVideo(){
		global $post,$wpdb;
		$sinopse = get_post_meta($post->ID,'sinopse-video',true);
		$settings = array('editor_height'=>100,'tinymce'=> true ,'media_buttons'=>false);
		wp_editor( $sinopse, 'sinopse-video',$settings);
	}

	function fichaVideo(){
		global $post,$wpdb;
		$ficha = get_post_meta($post->ID,'ficha-video',true);
		$settings = array('editor_height'=>100,'tinymce'=> true ,'media_buttons'=>false);
		wp_editor($ficha,'ficha-video',$settings);
	}

	add_action( 'admin_init', 'metabox_planos_detalhes_principais' );
	function metabox_planos_detalhes_principais() {
		add_meta_box( 'metaboxPlanosDetalhesPrincipais', 'Detalhes do Plano', 'conteudoMetaboxPlanosDetalhesPrincipais', 'planos', 'normal', 'high' );
		function conteudoMetaboxPlanosDetalhesPrincipais(){
			global $post,$wpdb;
			$detalhesPlano = get_post_meta($post->ID,'detalhes-plano',true);
			echo "	<table width='100%'>
						<tr>
							<td>Ordem:</td>
							<td>Valor Mensal:</td>
							<td>Valor Trimestral:</td>
							<td>Valor Semestral:</td>
							<td>Valor Anual:</td>
						</tr>
						<tr>
							<td><input type='text' Style='width:095%' name='detalhes[ordem]' 	 	onkeypress='mascaraVal(this)' 							   maxlength='2' value='".$detalhesPlano[ordem]."'></td>
							<td><input type='text' Style='width:095%' name='detalhes[mensal]' 	 	onkeypress='mascaraVal(this)' onkeyup='formataValor(this)' maxlength='8' value='".$detalhesPlano[mensal]."'></td>
							<td><input type='text' Style='width:095%' name='detalhes[trimestral]' 	onkeypress='mascaraVal(this)' onkeyup='formataValor(this)' maxlength='8' value='".$detalhesPlano[trimestral]."'></td>
							<td><input type='text' Style='width:095%' name='detalhes[semestral]'  	onkeypress='mascaraVal(this)' onkeyup='formataValor(this)' maxlength='8' value='".$detalhesPlano[semestral]."'></td>
							<td><input type='text' Style='width:100%' name='detalhes[anual]'  	 	onkeypress='mascaraVal(this)' onkeyup='formataValor(this)' maxlength='8' value='".$detalhesPlano[anual]."'></td>
						</tr>
					</table>";
		}
	}
	add_action( 'save_post', 'metabox_planos_detalhes_principais_Save' );
	function metabox_planos_detalhes_principais_Save(){
		global $post;
		update_post_meta($post->ID,'detalhes-plano',$_POST['detalhes']);
	}

	add_filter( 'wp_mail_content_type', 'set_content_type' );
	function set_content_type( $content_type ) {
		return 'text/html';
	}

	function anexaVideo(){
		global $post;
		$videosSamba = localizaVideosSamba();
		$videoKey = get_post_meta($post->ID, 'video-anexado', true);
		foreach($videosSamba as $indice=>$video){
			$videoSelected[$video[media_key]] = $indice;
			$arrayVideo[$indice][catID] = $video[categoriaID];
			$arrayVideo[$indice][title] = $video[postTitle];
			$arrayVideo[$indice][key] 	= $video[media_key];
		}
		$videosSambaArray = json_encode($arrayVideo);
		$texto .= "	<script>var videosSamba = JSON.stringify($videosSambaArray);</script>";

		$selecionadoCat[$arrayVideo[$videoSelected[$videoKey]][catID]] = "selected";
		$selecionadoVideo[$arrayVideo[$videoSelected[$videoKey]][key]] = "selected";
		foreach($videosSamba as $videos){
			$dadosVideo .=  "<option value='$videos[media_key]' ".$selecionadoVideo[$videos[media_key]]." >$videos[postTitle]</option>";
			$categorias[$videos[categoriaID]] = "<option value='$videos[categoriaID]' ".$selecionadoCat[$videos[categoriaID]].">$videos[categoria]</option>";
		}

		$texto .= "	<table width='100%' class='customize-pane-parent'>
						<tr>
							<td>
								<select name='select-categoria' id='select-categoria' Style='width:100%;'>
									<option value=''>Selecione a Categoria</option>";
		foreach($categorias as $categoria){
			$texto .= $categoria;
		}
		$texto .= "				</select>
							</td>
						</tr>
						<tr>
							<td>
								<select name='select-video' id='select-video' Style='width:100%;' class='chosen-select'>
									<option value=''>Selecione o Video</option>
									$dadosVideo
								</select>
							</td>
						</tr>
					</table>
					<table width='100%' id='player-video-samba'>
						<tr>
							<td colspan='2'>
								<iframe allowfullscreen webkitallowfullscreen mozallowfullscreen
										width='100%'
										height='150'
										src='//fast.player.liquidplatform.com/pApiv2/embed/71936e9f933f46a293eb9979c3b78b60/$videoKey'
										scrolling='no'
										frameborder='0'>
								</iframe>
							</td>
						</tr>
					</table>
		";
		echo $texto;
	}



	function localizaVideosSamba(){
		$apiKey = "50a21afd2580fdbfe10f0cfc132fe5d5";
		$categorias = retornaCurlNovo("channels?key=$apiKey&filter=id;name;");
		foreach($categorias as $categoria){
			for($i=1;$i<=1;$i++){
				$paginador = ($i*50)-50;
				$artigos = retornaCurlNovo("medias?key=$apiKey&search=channelId:$categoria->id&sort=asc&first=$paginador");
				foreach($artigos as $artigo){
					$categoria  = json_decode(json_encode($categoria),TRUE);
					$artigo 	= json_decode(json_encode($artigo),TRUE);
					$dados['categoriaID']	= $categoria['id'];
					$dados['categoria']		= $categoria['name'];
					$dados['media_key']		= $artigo['id'];
					$dados['postTitle'] 	= $artigo['title'];

					$videoSamba[] = $dados;
				}
			}
		}
		return $videoSamba;
	}

	function retornaCurlNovo($urlComplemento){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://fast.api.liquidplatform.com/2.0/".$urlComplemento);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        return simplexml_load_string($output);
	}

	function videosTemporada(){
		global $wpdb, $post, $videos, $temporadas,$episodios;
		$videos = $wpdb->get_results("select ID, Post_Title from wp_posts where post_type = 'video' and post_status = 'publish' order by post_title");
		foreach($videos as $video)
			$optionVideo .= "<option value='$video->ID'>$video->Post_Title</option>";

		$temporadas = $wpdb->get_results("select t.term_id, t.name from wp_terms t inner join wp_term_taxonomy tx on tx.term_id = t.term_id where taxonomy = 'Temporada_video' and parent = 0 order by name");
		foreach($temporadas as $temporada)
			$optionTemporada .= "<option value='$temporada->term_id'>$temporada->name</option>";

		$episodios = $wpdb->get_results("select t.term_id, t.name from wp_terms t inner join wp_term_taxonomy tx on tx.term_id = t.term_id where taxonomy = 'Temporada_video' order by name");

		$texto = "	<table width='100%' class='customize-pane-parent'>
						<tr>
							<td>Video</td>
							<td width='150'>Temporada</td>
							<td width='150'>Episodio</td>
							<td width='90'>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<select Style='width:100%;padding:10px;' name='serie[0][video]' id='videos-serie' class='required'>
									<option value=''>selecione</option>
									$optionVideo
								</select>
							</td>
							<td>
								<select Style='width:100%;' name='serie[0][temporada]' id='temporada-serie' class='required'>
									<option value=''>selecione</option>
									$optionTemporada
								</select>
							</td>
							<td>
								<select Style='width:100%;' name='serie[0][episodio]' id='episodio-serie' class='required'>
									<option value=''>selecione</option>
								</select>
							</td>
							<td>
								<input type='button' class='button button-primary button-large' value='Adicionar' id='adiciona-video-serie' Style='width:89px;'>
							</td>
						</tr>
					</table>
					<table width='100%' Style='margin-top:0px;'>";
		$videosCadastrados = get_post_meta($post->ID,'video-serie',true);
		foreach($videosCadastrados as $videoCadastrado){
			if($videoCadastrado[video] != ""){
				$indice++;
				$detalhesVideo = localizaDetalhesVideo($videoCadastrado);
				$texto .= "	<tr>
								<td>&nbsp;".$detalhesVideo[titulo][video]."</td>
								<td width='150'>&nbsp;".$detalhesVideo[titulo][temporada]."</td>
								<td width='150'>&nbsp;".$detalhesVideo[titulo][episodio]."</td>
								<td width='90' align='center' class='submitbox'>
									<a class='submitdelete remove-video-serie' attr='$indice' Style='cursor:pointer;'>remover video</a>
									<input type='hidden' class='detalhes-video-$indice' name='serie[$indice][video]' 		value='".$detalhesVideo[video]."'>
									<input type='hidden' class='detalhes-video-$indice' name='serie[$indice][temporada]' 	value='".$detalhesVideo[temporada]."'>
									<input type='hidden' class='detalhes-video-$indice' name='serie[$indice][episodio]' 	value='".$detalhesVideo[episodio]."'>
								</td>
							</tr>";
			}
		}
		$texto .= "	</table>";
		echo $texto;
	}

	function localizaDetalhesVideo($dados){
		global $videos, $temporadas,$episodios;

		foreach($videos as $video)
			if($video->ID==$dados[video])
				$dados[titulo][video] = $video->Post_Title;
		foreach($temporadas as $temporada)
			if($temporada->term_id==$dados[temporada])
				$dados[titulo][temporada] = $temporada->name;
		foreach($episodios as $episodio)
			if($episodio->term_id==$dados[episodio])
				$dados[titulo][episodio] = $episodio->name;

		return $dados;
	}




















	add_shortcode('video-slider', 'videoSlider');
	function videoSlider($atributos){
		global $wpdb;
		$texto .= "<script src='/wp-content/plugins/integracao-samba-videos/javascript/jquery.bxslider.min.js'></script>
				<link rel='stylesheet' href='/wp-content/plugins/integracao-samba-videos/css/jquery.bxslider.min.css' type='text/css' media='all'>
				<script>
					jQuery('.video-list-new').live('mouseover', function(){
						jQuery('.video-thumbnail-capa').hide();
						jQuery(this).children().show();
					});
					jQuery('.video-list-new').live('mouseleave', function(){
						jQuery('.video-thumbnail-capa').hide();
						jQuery('.box-video-description').remove();
					});
					jQuery('.video-list-new').live('click', function(){
						jQuery('.video-list-close').click();
						jQuery('html, body').animate({scrollTop: $($(this).attr('attrtop')).offset().top-40}, 600);
						jQuery($(this).attr('attrid')).html('');
						jQuery($(this).attr('attrid')).load('/wp-content/plugins/integracao-samba-videos/functions/video-description.php?id='+jQuery(this).attr('attr'))
						jQuery('.setinha-carrossel-'+jQuery(this).attr('attr')).show().css('z-index', 9999);
						jQuery($(this).attr('attrid')).slideUp(200).delay(800).fadeIn();
					})
					jQuery('.video-list-close').live('click', function(){
						jQuery('#video_principal').remove();
						jQuery('.description-carrossel').hide();
					})

				</script>";

			$videos = $wpdb->get_results("select p.ID,p.Post_Title, p.Post_Content, p.Post_Excerpt, p.Post_name, pm4.meta_value Description,
										  month(post_date) Mes, year(post_date) Ano
										  from wp_posts p
										  inner join wp_postmeta pm3 on pm3.post_id = p.ID and pm3.meta_key = 'video_destaque'
										  inner join wp_postmeta pm4 on pm4.post_id = p.ID and pm4.meta_key = 'video-detalhes'
										  where post_type = 'video' and post_status = 'publish' $strConsulta $ordem");
			foreach($videos as $video){
				if($atributos[type]=='H'){
					$thumb_id = get_post_meta($video->ID, 'video_thumb-landscape_thumbnail_id', true );
					$thumb = wp_get_attachment_image_src($thumb_id,'imagem-horizontal');
				}
				if($atributos[type]=='V'){
					$thumb_id = get_post_meta($video->ID, 'video_thumb-portrait_thumbnail_id', true );
					$thumb = wp_get_attachment_image_src($thumb_id,'imagem-vertical');
				}


				$imagesCarrousel .= " 	<li class='video-list-new' attr='$video->ID' Style='cursor:pointer' link='$video->Post_name' attrid='.".$argumentos['widget_id']."-desc' attrtop='.".$argumentos['widget_id']."'>
											<div Style='position:fixed;display:none;' class='video-thumbnail-capa'>
												<img src='/wp-content/uploads/img-hover-produto.png'>
											</div>
											<img src='$thumb[0]' class='video-thumbnail' alt='$video->Post_Title'/>
										</li>";
				$sinopse   	= get_post_meta($video->ID,'sinopse-video',true);
				$ficha	 	= get_post_meta($video->ID,'ficha-video',true);
				$amostra	= str_replace('.mp4','-amostra_x264.mp4',$upload_dir['url']."/amostra/".get_post_meta($video->ID,'video_principal',true));
			}
			$texto .= "<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('.bxslider').bxSlider({
								minSlides: 3,
								maxSlides: 12,
								slideWidth: ".$atributos[width].",
								slideMargin: 5,
								pager:false
							});
						});
					</script>";
			$texto .= "<p class='tituloLogadaCinza'>$atributos[title]</p>";
			$texto .= "<div class='videos-destaques'>
						<ul class='bxslider' Style='float:left;'>
							$imagesCarrousel
							$imagesCarrousel
							$imagesCarrousel
						</ul>
					</div>
					<div Style='float:left;width:100%;position:relative;margin-bottom:10px;display:none;text-align:center;' id='faixa-carrossel' class='".$argumentos['widget_id']."-desc description-carrossel'></div>";
			return $texto;
		}

add_action('category_edit_form_fields','category_edit_form_fields');
add_action('category_add_form_fields','category_edit_form_fields');

function category_edit_form_fields ( $term ) {
	$posicao = get_term_meta( $term->term_id, 'posicao_home', true );
?>
    <tr class="form-field">
            <th valign="top" scope="row">
                <label for="catpic"><?php _e('Posi&ccedil;&atilde;o na Home', ''); ?></label>
            </th>
            <td>
            	<input type="text" id="posicao_home" name="posicao_home" value='<?php echo $posicao;?>'/>
            </td>
        </tr>
        <?php
    }

function category_save_posicao( $term_id ) {

	if ( isset( $_POST['posicao_home'] ) ) {
		$posicao = $_POST['posicao_home'];
		if( $posicao ) {
			 update_term_meta( $term_id, 'posicao_home', $posicao);
		}
	}

}
add_action( 'edited_category', 'category_save_posicao' );
add_action( 'create_category', 'category_save_posicao' );
?>