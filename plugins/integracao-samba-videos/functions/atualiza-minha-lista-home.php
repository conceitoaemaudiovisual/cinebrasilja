<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	session_start();
	global $wpdb, $post;
	require_once('../../../../wp-config.php');

	$post = get_post($_GET['pID']);

	$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
	$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
	$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
	$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
	$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
	$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
	$nonce = wp_create_nonce( 'streamium_likes_nonce' );
	$linkID = $post->ID;
?>


	<div class="tile my-list-item-add" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="my-list">
		<div class="tile_inner" style="background-image: url(<?php echo esc_url($image[0]); ?>);height: 154px; transform: scale(1); border: 0px solid transparent;"">
			<div class="content">
		      <div class="overlay background-expanded background-img-<?php echo $post->ID;?>" style="background-image: url(<?php echo esc_url($imageExpanded[0]); ?>);" attr0="<?php echo esc_url($imageExpanded[0]); ?>" attr1="<?php echo esc_url($imageExpanded2[0]); ?>">
			<div class="overlay-gradient"></div>
			<a class="play-icon-wrap hidden-xs" href="<?php the_permalink($linkID); ?>">
					<div class="play-icon-wrap-rel">
						<div class="play-icon-wrap-rel-ring"></div>
						<span class="play-icon-wrap-rel-play">
							<i class="fa fa-play fa-1x" aria-hidden="true"></i>
					</span>
				</div>
			</a>
			<div class="overlay-meta hidden-xs">
			<h4><?php the_title(); ?></h4>
			<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="my-list" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
			</div>
		      </div>
		    </div>
		</div>
    </div>
