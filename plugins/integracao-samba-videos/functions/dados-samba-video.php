<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	session_start();
	global $wpdb;
	require_once('../../../../wp-config.php');

	global $current_user;
	get_currentuserinfo();


	unset($dados);
	$post = get_post($_GET['vID']);

	if($post->post_type == "series"){
		$dadosAux = $wpdb->get_var("select meta_value from wp_postmeta where post_id = ".$_GET['vID']." and meta_key = 'video-serie'");
		$dadosAux = unserialize($dadosAux);
		foreach($dadosAux as $dadoAux){
			if($dadoAux[video] != ""){
				$post = get_post($dadoAux['video']);
				$_GET['vID'] = $dadoAux['video'];
			}
		}
	}

	$dados = $wpdb->get_row("select midias, duracao, capa from aux_samba_videos
							 where IDmedia = (select meta_value from wp_postmeta where meta_key = 'video-anexado' and post_id = ".$_GET['vID'].")");
	if($dados->midias == '')
		$dados = $wpdb->get_row("select midias, duracao, capa from aux_samba_videos
								 where IDmedia = (select meta_value from wp_postmeta where meta_key = 'video-anexado-secundario' and post_id = ".$_GET['vID'].")");
	$dados->midia = unserialize($dados->midias);

	$segundoAudio = $wpdb->get_row("select midias, duracao, capa from aux_samba_videos
								 where IDmedia = (select meta_value from wp_postmeta where meta_key = 'video-anexado-secundario' and post_id = ".$_GET['vID'].")");
	if($segundoAudio->midias != "")
		$dados2->midia = unserialize($segundoAudio->midias);

	$legenda = get_post_meta($_GET['vID'],'select-legenda',true);

	echo $dados->midia[0][url]."|".$dados->duracao."|".$dados->capa."|".$post->post_title."|".$legenda."|".$dados2->midia[0][url];
?>