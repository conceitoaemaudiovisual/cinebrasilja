jQuery(document).ready(function($){

	jQuery('#select-categoria').live('change', function(){
		var arrayVid = JSON.parse(videosSamba);
		jQuery('#select-video').empty();
	    jQuery('#select-video').append("<option value=''>Selecione o Video</option>");
		$.each(arrayVid,function(index, value){
			if(jQuery('#select-categoria').val()==value.catID){
			    jQuery('#select-video').append("<option value='"+value.key+"'>"+value.title+"</option>");
			}
		})
	});


	jQuery('#adiciona-video-serie').live('click', function(){
		var erro = 0;
		jQuery('.required').each(function(){
			if($(this).val()==''){
				$('#'+$(this).attr('id').replace('-','_')+'_chosen a').css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD');
				erro = 1;
			}
		});
		if(erro==0)
			$('#post').submit();
	});
	jQuery('.required').live('change', function(){
		$('#'+$(this).attr('id').replace('-','_')+'_chosen a').css('background-color', '').css('border', '');
	});
	jQuery('#temporada-serie').live('change', function(){
       	var xmlhttp = new XMLHttpRequest();
       	xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
				jQuery('#episodio-serie').empty();
				jQuery('#episodio-serie').append(this.responseText);
				$('#episodio-serie').trigger( 'chosen:updated' );

            }
        };
        xmlhttp.open("GET", "http://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/localiza-episodios.php?temporada="+jQuery(this).val(), true);
        xmlhttp.send();

	});
	jQuery('.remove-video-serie').live('click', function(){
		jQuery('.detalhes-video-'+jQuery(this).attr('attr')).remove();
		jQuery('#post').submit();
	});










	jQuery('.input-login').live('keypress', function(){
		mascaraCampo($(this));
	});
	jQuery('.botao-login-normal').live('click',function(e){
		var erro = 0;
		jQuery('.input-login').each(function(){
			if(jQuery(this).val()==''){
				erro = erro + 1;
				jQuery(this).css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD');
				if(erro==1)
					jQuery(this).focus();

			}
		});
		if(erro==0){
			if(!validaEmailCadastro(document.getElementById('text-login'))){
				jQuery('#text-login').css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD').focus();
				jQuery('#erro-login').html('Endere\u00e7o de e-mail inv\u00e1lido');
				erro = erro + 1;
			}
		}
		if(erro==0){
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/valida-login.php', data: jQuery('.input-login').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					if(retorno.trim()==0){
						jQuery('#erro-login').html('Login ou Senha inv\u00e1lida.');
					}else{
						console.log(retorno);
						window.location.href = '/';
					}
				}
			});
		}
	});

	jQuery('#menu-topo-navegar').live('hover',function(e){
		jQuery('#menu-topo-navegar-interno').show();
	});
	jQuery('#menu-topo-navegar').live('mouseleave',function(e){
		jQuery('#menu-topo-navegar-interno').hide();
	});

	jQuery('#menu-topo-buscar').live('hover',function(e){
		jQuery('#menu-topo-buscar-interno').show();
		jQuery('#txt-menu-buscar').val('').focus();
	});
	jQuery('#menu-topo-buscar').live('mouseleave',function(e){
		jQuery('#menu-topo-buscar-interno').hide();
	});

	jQuery('#menu-topo-usuario').live('hover',function(e){
		jQuery('#menu-topo-usuario-interno').show();
	});
	jQuery('#menu-topo-usuario').live('mouseleave',function(e){
		jQuery('#menu-topo-usuario-interno').hide();
	});
	jQuery('#menu-usuario-navegar-interno-sair').live('click',function(e){
		jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/logout.php', data: jQuery('form').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
			success: function(retorno){
				window.location.href = '/';
			}
		});
	});
	jQuery('#menu-topo-navegar-interno-inicio').live('click',function(e){
		window.location.href = '/';
	});
	jQuery('#menu-topo-navegar-interno-lista').live('click',function(e){
		window.location.href = '/lista/';
	});
	jQuery('#menu-topo-navegar-interno-contato').live('click',function(e){
		window.location.href = '/contato/';
	});
	jQuery('#menu-buscar-botao').live('click',function(e){
		if(jQuery('#txt-menu-buscar').val()==''){
			jQuery('#txt-menu-buscar').css('background-color', '#feefef').focus();
			return false;
		}
		jQuery('#txt-menu-buscar').css('background-color', '');
		jQuery('#busca-geral').attr('action','/busca/').attr('method','post').submit();
	});
	jQuery('#menu-usuario-navegar-interno-cadastro').live('click',function(e){
		window.location.href = '/homolog/inscricao/';
	});

	jQuery('.botao-atualiza-cadastro').live('click',function(e){
		var erro = 0;
		jQuery('.required').each(function(){
			if(jQuery(this).val()==''){
				erro = erro + 1;
				jQuery(this).css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
				if(erro==1)
					jQuery(this).focus();
			}
		});
		if(erro==0){
			if(!validaEmailCadastro(document.getElementById('email-cadastro'))){
				jQuery('#email-cadastro').css('background-color', '#feefef').css('border', '1px solid #FFCDCD').focus();
				jQuery('#erro-cadastro').html('Endere\u00e7o de e-mail inv\u00e1lido');
				erro = erro + 1;
			}
		}
		if(erro==0){
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/atualiza-cadastro.php', data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					jQuery('#erro-cadastro').html('Cadastro Atualizado com sucesso.');
				}
			});
		}
	});

	jQuery("#cpf").mask("999.999.999-99");
	jQuery("#telefone").mask("(99) 9999-9999?9");
	jQuery("#cep").mask("99999-999");
	jQuery('.input-texto').live('keypress',function(e){mascaraCampo($(this));});
	jQuery('.input-numero').live('keypress',function(e){mascaraVal($(this));});
	jQuery('#cep').live('blur',function(e){
		jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/localiza-endereco.php?cep='+jQuery(this).val(), data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
			success: function(retorno){
				var campo = retorno.split(',');
				if(campo[0] != ''){
					jQuery('#endereco').val(campo[1]+' '+campo[2]);
					jQuery('#bairro').val(campo[3]);
					jQuery('#cidade').val(campo[4]);
					jQuery('#estado').val(campo[5]);
					jQuery('#numero').focus();
				}else{
					jQuery('#endereco').val('');
					jQuery('#bairro').val('');
					jQuery('#cidade').val('');
					jQuery('#estado').val('');
					jQuery('#endereco').focus();
				}
			}
		});
	});

	jQuery('.required, .li-tipo-cartao').live('click change keypress keyup',function(e){
		jQuery('.required, .input-cadastro').css('background-color', '').css('border', '');
		jQuery('.li-tipo-cartao').css('background-color', '').css('border', '1px solid #ffffff');
	});

	jQuery('.botao-finaliza-cadastro').live('click',function(e){
		var erro = 0;
		jQuery('.required').each(function(){
			if(jQuery(this).val()==''){
				erro = eval(erro) + 1;
				jQuery(this).css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
			}
		});
		if(jQuery('#pais').val()=='Brasil'){
			if(jQuery('#cpf').val()==''){
				jQuery('#cpf').css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
				erro = eval(erro) + 1;
				if(erro==1)
					jQuery('#cpf').focus();
			}
		}
		jQuery('.tipo-cartao').each(function(){
			if($( "input:checked" ).length==0){
				jQuery('.li-tipo-cartao').css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
				erro = eval(erro) + 1;
			}
		});
		if(erro==0){
			if(!validaEmailCadastro(document.getElementById('email-cadastro'))){
				jQuery('#email-cadastro').css('background-color', '#feefef').css('border', '1px solid #FFCDCD').focus();
				jQuery('#erro-cadastro').html('Endere\u00e7o de e-mail inv\u00e1lido');
				erro = eval(erro) + 1;
			}
		}
		if(erro==0){
			jQuery(this).removeClass('botao-finaliza-cadastro').addClass('botao-atualiza-cadastro-aguarde');
			jQuery('.required').css('background-color', '').css('border', '');
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/gateway-cielo/novo-cliente.php', data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					jQuery('#form-pgto').attr('target','_self').submit();
				}
			});
		}
	});
});






























function validaEmailCadastro(email){
	valor = email.value
	Arroba= valor.indexOf("@");
	Ponto= valor.lastIndexOf(".");
	espaco= valor.indexOf(" ");
	if (valor == '')
		return false;
	if  ((Arroba != -1) && (Ponto > Arroba +1) && (espaco==-1)){
		return true;
	}else{
		return false;
	}
}
function mascaraVal(formato, keypress, objeto){
	campo = eval (objeto);
	if((event.keyCode >= 48)&&(event.keyCode <= 57)){
		event.returnValue = true;
	}else{
	  event.returnValue = false;
	}
}

function mascaraCampo(formato, keypress, objeto){
    campo = eval (objeto);
	if(((event.keyCode >= 65)&&(event.keyCode <= 90))
	 ||((event.keyCode >= 97)&&(event.keyCode <= 122))
	 ||((event.keyCode >= 192)&&(event.keyCode <= 255))
	 ||((event.keyCode >= 32)&&(event.keyCode <= 38))
	 ||((event.keyCode >= 40)&&(event.keyCode <= 47))
	 ||((event.keyCode >= 58)&&(event.keyCode <= 64))
	 ||((event.keyCode >= 91)&&(event.keyCode <= 95))
	 ||((event.keyCode >= 123)&&(event.keyCode <= 255))
	 ||((event.keyCode <= 57)&&(event.keyCode >= 48))
	 ||((event.keyCode >= 44)&&(event.keyCode <= 47))
	 ||((event.keyCode == 38))||((event.keyCode == 95))){
	 		event.returnValue = true;
	 	}else{
	 		event.returnValue = false;}
}
function formataValor(campo){
  valor = campo.value;
  tamanho =valor.length;
  decimal = tamanho - 2;

  antes= valor.indexOf(",");
  depois = antes+1

  inteiro = valor.substring(0,antes)+valor.substring(tamanho,depois);
  tamanho = inteiro.length;

  if(tamanho>=3)
  	campo.value = inteiro.substring(0,tamanho-2)+','+inteiro.substring(tamanho-2,tamanho);

  if(tamanho<3)
  	campo.value = inteiro.substring(0,tamanho-2)+inteiro.substring(tamanho-2,tamanho);

}


jQuery(document).ready(function(){
	jQuery('#loop-interna').css('padding','');
	jQuery('.input-login').live('click keypress',function(e){
		jQuery('.input-login').css('background-color', '').css('border', '');
		jQuery('#erro-login').html('');
	});
	jQuery('.esqueceu-senha').live('click',function(e){
		jQuery('.texto-superior-entrar').html('Recuperar Senha').css('width','65%');
		jQuery('#erro-login').css('width','35%');
		jQuery('.campo-login-inicio').hide();
		jQuery('.campo-login-recupera').show();
		jQuery('#erro-login').html('');
	});
	jQuery('.cancelar-senha').live('click',function(e){
		jQuery('.texto-superior-entrar').html('Entrar').css('width','40%');
		jQuery('#erro-login').css('width','60%');
		jQuery('.campo-login-inicio').show();
		jQuery('.campo-login-recupera').hide();
	});

	jQuery('.localizar-senha').live('click',function(e){
		if($('#text-login').val()==''){
			jQuery('#text-login').css('background-color', '#FFE4E4').css('border', '1px solid #FFCDCD');
			return false;
		}
		jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/localiza-senha.php', data: $('#text-login').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
			success: function(retorno){
				console.log(retorno)
				if(retorno=='')
					jQuery('#erro-login').html('E-mail inv\u00e1lido');
				else{
					$('.cancelar-senha').click();
					jQuery('#erro-login').html('Senha enviada com sucesso!');
				}
			}
		});
	});
	jQuery('.tipo-cartao').live('click', function(){
		if(jQuery(this).val()=='aura'){
			jQuery('#numero_cartao').attr('maxlength','19');
		}else
			jQuery('#numero_cartao').attr('maxlength','16');
	});
});




	jQuery('.botao-finaliza-cadastro-antigo').live('click',function(e){
		var erro = 0;
		jQuery('.required').each(function(){
			if(jQuery(this).val()==''){
				erro = erro + 1;
				jQuery(this).css('background-color', '#feefef').css('border', '1px solid #FFCDCD');
				if(erro==1)
					jQuery(this).focus();
			}
		});
		if(erro==0){
			if(!validaEmailCadastro(document.getElementById('email-cadastro'))){
				jQuery('#email-cadastro').css('background-color', '#feefef').css('border', '1px solid #FFCDCD').focus();
				jQuery('#erro-cadastro').html('Endere\u00e7o de e-mail inv\u00e1lido');
				erro = erro + 1;
			}
		}
		if(erro==0){
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/valida-cadastro.php', data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					console.log('envia pgto');
					jQuery('.required').css('background-color', '').css('border', '');
					jQuery.ajax({type: 'POST',url: '/wp-content/plugins/gateway/pagseguro/', data: jQuery('.input-cadastro').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
						success: function(retorno){
							jQuery('#finaliza-pagamento').attr('href','/wp-content/plugins/gateway/finaliza-pagamento.php?cod='+retorno).trigger('click');
						}
					});
				}
			});
		}
	});


	jQuery('.botao-minha-lista-home').live('click', function(){

	})
