<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	session_start();
	global $wpdb, $post;
	require_once('../../../../wp-config.php');

	$postID = $_GET['pID'];
	$post = get_post($postID);
	$tituloSerie = $post->post_title;
	$videosNot = $postID;

	$videosSerie = get_post_meta($postID,'video-serie',true);
	foreach($videosSerie as $indice=>$episodios){
		if($episodios['episodio'] != ""){
			$videosNot .=  ','.$episodios['video'];
		}
	}
	$semelhantes = $wpdb->get_results("select id
								  	  from wp_term_relationships tr1
								  	  inner join wp_posts p on p.ID = tr1.object_id
								  	  where tr1.term_taxonomy_id in (select tx.term_taxonomy_id
																	from wp_term_taxonomy tx
																	inner join wp_term_relationships tr on tr.term_taxonomy_id = tx.term_taxonomy_id
																	where taxonomy = 'genero_video' and object_id = $postID)
									 and p.id not in ($videosNot)");
	foreach($semelhantes as $indice=>$semelhante){
		if($semelhante->id != ""){
			$videos[] =  $semelhante->id;
		}
	}
?>
<ul class="bxslider-series carousels overlay">
<?php
	$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
	foreach($videosSerie as $videoSerie){
		$postsSeries = unserialize($videoSerie->meta_value);
		foreach($postsSeries as $postSeries){
			if($postSeries['video'] != '') {
				$listVideoSerie[] = $postSeries['video'];
			}
		}
	}
	$args = array(
			'posts_per_page' => (int)get_theme_mod( 'streamium_global_options_homepage_desktop' ),
			'post__in' => $videos,
			'post__not_in' => $listVideoSerie
	);
	$loop = new WP_Query( $args );
	if($loop->have_posts()):
		while ( $loop->have_posts() ) : $loop->the_post();
		if ( has_post_thumbnail() ) : // thumbnail check
		$imgMetaPadraoID = get_post_meta($post->ID,'post_thumb-padrao_thumbnail_id',true);
		$imgMetaHover1ID = get_post_meta($post->ID,'post_thumb-hover-1_thumbnail_id',true);
		$imgMetaHover2ID = get_post_meta($post->ID,'post_thumb-hover-2_thumbnail_id',true);
		$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
		$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
		$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
		$nonce = wp_create_nonce( 'streamium_likes_nonce' );

		echo "	<li class='temporadas-episodios-container'>
					<img src='$image[0]'/>
					<a class='play-icon-wrap-serie hidden-xs'  href='".get_permalink($post->ID)."'>
						<div class='play-icon-wrap-rel'>
							<div class='play-icon-wrap-rel-ring' Style='border: 0.27vw solid #fff !important;'></div>
							<span class='play-icon-wrap-rel-play'>
								<i class='fa fa-play fa-1x' aria-hidden='true' Style='margin-left: 6px !important;'></i>
							</span>
						</div>
					</a>
					<span class='carrossel-titulo-episodios-video'>$post->post_title</span>
					<span class='carrossel-sinopse-episodios-video'>".get_post_meta($post->ID, 'sinopse-video', true)."</span>
				</li>";
	endif;
	endwhile;
	endif;
	wp_reset_query();
?>
</ul>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.bxslider').bxSlider({
			minSlides: 2,
			maxSlides: 12,
			slideWidth: 270	,
			slideMargin: 10,
			pager:false
		});
	});
</script>
