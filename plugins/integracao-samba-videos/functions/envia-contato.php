<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	error_reporting(E_ALL);
	ini_set('display_errors', 'On');

	session_start();
	global $wpdb;
	require_once('../../../../wp-config.php');

	$msg  = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
				<html xmlns='http://www.w3.org/1999/xhtml'>
					<head>
						<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
						<title>$siteName</title>
					</head>
					<body bgcolor='#f0f1f2'>
						<table width='707' border='0' align='center' cellpadding='5' cellspacing='5' class='bordaPadrao'  bgcolor='#FFFFFF' >
							<tr>
								<td align='left' valign='middle' class='bordaTabela'><img src='".get_bloginfo('wpurl')."/wp-content/images/layout/topo-email.jpg' width='707'/></td>
							</tr>
							<tr>
								<td align='left' valign='top' >
									<table width='90%' align='center' height='220'>
										<tr>
											<td>Nome: ".$_POST[nome]."</td>
										</tr>
										<tr>
											<td>E-mail: ".$_POST[email]."</td>
										</tr>
										<tr>
											<td>Telefones: ".$_POST[celular]." ".$_POST[residencial]."</td>
										</tr>
										<tr>
											<td>Mensagem: ".$_POST[mensagem]."</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</body>
				</html>";


	$headers[] = "From: Clube kardec <contato@clubekardec.com.br>";
	$headers[] = "Bcc: Clube kardec <contato@clubekardec.com.br>";

	wp_mail($_POST[email],"Clube Kardec - Fale Conosco",$msg,$headers);
?>
