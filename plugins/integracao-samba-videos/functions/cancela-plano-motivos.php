<?php


?>
<style>
	.confirma-cancelamento-plano{
		text-align:center;
		float:right;
		padding:5px 15px 5px 15px;
		background-color:#640000;
		color:#ffffff;
		border-radius:5px;
		margin-right:0px;
		margin-top:10px;
		cursor:pointer;
		border:1px solid silver;
	}
	.confirma-cancelamento-plano:hover{
		opacity:0.8;
		color:#f0f0f0;
	}
	.cancelamento-confirma{
		display:none;
	}
	.confirma-cancelamento-plano-finaliza{
		text-align:center;
		padding:5px 15px 5px 15px;
		background-color:#640000;
		color:#ffffff;
		border-radius:5px;
		margin-right:0px;
		margin-top:10px;
		cursor:pointer;
		border:1px solid silver;
	}
	.confirma-cancelamento-plano-finaliza:hover{
		opacity:0.8;
		color:#f0f0f0;
	}
</style>
<script>
	jQuery(document).ready(function(){
		var selecionado = 0;
		jQuery('#confirma-cancelamento-plano').live('click', function(){
			jQuery('.radio-motivo-cancelamento').each(function(){
				if(jQuery(this).is(':checked'))
					selecionado = 1;
			});
			if(selecionado == 1){
				jQuery('.cancelamento-inicio').hide();
				jQuery('.cancelamento-confirma').show();
				jQuery('#erro-motivo').hide();
			}else{
				jQuery('#erro-motivo').show();
			}
		});
		jQuery('.radio-motivo-cancelamento').live('click', function(){
			jQuery('#erro-motivo').hide();
		});

		jQuery('#confirma-cancelamento-plano-finaliza').live('click', function(){
			jQuery.ajax({type: 'POST',url: '/wp-content/plugins/clube-kardec/functions/cancela-plano-finaliza.php', data: $('.radio-motivo-cancelamento').serialize(), dataType: 'html', contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				success: function(retorno){
					window.location.href = "/homolog/inscricao/";
				}
			});
		});

	});
</script>

<div Style='width:500px;float:left;'>
	<div style="width:95%;float:left;text-align:center;background-color:#640000;color:#ffffff;font-weight:bold;border-radius:5px;padding:10px 10px 10px 10px;font-size:16px;"  class='cancelamento-inicio'>Selecione o motivo do cancelamento</div>
	<ul Style='float:left;margin-top:10px;width:95%' class='cancelamento-inicio'>
		<li Style='padding:4px;font-size:14px;'><input type='radio' name='rd-motivo-cancelamento' value='Insatisfa&ccedil;&atilde;o' 	id='motivo1' class='radio-motivo-cancelamento'><label for='motivo1'>Insatisfa&ccedil;&atilde;o</label></li>
		<li Style='padding:4px;font-size:14px;'><input type='radio' name='rd-motivo-cancelamento' value='Dificuldades financeiras' 		id='motivo2' class='radio-motivo-cancelamento'><label for='motivo2'>Dificuldades financeiras</label></li>
		<li Style='padding:4px;font-size:14px;'><input type='radio' name='rd-motivo-cancelamento' value='N&atilde;o utiliza' 			id='motivo3' class='radio-motivo-cancelamento'><label for='motivo3'>N&atilde;o utiliza</label></li>
		<li Style='padding:4px;font-size:14px;'><input type='radio' name='rd-motivo-cancelamento' value='Outros' 	 					id='motivo4' class='radio-motivo-cancelamento'><label for='motivo4'>Outros</label></li>
	</ul>
	<div id='erro-motivo' Style='float:left;color:red;display:none;padding:10px;margin-top:9px;'>O motivo do cancelamento deve ser informado.</div>
	<input type='button' class='confirma-cancelamento-plano cancelamento-inicio' id='confirma-cancelamento-plano' value='Cancelar Assinatura'>
	<div Style='float:left;text-align:center;width:100%;' class='cancelamento-confirma'>
		<p Style='float:left;width:95%;padding:5px;text-align:center;font-size:16px;margin-top:25px;font-weight:bold;'>Confirma o cancelamento da assinatura?</p>
		<p Style='float:left;width:95%;padding:5px;text-align:center;font-size:16px;margin-top:5px;font-weight:bold;'>Se confirmada esta opera&ccedil;&atilde;o n&atilde;o poder&aacute; ser cancelada.</p>
		<input type='button' class='confirma-cancelamento-plano-finaliza' id='confirma-cancelamento-plano-finaliza' value='Confirmar Cancelamento' Style='margin-top:23px;margin-bottom:15px;'>
	</div>
</div>
