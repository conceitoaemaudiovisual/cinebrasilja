<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	session_start();
	global $wpdb;
	require_once('../../../../wp-config.php');

	global $current_user;
	get_currentuserinfo();

	$userID  	= $current_user->data->ID;
	if($_GET['userID'] != "")
		$userID = $_GET['userID'];

	$videoID 	= $_GET['pID'];
	$tipo  	 	= $_GET['type'];
	$userList 	= get_user_meta($userID, 'my-list');
	$userList 	= $userList[0];

	$mensagem =  "";
	if($tipo==1){
		$userList[count($userList)] = $videoID;
		$newList = update_user_meta($userID, 'my-list',$userList);
		$mensagem = "Incluido com sucesso!";
	}

	if($tipo==0){
		$videoList = array_search($videoID, $userList);
		unset($userList[$videoList]);
		sort($userList);
		if($userList[0] == '')
			$newList = delete_user_meta($userID, 'my-list');
		else
			$newList = update_user_meta($userID, 'my-list',$userList);
		$mensagem = "Removido com sucesso!";
	}

	$classe = "ion-android-add-borda";
	$acao 	= "1";
	$userList 	= get_user_meta($userID, 'my-list');
	$dados = (string) array_search($videoID,$userList[0]);
	if($dados != ""){
		//$classe = "ion-android-remove";
		$classe = "ion-ios-checkmark-outline";
		$acao 	= "0";
	}

	$statusLike = $wpdb->get_var("select type from wp_likebtn_vote where identifier = $videoID and user_id = $userID  order by id desc limit 1");
	if($statusLike==1){
		$imgLike[up] = "http://www.cinebrasilplay.com.br/wp-content/uploads/icone-aba-aberta-hand-up-hover.png";
		$imgLike[down] = "http://www.cinebrasilplay.com.br/wp-content/uploads/icone-aba-aberta-hand-down.png";
	}else if($statusLike==-1){
		$imgLike[up] = "http://www.cinebrasilplay.com.br/wp-content/uploads/icone-aba-aberta-hand-up.png";
		$imgLike[down] = "http://www.cinebrasilplay.com.br/wp-content/uploads/icone-aba-aberta-hand-down-hover.png";
	}else{
		$imgLike[down] = "http://www.cinebrasilplay.com.br/wp-content/uploads/icone-aba-aberta-hand-down.png";
		$imgLike[up] = "http://www.cinebrasilplay.com.br/wp-content/uploads/icone-aba-aberta-hand-up.png";
	}
	$retorno['liked'] 	 = $imgLike;
	$retorno['lista'] 	 = $userList[0];
	$retorno['classe']   = $classe;
	$retorno['acao']  	 = $acao;
	$retorno['mensagem'] = $mensagem;
	echo json_encode($retorno);

?>
