<?php
	header("Content-type: text/html; charset=UTF-8");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	error_reporting(E_ALL);
	ini_set('display_errors', 'On');

	session_start();
	global $wpdb;
	require_once('../../../../wp-config.php');

	if($_SESSION['login']->Cadastro_ID != "")
		$mostraInscreva = " Style='display:none;' ";

	$termName = $wpdb->get_var("select t.name
								from wp_term_relationships tr
								inner join wp_term_taxonomy tx on tx.term_taxonomy_id = tr.term_taxonomy_id
								inner join wp_terms t on t.term_id = tx.term_id
								where object_id = ".$_GET['id']);

	$videos = $wpdb->get_results("select p.ID,p.Post_Title, p.Post_Content, p.Post_Excerpt, p.Post_name, pm2.meta_value Thumb, pm4.meta_value Description,
								  month(post_date) Mes, year(post_date) Ano
								  from wp_posts p
								  inner join wp_postmeta pm1 on pm1.post_id = p.ID and pm1.meta_key = '_thumbnail_id'
								  inner join wp_postmeta pm2 on pm2.post_id = pm1.meta_value and pm2.meta_key = '_wp_attachment_metadata'
								  inner join wp_postmeta pm3 on pm3.post_id = p.ID and pm3.meta_key = 'video_destaque'
								  inner join wp_postmeta pm4 on pm4.post_id = p.ID and pm4.meta_key = 'video-detalhes'
								  where post_type = 'video' and post_status = 'publish' and p.ID = ".$_GET['id']);
	foreach($videos as $video){
			$sinopse   	= get_post_meta($video->ID,'sinopse-video',true);
			$ficha	 	= get_post_meta($video->ID,'ficha-video',true);
			if($_SESSION[login]->Cadastro_ID == '')
				$amostra = str_replace('.mp4','-amostra_x264.mp4',$upload_dir['url']."/amostra/".get_post_meta($video->ID,'video_principal',true));
			else
				$amostra = "/videos/".get_post_meta($video->ID,'video_principal',true);
	}

	if($video->ID=='336'){
		$legenda = "	<track label='English' 	 		kind='captions' srclang='en' src='".$upload_dir['url']."/videos/legendas/compreender-kardec-volume-1-en.vtt'></track>
						<track label='Frances' 	 		kind='captions' srclang='fr' src='".$upload_dir['url']."/videos/legendas/compreender-kardec-volume-1-fr.vtt'></track>
						<track label='Espa&ntilde;ol' 	kind='captions' srclang='es' src='".$upload_dir['url']."/videos/legendas/compreender-kardec-volume-1-es.vtt'></track>
						<track label='Italiano'  		kind='captions' srclang='it' src='".$upload_dir['url']."/videos/legendas/compreender-kardec-volume-1-it.vtt'></track>
						<track label='Portugu&ecirc;s' 	kind='captions' srclang='pt' src='".$upload_dir['url']."/videos/legendas/compreender-kardec-volume-1-pt.vtt'></track>
						<track label='German' 			kind='captions' srclang='ge' src='".$upload_dir['url']."/videos/legendas/compreender-kardec-volume-1-ge.vtt'></track>";
	}

	echo "	<table width='98%' cellspacing='0' cellpadding='0' align='center' Style='margin:0 auto;'>
				<tr>
					<td align='center' valign='top' bgcolor='#3c0606' width='100%'>
						<div Style='padding:10px 0 10px 0;'>
							<img src='http://clubekardec.com.br/wp-content/images/layout/video-list-close.png' class='video-list-close' Style='width:30px;height:30px;float:right;margin-top:-18px;margin-right:-10px;cursor:pointer;'>
							<table width='100%' cellspacing='0' cellpadding='0' id='video-description-container'>
								<tr>
									<td colspan='2' align='center' valign='middle' bgcolor='#3c0606'>
										<table width='98%' cellspacing='0' cellpadding='0' align='center' Style='margin-left:1%'>
											<tr>
												<td style='font-family: Arial, Helvetica, Tahoma, Verdana, sans-serif; font-weight: bold; font-size: 24px; color: #a44a45; text-decoration: none;' width='45%' height='56'>
													$video->Post_Title
												</td>
												<td style='font-family: Arial, Helvetica, Tahoma, Verdana, sans-serif; font-weight: normal; font-size: 14px; color: #ffffff; text-decoration: none; text-align: center;' rowspan='3' align='center' width='20%'>
													<div Style='padding:10px;'>
														$ficha
													</div>
												</td>
												<td rowspan='4' align='right' valign='middle' width='30%'>
													<video id='video_principal' class='video-js vjs-default-skin' controls preload='auto' autoplay width='98%' height='300' poster='$capa' data-setup='{}' Style='margin:0 auto;'>
														<source src='/wp-content/uploads/$amostra' type='video/mp4'/>
														$legenda
													</video>
												</td>
											</tr>
											<tr>
												<td style='font-family: Arial, Helvetica, Tahoma, Verdana, sans-serif; font-weight: bold; font-size: 19px; line-height: 24px; color: #ffffff; text-decoration: none;' valign='middle' height='77'>
													$termName<br>
												</td>
											</tr>
											<tr>
												<td style='font-family: Arial, Helvetica, Tahoma, Verdana, sans-serif; font-weight: normal; font-size: 14px; color: #ffffff; text-decoration: none; line-height: 18px;' valign='top' height='61'>
													$video->Post_Excerpt
												</td>
											</tr>
											<tr>
												<td colspan='2' valign='bottom'>
													<table width='100%' cellspacing='0' cellpadding='0'>
														<tr>
															<td valign='bottom' width='50%'>
																<a href='/cadastro/' $mostraInscreva>
																	<img src='/wp-content/uploads/bt-home-inscrevase.png' width='50%' alt=''>
																</a>&nbsp;
															</td>
															<td align='right' valign='bottom'>
																<a href='/video/$video->Post_name/'>
																	<img src='wp-content/uploads/bt-home-assistir-agora.png' width='50%' alt='' Style='margin-right:1%;'>
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>





	<!--
			<div id='faixa-carrossel-ficha' Style='width:25%;float:left;margin-left:1%;'>
				<div Style='width:85%;margin-left:15%;'>
					$ficha
				</div>
			</div>
			<div Style='width:60%;float:left;height:55px;'></div>

			<div Style='width:60%;float:left;position:absolute;bottom:10px;text-align:center;'>
				<table width='450' cellspacing='0' cellpadding='0' Style='margin:0 auto;'>
					<tbody>
						<tr>
							<td width='33%' height='24' align='center' valign='top'>
								<span>
								<input name='bt_enviar' type='button' id='bt_inscreva' value='Increva-se agora' class='botaoinscreva' Style='width:90%'>
								</span>
							</td>
							<td width='33%' align='center' valign='top'>
								<span>
								<input name='bt_enviar' type='button' id='bt_alugue' value='Alugar R$ 4,90' class='botaoalugue' Style='width:90%'>
							  </span>
						  </td>
							<td width='33%' align='right' valign='top'>
								<span>
								<input name='bt_enviar' type='button' id='bt_compre' value='Comprar R$ 24,90' class='botaocompre' Style='width:90%'>
								</span>
							</td>
					  </tr>
					</tbody>
				</table>
			</div>
-->
			";
?>