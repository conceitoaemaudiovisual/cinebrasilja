<?php
/*
Controller name: V&iacute;deos
Controller description: M&eacute;todos de manipula&ccedil;&atilde;o de dados para v&iacute;deos
*/

global $keySamba, $wpdb, $dadosMidia,$categoriaEpisodios;
$keySamba = "e416100f0c85f292fbc2ad198c88ae11";
$sambaMedias = $wpdb->get_results("select * from aux_samba_videos");
foreach($sambaMedias as $indiceMedia=>$sambaMedia){
	$dadosMidia[$sambaMedia->IDMedia]					= unserialize($sambaMedia->Midias);
	$dadosMidia[$sambaMedia->IDMedia][duracao] 			= $sambaMedia->Duracao;
	$dadosMidia[$sambaMedia->IDMedia][captions]			= unserialize($sambaMedia->Legendas);
}
$categorias = $wpdb->get_results("select * from wp_terms");
foreach($categorias as $categoria)
	$categoriaEpisodios[$categoria->term_id] = $categoria->name;

class JSON_API_Posts_Controller {

  public function create_post() {
    global $json_api;
    if (!current_user_can('edit_posts')) {
      $json_api->error("You need to login with a user that has 'edit_posts' capacity.", 403);
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to create posts. Use the `get_nonce` Core API method.", 403);
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'create_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.", 403);
    }
    nocache_headers();
    $post = new JSON_API_Post();
    $id = $post->create($_REQUEST);
    if (empty($id)) {
      $json_api->error("Could not create post.", 500);
    }
    return array(
      'post' => $post
    );
  }

  public function update_post() {
    global $json_api;
    $post = $json_api->introspector->get_current_post();
    if (empty($post)) {
      $json_api->error("Post not found.");
    }
    if (!current_user_can('edit_post', $post->ID)) {
      $json_api->error("You need to login with a user that has the 'edit_post' capacity for that post.", 403);
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to update posts. Use the `get_nonce` Core API method.", 403);
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'update_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.", 403);
    }
    nocache_headers();
    $post = new JSON_API_Post($post);
    $post->update($_REQUEST);
    return array(
      'post' => $post
    );
  }

  public function delete_post() {
    global $json_api;
    $post = $json_api->introspector->get_current_post();
    if (empty($post)) {
      $json_api->error("Post not found.");
    }
    if (!current_user_can('edit_post', $post->ID)) {
      $json_api->error("You need to login with a user that has the 'edit_post' capacity for that post.", 403);
    }
    if (!current_user_can('delete_posts')) {
      $json_api->error("You need to login with a user that has the 'delete_posts' capacity.", 403);
    }
    if ($post->post_author != get_current_user_id() && !current_user_can('delete_other_posts')) {
      $json_api->error("You need to login with a user that has the 'delete_other_posts' capacity.", 403);
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to update posts. Use the `get_nonce` Core API method.", 403);
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'delete_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.", 403);
    }
    nocache_headers();
    wp_delete_post($post->ID);
    return array();
  }


	  public function get_videos() {
		global $json_api, $wpdb, $post,$keySamba, $dadosMidia,$categoriaEpisodios;
		$posMidia = array_search($keySamba,$dadosMidia);

		$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
		foreach($videosSerie as $videoSerie){
			$postsSeries = unserialize($videoSerie->meta_value);
			foreach($postsSeries as $postSeries){
				if($postSeries['video'] != '') {
					$episodiosSeries[] = $postSeries['video'];
				}
			}
		}

		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}
		$userID  	= $json_api->query->userID;
		$userList 	= get_user_meta($userID, 'my-list');
		$userList 	= array_reverse($userList[0]);
		if(isset($userList)){
			$args = array(
				'post_type' => array( 'post', 'series', 'palestrante' ),
				'post__in'  => $userList,
				'orderby'   => 'post__in',
				'post__not_in' =>$episodiosSeries,
			);
			$myIndex = 0;
			$the_query = new WP_Query($args);
			$dados['titulo'] = 'Minha Lista';
			if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
					global $post;
					$dados['dados'][$myIndex] 		 		  = (array)  $post;
					$imgPadraoID 					 		  = get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
					$imgMetaPadraoID 				 		  = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
					$imgMobileID 					 		  = get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
					$dados['dados'][$myIndex]['banner']	  	  = wp_get_attachment_image_src( $imgPadraoID, 'large' );
					$dados['dados'][$myIndex]['image']		  = wp_get_attachment_image_src( $imgMobileID, 'large' );
					$dados['dados'][$myIndex]['thumb'] 	  	  = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados['dados'][$myIndex]['post_excerpt'] = nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)))."<br><br>".nl2br(strip_tags(get_post_meta($post->ID,'ficha-video',true)));
					$dados['dados'][$myIndex]['key']		  = $keySamba;
					$dados['dados'][$myIndex]['media'][captions]		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][captions][0];
					$dados['dados'][$myIndex]['media']['pt'][mediakey]  = get_post_meta($post->ID,'video-anexado',true);
					$dados['dados'][$myIndex]['media']['pt'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
					$dados['dados'][$myIndex]['media']['en'][mediakey]  = get_post_meta($post->ID,'video-anexado-secundario',true);
					$dados['dados'][$myIndex]['media']['en'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
					$dados['dados'][$myIndex]['media']['en'][legenda]	= get_post_meta($post->ID,'select-legenda',true);
					$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
					if($tituloEpisodios == '')
						$tituloEpisodios = utf8_encode("Epis�dios");
					$dados['dados'][$myIndex]['titulo-episodio'] 		= $tituloEpisodios;
					$dados['dados'][$myIndex]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
					if($dados['dados'][$myIndex]['media']['duracao'] == '')
						$dados['dados'][$myIndex]['media']['duracao'] 	= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
					if($post->post_type=="palestrante"){
						$imgPalestranteID 				 		  		= get_post_meta($post->ID,$post->post_type.'_thumb-palestrante_thumbnail_id',true);
						$dados['dados'][$myIndex]['image']		  		= wp_get_attachment_image_src( $imgPalestranteID, 'large' );
						$dados['dados'][$myIndex]['thumb']		  		= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
						$dados['dados'][$myIndex]['post_excerpt'] 		=  nl2br(strip_tags($post->post_content));
						$dados['dados'][$myIndex][videos] 	   			= get_palestras($post->ID);
						$dados['dados'][$myIndex][series] 	   			= get_series_palestrante($post->ID);
						$dados['dados'][$myIndex][episodios] 			= '';
					}else{
						$dados['dados'][$myIndex][videos] 	   			= '';
						$dados['dados'][$myIndex][series] 	   			= '';
						$dados['dados'][$myIndex][episodios] 			= get_episodios(get_post_meta($post->ID,'video-serie',true));
					}
				$myIndex++;
				endwhile;
			endif;
			$retorno[listas][] = $dados;
			unset($dados);
			$dados['titulo'] = utf8_encode('Sugest�o para voc�');
			foreach($userList as $categorias){
				if(isset($categorias)){
					$catList .= $virgulaCat.$categorias;
					$virgulaCat = ",";
				}
			}
			$categoryLists = $wpdb->get_results("select term_id
												from wp_term_relationships tr
												inner join wp_term_taxonomy tx on tx.term_taxonomy_id = tr.term_taxonomy_id
												where object_id in ($catList)");
			foreach($categoryLists as $categoryList){
				$categoryID[] = $categoryList->term_id;
			}
			$args = array(
				'post_type' => array( 'post', 'series','palestrante' ),
				'category__in' => $categoryID,
				'post__not_in' => $userList
			);
			$the_query = new WP_Query($args);
			if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
					global $post;
					$dados['dados'][$post->ID] 				  = (array)  $post;
					$imgPadraoID 							  = get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
					$imgMetaPadraoID 				 		  = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
					$imgMobileID 					 		  = get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
					$dados['dados'][$post->ID]['banner']	  = wp_get_attachment_image_src( $imgPadraoID, 'large' );
					$dados['dados'][$post->ID]['image']		  = wp_get_attachment_image_src( $imgMobileID, 'large' );
					$dados['dados'][$post->ID]['thumb'] 	  = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados['dados'][$post->ID]['post_excerpt']= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)))."<br><br>".nl2br(strip_tags(get_post_meta($post->ID,'ficha-video',true)));
					$dados['dados'][$post->ID]['key']		  = $keySamba;
					$dados['dados'][$post->ID]['media'][captions]		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][captions][0];
					$dados['dados'][$post->ID]['media']['pt'][mediakey] = get_post_meta($post->ID,'video-anexado',true);
					$dados['dados'][$post->ID]['media']['pt'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
					$dados['dados'][$post->ID]['media']['en'][mediakey] = get_post_meta($post->ID,'video-anexado-secundario',true);
					$dados['dados'][$post->ID]['media']['en'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
					$dados['dados'][$post->ID]['media']['en'][legenda]	= get_post_meta($post->ID,'select-legenda',true);
					$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
					if($tituloEpisodios == '')
						$tituloEpisodios = utf8_encode("Epis�dios");
					$dados['dados'][$post->ID]['titulo-episodio'] = $tituloEpisodios;
					$dados['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
					if($dados['dados'][$post->ID]['media']['duracao'] == '')
						$dados['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
					if($post->post_type=="palestrante"){
						$imgPalestranteID 				 		  		= get_post_meta($post->ID,$post->post_type.'_thumb-palestrante_thumbnail_id',true);
						$dados['dados'][$post->ID]['image']		  		= wp_get_attachment_image_src( $imgPalestranteID, 'large' );
						$dados['dados'][$post->ID]['thumb']		  		= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
						$dados['dados'][$post->ID]['post_excerpt'] 		=  nl2br(strip_tags($post->post_content));
						$dados['dados'][$post->ID][videos] 	   			= get_palestras($post->ID);
						$dados['dados'][$post->ID][series] 	   			= get_series_palestrante($post->ID);
						$dados['dados'][$post->ID][episodios] 			= '';
					}else{
						$dados['dados'][$post->ID][videos] 	   			= '';
						$dados['dados'][$post->ID][series] 	   			= '';
						$dados['dados'][$post->ID][episodios] 			= get_episodios(get_post_meta($post->ID,'video-serie',true));
					}
				endwhile;
			endif;
			$retorno[listas][] = $dados;
		}
		$bloco[0]['titulo'] = '';
		$bloco[0]['imagem'] = '';
		$bloco[0]['dados'] = $retorno;
		unset($dados);
		unset($retorno);

		$incicePos[1]['img'] 	=  get_bloginfo('wpurl')."/wp-content/uploads/icone-filmes.png";
		$incicePos[1]['titulo'] =  utf8_encode("Filmes, S�ries e Document�rios");

		$incicePos[2]['img'] 	=  get_bloginfo('wpurl')."/wp-content/uploads/icone-cursos.png";
		$incicePos[2]['titulo'] =  utf8_encode("Cursos, Palestras e Serm�es");

		$incicePos[3]['img'] 	=  get_bloginfo('wpurl')."/wp-content/uploads/icone-musica.png";
		$incicePos[3]['titulo'] =  utf8_encode("M�sica");

		for($posicaoH=1;$posicaoH<=3;$posicaoH++){
			$categories = $wpdb->get_results("select t.term_id, t.name, t.slug, t.term_group, tx.term_taxonomy_id, tx.taxonomy, tx.description, tx.parent, tx.count,
											 'raw' filter, t.term_id cat_ID, tx.count category_count, tx.description category_description, t.name cat_name,
											 t.name category_nicename, tx.parent category_parent, m.meta_key meta_key, m2.meta_value, m.meta_value
											 from wp_terms t
											 inner join wp_termmeta m on m.term_id = t.term_id and meta_key = 'posicao_home'
											 inner join wp_termmeta m2 on m2.term_id = m.term_id and m2.meta_key = 'posicao_home_modulo' and m2.meta_value = $posicaoH
											 inner join wp_term_taxonomy tx on tx.term_id = t.term_id
											 where m.meta_value <> '' and m.meta_value is not null and m.meta_value > 0
											 order by m2.meta_value,m.meta_value");
			foreach ($categories as $indice=>$category){
				$dados['titulo'] = $category->name;
				$args = array(
						'posts_per_page' => (int)get_theme_mod( 'streamium_global_options_homepage_desktop' ),
						'cat' => $category->cat_ID,
						'post_type' => array( 'post', 'series', 'palestrante' ),
						'post__not_in' =>$episodiosSeries,
				);
				$the_query = new WP_Query($args);
				if ( $the_query->have_posts() ) :
					while ( $the_query->have_posts() ) : $the_query->the_post();
						global $post;
						$dados['dados'][$post->ID] 				 = (array)  $post;
						$imgPadraoID 							 = get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
						$imgMetaPadraoID 				 		 = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
						$imgMobileID 					 		 = get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
						$dados['dados'][$post->ID]['banner']	 = wp_get_attachment_image_src( $imgPadraoID, 'large' );
						$dados['dados'][$post->ID]['image']		 = wp_get_attachment_image_src( $imgMobileID, 'large' );
						$dados['dados'][$post->ID]['thumb'] 	 = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
						$dados['dados'][$post->ID]['post_excerpt']= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)))."<br><br>".nl2br(strip_tags(get_post_meta($post->ID,'ficha-video',true)));
						$dados['dados'][$post->ID]['key']		 = $keySamba;
						$dados['dados'][$post->ID]['media'][captions]		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][captions][0];
						$dados['dados'][$post->ID]['media']['pt'][mediakey] = get_post_meta($post->ID,'video-anexado',true);
						$dados['dados'][$post->ID]['media']['pt'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
						$dados['dados'][$post->ID]['media']['en'][mediakey] = get_post_meta($post->ID,'video-anexado-secundario',true);
						$dados['dados'][$post->ID]['media']['en'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
						$dados['dados'][$post->ID]['media']['en'][legenda]	= get_post_meta($post->ID,'select-legenda',true);
						$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
						if($tituloEpisodios == '')
							$tituloEpisodios = utf8_encode("Epis�dios");
						$dados['dados'][$post->ID]['titulo-episodio'] = $tituloEpisodios;
						$dados['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
						if($dados['dados'][$post->ID]['media']['duracao'] == '')
							$dados['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
						if($post->post_type=="palestrante"){
							$dados['dados'][$post->ID]['thumb']		  		= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
							$dados['dados'][$post->ID]['post_excerpt'] 		=  nl2br(strip_tags($post->post_content));
							$dados['dados'][$post->ID][videos] 	   			= get_palestras($post->ID);
							$dados['dados'][$post->ID][series] 	   			= get_series_palestrante($post->ID);
							$dados['dados'][$post->ID][episodios] 			= '';
						}else{
							$dados['dados'][$post->ID][videos] 	   			= '';
							$dados['dados'][$post->ID][series] 	   			= '';
							$dados['dados'][$post->ID][episodios] 			= get_episodios(get_post_meta($post->ID,'video-serie',true));
						}
					endwhile;
				endif;

				$retorno[listas][] = $dados;
				unset($dados);
			}
			$bloco[$posicaoH]['titulo'] = $incicePos[$posicaoH]['titulo'];
			$bloco[$posicaoH]['imagem'] = $incicePos[$posicaoH]['img'];
			$bloco[$posicaoH]['dados'] = $retorno;
			unset($retorno);
		}

		$dados['bloco'] = $bloco;
		return $bloco;
	}


	  public function get_banners() {
		global $json_api, $wpdb, $post,$keySamba,$dadosMidia;

		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}

		$userID  	= $json_api->query->userID;
		$userList 	= get_user_meta($userID, 'my-list');
		$userList 	= $userList[0];

		$args = array(
			'post_status' 		  => 'publish',
			'posts_per_page'      => 4,
			'post__in'            => get_option( 'sticky_posts' ),
			'ignore_sticky_posts' => 1,
			'post_type' 		  => array( 'post', 'series' ),
			'meta_key' 			  => 'video_destaque',
			'meta_value' 		  => 'checked',
			'orderby' 			  => 'rand'
		);
		$loop = new WP_Query( $args );
		if($loop->have_posts()):
			while ( $loop->have_posts() ) : $loop->the_post();
				global $post;
				$dados[$post->ID] 				 = (array)  $post;
				$imgPadraoID 					 = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
				$imgMetaPadraoID 				 = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
				$imgMobileID 					 = get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
				$dados[$post->ID]['chamada'] 	 = get_post_meta($post->ID,'video_destaque_texto',true);
				$dados[$post->ID]['post_excerpt']= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)))."<br><br>".nl2br(strip_tags(get_post_meta($post->ID,'ficha-video',true)));
				if($dados[$post->ID]['chamada'] == '') $dados[$post->ID]['chamada'] = utf8_encode("Assista J�");
				$dados[$post->ID]['banner']		 = wp_get_attachment_image_src( $imgPadraoID, 'large' );
				$dados[$post->ID]['image']		 = wp_get_attachment_image_src( $imgMobileID, 'large' );
				$dados[$post->ID]['thumb'] 		 = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
				$dados[$post->ID]['key']		 = $keySamba;
				$dados[$post->ID]['media']['pt'][mediakey] = get_post_meta($post->ID,'video-anexado',true);
				$dados[$post->ID]['media']['pt'][url] 	   = $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
				$dados[$post->ID]['media']['en'][mediakey] = get_post_meta($post->ID,'video-anexado-secundario',true);
				$dados[$post->ID]['media']['en'][url] 	   = $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
				$dados[$post->ID]['media']['en'][legenda]	= get_post_meta($post->ID,'select-legenda',true);
				$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
				if($tituloEpisodios == '')
					$tituloEpisodios = utf8_encode("Epis�dios");
				$dados[$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
				if($dados[$post->ID]['media']['duracao'] == '')
					$dados[$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
				$dados[$post->ID]['titulo-episodio'] = $tituloEpisodios;
				if($post->post_type=="palestrante"){
					$dados[$post->ID]['thumb']		  		= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados[$post->ID]['post_excerpt'] 		=  nl2br(strip_tags($post->post_content));
					$dados[$post->ID][videos] 	   			= get_palestras($post->ID);
					$dados[$post->ID][series] 	   			= get_series_palestrante($post->ID);
					$dados[$post->ID][episodios] 			= '';
				}else{
					$dados[$post->ID][videos] 	   			= '';
					$dados[$post->ID][series] 	   			= '';
					$dados[$post->ID][episodios] 			= get_episodios(get_post_meta($post->ID,'video-serie',true));
				}

				if($imgPadraoID=='')
					unset($dados[$post->ID]);
				else
					break;
			endwhile;
		endif;
		wp_reset_query();

		$retorno[dados]  = $dados;
		return $retorno;
	}


	public function search_videos(){

		global $json_api, $wpdb, $post,$keySamba,$dadosMidia;

		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}
		if (!$json_api->query->search) {
		  $json_api->error("You must include a 'search' value.");
		}

		$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
		foreach($videosSerie as $videoSerie){
			$postsSeries = unserialize($videoSerie->meta_value);
			foreach($postsSeries as $postSeries){
				if($postSeries['video'] != '') {
					$episodios .= $virgula.$postSeries['video'];
					$virgula = ",";
				}
			}
		}

		$videosLocalizados = $wpdb->get_results("SELECT distinct ID
												 FROM wp_posts
												 inner join wp_term_relationships tr on tr.object_id = wp_posts.ID
												 inner join wp_term_taxonomy tx on tx.term_taxonomy_id = tr.term_taxonomy_id
												 inner join wp_terms t on t.term_id = tx.term_id
												 WHERE (((wp_posts.post_title LIKE '%".$_GET['search']."%') OR (wp_posts.post_excerpt LIKE '%".$_GET['search']."%') OR (wp_posts.post_content LIKE '%".$_GET['search']."%') OR (t.name LIKE '%".$_GET['search']."%')))
												 AND wp_posts.post_type IN ('post', 'series', 'palestrante')
												 AND wp_posts.post_status = 'publish'
												 AND wp_posts.ID not in ($episodios)
												 ORDER BY wp_posts.post_title DESC");
		foreach($videosLocalizados as $videosLocalizado){
			$vid++;
			$videos[] = $videosLocalizado->ID;
		}
		if($vid==''){
		  	$json_api->error("Nenhum resultado para a busca.");
		}
		$args = array(
			'post__in' 		=> $videos,
			'post_type' 	=> array( 'post', 'series','palestrante' ),
			'post_status'	=> 'publish',
		);

		$loop = new WP_Query( $args );

		if($loop->have_posts()):
			while ( $loop->have_posts() ) : $loop->the_post();
				$dados['dados'][$post->ID] 				 = (array)  $post;
				$imgPadraoID 							 = get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
				$imgMetaPadraoID 				 		 = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
				$imgMobileID 					 		 = get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
				$dados['dados'][$post->ID]['banner']	 = wp_get_attachment_image_src( $imgPadraoID, 'large' );
				$dados['dados'][$post->ID]['image']		 = wp_get_attachment_image_src( $imgMobileID, 'large' );
				$dados['dados'][$post->ID]['thumb'] 	 = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
				$dados['dados'][$post->ID]['post_excerpt']= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)))."<br><br>".nl2br(strip_tags(get_post_meta($post->ID,'ficha-video',true)));
				$dados['dados'][$post->ID]['key']		 = $keySamba;
				$dados['dados'][$post->ID]['media']['pt'][mediakey] = get_post_meta($post->ID,'video-anexado',true);
				$dados['dados'][$post->ID]['media']['pt'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
				$dados['dados'][$post->ID]['media']['en'][mediakey] = get_post_meta($post->ID,'video-anexado-secundario',true);
				$dados['dados'][$post->ID]['media']['en'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
				$dados['dados'][$post->ID]['media']['en'][legenda]	= get_post_meta($post->ID,'select-legenda',true);
				$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
				if($tituloEpisodios == '')
					$tituloEpisodios = utf8_encode("Epis�dios");
				$dados['dados'][$post->ID]['titulo-episodio'] = $tituloEpisodios;
				$dados['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
				if($dados['dados'][$post->ID]['media']['duracao'] == '')
					$dados['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
				if($post->post_type=="palestrante"){
					$dados['dados'][$post->ID]['thumb']		  		= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados['dados'][$post->ID]['post_excerpt'] 		=  nl2br(strip_tags($post->post_content));
					$dados['dados'][$post->ID][videos] 	   			= get_palestras($post->ID);
					$dados['dados'][$post->ID][series] 	   			= get_series_palestrante($post->ID);
					$dados['dados'][$post->ID][episodios] 			= '';
				}else{
					$dados['dados'][$post->ID][videos] 	   			= '';
					$dados['dados'][$post->ID][series] 	   			= '';
					$dados['dados'][$post->ID][episodios] 			= get_episodios(get_post_meta($post->ID,'video-serie',true));
				}
			endwhile;
		endif;
		wp_reset_query();
		$retorno  = $dados;

		return $retorno;

	}

	public function get_videos_category(){
		global $json_api, $wpdb, $post,$keySamba, $categoriaEpisodios, $dadosMidia;

		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}
		if (!$json_api->query->categoryID) {
		  $json_api->error("You must include a 'categoryID' value.");
		}
		$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
		foreach($videosSerie as $videoSerie){
			$postsSeries = unserialize($videoSerie->meta_value);
			foreach($postsSeries as $postSeries){
				if($postSeries['video'] != '') {
					$episodiosSeries[] = $postSeries['video'];
				}
			}
		}

		$dados = array();
		$itensLocalizados = 0;
		$categorias = $wpdb->get_results("	SELECT distinct COALESCE(t2.term_id,COALESCE(t1.term_id, COALESCE(t.term_id))) term_id,
											concat(t.name, COALESCE(concat(' ',lower(t1.name)),''), COALESCE(concat(' ',lower(t2.name)),'')) name
											FROM wp_terms t
											INNER JOIN wp_term_taxonomy tx ON tx.term_id = t.term_id
											INNER JOIN wp_term_relationships tr ON tr.term_taxonomy_id = tx.term_taxonomy_id
											left JOIN wp_term_taxonomy tx1 ON tx1.parent = tx.term_id
											left JOIN wp_term_relationships tr1 ON tr1.term_taxonomy_id = tx1.term_taxonomy_id
											left join wp_terms t1 on t1.term_id = tx1.term_id
											left JOIN wp_term_taxonomy tx2 ON tx2.parent = tx1.term_id
											left JOIN wp_term_relationships tr2 ON tr2.term_taxonomy_id = tx2.term_taxonomy_id
											left join wp_terms t2 on t2.term_id = tx2.term_id
											WHERE tx.term_id = ".$json_api->query->categoryID."
											AND tx.taxonomy = 'category'
											ORDER BY t.name");
		$itensLocalizados = $wpdb->num_rows;
		foreach($categorias as $indice=>$categoria){
			$dados[$categoria->term_id][term_id] 	= $categoria->term_id;
			$dados[$categoria->term_id][name] 		= $categoria->name;
			$dados[$categoria->term_id][slug] 		= $categoria->slug;

			$args = array(
				'post_status' 		  => 'publish',
				'post_type' 		  => array( 'post', 'series', 'palestrante' ),
				'posts_per_page'      => -1,
				'cat' => $categoria->term_id,
				'post__not_in' =>$episodiosSeries,
			);
			$loop = new WP_Query( $args );
			if($loop->have_posts()):
				while ( $loop->have_posts() ) : $loop->the_post();
					global $post;
					$dados[$categoria->term_id]['dados'][$post->ID] 				 = (array)  $post;
					$imgPadraoID 							 = get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
					$imgMetaPadraoID 				 		 = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
					$imgMobileID 					 		 = get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
					$dados[$categoria->term_id]['dados'][$post->ID]['banner']	 = wp_get_attachment_image_src( $imgPadraoID, 'large' );
					$dados[$categoria->term_id]['dados'][$post->ID]['image']		 = wp_get_attachment_image_src( $imgMobileID, 'large' );
					$dados[$categoria->term_id]['dados'][$post->ID]['thumb'] 	 = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados[$categoria->term_id]['dados'][$post->ID]['post_excerpt']= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)))."<br><br>".nl2br(strip_tags(get_post_meta($post->ID,'ficha-video',true)));
					$dados[$categoria->term_id]['dados'][$post->ID]['key']		 = $keySamba;
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['pt'][mediakey] = get_post_meta($post->ID,'video-anexado',true);
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['pt'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['en'][mediakey] = get_post_meta($post->ID,'video-anexado-secundario',true);
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['en'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['en'][legenda]	= get_post_meta($post->ID,'select-legenda',true);
					$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
					if($tituloEpisodios == '')
						$tituloEpisodios = utf8_encode("Epis�dios");
					$dados[$categoria->term_id]['dados'][$post->ID]['titulo-episodio'] = $tituloEpisodios;
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
					if($dados[$categoria->term_id]['dados'][$post->ID]['media']['duracao'] == '')
						$dados[$categoria->term_id]['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
					if($post->post_type=="palestrante"){
						$dados[$categoria->term_id]['dados'][$post->ID]['thumb']		  		= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
						$dados[$categoria->term_id]['dados'][$post->ID]['post_excerpt'] 		=  nl2br(strip_tags($post->post_content));
						$dados[$categoria->term_id]['dados'][$post->ID][videos] 	   			= get_palestras($post->ID);
						$dados[$categoria->term_id]['dados'][$post->ID][series] 	   			= get_series_palestrante($post->ID);
						$dados[$categoria->term_id]['dados'][$post->ID][episodios] 			= '';
					}else{
						$dados[$categoria->term_id]['dados'][$post->ID][videos] 	   			= '';
						$dados[$categoria->term_id]['dados'][$post->ID][series] 	   			= '';
						$dados[$categoria->term_id]['dados'][$post->ID][episodios] 			= get_episodios(get_post_meta($post->ID,'video-serie',true));
					}
				endwhile;
			endif;
			wp_reset_query();
			if(count($dados[$categoria->term_id]['dados']) == 0){
				unset($dados[$categoria->term_id]);
				$itensLocalizados--;
			}
		}
		$retorno['listas'] = $dados;
		$retorno[total] = $itensLocalizados;

		return $retorno;
	}

	public function my_list(){
		global $json_api, $wpdb, $post,$keySamba;
		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}
		if (!$json_api->query->postID) {
		  $json_api->error("You must include a 'postID' value.");
		}
		if ($json_api->query->acao === "") {
		  $json_api->error("You must include a 'acao' value.");
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.teomidia.org/wp-content/plugins/integracao-samba-videos/functions/atualiza-minha-lista.php?type=".$json_api->query->acao."&pID=".$json_api->query->postID."&userID=".$json_api->query->userID);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$retorno = curl_exec($ch);
		curl_close($ch);
		$dados['dados'] 	= json_decode($retorno);
		$dados['dados']->ip	= $_SERVER['REMOTE_ADDR'];
		return $dados;
	}


	public function get_videos_kids() {
		global $json_api, $wpdb, $post,$keySamba, $dadosMidia,$categoriaEpisodios;

		$posMidia = array_search('114054969a5a13c23c423dce27280610',$dadosMidia);

		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}
		$userID  	= $json_api->query->userID;


		$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
		foreach($videosSerie as $videoSerie){
			$postsSeries = unserialize($videoSerie->meta_value);
			foreach($postsSeries as $postSeries){
				if($postSeries['video'] != '') {
					$episodiosSeries[] = $postSeries['video'];
				}
			}
		}

		unset($dados);
		$categories = $wpdb->get_results("select t.term_id, t.name, t.slug, t.term_group, tx.term_taxonomy_id, tx.taxonomy, tx.description, tx.parent, tx.count,
										 'raw' filter, t.term_id cat_ID, tx.count category_count, tx.description category_description, t.name cat_name,
										 t.name category_nicename, tx.parent category_parent
										 from wp_terms t
										 inner join wp_term_taxonomy tx on tx.term_id = t.term_id and tx.taxonomy = 'kids'
										 order by t.name");
		foreach ($categories as $indice=>$category){
			unset($dados);
			$dados['titulo'] = $category->name;
			$args = array(
					'posts_per_page' => (int)get_theme_mod( 'streamium_global_options_homepage_desktop' ),
					'tax_query' => array(
						array(
							'taxonomy' => 'kids',
							'field'    => 'id',
							'terms'    => $category->term_taxonomy_id,
						),
					),
					'post_type' => array( 'post', 'series' ),
					'post__not_in' =>$episodiosSeries,
			);
			$the_query = new WP_Query($args);
			if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
					global $post;
					$dados['dados'][$post->ID] 				 = (array)  $post;
					$imgPadraoID 							 = get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
					$imgMetaPadraoID 				 		 = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
					$imgMobileID 					 		 = get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
					$dados['dados'][$post->ID]['banner']	 = wp_get_attachment_image_src( $imgPadraoID, 'large' );
					$dados['dados'][$post->ID]['image']		 = wp_get_attachment_image_src( $imgMobileID, 'large' );
					$dados['dados'][$post->ID]['thumb'] 	 = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados['dados'][$post->ID]['post_excerpt']= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)))."<br><br>".nl2br(strip_tags(get_post_meta($post->ID,'ficha-video',true)));
					$dados['dados'][$post->ID]['key']		 = $keySamba;
					$dados['dados'][$post->ID]['media']['pt'][mediakey] = get_post_meta($post->ID,'video-anexado',true);
					$dados['dados'][$post->ID]['media']['pt'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
					$dados['dados'][$post->ID]['media']['en'][mediakey] = get_post_meta($post->ID,'video-anexado-secundario',true);
					$dados['dados'][$post->ID]['media']['en'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
					$dados['dados'][$post->ID]['media']['en'][legenda]	= get_post_meta($post->ID,'select-legenda',true);
					$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
					if($tituloEpisodios == '')
						$tituloEpisodios = utf8_encode("Epis�dios");
					$dados['dados'][$post->ID]['titulo-episodio'] = $tituloEpisodios;
					$dados['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
					if($dados['dados'][$post->ID]['media']['duracao'] == '')
						$dados['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
					if($post->post_type=="palestrante"){
						$dados['dados'][$post->ID]['thumb']		  		= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
						$dados['dados'][$post->ID]['post_excerpt'] 		=  nl2br(strip_tags($post->post_content));
						$dados['dados'][$post->ID][videos] 	   			= get_palestras($post->ID);
						$dados['dados'][$post->ID][series] 	   			= get_series_palestrante($post->ID);
						$dados['dados'][$post->ID][episodios] 			= '';
					}else{
						$dados['dados'][$post->ID][videos] 	   			= '';
						$dados['dados'][$post->ID][series] 	   			= '';
						$dados['dados'][$post->ID][episodios] 			= get_episodios(get_post_meta($post->ID,'video-serie',true));
					}
				endwhile;
			endif;
			$retorno[listas][] = $dados;
			unset($dados);
		}
		$bloco[0]['titulo'] = ' ';
		//$bloco[0]['imagem'] = get_bloginfo('wpurl')."/wp-content/uploads/icone-kids.png";
		$bloco[0]['imagem'] = '';
		$bloco[0]['dados'] = $retorno;
		$dados['bloco'] = $bloco;
		return $bloco;
	}

	public function search_videos_kids() {
		return search_videos();
	}


	public function get_videos_category_kids(){
		global $json_api, $wpdb, $post,$keySamba, $categoriaEpisodios, $dadosMidia;

		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}
		if (!$json_api->query->categoryID) {
		  $json_api->error("You must include a 'categoryID' value.");
		}
		$dados = array();
		$itensLocalizados = 0;
		$categorias = $wpdb->get_results("	SELECT distinct COALESCE(t2.term_id,COALESCE(t1.term_id, COALESCE(t.term_id))) term_id,
											concat(t.name, COALESCE(concat(' ',lower(t1.name)),''), COALESCE(concat(' ',lower(t2.name)),'')) name
											FROM wp_terms t
											INNER JOIN wp_term_taxonomy tx ON tx.term_id = t.term_id
											INNER JOIN wp_term_relationships tr ON tr.term_taxonomy_id = tx.term_taxonomy_id
											left JOIN wp_term_taxonomy tx1 ON tx1.parent = tx.term_id
											left JOIN wp_term_relationships tr1 ON tr1.term_taxonomy_id = tx1.term_taxonomy_id
											left join wp_terms t1 on t1.term_id = tx1.term_id
											left JOIN wp_term_taxonomy tx2 ON tx2.parent = tx1.term_id
											left JOIN wp_term_relationships tr2 ON tr2.term_taxonomy_id = tx2.term_taxonomy_id
											left join wp_terms t2 on t2.term_id = tx2.term_id
											WHERE tx.term_id = ".$json_api->query->categoryID."
											AND tx.taxonomy = 'kids'
											ORDER BY t.name");
		$itensLocalizados = $wpdb->num_rows;
		foreach($categorias as $indice=>$categoria){
			$dados[$categoria->term_id][term_id] 	= $categoria->term_id;
			$dados[$categoria->term_id][name] 		= $categoria->name;
			$dados[$categoria->term_id][slug] 		= $categoria->slug;

			$args = array(
				'post_status' 		  => 'publish',
				'post_type' 		  => array( 'post', 'series', 'palestrante' ),
				'posts_per_page'      => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'kids',
						'field'    => 'id',
						'terms'    => $json_api->query->categoryID,
					),
				),
				'post__not_in' =>$episodiosSeries,
			);

			$loop = new WP_Query( $args );
			if($loop->have_posts()):
				while ( $loop->have_posts() ) : $loop->the_post();
					global $post;
					$dados[$categoria->term_id]['dados'][$post->ID] 				 = (array)  $post;
					$imgPadraoID 							 = get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
					$imgMetaPadraoID 				 		 = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
					$imgMobileID 					 		 = get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
					$dados[$categoria->term_id]['dados'][$post->ID]['banner']	 = wp_get_attachment_image_src( $imgPadraoID, 'large' );
					$dados[$categoria->term_id]['dados'][$post->ID]['image']		 = wp_get_attachment_image_src( $imgMobileID, 'large' );
					$dados[$categoria->term_id]['dados'][$post->ID]['thumb'] 	 = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados[$categoria->term_id]['dados'][$post->ID]['post_excerpt']= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)))."<br><br>".nl2br(strip_tags(get_post_meta($post->ID,'ficha-video',true)));
					$dados[$categoria->term_id]['dados'][$post->ID]['key']		 = $keySamba;
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['pt'][mediakey] = get_post_meta($post->ID,'video-anexado',true);
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['pt'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['en'][mediakey] = get_post_meta($post->ID,'video-anexado-secundario',true);
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['en'][url] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['en'][legenda]	= get_post_meta($post->ID,'select-legenda',true);
					$tituloEpisodios = get_post_meta($post->ID, 'titulo-episodios', true);
					if($tituloEpisodios == '')
						$tituloEpisodios = utf8_encode("Epis�dios");
					$dados[$categoria->term_id]['dados'][$post->ID]['titulo-episodio'] = $tituloEpisodios;
					$dados[$categoria->term_id]['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
					if($dados[$categoria->term_id]['dados'][$post->ID]['media']['duracao'] == '')
						$dados[$categoria->term_id]['dados'][$post->ID]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
					if($post->post_type=="palestrante"){
						$dados[$categoria->term_id]['dados'][$post->ID]['thumb']		  		= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
						$dados[$categoria->term_id]['dados'][$post->ID]['post_excerpt'] 		=  nl2br(strip_tags($post->post_content));
						$dados[$categoria->term_id]['dados'][$post->ID][videos] 	   			= get_palestras($post->ID);
						$dados[$categoria->term_id]['dados'][$post->ID][series] 	   			= get_series_palestrante($post->ID);
						$dados[$categoria->term_id]['dados'][$post->ID][episodios] 			= '';
					}else{
						$dados[$categoria->term_id]['dados'][$post->ID][videos] 	   			= '';
						$dados[$categoria->term_id]['dados'][$post->ID][series] 	   			= '';
						$dados[$categoria->term_id]['dados'][$post->ID][episodios] 			= get_episodios(get_post_meta($post->ID,'video-serie',true));
					}
				endwhile;
			endif;
			wp_reset_query();
			if(count($dados[$categoria->term_id]['dados']) == 0){
				unset($dados[$categoria->term_id]);
				$itensLocalizados--;
			}
		}
		$retorno['listas'] = $dados;
		$retorno[total] = $itensLocalizados;

		return $retorno;
	}

	public function update_liked(){
		global $json_api, $wpdb, $post,$keySamba;

		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}
		if (!$json_api->query->postID) {
		  $json_api->error("You must include a 'postID' value.");
		}
		if (!$json_api->query->acao) {
		  $json_api->error("You must include a 'acao' value.");
		}
		if (!$json_api->query->ip) {
		  $json_api->error("You must include a 'ip' value.");
		}

		$wpdb->query("INSERT INTO wp_likebtn_vote(identifier, identifier_hash, client_identifier, `type`, user_id, ip, country, lat, lng, created_at)
				  	 VALUES ('".$json_api->query->postID."', md5(now()),  md5(now()), ".$json_api->query->acao.",'".$json_api->query->userID."', '".$json_api->query->ip."', '', 0, 0, NOW())");

		$statusLike = $wpdb->get_var("select type from wp_likebtn_vote where identifier = ".$json_api->query->postID." and user_id = ".$json_api->query->userID." order by id desc limit 1");
		if($statusLike==1){
			$imgLike[up] = "http://www.teomidia.org/wp-content/uploads/icone-aba-aberta-hand-up-hover.png";
			$imgLike[down] = "http://www.teomidia.org/wp-content/uploads/icone-aba-aberta-hand-down.png";
		}else if($statusLike==-1){
			$imgLike[up] = "http://www.teomidia.org/wp-content/uploads/icone-aba-aberta-hand-up.png";
			$imgLike[down] = "http://www.teomidia.org/wp-content/uploads/icone-aba-aberta-hand-down-hover.png";
		}else{
			$imgLike[down] = "http://www.teomidia.org/wp-content/uploads/icone-aba-aberta-hand-down.png";
			$imgLike[up] = "http://www.teomidia.org/wp-content/uploads/icone-aba-aberta-hand-up.png";
		}
		$retorno['liked'] 	 = $imgLike;
		return $retorno;
	}


	public function update_time_video_user(){
		global $json_api, $wpdb, $post,$keySamba;
		if (!$json_api->query->userID) {
		  $json_api->error("You must include a 'userID' value.");
		}
		if (!$json_api->query->postID) {
		  $json_api->error("You must include a 'postID' value.");
		}
		if (!$json_api->query->tempo) {
		  $json_api->error("You must include a 'tempo' value.");
		}
		if (!$json_api->query->porcent) {
		  $json_api->error("You must include a 'porcent' value.");
		}

		update_user_meta($json_api->query->userID,'time_video_user',$json_api->query->tempo);
		$videoUser = $wpdb->get_var("select ID from aux_users_videos where user_ID = ".$json_api->query->userID." and Video_ID = ".$json_api->query->postID." and video_porcento < 90 or Sessao_Porcento < 90  order by ID desc limit 1");
/*
>
*/
		if($json_api->query->SessaoVideoTime==''){
			$json_api->query->SessaoVideoTime = $wpdb->get_var("select tempo_sessao from aux_users_videos where user_ID = ".$json_api->query->userID." and Video_ID = ".$json_api->query->postID." order by ID desc limit 1");
		}

		$sessaoPorcento = ($json_api->query->SessaoVideoTime*100)/$json_api->query->totalTime;
		$sessao = $json_api->query->SessaoVideoTime;
		if($videoUser == ''){
			$wpdb->query("INSERT INTO aux_users_videos(User_ID, Video_ID, Video_Time, Video_Porcento, Tempo_Sessao,Sessao_Porcento,Video_Total)VALUES(".$json_api->query->userID.", ".$json_api->query->postID.", 0,0,0,0,".$json_api->query->totalTime.")");
			$sessao = 0;
		}else{
			$wpdb->query("UPDATE aux_users_videos SET Video_Time='".$json_api->query->tempo."', Video_Porcento='".$json_api->query->porcent."',Data_Cadastro = now(), Tempo_Sessao = ".$json_api->query->SessaoVideoTime.", Sessao_Porcento='".$sessaoPorcento."', Video_Total='".$json_api->query->totalTime."' WHERE ID = $videoUser");
			$sessao = 1;
		}

		return $json_api->query->SessaoVideoTime;
	}
}

	function get_episodios($videos){
		global $categoriaEpisodios, $keySamba, $dadosMidia;

		$userID  	= $_GET['userID'];
		$userList 	= get_user_meta($userID, 'my-list');
		$userList 	= $userList[0];

		foreach($videos as $video){
			if($video[video] != ""){
				$indice  = str_replace('temporada ','',trim(strtolower($categoriaEpisodios[$video[temporada]])));
				$indice .= str_replace(utf8_encode('episodio '),'',str_replace(utf8_encode('epis�dio '),'',trim(strtolower($categoriaEpisodios[$video[episodio]]))));
				$post = get_post($video[video]);
				$dados[$indice] 		 		 			= (array)  $post;
				$imgPadraoID 					 		  	= get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
				$imgMetaPadraoID 				 		  	= get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
				$imgMobileID 					 		  	= get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
				$dados[$indice]['banner']	 			 	= wp_get_attachment_image_src( $imgPadraoID, 'large' );
				$dados[$indice]['image']		 		 	= wp_get_attachment_image_src( $imgMobileID, 'large' );
				$dados[$indice]['thumb'] 	 			 	= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
				$dados[$indice]['post_excerpt']				= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)));
				$dados[$indice]['key']		  				= $keySamba;
				$dados[$indice]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
				if($dados[$indice]['media']['duracao'] == '')
					$dados[$indice]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
				$dados[$indice]['media']['pt']['mediakey'] 	= get_post_meta($post->ID,'video-anexado',true);
				$dados[$indice]['media']['pt']['url'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
				$dados[$indice]['media']['en']['mediakey'] 	= get_post_meta($post->ID,'video-anexado-secundario',true);
				$dados[$indice]['media']['en']['url'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
				$dados[$indice]['media']['en']['legenda']	= get_post_meta($post->ID,'select-legenda',true);
				$dados[$indice]['temporada']  				= $categoriaEpisodios[$video['temporada']];
				//$dados[$indice]['episodio']   				= $categoriaEpisodios[$video['episodio']];
				$dados[$indice]['episodio']   				= preg_replace("/[^0-9]/","", $categoriaEpisodios[$video['episodio']]);
			}
		}
		ksort($dados);
		return $dados;
	}

	function get_palestras($palestranteID){

		global $categoriaEpisodios, $keySamba,$dadosMidia, $post, $wpdb;

		$videos = json_decode(json_encode($wpdb->get_results("SELECT post_id video from wp_postmeta where meta_key = 'video-palestrante' and meta_value = $palestranteID")),true);

		foreach($videos as $indice=>$video){
			if($video[video] != ""){
				$poste = get_post($video[video]);
				if($poste->post_type=='post'){
					$palestraADD++;
					$dados[$indice] 		 		 			= (array)  $post;
					$imgPadraoID 					 		  	= get_post_meta($poste->ID,$poste->post_type.'_thumb-banner_thumbnail_id',true);
					$imgMetaPadraoID 				 		  	= get_post_meta($poste->ID,$poste->post_type.'_thumb-padrao_thumbnail_id',true);
					$imgMobileID 					 		  	= get_post_meta($poste->ID,$poste->post_type.'_thumb-mobile_thumbnail_id',true);
					$dados[$indice]['banner']	 			 	= wp_get_attachment_image_src( $imgPadraoID, 'large' );
					$dados[$indice]['image']		 		 	= wp_get_attachment_image_src( $imgMobileID, 'large' );
					$dados[$indice]['thumb'] 	 			 	= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados[$indice]['post_excerpt']				= nl2br(strip_tags(get_post_meta($poste->ID,'sinopse-video',true)));
					$dados[$indice]['key']		  				= $keySamba;
					$dados[$indice]['media']['duracao'] 		= $dadosMidia[get_post_meta($poste->ID,'video-anexado-secundario',true)]['duracao'];
					if($dados[$indice]['media']['duracao'] == '')
						$dados[$indice]['media']['duracao'] 		= $dadosMidia[get_post_meta($poste->ID,'video-anexado',true)]['duracao'];
					$dados[$indice]['media']['pt']['mediakey'] 	= get_post_meta($poste->ID,'video-anexado',true);
					$dados[$indice]['media']['pt']['url'] 		= $dadosMidia[get_post_meta($poste->ID,'video-anexado',true)][0]['url'];
					$dados[$indice]['media']['en']['mediakey'] 	= get_post_meta($poste->ID,'video-anexado-secundario',true);
					$dados[$indice]['media']['en']['url'] 		= $dadosMidia[get_post_meta($poste->ID,'video-anexado-secundario',true)][0]['url'];
					$dados[$indice]['media']['en']['legenda']	= get_post_meta($poste->ID,'select-legenda',true);
					$dados[$indice]['temporada']  				= $categoriaEpisodios[$video['temporada']];
					//$dados[$indice]['episodio']   				= $categoriaEpisodios[$video['episodio']];
					$dados[$indice]['episodio']   				= preg_replace("/[^0-9]/","", $categoriaEpisodios[$video['episodio']]);
				}
			}
		}
		ksort($dados);
		return $dados;
	}

	function get_series_palestrante($palestranteID){

		global $categoriaEpisodios, $keySamba,$dadosMidia, $post, $wpdb;

		$videos = json_decode(json_encode($wpdb->get_results("SELECT post_id video from wp_postmeta where meta_key = 'video-palestrante' and meta_value = $palestranteID")),true);

		foreach($videos as $indice=>$video){
			if($video[video] != ""){
				$post = get_post($video[video]);
				if($post->post_type=='series'){
					$palestraADD++;
					$dados[$indice] 		 		 			= (array)  $post;
					$imgPadraoID 					 		  	= get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
					$imgMetaPadraoID 				 		  	= get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
					$imgMobileID 					 		  	= get_post_meta($post->ID,$post->post_type.'_thumb-mobile_thumbnail_id',true);
					$dados[$indice]['banner']	 			 	= wp_get_attachment_image_src( $imgPadraoID, 'large' );
					$dados[$indice]['image']		 		 	= wp_get_attachment_image_src( $imgMobileID, 'large' );
					$dados[$indice]['thumb'] 	 			 	= wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
					$dados[$indice]['post_excerpt']				= nl2br(strip_tags(get_post_meta($post->ID,'sinopse-video',true)));
					$dados[$indice]['key']		  				= $keySamba;
					$dados[$indice]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)]['duracao'];
					if($dados[$indice]['media']['duracao'] == '')
						$dados[$indice]['media']['duracao'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)]['duracao'];
					$dados[$indice]['media']['pt']['mediakey'] 	= get_post_meta($post->ID,'video-anexado',true);
					$dados[$indice]['media']['pt']['url'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado',true)][0]['url'];
					$dados[$indice]['media']['en']['mediakey'] 	= get_post_meta($post->ID,'video-anexado-secundario',true);
					$dados[$indice]['media']['en']['url'] 		= $dadosMidia[get_post_meta($post->ID,'video-anexado-secundario',true)][0]['url'];
					$dados[$indice]['media']['en']['legenda']	= get_post_meta($post->ID,'select-legenda',true);
					$dados[$indice]['temporada']  				= $categoriaEpisodios[$video['temporada']];
					//$dados[$indice]['episodio']   				= $categoriaEpisodios[$video['episodio']];
					$dados[$indice]['episodio']   				= preg_replace("/[^0-9]/","", $categoriaEpisodios[$video['episodio']]);
					$dados[$indice]['episodios'] 				= get_episodios(get_post_meta($post->ID,'video-serie',true));
				}
			}
		}
		ksort($dados);
		sort($dados);
		return $dados;

	}



?>