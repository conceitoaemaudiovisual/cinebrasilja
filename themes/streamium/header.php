<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<div class="footer-cast" Style='display:none;'>
	<div class="title-footer-cast">Pronto para transmitir</div>
	<div class="content-footer-cast">Comece a assistir um t&iacute;tulo para iniciar a transmiss&atilde;o</div>
	<div class="player-footer-cast">
		<table class='player-footer-cast-container'>
			<tr>
				<td class='icon-player'>
					<i class="fa fa-pause" aria-hidden="true"></i>
					<i class="fa fa-play" aria-hidden="true"></i>
				</td>
				<td class='icon-player'>
					<i class="fa fa-stop-fotter" aria-hidden="true"></i>
				</td>
				<td class='icon-player icon-player-cast-volume'>
					<input id="cast-volume" type="range"  class="fa fa-volume-range" min="0" max="1" step="0.1"/>
					<i class="fa fa-volume-up" aria-hidden="true"></i>
				</td>
				<td class='icon-duracao-atual'>
					<span class='cast-tempo-atual'> 00:00:00 </span>
				</td>
				<td class='icon-ducacao-range'>
					<input id="cast-duracao" type="range"  class="fa fa-duracao-range" min="0" step="1"/>
				</td>
				<td class='icon-duracao'>
					<span class='cast-duracao'> 00:00:00 </span>
				</td>
				<td class='icon-player'>
					<i class="fa fa-cc" aria-hidden="true"></i>
				</td>
				<td class='icon-player'>
					<i class="fa fa-audio-description" aria-hidden="true"></i>
				</td>
			</tr>
		</table>
	</div>
</div>

<style>
	.footer-cast {
		height:auto !important;
	}
	.title-footer-cast{
	   	padding: 0px 0 5px;
	}
	.content-footer-cast{
	   	padding: 0px 0 10px;
	}
	.player-footer-cast{
		display:none;
		margin-top:-5px;
		margin-bottom: 10px;
	}
	.player-footer-cast .fa{
		cursor:pointer;
		float:left;
	}
	.player-footer-cast .fa:hover{
		color: #ff4e02;
	}
	.player-footer-cast .fa-play{
		display:none;
	}
	.player-footer-cast .fa-duracao-range{
		width:100%;
		margin-top: 2px;
    	padding:0 !important;
	}
	.player-footer-cast .icon-player{
		width:30px;
	}
	.player-footer-cast .icon-duracao{
		width:90px;
	}
	.player-footer-cast .icon-duracao-atual{
		width:70px;
	}
	.cast-tempo-atual{
		font-family: 'Lato', sans-serif !important;
    	font-size: 14px !important;
	}
	.player-footer-cast .cast-duracao{
		text-align:center;
	}
	.player-footer-cast-container{
		width:90%;
		margin:0 auto;
	}
	.fa-stop-fotter:before {
	    content: "\f04d";
	}
	#cast-volume{
		width:50px;
      	-webkit-transform:rotate(-90deg);
    	-moz-transform:rotate(-90deg);
    	-o-transform:rotate(-90deg);
    	-ms-transform:rotate(-90deg);
    	transform:rotate(-90deg);
    	padding:0 !important;
    	margin-left: -17px;
    	margin-top: -15px;
    	display:none;
    }
</style>


<head data-cast-api-enabled="true">
	<!-- Meta Data -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!-- favicons -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_bloginfo('template_url'); ?>/production/img/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_url'); ?>/production/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_bloginfo('template_url'); ?>/production/img/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_url'); ?>/production/img/favicon-16x16.png">
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-99562718-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/3a38285a-9f5d-456b-81cd-b0320082eb52-loader.js" ></script>
	<!-- Trackback -->
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<!-- Wordpress Scripts -->
	<?php wp_head(); ?>

<?php
	global $current_user;
	get_currentuserinfo();

	$visualizados = get_user_meta($current_user->data->ID, 'avisos-lidos', true);
	$avisosCadastrados = get_posts(array('post_type' => 'avisos'));

	if(isset($avisosCadastrados)){
		foreach($avisosCadastrados as $avisosCadastrado){
			if(!in_array($avisosCadastrado->ID,$visualizados)){
				$totalAvisos++;
				$msgContent .= "<div class='mensagem-usuario-content' attr='$avisosCadastrado->ID'>$avisosCadastrado->post_content</div>";
			}
		}
		if($totalAvisos >= 1){
			$msgUser = "<span class='mensagem-usuario-header'>$totalAvisos</span>";
			$msgContent  = "<div class='mensagem-usuario-container'>$msgContent</div>";
		}
	}

	$minhaConta = "	<div class='menu-minha-conta'>
						<a href='/minha-conta/'>Minha Conta</a>
						<a href='/wp-login.php?action=logout&redirect_to=/home/' id='logOut'>Sair</a>
					</div>";

	$loginName = $current_user->user_login;
	if(strpos($current_user->user_login,'@') > 0)
		$loginName = substr($current_user->user_login, 0, strpos($current_user->user_login,'@'));

	global $logoEmpresa;
	$codigoVinculo = get_user_meta($current_user->data->ID, 'codigo-vinculo', true);
	if($codigoVinculo != ""){
		$empresaID = $wpdb->get_var("select post_id
									 from wp_postmeta
									 where meta_key = 'membros-plano' and meta_value like '%$codigoVinculo%'");
		$imgPadraoID 	= get_post_meta($empresaID,'_thumbnail_id',true);
		$imageEmpresa  	= wp_get_attachment_image_src( $imgPadraoID, 'thumbnail' );
		if($imageEmpresa[0] != ""){
			$logoEmpresa = "<img src='$imageEmpresa[0]' id='logo-empresa' Style='width:185px !important;position:absolute;bottom:0;left:0;'>";
		}
	}
?>
</head>
<body <?php body_class(); ?>>

	<input type='hidden' name='log' id='header-buttons-id' class='logoutid' value='<?php echo $current_user->user_login?>'>

	<header class="cd-main-header fixed">

		<?php if ( get_theme_mod( 'streamium_logo' ) ) : ?>

		    <a class="cd-logo" href="<?php echo esc_url( home_url('/') ); ?>"><img src='/wp-content/uploads/logo-cinebrasilplay-topo.png' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>

		<?php else : ?>

		    <a class="cd-logo" href="<?php echo esc_url( home_url('/') ); ?>"><?php bloginfo( 'name' ); ?></a>

		<?php endif; ?>

		<nav class='cd-nav-container'>
			<?php echo $msgContent;?>
			<ul id='cd-primary-nav' class='cd-primary-nav is-fixed'>
				<li id='menu-item-5' class='menu-item menu-item-type-custom menu-item-object-custom menu-item-5'>
					<a href='#' class='menu-item-notificacao'>
						<img src='/wp-content/uploads/icone-notificacao.png'>
						<?php echo $msgUser;?>
					</a>
				</li>
				<li id='menu-item-4' class='menu-item menu-item-type-custom menu-item-object-custom menu-item-4 menu-minha-conta-container'>
					<a href='#'>
						<img src='/wp-content/uploads/icone-usuario.png'>
						<span class='menu-usuario'><?php echo $loginName;?></span>
					</a>
					<?php echo $minhaConta;?>
				</li>
				<li id='menu-item-3' class='menu-item menu-item-type-custom menu-item-object-custom menu-item-3'>
					<a href="#cd-search"  class="cd-search-trigger">
						<img src='/wp-content/uploads/icone-buscar.png'>
						<span class='menu-buscar'>Buscar</span>
					</a>
		<?php get_search_form(); ?>

				</li>

				<li id='menu-item-2' class='menu-item menu-item-type-custom menu-item-object-custom menu-item-2'>
					<a href='/ao-vivo/'>
					    <img src='/wp-content/uploads/icone-ao-vivo.png'>
						<span class='menu-infantil'>Ao Vivo</span>
					</a>
				</li>

				<li id='menu-item-1' class='menu-item menu-item-type-custom menu-item-object-custom menu-item-1'>
					<a href='#'>
						<span class='menu-navegar'>Navegar</span>
					</a>

					<ul class='sub-menu'>
						<li class='sub-menu-item-container'>
							<ul class='sub-menu-item-1'>
								<li>
									<a href='/' class='inicio'>
										In&iacute;cio
									</a>
								</li>
								<li>
									<a href='/minha-lista/'>
										Minha Lista
									</a>
								</li>
								<li>
									<a href='/sugestoes/'>
										Sugest&atilde;o para voc&ecirc;
									</a>
								</li>
<?php
		$menus1 = $wpdb->get_results("SELECT t.term_id, t.slug, t.name FROM wp_termmeta m inner join wp_terms t on t.term_id = m.term_id WHERE meta_key='modulo_menu' and meta_value = 1 ORDER BY meta_value");
		foreach($menus1 as $menu1){
			echo "				<li>
									<a href='/acervo/$menu1->slug/'>
										$menu1->name
									</a>
								</li>";
		}
?>
							</ul>
						</li>
						<li class='sub-menu-item-container'>
							<ul class='sub-menu-item-2'>
								<li><a href='#'>&nbsp;</a></li>
<?php
		$menus2 = $wpdb->get_results("SELECT t.term_id, t.slug, t.name FROM wp_termmeta m inner join wp_terms t on t.term_id = m.term_id WHERE meta_key='modulo_menu' and meta_value = 2 ORDER BY meta_value");
		foreach($menus2 as $menu2){
			echo "				<li>
									<a href='/acervo/$menu2->slug/'>
										$menu2->name
									</a>
								</li>";
		}
?>
							</ul>
						</li>
						<li class='sub-menu-item-container'>
							<ul class='sub-menu-item-3'>
								<li><a href='#'>&nbsp;</a></li>
<?php
		$menus3 = $wpdb->get_results("SELECT t.term_id, t.slug, t.name FROM wp_termmeta m inner join wp_terms t on t.term_id = m.term_id WHERE meta_key='modulo_menu' and meta_value = 3 ORDER BY meta_value");
		foreach($menus3 as $menu3){
			echo "				<li>
									<a href='/acervo/$menu3->slug/'>
										$menu3->name
									</a>
								</li>";
		}
?>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
		<ul class="cd-header-buttons">
			<li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
		</ul>

	</header>
