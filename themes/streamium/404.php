<?php get_header(); ?>
<div>
    <div class="row">


        <div class="col-md-12">
            <div class="containerPage404" style="margin-top: 4%; margin-bottom: 3%;">
                <div><img src="/wp-content/uploads/2017/04/icon-404.png" alt="" class="alignnone size-full wp-image-650" />
                 </div>
                <h1>P&aacute;gina n&atilde;o localizada!</h1>
                <div  style="margin: 20px;">
                 <p>Clique abaixo para acessar a p&aacute;gina inicial do Cine Brasil</p>
                </div>
                <div class="error-actions">
                    <a href="\" class="btn btn-default btn-lg">
                        Home</a>
                </div>
            </div>
        </div>


    </div>
</div>
<?php get_footer(); ?>