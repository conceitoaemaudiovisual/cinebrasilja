<?php
if (!function_exists('streamium_theme_setup')) {
    function streamium_theme_setup() {
        // Create aspects based on average
        $width = 1366/6;
        $height = $width/16*9;
    	  add_theme_support('post-thumbnails');
        add_theme_support( 'automatic-feed-links' );
        add_image_size( 'streamium-video-poster', 600, 338, true ); // (cropped)
        add_image_size( 'streamium-video-category', $width, $height ); //, 285, 160
        add_image_size( 'streamium-video-tile-expanded', ($width*2), ($height*2)); //, 285, 160
        add_image_size( 'streamium-home-slider', 1600, 900 );
        add_image_size( 'streamium-site-logo', 0, 56, true );
		add_image_size( 'imagem-padrao',  '245', '137', false );
		add_image_size( 'imagem-hover-1', '245', '137', false );
		add_image_size( 'imagem-hover-2', '245', '137', false );
		add_image_size( 'imagem-vertical-1', '137', '245', false );
		add_image_size( 'imagem-aplicativo', '400', '600', true );

        add_theme_support( 'title-tag' );
    }
}

	add_image_size( 'imagem-aplicativo', '400', '600', true );

add_action('after_setup_theme', 'streamium_theme_setup');

flush_rewrite_rules( false );

if (!function_exists('streamium_enqueue_scripts')) {

    function streamium_enqueue_scripts() {

        /* Register styles -----------------------------------------------------*/
        wp_enqueue_style( 'streamium-styles', get_stylesheet_uri() );
        wp_enqueue_style('streamium-production', get_template_directory_uri() . '/production/css/streamium.min.css');

        /* Register scripts -----------------------------------------------------*/
        wp_enqueue_script( 'plupload' );
        wp_enqueue_script( 'streamium-production', get_template_directory_uri() . '/production/js/streamium.min.js', array( 'jquery' ),'1.1', true);
        wp_localize_script( 'streamium-production', 'streamium_object',
            array(
                'ajax_url' => admin_url( 'admin-ajax.php')
            )
        );

        //wp_enqueue_style('streamium-s3bubble-cdn', 'http://local.hosted.com/assets/hosted/s3bubble-hosted-cdn.min.css');
        //wp_enqueue_script( 'streamium-s3bubble-cdn', 'http://local.hosted.com/assets/hosted/s3bubble-hosted-cdn.min.js', array( 'jquery'),'1.1', true );

        // not valid
//        wp_enqueue_style('streamium-s3bubble-cdn', 'https://s3.amazonaws.com/s3bubble-cdn/v1/s3bubble-hosted-cdn.min.css');
//        wp_enqueue_script( 'streamium-s3bubble-cdn', 'https://s3.amazonaws.com/s3bubble-cdn/v1/s3bubble-hosted-cdn.min.js','','1.1', true );

	}

    add_action('wp_enqueue_scripts', 'streamium_enqueue_scripts');

}

if (!function_exists('streamium_enqueue_admin_scripts')) {

    /**
     * Include the scripts for the meta boxes
     *
     * @return null
     * @author  @sameast
     */
    function streamium_enqueue_admin_scripts(){

      $streamium_connected_website = get_option("streamium_connected_website");
      $streamium_connected_nonce = wp_create_nonce( 'streamium_connected_nonce' );
      wp_enqueue_style( 'streamium-admin', get_template_directory_uri() . '/production/css/admin.min.css', array() );
      wp_enqueue_script( 'streamium-admin', get_template_directory_uri() . '/production/js/admin.min.js', array( 'jquery'),'1.1', true );
      wp_localize_script('streamium-admin', 'streamium_meta_object', array(
        'ajax_url' => admin_url( 'admin-ajax.php'),
        's3website' => (!empty($streamium_connected_website) ? $streamium_connected_website : ""),
        'streamiumPremium' => get_theme_mod( 'streamium_enable_premium' ),
        'connected_website' => $streamium_connected_website,
        'connected_nonce' => $streamium_connected_nonce
      ));

    }

    add_action( 'admin_enqueue_scripts', 'streamium_enqueue_admin_scripts' );

}


if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Destacada palestrante',
            'id' => 'thumb-palestrante-interna',
            'post_type' => 'palestrante'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Imagem do Banner',
            'id' => 'thumb-banner',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Padrao',
            'id' => 'thumb-padrao',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 1',
            'id' => 'thumb-hover-1',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 2',
            'id' => 'thumb-hover-2',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 3',
            'id' => 'thumb-hover-3',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Vertical',
            'id' => 'thumb-vertical',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Aplicativo',
            'id' => 'thumb-mobile',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Imagem do Banner',
            'id' => 'thumb-banner',
            'post_type' => 'series'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Padrao',
            'id' => 'thumb-padrao',
            'post_type' => 'series'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 1',
            'id' => 'thumb-hover-1',
            'post_type' => 'series'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 2',
            'id' => 'thumb-hover-2',
            'post_type' => 'series'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 3',
            'id' => 'thumb-hover-3',
            'post_type' => 'series'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Vertical',
            'id' => 'thumb-vertical',
            'post_type' => 'series'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Aplicativo',
            'id' => 'thumb-mobile',
            'post_type' => 'series'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Padrao',
            'id' => 'thumb-padrao',
            'post_type' => 'palestrante'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 1',
            'id' => 'thumb-hover-1',
            'post_type' => 'palestrante'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 2',
            'id' => 'thumb-hover-2',
            'post_type' => 'palestrante'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Hover 3',
            'id' => 'thumb-hover-3',
            'post_type' => 'palestrante'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Vertical',
            'id' => 'thumb-vertical',
            'post_type' => 'palestrante'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Miniatura Aplicativo',
            'id' => 'thumb-mobile',
            'post_type' => 'palestrante'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Palestrante Horizontal',
            'id' => 'thumb-palestrante',
            'post_type' => 'palestrante'
        )
    );
}

register_sidebar( array(
	'name' => 'Rodape',
	'id' => 'rodape',
	'before_widget' => '<session class="session-rodape">',
	'after_widget' => '</session>',
	'before_title' => '<p class="article-rodape">',
	'after_title' => '</p>',
) );




function change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'V&iacute;deos';
    $submenu['edit.php'][5][0] = 'V&iacute;deos';
    $submenu['edit.php'][10][0] = 'Adicionar V&iacute;deo';
    $submenu['edit.php'][16][0] = 'Tags';
    echo '';
}
function change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'V&iacute;deos';
    $labels->singular_name = 'V&iacute;deo';
    $labels->add_new = 'Adicionar V&iacute;deo';
    $labels->add_new_item = 'Adicionar V&iacute;deo';
    $labels->edit_item = 'Editar V&iacute;deo';
    $labels->new_item = 'V&iacute;deo';
    $labels->view_item = 'Ver V&iacute;deo';
    $labels->search_items = 'Buscar V&iacute;deos';
    $labels->not_found = 'Nenhum V&iacute;deo encontrado';
    $labels->not_found_in_trash = 'Nenhum V&iacute;deo encontrado no Lixo';
    $labels->all_items = 'Todos V&iacute;deos';
    $labels->menu_name = 'V&iacute;deos';
    $labels->name_admin_bar = 'V&iacute;deos';
}

add_action( 'admin_menu', 'change_post_label' );
add_action( 'init', 'change_post_object' );


/*-----------------------------------------------------------------------------------*/
/*  Include the Streamium Framework
/*-----------------------------------------------------------------------------------*/
$tempdir = get_template_directory();
require_once($tempdir .'/inc/init.php');






add_action('check_admin_referer', 'logout_without_confirm', 10, 2);
function logout_without_confirm($action, $result){
	if ($action == 'log-out' && !isset($_GET['_wpnonce'])) {
		wp_logout();
		header("Location: /home");
		die;
	}
}