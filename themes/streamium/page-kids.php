<?php
	/*
	 Template Name: Kids
	 */
	$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
	foreach($videosSerie as $videoSerie){
		$postsSeries = unserialize($videoSerie->meta_value);
		foreach($postsSeries as $postSeries){
			if($postSeries['video'] != '') {
				$vidSerie++;
				$listVideoSerie[] = $postSeries['video'];
				$videoListNotIn .= $virgula.$postSeries['video'];
				$virgula = ',';
			}
		}
	}
	if($vidSerie=='') $listVideoSerie[] = -1500;
?>
<?php get_header(kids); ?>
	  	<section class="videos" Style='margin-bottom:30px; padding-bottom: 56px;background: #ff4e02 !important;'>
			<div class="container-fluid" Style='margin-top:70px;'>
				<div class="row">
					<div class="col-sm-12 video-header" Style='height:50px;'>
						<h3 Style='margin-bottom:5px; margin-left: 5px; color:#fff !important;'>Destaques</h3>
					</div><!--/.col-sm-12-->
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="prev_next"></div>
						<div class="carousels">
					  	<?php
					  		$category->slug = 'destaques-kids';
								$destaquesKids = $wpdb->get_results("select distinct object_id ID
																	from wp_terms t
																	inner join wp_term_taxonomy tx on tx.term_id = t.term_id
																	inner join wp_term_relationships tr on tr.term_taxonomy_id = tx.term_taxonomy_id
																	inner join wp_postmeta pm on pm.post_id = tr.object_id and pm.meta_key = 'video_kids' and pm.meta_value = 'checked'
																	where taxonomy = 'kids' and pm.post_id not in($videoListNotIn)");
								foreach($destaquesKids as $destaqueKids)
									$destaque[] = $destaqueKids->ID;
								$args = array(
										'post_type' => array( 'post', 'series' ),
										'post__not_in' => $listVideoSerie,
										'post__in' => $destaque,
									);
								$loop = new WP_Query( $args );
								if($loop->have_posts()):
									while ( $loop->have_posts() ) : $loop->the_post();
									if ( has_post_thumbnail() ) : // thumbnail check
										$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-vertical_thumbnail_id',true);
										$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
										$imageExpanded    = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
										$nonce = wp_create_nonce( 'streamium_likes_nonce' );

						?>
							<div class="tile" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="<?php echo $category->slug; ?>">

								<div class="tile_inner carrosel-vertical" style="background-image: url(<?php echo esc_url($image[0]); ?>);"  attr-image="<?php echo esc_url($image[0]); ?>">
									<div class="content">
								      <div class="overlay" style="background-image: url(<?php echo esc_url($imageExpanded[0]); ?>);">
								        <div class="overlay-gradient"></div>
								        <a class="play-icon-wrap hidden-xs" href="<?php the_permalink(); ?>" Style='border:1px solid red;height:100%;width:100%;top:7%;left:10%;border-radius:0px;'>
											<div class="play-icon-wrap-rel">
												<div class="play-icon-wrap-rel-ring"></div>
												<span class="play-icon-wrap-rel-play">
													<i class="fa fa-play fa-1x" aria-hidden="true"></i>
									        	</span>
								        	</div>
							        	</a>
							          	<div class="overlay-meta hidden-xs">
							            	<h4><?php the_title(); ?></h4>
							            	<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="<?php echo $category->slug; ?>" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
							          	</div>
								      </div>
								    </div>
								</div>
						    </div>
						<?php

							endif;
							endwhile;
							endif;
							wp_reset_query();
						?>
						</div><!--/.carousel-->
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.container-->
		</section><!--/.videos-->
		<section class="s3bubble-details-full <?php echo $category->slug; ?>">
			<div class="s3bubble-details-full-overlay"></div>
			<div class="container-fluid s3bubble-details-inner-content">
				<div class="row">
					<div class="col-sm-12 col-xs-12 rel">
						<div class="synopis-outer synopis-outer-ajax ">
							<div class="synopis-middle">
								<div class="synopis-inner">
									<h2 class="synopis hidden-xs"></h2>
									<div class="synopis content"></div>
								</div>
							</div>
						</div>


						<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
							<a class="play-icon-wrap synopis" href="#">
								<div class="play-icon-wrap-rel">
									<div class="play-icon-wrap-rel-ring play-ajax"></div>
									<span class="play-icon-wrap-rel-play">
										<i class="fa fa-play fa-3x" aria-hidden="true"></i>
									</span>
								</div>
							</a>
							<a href="#" class="synopis-video-trailer streamium-btns hidden-xs">Watch Trailer</a>
							<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
						</div><!--/.col-sm-12-->

					</div>
				</div><!--/.row-->
			</div><!--/.container-->
		</section><!--/.videos-->

		<?php
			$args = array(
			  'parent' => 0,
			  'taxonomy' => 'kids'
			);
		  	$categories = get_categories($args);
		  	foreach ($categories as $category) :
	  	?>
	  	<section class="videos">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header">
						<h3><?php echo ucfirst($category->cat_name); ?></h3>
					</div><!--/.col-sm-12-->
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="prev_next"></div>
						<div class="carousels">
					  	<?php
							$args = array(
								    'posts_per_page' => (int)get_theme_mod( 'streamium_global_options_homepage_desktop' ),
									'post__not_in' => $listVideoSerie,
									'tax_query' => array(
										array(
											'taxonomy' => 'kids',
											'field'    => 'slug',
											'terms'    => $category->slug,
										),
									)
								);
								$loop = new WP_Query( $args );
								if($loop->have_posts()):
									while ( $loop->have_posts() ) : $loop->the_post();
									if ( has_post_thumbnail() ) : // thumbnail check
										global $post;
										$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
										$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'__thumb-hover-1_thumbnail_id',true);
										$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'__thumb-hover-2_thumbnail_id',true);
										$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
										$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
										$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
										$nonce = wp_create_nonce( 'streamium_likes_nonce' );
										$linkID = $post->ID;
										if($post->post_type=='series'){
											$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
											$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
											$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
											$image  		 = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
											$imageExpanded   = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
											$imageExpanded2  = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
											$series = get_post_meta($post->ID,'video-serie',true);
											foreach($series as $indice=>$episodios){
												$series[$indice]['pos'] = $wpdb->get_var("select concat(substring(name, LOCATE(' ', name)+1, 10) ,(select substring(name, LOCATE(' ', name)+1, 10) from wp_terms where term_id = $episodios[temporada])) from wp_terms where term_id = $episodios[episodio]");
												if(isset($series[$indice]['pos']))
													$parametro[$indice] = $series[$indice]['pos'];
											}
											$videoSerieAtual = array_search(min($parametro),$parametro);
											$episodio = get_post($series[$videoSerieAtual][video]);
											$linkID = $episodio->ID;
										}
						?>
							<div class="tile" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="<?php echo $category->slug; ?>">

								<div class="tile_inner" style="background-image: url(<?php echo esc_url($image[0]); ?>);" attr-image="<?php echo esc_url($image[0]); ?>">
									<?php if($post->premium) : ?>
										<div class="tile_payment_details">
											<div class="tile_payment_details_inner">
												<h2>Available on <?php echo str_replace(array("_"), " ", $post->plans[0]); ?></h2>
											</div>
										</div>
									<?php endif; ?>
									<?php if (function_exists('is_protected_by_s2member')) :
										$check = is_protected_by_s2member(get_the_ID());
										if($check) : ?>
										<div class="tile_payment_details">
											<div class="tile_payment_details_inner">
												<h2>Available on <?php
													$comma_separated = implode(",", $check);
													echo "plan " . $comma_separated;
												?></h2>
											</div>
										</div>
									<?php endif; endif; ?>
									<div class="content">
								      <div class="overlay" style="background-image: url(<?php echo esc_url($imageExpanded[0]); ?>);">
								        <div class="overlay-gradient"></div>
								        <a class="play-icon-wrap hidden-xs" href="<?php the_permalink(); ?>">
											<div class="play-icon-wrap-rel">
												<div class="play-icon-wrap-rel-ring"></div>
												<span class="play-icon-wrap-rel-play">
													<i class="fa fa-play fa-1x" aria-hidden="true"></i>
									        	</span>
								        	</div>
							        	</a>
							          	<div class="overlay-meta hidden-xs">
							            	<h4><?php the_title(); ?></h4>
							            	<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="<?php echo $category->slug; ?>" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
							          	</div>
								      </div>
								    </div>
								</div>
								<?php if(is_user_logged_in() && get_theme_mod( 'streamium_enable_premium' )):
							    		$userId = get_current_user_id();
							    		$percentageWatched = get_post_meta( get_the_ID(), 'user_' . $userId, true );
							    ?>
								    <div class="progress tile_progress">
									  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentageWatched; ?>"
									  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageWatched; ?>%">
									  </div>
									</div>
								<?php endif; ?>
						    </div>
						<?php

							endif;
							endwhile;
							endif;
							wp_reset_query();
						?>
						</div><!--/.carousel-->
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.container-->
		</section><!--/.videos-->
		<section class="s3bubble-details-full <?php echo $category->slug; ?>">
			<div class="s3bubble-details-full-overlay"></div>
			<div class="container-fluid s3bubble-details-inner-content">
				<div class="row">
					<div class="col-sm-12 col-xs-12 rel">
						<div class="synopis-outer synopis-outer-ajax ">
							<div class="synopis-middle">
								<div class="synopis-inner">
									<h2 class="synopis hidden-xs"></h2>
									<div class="synopis content"></div>
								</div>
							</div>
						</div>


						<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
							<a class="play-icon-wrap synopis" href="#">
								<div class="play-icon-wrap-rel">
									<div class="play-icon-wrap-rel-ring play-ajax"></div>
									<span class="play-icon-wrap-rel-play">
										<i class="fa fa-play fa-3x" aria-hidden="true"></i>
									</span>
								</div>
							</a>
							<a href="#" class="synopis-video-trailer streamium-btns hidden-xs">Watch Trailer</a>
							<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
						</div><!--/.col-sm-12-->

					</div>
				</div><!--/.row-->
			</div><!--/.container-->
		</section><!--/.videos-->

		<?php
			endforeach;
		?>

		<section class="videos">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header">
						<h3></h3>
					</div><!--/.col-sm-12-->
				</div>
			</div>
		</section><!--/.videos-->

 <?php get_footer(); ?>