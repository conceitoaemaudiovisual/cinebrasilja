<?php

	/*
	 Template Name: Internas
	 */
?>
<?php get_header('internas'); ?>
<main class="cd-main-content page-template">

 	<div class="container">
	 	<div class="main-spacer-internas"></div>

			<div class="page-internas">
				<div class="row">
					<div>
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

							<?php the_content(); ?>

						<?php endwhile; else : ?>

							<p><?php _e( 'Sorry, no posts matched your criteria.', 'streamium' ); ?></p>

						<?php endif; ?>
					</div>
				</div>
			</div>

	 	</div>
	</div>

	<div class="main-spacer"></div>

 <?php get_footer(); ?>

