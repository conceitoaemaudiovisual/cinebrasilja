<?php
	/*
	 Template Name: Login
	 */
?>
<?php get_header('login'); ?>
<main class="cd-main-content-login page-template" Style='padding-bottom: 0px;'>

	<div class="page-home-login">
		<div class="row">
			<div>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				 	<?php the_content(); ?>

				<?php endwhile; else : ?>

				 	<p><?php _e( 'Sorry, no posts matched your criteria.', 'streamium' ); ?></p>

				<?php endif; ?>
			</div>
		</div>
	</div>


 <?php get_footer(); ?>