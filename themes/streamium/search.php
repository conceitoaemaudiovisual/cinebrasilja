<?php
	if($_GET['page']=='kids'){
		$page 	= 'kids';
		$classe	= 'page-kids-search';
		$query 	= " and tx.taxonomy = 'kids' ";
		echo "	<style>
    				h3 {
    					font-weight: bold;
    					color: #002244 !important;
    				}
					body {background: #f3f3f3 !important};
				</style>";
	}
?>
<?php get_header($page); ?>
	<main class="cd-main-content <?php echo $classe;?>">
		<div class="main-spacer"></div>
		<section class="categories">
			<?php
				global $wpdb;
				$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
				foreach($videosSerie as $videoSerie){
					$postsSeries = unserialize($videoSerie->meta_value);
					foreach($postsSeries as $postSeries){
						if($postSeries['video'] != '') {
							$episodios .= $virgula.$postSeries['video'];
							$virgula = ",";
						}
					}				}
				if($episodios=='') $episodios = -1500;
				$videosLocalizados = $wpdb->get_results("SELECT ID														 FROM wp_posts														 inner join wp_term_relationships tr on tr.object_id = wp_posts.ID														 inner join wp_term_taxonomy tx on tx.term_taxonomy_id = tr.term_taxonomy_id $query														 inner join wp_terms t on t.term_id = tx.term_id														 WHERE (((wp_posts.post_title LIKE '%".$_GET['s']."%') OR (wp_posts.post_excerpt LIKE '%".$_GET['s']."%') OR (wp_posts.post_content LIKE '%".$_GET['s']."%') OR (t.name LIKE '%".$_GET['s']."%')))														 AND wp_posts.post_type IN ('post', 'series', 'palestrante')														 AND wp_posts.post_status = 'publish'														 AND wp_posts.ID not in ($episodios)														 ORDER BY wp_posts.post_title DESC");				foreach($videosLocalizados as $videosLocalizado){					$vid++;					$videos[] = $videosLocalizado->ID;				}
				if($vid=='') $videos[] = '-1500';				$args = array(						'post__in' => $videos,						'post_type' => array( 'post', 'series','palestrante' ),				);
				$the_query = new WP_Query( $args );
			?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header-archive">
						<h3 class="pull-left"><?php printf( __( 'Pesquisar por: %s', 'streamium' ), get_search_query() ); ?></h3>
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.container-->
			<?php if ( $the_query->have_posts() ) : ?>
			<div class="container-fluid">
				<div class="row static-row static-row-first carousels">
					<?php

						$count = 0;
						$cat_count = 0;
						$total_count = $the_query->post_count;
						while ( $the_query->have_posts() ) : $the_query->the_post();
							$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
							$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
							$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
							$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
							$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
							$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
							$nonce = wp_create_nonce( 'streamium_likes_nonce' );
							$nonce = wp_create_nonce( 'streamium_likes_nonce' );
						?>
						<div class="col-md-5ths tile" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="static-<?php echo $cat_count; ?>">

							<div class="tile_inner" style="background-image: url(<?php echo esc_url($image[0]); ?>);" attr-image="<?php echo esc_url($image[0]); ?>">

								<?php if($post->premium) : ?>
									<div class="tile_payment_details">
										<div class="tile_payment_details_inner">
											<h2>Available on <?php echo str_replace(array("_"), " ", $post->plans[0]); ?></h2>
										</div>
									</div>
								<?php endif; ?>
								<?php if (function_exists('is_protected_by_s2member')) :
									$check = is_protected_by_s2member(get_the_ID());
									if($check) : ?>
									<div class="tile_payment_details">
										<div class="tile_payment_details_inner">
											<h2>Available on <?php
												$comma_separated = implode(",", $check);
												echo "plan " . $comma_separated;
											?></h2>
										</div>
									</div>
								<?php endif; endif; ?>

								<div class="content">
							      <div class="overlay" style="background-image: url(<?php echo esc_url($imageExpanded[0]); ?>);">
							        <div class="overlay-gradient"></div>
							        <a class="play-icon-wrap hidden-xs" href="<?php the_permalink(); ?>">
										<div class="play-icon-wrap-rel">
											<div class="play-icon-wrap-rel-ring"></div>
											<span class="play-icon-wrap-rel-play">
												<i class="fa fa-play fa-1x" aria-hidden="true"></i>
								        	</span>
							        	</div>
						        	</a>
						          	<div class="overlay-meta hidden-xs">
						            	<h4><?php the_title(); ?></h4>
						            	<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="static-<?php echo $cat_count; ?>" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
						          	</div>
							      </div>
							    </div>

							</div>

							<?php if(is_user_logged_in() && get_theme_mod( 'streamium_enable_premium' )):
						    		$userId = get_current_user_id();
						    		$percentageWatched = get_post_meta( get_the_ID(), 'user_' . $userId, true );
						    ?>
							    <div class="progress tile_progress">
								  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentageWatched; ?>"
								  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageWatched; ?>%">
								  </div>
								</div>
							<?php endif; ?>

						</div>
						<?php
							$count++;
  							if ($count % (isMobile() ? 1 : 5) == 0 || $count == $total_count) {
  						?>
  						</div>
  						</div>
  						<section class="s3bubble-details-full static-<?php echo $cat_count; ?>">
							<div class="s3bubble-details-full-overlay"></div>
			<div class="container-fluid s3bubble-details-inner-content">
				<div class="row">
					<div class="col-sm-12 col-xs-12 rel">
						<div class="synopis-outer synopis-outer-ajax ">
							<div class="synopis-middle">
								<div class="synopis-inner">
									<h2 class="synopis hidden-xs"></h2>
									<div class="synopis content"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
							<a class="play-icon-wrap synopis" href="#">
								<div class="play-icon-wrap-rel">
									<div class="play-icon-wrap-rel-ring play-ajax"></div>
									<span class="play-icon-wrap-rel-play">
										<i class="fa fa-play fa-3x" aria-hidden="true"></i>
									</span>
								</div>
							</a>
							<a href="#" class="synopis-video-trailer streamium-btns hidden-xs">Watch Trailer</a>
							<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
						</div><!--/.col-sm-12-->
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
						</section><div class="container-fluid"><div class="row static-row carousels">
					<?php $cat_count++; } ?>
					<?php endwhile; ?>
				</div><!--/.row-->
				<div class="row">
					<div class="col-sm-12">
						<?php if (function_exists("streamium_pagination")) {
						    streamium_pagination();
						} ?>
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
			<?php else : ?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header-archive">
						<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.row-->
			<?php endif; ?>
		</section><!--/.videos-->

		<div class="main-spacer"></div>

<?php get_footer(); ?>