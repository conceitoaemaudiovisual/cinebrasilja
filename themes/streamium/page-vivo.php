<?php

	/*
	 Template Name: Vivo
	 */
?>
<?php get_header(); ?>
<main class="cd-main-content page-template-page-minha-lista">
	<div class="main-spacer-vivo"></div>
	 	<div class="container-fluid">
 				<div class="titulo-aovivo">Ao Vivo</h3>


			<div class="page-vivo">

				<div class="row">
					<div>
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

							<?php the_content(); ?>

						<?php endwhile; else : ?>

							<p><?php _e( 'Sorry, no posts matched your criteria.', 'streamium' ); ?></p>

						<?php endif; ?>
					</div>
				</div>
			</div>

	 	</div>
	</div>

	<div class="main-spacer"></div>

 <?php get_footer(); ?>

