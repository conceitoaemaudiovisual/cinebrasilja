<?php

/**
 * Ajax post scipts for single post
 *
 * @return bool
 * @author  @sameast
 */
function streamium_single_video_scripts() {

    if( is_single() ){

    	global $post;

    	$nonce = wp_create_nonce( 'single_nonce' );
    	$s3videoid = get_post_meta( $post->ID, 's3bubble_video_code_meta_box_text', true );
    	$streamiumVideoTrailer = get_post_meta( $post->ID, 'streamium_video_trailer_meta_box_text', true );


    	if(is_user_logged_in()){
    		$userId = get_current_user_id();
    		$percentageWatched = get_post_meta( $post->ID, 'user_' . $userId, true );
    	}

    	if ( get_theme_mod( 'streamium_enable_premium' ) ) {

//    		if(pathinfo($s3videoid, PATHINFO_DIRNAME) !== "."){
//			    $s3videoid = pathinfo($s3videoid, PATHINFO_BASENAME);
//			}

			// Setup a array for codes
//			$codes = [];

			// Check for resume
//			$resume = !empty($percentageWatched) ? $percentageWatched : 0;

			// Check for a video trailer
//			if(isset($_GET['trailer']) && isset($streamiumVideoTrailer)){
//				$codes[] = $streamiumVideoTrailer;
//				$resume = 0;
//			}

			// Check if this post has programs
			$episodes = get_post_meta(get_the_ID(), 'repeatable_fields' , true);
			if(!empty($episodes)) {
				foreach ($episodes as $key => $value) :
					$codes[] = $value['codes'];
				endforeach;
			}else{
				$codes[] = $s3videoid;
			}

    		// Setup premium

	        wp_localize_script( 'streamium-production', 'video_post_object',
	            array(
	                'ajax_url' => admin_url( 'admin-ajax.php'),
	                'post_id' => $post->ID,
	                'percentage' => $resume,
	                'codes' => $codes,
	                'nonce' => $nonce
	            )
	        );

        }else{

        	//setup standard
	        wp_localize_script( 'streamium-production', 'video_post_object',
	            array(
	                'ajax_url' => admin_url( 'admin-ajax.php'),
	                'post_id' => $post->ID,
	                'code' => (!empty( $s3videoid ) && filter_var($s3videoid, FILTER_VALIDATE_URL)) ? $s3videoid : NULL
	            )
	        );

        }
    }
}

add_action('wp_enqueue_scripts', 'streamium_single_video_scripts');

/**
 * Ajax post scipts for content
 *
 * @return bool
 * @author  @sameast
 */
function streamium_get_dynamic_content() {

	global $wpdb;

	// Get params
	$cat = $_REQUEST['cat'];
	$postId = (int) $_REQUEST['post_id'];

    if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'streamium_likes_nonce' ) || ! isset( $_REQUEST['nonce'] ) ) {
        exit( "No naughty business please" );
    }

    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

    	$post_object = get_post( $postId );

    	if(!empty($post_object)){

    		$like_text = '';
		    if ( get_theme_mod( 'streamium_enable_premium' ) ) {

		    	$buildMeta = '<ul>';

				// Tags
				$posttags = get_the_tags($postId);
				$staring = 'Staring: ';
				if ($posttags) {
					$numItems = count($posttags);
					$i = 0;
				  	foreach($posttags as $tag) {

					  	$staring .= '<a href="/?s=' . esc_html( $tag->name ) . '">' . $tag->name . '</a>';
					  	if(++$i !== $numItems) {
				    		$staring .= ', ';
				  		}

				    }
//				    $buildMeta .= '<li class="synopis-meta-spacer">' . $staring . '</li>';
				}

				// Cats
				$categories = get_the_category($postId);
				if($post_object->post_type === 'tv'){
					$categories = get_terms( 'programs', array('hide_empty' => false) );
				}
				$genres = 'Genres: ';
				if ($categories) {
					$numItems = count($categories);
					$g = 0;
				  	foreach($categories as $cats) {

				  		$genres .= '<a href="' . esc_url( get_category_link( $cats->term_id ) ) . '">' . $cats->name . '</a>';
				  		if(++$g !== $numItems) {
				    		$genres .= ', ';
				  		}

				  	}
				}

				$buildMeta .= '<li class="synopis-meta-spacer">Released: <a href="/?s=all&date=' . get_the_date('Y/m/d', $postId) . '">' . get_the_date('l, F j, Y', $postId) . '</a></li></ul>';

				// Likes and reviews
		        $nonce = wp_create_nonce( 'streamium_likes_nonce' );
		    	$link = admin_url('admin-ajax.php?action=streamium_likes&post_id='. $postId .'&nonce='.$nonce);


		        $like_text = '<div class="synopis-premium-meta hidden-xs">
		        				<a id="like-count-' . $postId . '" class="streamium-review-like-btn streamium-btns streamium-reviews-btns" data-toggle="tooltip" title="CLICK TO LIKE!" data-id="' . $postId . '" data-nonce="' . $nonce . '">' . get_streamium_likes($postId) . '</a>
		        				<a class="streamium-list-reviews streamium-btns streamium-reviews-btns" data-id="' . $postId . '" data-nonce="' . $nonce . '">Read reviews</a>
							</div>';

		    }

		    $content = get_post_meta($post_object->ID,'sinopse-video',true);
		    $content .= json_decode(str_replace('\\u00a0',' ',json_encode(get_post_meta($post_object->ID,'ficha-video',true))));

		    $details = get_post_meta($post_object->ID,'detalhes-video',true);
			$postLinkID = $post_object->ID;

			$series = get_post_meta($post_object->ID,'video-serie',true);
			foreach($series as $indice=>$episodios){
				$series[$indice]['pos'] = $wpdb->get_var("select concat(substring(name, LOCATE(' ', name)+1, 10) ,(select substring(name, LOCATE(' ', name)+1, 10) from wp_terms where term_id = $episodios[temporada])) from wp_terms where term_id = $episodios[episodio]");
				if(isset($series[$indice]['pos']))
					$parametro[$indice] = $series[$indice]['pos'];
			}
			$videoSerieAtual = array_search(min($parametro),$parametro);
			if(isset($videoSerieAtual)){
				$postSerie = $post_object;
				$post_object = get_post($series[$videoSerieAtual][video]);
				$cat1 = get_term($series[$videoSerieAtual][temporada], 'Temporada_video');
				$cat2 = get_term($series[$videoSerieAtual][episodio],  'Temporada_video');
				$content = "<p class='temporada-titulo'>$cat1->name - $cat2->name</p>";
			    $content .= get_post_meta($post_object->ID,'sinopse-video',true);
			    $content .= "<span class='banner-ficha-tecnica'>".get_post_meta($post_object->ID,'ficha-video',true)."</span>";
				$postLinkID = $post_object->ID;
				$post_object = $postSerie;
				$isSerie = true;
			}

			global $current_user;
			get_currentuserinfo();

			$userID  	= $current_user->data->ID;
			$userList 	= get_user_meta($userID, 'my-list');
			$userList 	= $userList[0];
			$userList[999999] = 1;

			$videoList = array_search($post_object->ID, $userList);
			if($videoList !== false) $myListClass = "botao-minha-lista-del-interna"; else $myListClass = "botao-minha-lista-home-interna";
			$content .= "	<div class='container-banner-botoes-ajax'>
								<span class='$myListClass' attr='$post_object->ID'></span>
							</div>";

			$content .= "	<div class='video-like-buttom-home' Style='float:left;margin-left:15px;'>";
			$content .= 	do_shortcode('[likebtn identifier="'.$post_object->post_title.'" theme="custom" btn_size="43" icon_l_url="https://www.cinebrasilja.com/wp-content/uploads/icone-aba-aberta-hand-up.png" icon_l_url_v="https://www.cinebrasilja.com/wp-content/uploads/icone-aba-aberta-hand-up-hover.png" icon_d_url="https://www.cinebrasilja.com/wp-content/uploads/icone-aba-aberta-hand-down.png" icon_d_url_v="https://www.cinebrasilja.com/wp-content/uploads/icone-aba-aberta-hand-down-hover.png" bg_c="transparent" brdr_c="transparent" lang="pt_BR" show_like_label="0" icon_like_show="0" icon_dislike_show="0" counter_type="single_number" tooltip_enabled="0" counter_show="0" share_enabled="0" popup_disabled="1"]');
			$content .= "	</div>";


			$content = "	<div class='col-sm-5 col-xs-5 rel'>$content</div>
							<div class='detalhes-video-view'>$details</div>";


			$tituloEpisodios = get_post_meta($postId, 'titulo-episodios', true);
			if($tituloEpisodios == '')
				$tituloEpisodios = "Epis&oacute;dios";



			if($isSerie)
				$content .= "	<div class='episodios-serie-view' Style='position: absolute;top:20%;float:left;width:100%;margin-bottom:35px;margin-top:-10px;background:url(/wp-content/uploads/fundo-preto-episodios.png) top left repeat;display:none;'></div>
								<div class='tablist-container' Style='position:absolute;bottom:10px;left:0;width:100%;text-align:center;z-index:9999;'>
									<div class='tablist-itens-video'>
										<span class='tablist-ficha-tecnica' attr='$post_object->ID'>FICHA T&Eacute;CNICA</span>
										<span class='tablist-item-episodio' attr='$post_object->ID'>$tituloEpisodios</span>
										<span class='tablist-item-semelhante'attr='$post_object->ID'>T&iacute;tulos Semelhantes</span><!--
										<span class='tablist-item-detalhes'attr='$post_object->ID'>Detalhes</span>-->
									</div>
								</div>";
			else if($post_object->post_type=='palestrante')
				$content .= "	<div class='episodios-serie-view' Style='position: absolute;top:20%;float:left;width:100%;margin-bottom:35px;margin-top:-10px;background:url(/wp-content/uploads/fundo-preto-episodios.png) top left repeat;display:none;'></div>
								<div class='tablist-container' Style='position:absolute;bottom:10px;left:0;width:100%;text-align:center;z-index:9999;'>
									<div class='tablist-itens-video'>
										<span class='tablist-ficha-tecnica' attr='$post_object->ID'>FICHA T&Eacute;CNICA</span>
										<span class='tablist-item-episodio' attr='$post_object->ID'>Palestras</span>
										<span class='tablist-item-semelhante'attr='$post_object->ID'>T&iacute;tulos Semelhantes</span><!--
										<span class='tablist-item-detalhes'attr='$post_object->ID'>Detalhes</span>-->
									</div>
								</div>";
			else
				$content .= "	<div class='episodios-serie-view' Style='position: absolute;top:20%;float:left;width:100%;margin-bottom:35px;margin-top:-10px;background:url(/wp-content/uploads/fundo-preto-episodios.png) top left repeat;display:none;'></div>
								<div class='tablist-container' Style='position:absolute;bottom:10px;left:0;width:100%;text-align:center;z-index:9999;'>
									<div class='tablist-itens'>
										<span class='tablist-ficha-tecnica' attr='$post_object->ID'>FICHA T&Eacute;CNICA</span>
										<span class='tablist-item-semelhante'attr='$post_object->ID'>T&iacute;tulos Semelhantes</span><!--
										<span class='tablist-item-detalhes'attr='$post_object->ID'>Detalhes</span>-->
									</div>
								</div>";
		    if(isMobile()){
		    	$content = (empty($post_object->post_excerpt) ? strip_tags($post_object->post_content) : $post_object->post_excerpt);
		    }
	    	$fullImage  = str_replace('http://','https://',wp_get_attachment_image_src( get_post_thumbnail_id( $postId ), 'streamium-home-slider' ));
	    	$streamiumVideoTrailer = get_post_meta( $postId, 'streamium_video_trailer_meta_box_text', true );

	    	echo json_encode(
		    	array(
		    		'error' => false,
		    		'cat' => $cat,
		    		'title' => $post_object->post_title,
		    		'content' => str_replace('http://www.cinebrasilplay.com','https://www.cinebrasilja.com',$content),
		    		'bgimage' => isset($fullImage) ? $fullImage[0] : "",
		    		'trailer' => $streamiumVideoTrailer,
		    		'href' 	  => get_permalink($postLinkID),
		    		'viden' => get_post_meta($postLinkID, 'video-anexado-secundario', true),
		    		'vidpt' => get_post_meta($postLinkID, 'video-anexado', true),
		    		'post' => $post_object
		    	)
		    );

	    }else{

	    	echo json_encode(
		    	array(
		    		'error' => true,
		    		'message' => 'We could not find this post.'
		    	)
		    );

	    }

        die();

    }
    else {

        wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
        exit();

    }

}

add_action( 'wp_ajax_nopriv_streamium_get_dynamic_content', 'streamium_get_dynamic_content' );
add_action( 'wp_ajax_streamium_get_dynamic_content', 'streamium_get_dynamic_content' );

/**
 * Ajax post scipts for content
 *
 * @return bool
 * @author  @sameast
 */
function streamium_programs_get_dynamic_content() {

	global $wpdb;

	// Get params
	$cat = $_REQUEST['cat'];
	$termId =  (int) $_REQUEST['term_id'];

    if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'streamium_likes_nonce' ) || ! isset( $_REQUEST['nonce'] ) ) {
        exit( "No naughty business please" );
    }

    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

    	$post_object = get_term( $termId, 'tv' );

    	if(!empty($post_object)){

    		$like_text = '';
		    if ( get_theme_mod( 'streamium_enable_premium' ) ) {
		        $nonce = wp_create_nonce( 'streamium_likes_nonce' );
		    	$link = admin_url('admin-ajax.php?action=streamium_likes&post_id='. $termId .'&nonce='.$nonce);
		        $like_text = '<div class="synopis-premium-meta hidden-xs">
								<div class="streamium-review-like-btn">
			                        <a class="like-button"  data-id="' . $termId . '" data-nonce="' . $nonce . '">' . __( 'Like it' ) . '</a>
			                        <span id="like-count-' . $termId . '" class="like-count">' . get_streamium_likes($termId) . '</span>
			                    </div>
			                    <div class="streamium-review-reviews-btn">
			                        <a class="streamium-list-reviews" data-id="' . $termId . '" data-nonce="' . $nonce . '">Read reviews</a>
			                    </div>
							</div>';
		    }
    		$content = (isMobile()) ? strip_tags($post_object->description) : $post_object->description . $like_text;
	    	$fullImage  =get_tax_meta($termId,'streamium_program_image');
	    	//$streamiumVideoTrailer = get_post_meta( $termId, 'streamium_video_trailer_meta_box_text', true );

	    	echo json_encode(
		    	array(
		    		'error' => false,
		    		'cat' => $cat,
		    		'title' => $post_object->name,
		    		'content' => $content,
		    		'bgimage' =>  $fullImage[0]['url'],
		    		'trailer' => '',//$streamiumVideoTrailer,
		    		'href' => get_term_link( $termId )
		    	)
		    );

	    }else{

	    	echo json_encode(
		    	array(
		    		'error' => true,
		    		'message' => 'We could not find this post.'
		    	)
		    );

	    }

        die();

    }
    else {

        wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
        exit();

    }

}

add_action( 'wp_ajax_nopriv_streamium_programs_get_dynamic_content', 'streamium_programs_get_dynamic_content' );
add_action( 'wp_ajax_streamium_programs_get_dynamic_content', 'streamium_programs_get_dynamic_content' );