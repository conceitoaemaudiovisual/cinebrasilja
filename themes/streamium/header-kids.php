<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head data-cast-api-enabled="true">
	<!-- Meta Data -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- favicons -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_bloginfo('template_url'); ?>/production/img/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_bloginfo('template_url'); ?>/production/img/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_url'); ?>/production/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_bloginfo('template_url'); ?>/production/img/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_url'); ?>/production/img/favicon-16x16.png">
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-99562718-1', 'auto');
	  ga('send', 'pageview');

	</script>

	<!-- Trackback -->
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<!-- Wordpress Scripts -->
	<?php wp_head(); ?>

</head>
<?php
	if($_GET['page']=='kids'){
		$classe	= 'page-template-page-kids';
	}
?>
<body <?php body_class($classe); ?>>


	<header class="cd-main-header-paginkids fixed">
	    <a class="cd-logo" href="/kids/"><img src='/wp-content/uploads/logotipo-cinebrasil-cor.png' alt='Kids'></a>
		<a href='/kids/' class="logo-kids-centro">
			<img src='/wp-content/uploads/logotipo-pagina-kids.png'>
		</a>

		<nav class='cd-nav-container'>
			<ul id='cd-primary-nav-kids' class='cd-primary-nav is-fixed'>
				<li id='menu-item-6-kids' class='menu-item menu-item-type-custom menu-item-object-custom menu-item-6'>
					<a href='/'>
						<img src='/wp-content/uploads/bt-sair-do-kids.png'>
					</a>
				</li>
				<li id='menu-item-3-kids' class='menu-item menu-item-type-custom menu-item-object-custom menu-item-3'>
					<a href="#cd-search"  class="cd-search-trigger">
						<img src='/wp-content/uploads/icone-buscar-kids.png'>
						<span class='menu-buscar cor-categorias'>Buscar</span>
					</a>
					<div id="cd-search" class="cd-search">
						<form method="get" action="<?php echo esc_url(home_url('/')); ?>" role="search">
								<input type="search" name="s" placeholder="<?php _e( 'Buscar...', 'streamium' ); ?>">
								<input type="hidden" name='page' value="kids">
						</form>
					</div>
				</li>
				<li id='menu-item-1-kids' class='menu-item menu-item-type-custom menu-item-object-custom menu-item-1-kids'>
					<a href='#'>
						<span class='menu-navegar cor-categorias'>Categorias</span>
					</a>
					<ul class='sub-menu-kids'>
						<li class='sub-menu-item-container-kids'>
							<ul class='sub-menu-item-1-kids'>
								<li>
									<a href='/kids-adicionados-recentemente/'>
										Adicionados recentemente
									</a>
								</li>
								<li>
									<a href='/kids-mais-assistidos/'>
										Mais assistidos
									</a>
								</li>
								<li>
									<a href='/kids/para-ver-em-familia/'>
										Para ver em fam&iacute;lia
									</a>
								</li>
								<li>
									<a href='/kids/para-ver-com-os-amigos/'>
										Para ver com os amigos
									</a>
								</li>
								<li>
									<a href='/kids/biografico/'>
										Biogr&aacute;ficos
									</a>
								</li>
								<li>
									<a href='/kids/acao/'>
										A&ccedil;&atilde;o
									</a>
								</li>
								<li>
									<a href='/kids/aventura/'>
										Aventura
									</a>
								</li>
								<li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
<!--
		<ul class="cd-header-buttons">
			<li><a class="cd-search-trigger" href="#cd-search"><?php _e( 'Search', 'streamium' ); ?><span></span></a></li>
			<li><a class="cd-nav-trigger" href="#cd-primary-nav"><?php _e( 'Menu', 'streamium' ); ?><span></span></a></li>
		</ul>
-->


	</header>