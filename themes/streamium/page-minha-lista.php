<?php
	global $current_user;
	get_currentuserinfo();
	if(!isset($current_user->data->ID))
		header('location:/home');
	/*
	 Template Name: Minha Lista
	 */

	$userID  	= $current_user->data->ID;
	$userList 	= get_user_meta($userID, 'my-list');
	$userList 	= array_reverse($userList[0]);

?>
<?php get_header(); ?>
	<main class="cd-main-content ">
		<div class="main-spacer"></div>
		<section class="categories">
			<?php

				$args = array(
					'post_type' => array( 'post', 'series' ),
					'post__in' => $userList,
					'orderby'   => 'post__in',
				);

				$the_query = new WP_Query($args);

				if ( $the_query->have_posts() ) :
			?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header-archive carousels">
						<h3>Minha Lista</h3>
						<?php if(get_theme_mod( 'streamium_enable_premium' )) : ?>
							<div class="dropdown video-header-archive-dropdown">
							  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							    FILTER
							    <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
							    <li><a href="?sort=all">View All</a></li>
							    <li><a href="?sort=reviewed">Most Reviews</a></li>
							    <li><a href="?sort=newest">Recently Added</a></li>
							    <li><a href="?sort=oldest">Oldest First</a></li>
							  </ul>
							</div>
						<?php endif; ?>
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.container-->
			<div class="container-fluid">
				<div class="row static-row static-row-first carousels">
					<?php

						$count = 0;
						$cat_count = 0;
						$total_count = $the_query->post_count;

						while ( $the_query->have_posts() ) : $the_query->the_post();
							global $post;
							$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
							$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
							$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
							$imgMetaHover3ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-3_thumbnail_id',true);
							$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
							$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
							$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
							$imageExpanded3   = wp_get_attachment_image_src( $imgMetaHover3ID, 'large' );
							$nonce = wp_create_nonce( 'streamium_likes_nonce' );
							$linkID = $post->ID;
							if($post->post_type=='series'){
								$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
								$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
								$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
								$imgMetaHover3ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-3_thumbnail_id',true);
								$image  		 = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
								$imageExpanded   = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
								$imageExpanded2  = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
								$series = get_post_meta($post->ID,'video-serie',true);
								foreach($series as $indice=>$episodios){
									$series[$indice]['pos'] = $wpdb->get_var("select concat(substring(name, LOCATE(' ', name)+1, 10) ,(select substring(name, LOCATE(' ', name)+1, 10) from wp_terms where term_id = $episodios[temporada])) from wp_terms where term_id = $episodios[episodio]");
									if(isset($series[$indice]['pos']))
										$parametro[$indice] = $series[$indice]['pos'];
								}
								$videoSerieAtual = array_search(min($parametro),$parametro);
								$episodio = get_post($series[$videoSerieAtual][video]);
								$linkID = $episodio->ID;
							}
						?>
						<div class="col-xs-6 col-md-5ths tile" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="static-<?php echo $cat_count; ?>">

							<div class="tile_inner" style="background-image: url(<?php echo esc_url($image[0]); ?>);"  attr-image="<?php echo esc_url($image[0]); ?>">
								<div class="content">
							      <div class="overlay background-expanded background-img-<?php echo $post->ID;?>" attr0="<?php echo esc_url($imageExpanded[0]); ?>" attr1="<?php echo esc_url($imageExpanded2[0]); ?>" attr2="<?php echo esc_url($imageExpanded3[0]);?>">
							        <div class="overlay-gradient"></div>
							        <a class="play-icon-wrap hidden-xs" href="<?php the_permalink(); ?>">
										<div class="play-icon-wrap-rel">
											<div class="play-icon-wrap-rel-ring"></div>
											<span class="play-icon-wrap-rel-play">
												<i class="fa fa-play fa-1x" aria-hidden="true"></i>
								        	</span>
							        	</div>
						        	</a>
						          	<div class="overlay-meta hidden-xs">
						            	<h4><?php the_title(); ?></h4>
						            	<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="static-<?php echo $cat_count; ?>" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
						          	</div>
							      </div>
							    </div>

							</div>

						</div>
						<?php
							$count++;
  							if ($count % (isMobile() ? 2 : 5) == 0 || $count == $total_count) {
  						?>
  						</div>
  						</div>
  						<section class="s3bubble-details-full static-<?php echo $cat_count; ?>">
							<div class="s3bubble-details-full-overlay"></div>
			<div class="container-fluid s3bubble-details-inner-content">
				<div class="row">
					<div class="col-sm-12 col-xs-12 rel">
						<div class="synopis-outer synopis-outer-ajax ">
							<div class="synopis-middle">
								<div class="synopis-inner">
									<h2 class="synopis hidden-xs"></h2>
									<div class="synopis content"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
							<a class="play-icon-wrap synopis" href="#">
								<div class="play-icon-wrap-rel">
									<div class="play-icon-wrap-rel-ring play-ajax"></div>
									<span class="play-icon-wrap-rel-play">
										<i class="fa fa-play fa-3x" aria-hidden="true"></i>
									</span>
								</div>
							</a>
							<a href="#" class="synopis-video-trailer streamium-btns hidden-xs">Watch Trailer</a>
							<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
						</div><!--/.col-sm-12-->
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
						</section><div class="container-fluid"><div class="row static-row carousels">
					<?php $cat_count++; } ?>
					<?php endwhile; ?>
				</div><!--/.row-->
				<div class="row">
					<div class="col-sm-12">
						<?php if (function_exists("streamium_pagination")) {
						    streamium_pagination();
						} ?>
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
			<?php endif; ?>
		</section><!--/.videos-->

		<div class="main-spacer"></div>

<?php get_footer(); ?>