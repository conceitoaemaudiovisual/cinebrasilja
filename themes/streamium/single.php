<?php
	global $current_user;
	get_currentuserinfo();
	if(!isset($current_user->data->ID))
		header('location:/home');

	global $post;

	if($post->post_type=='palestrante'){
		$fotoPalestrante = get_post_meta($post->ID,'palestrante_thumb-palestrante-interna_thumbnail_id',true);
		$fotoPalestrante = wp_get_attachment_image_src( $fotoPalestrante, 'full' );
?>
	<?php get_header(); ?>

			<div class="row" Style='margin-top:60px;'>
				<div class="col-sm-12 col-sm-12 page-palestrantes-interna" Style='margin:0 0 40px;background: url(<?php echo $fotoPalestrante[0];?>) top left no-repeat;background-size:auto 100%;max-height:460px auto;overflow:hidden;background-color:#000000;'>
					<div class="col-sm-5 col-sm-5" Style='float:right;margin-right:30px;'>
						<h3 Style="float:left;font-family: 'Lato', sans-serif;font-size:30px;color:#fff;"><?php echo $post->post_title;?></h3>
						<h6 Style="float:left;font-family: 'Lato', sans-serif;"><?php echo $post->post_content;?></h6>
					</div>
				</div><!--/.col-sm-12-->
			</div>

			<main class="cd-main-content">
				<section class="categories page-palestrantes">
					<div class="container-fluid">
			<?php
				global $wpdb;
				$videosLocalizados = $wpdb->get_results("SELECT post_id ID
														 from wp_postmeta
														 where meta_key = 'video-palestrante'
														 and meta_value = $post->ID");

				foreach($videosLocalizados as $videosLocalizado){
					$vid++;
					$videos[] = $videosLocalizado->ID;
				}
				$args = array(
						'post__in' => $videos,
						'post_type' => array( 'post', 'series'),
				);
				$the_query = new WP_Query( $args );
			?>
			<?php if ( $the_query->have_posts() ) : ?>
			<div class="container-fluid" Style='margin: 0px !important;padding-left:0px !important;'>
				<div class="row static-row static-row-first carousels carrosel-horizontal">
					<?php

						$count = 0;
						$cat_count = 0;
						$total_count = $the_query->post_count;
						while ( $the_query->have_posts() ) : $the_query->the_post();
							$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
							$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
							$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
							$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
							$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
							$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
							$nonce = wp_create_nonce( 'streamium_likes_nonce' );
							$nonce = wp_create_nonce( 'streamium_likes_nonce' );
						?>
						<div class="tile slick-slide slick-active" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="static-<?php echo $cat_count; ?>">

							<div class="tile_inner" style="background-image: url(<?php echo esc_url($image[0]); ?>);background-size:auto 100%;" attr-image="<?php echo esc_url($image[0]); ?>">
								<div class="content">
							      <div class="overlay background-expanded" style="background-image: url(<?php echo esc_url($imageExpanded[0]); ?>);">
							        <div class="overlay-gradient"></div>
							        <a class="play-icon-wrap hidden-xs" href="<?php the_permalink(); ?>">
										<div class="play-icon-wrap-rel">
											<div class="play-icon-wrap-rel-ring"></div>
											<span class="play-icon-wrap-rel-play">
												<i class="fa fa-play fa-1x" aria-hidden="true"></i>
								        	</span>
							        	</div>
						        	</a>
						          	<div class="overlay-meta hidden-xs">
						            	<h4><?php the_title(); ?></h4>
						            	<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="static-<?php echo $cat_count; ?>" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
						          	</div>
							      </div>
							    </div>

							</div>


						</div>
						<?php
							$count++;
  							if ($count % (isMobile() ? 1 : 6) == 0 || $count == $total_count) {
  						?>
  						</div>
  						</div>
  						<section class="s3bubble-details-full static-<?php echo $cat_count; ?>">
							<div class="s3bubble-details-full-overlay"></div>
			<div class="container-fluid s3bubble-details-inner-content">
				<div class="row">
					<div class="col-sm-12 col-xs-12 rel">
						<div class="synopis-outer synopis-outer-ajax ">
							<div class="synopis-middle">
								<div class="synopis-inner">
									<h2 class="synopis hidden-xs"></h2>
									<div class="synopis content"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
							<a class="play-icon-wrap synopis" href="#">
								<div class="play-icon-wrap-rel">
									<div class="play-icon-wrap-rel-ring play-ajax"></div>
									<span class="play-icon-wrap-rel-play">
										<i class="fa fa-play fa-3x" aria-hidden="true"></i>
									</span>
								</div>
							</a>
							<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
						</div><!--/.col-sm-12-->
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
						</section><div class="container-fluid" Style='margin: 50px 0px !important;padding-right: 0px;padding-left: 0px;'><div class="row static-row carousels" Style='margin:20px 0 0 0;'>
					<?php $cat_count++; } ?>
					<?php endwhile; ?>
				</div><!--/.row-->
				<div class="row">
					<div class="col-sm-12">
						<?php if (function_exists("streamium_pagination")) {
						    streamium_pagination();
						} ?>
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
			<?php else : ?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header-archive">
						<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.row-->
			<?php endif; ?>
		</section><!--/.videos-->

		<div class="main-spacer"></div>


















		<?php get_footer(); ?>

<?php
	}else if($post->post_type=='series'){
?>





<?php get_header(); ?>
	<main class="cd-main-content">
		<div class="main-spacer"></div>
		<section class="categories">
			<?php
				global $wpdb;
				$videosSerie = get_post_meta($post->ID, 'video-serie',true);
				foreach($videosSerie as $videoSerie){
					if($videoSerie['video'] != '') {
						$episodios[$videoSerie['episodio']] = $videoSerie['video'];
					}
				}
				ksort($episodios);
				$args = array(
						'post__in' => $episodios,
						'post_type' => array( 'post' ),
						'orderby' => 'post__in',
						'order'   => 'ASC',
				);
				$the_query = new WP_Query( $args );
			?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header-archive">
						<h3 class="pull-left"><?php echo "Voc&ecirc est&aacute; em $post->post_title";?></h3>
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.container-->
			<?php if ( $the_query->have_posts() ) : ?>
			<div class="container-fluid">
				<div class="row static-row static-row-first carousels">
					<?php

						$count = 0;
						$cat_count = 0;
						$total_count = $the_query->post_count;
						while ( $the_query->have_posts() ) : $the_query->the_post();
							$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
							$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
							$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
							$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
							$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
							$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
							$nonce = wp_create_nonce( 'streamium_likes_nonce' );
							$nonce = wp_create_nonce( 'streamium_likes_nonce' );
						?>
						<div class="col-md-5ths tile" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="static-<?php echo $cat_count; ?>">

							<div class="tile_inner" style="background-image: url(<?php echo esc_url($image[0]); ?>);" attr-image="<?php echo esc_url($image[0]); ?>">

								<?php if($post->premium) : ?>
									<div class="tile_payment_details">
										<div class="tile_payment_details_inner">
											<h2>Available on <?php echo str_replace(array("_"), " ", $post->plans[0]); ?></h2>
										</div>
									</div>
								<?php endif; ?>
								<?php if (function_exists('is_protected_by_s2member')) :
									$check = is_protected_by_s2member(get_the_ID());
									if($check) : ?>
									<div class="tile_payment_details">
										<div class="tile_payment_details_inner">
											<h2>Available on <?php
												$comma_separated = implode(",", $check);
												echo "plan " . $comma_separated;
											?></h2>
										</div>
									</div>
								<?php endif; endif; ?>

								<div class="content">
							      <div class="overlay" style="background-image: url(<?php echo esc_url($imageExpanded[0]); ?>);">
							        <div class="overlay-gradient"></div>
							        <a class="play-icon-wrap hidden-xs" href="<?php the_permalink(); ?>">
										<div class="play-icon-wrap-rel">
											<div class="play-icon-wrap-rel-ring"></div>
											<span class="play-icon-wrap-rel-play">
												<i class="fa fa-play fa-1x" aria-hidden="true"></i>
								        	</span>
							        	</div>
						        	</a>
						          	<div class="overlay-meta hidden-xs">
						            	<h4><?php the_title(); ?></h4>
						            	<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="static-<?php echo $cat_count; ?>" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
						          	</div>
							      </div>
							    </div>

							</div>

							<?php if(is_user_logged_in() && get_theme_mod( 'streamium_enable_premium' )):
						    		$userId = get_current_user_id();
						    		$percentageWatched = get_post_meta( get_the_ID(), 'user_' . $userId, true );
						    ?>
							    <div class="progress tile_progress">
								  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentageWatched; ?>"
								  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageWatched; ?>%">
								  </div>
								</div>
							<?php endif; ?>

						</div>
						<?php
							$count++;
  							if ($count % (isMobile() ? 1 : 5) == 0 || $count == $total_count) {
  						?>
  						</div>
  						</div>
  						<section class="s3bubble-details-full static-<?php echo $cat_count; ?>">
							<div class="s3bubble-details-full-overlay"></div>
			<div class="container-fluid s3bubble-details-inner-content">
				<div class="row">
					<div class="col-sm-12 col-xs-12 rel">
						<div class="synopis-outer synopis-outer-ajax ">
							<div class="synopis-middle">
								<div class="synopis-inner">
									<h2 class="synopis hidden-xs"></h2>
									<div class="synopis content"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
							<a class="play-icon-wrap synopis" href="#">
								<div class="play-icon-wrap-rel">
									<div class="play-icon-wrap-rel-ring play-ajax"></div>
									<span class="play-icon-wrap-rel-play">
										<i class="fa fa-play fa-3x" aria-hidden="true"></i>
									</span>
								</div>
							</a>
							<a href="#" class="synopis-video-trailer streamium-btns hidden-xs">Watch Trailer</a>
							<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
						</div><!--/.col-sm-12-->
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
						</section><div class="container-fluid"><div class="row static-row carousels" Style='margin:20px 0 0 0;'>
					<?php $cat_count++; } ?>
					<?php endwhile; ?>
				</div><!--/.row-->
				<div class="row">
					<div class="col-sm-12">
						<?php if (function_exists("streamium_pagination")) {
						    streamium_pagination();
						} ?>
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
			<?php else : ?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header-archive">
						<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.row-->
			<?php endif; ?>
		</section><!--/.videos-->

		<div class="main-spacer"></div>

<?php get_footer(); ?>



	<?php }else{
?>

<?php get_header('video'); ?>
<div class="video-main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php
	$midia = get_post_meta($post->ID, 'video-anexado', true);
	if($midia == '')
		$midia = get_post_meta($post->ID, 'video-anexado-secundario', true);
	if(isset($_POST['midia']))
		$midia = $_POST['midia'];

	$videoUser = $wpdb->get_var("select Video_time from aux_users_videos where user_ID = ".$current_user->data->ID." and Video_ID = $post->ID and Video_porcento < 100");
	$tempoAssistindo = $wpdb->get_var("select Video_time from aux_users_videos where user_ID = ".$current_user->data->ID." and Video_ID = $post->ID and Video_porcento < 100  order by ID desc limit 1");
	if($tempoAssistindo=='') $tempoAssistindo = -1;
	if($videoUser=='undefined') $videoUser = 'false';
	if($videoUser=='') $videoUser = 'false';
?>
		<script type='text/javascript' samba-player-api='player' src='https://player.sambatech.com.br/v3/samba.player.api.js'></script>
		<script type='text/javascript'>
			jQuery(document).ready(function($){
				var tempoAssistindo = 0;

				var playerConfig = {
					height: 360,
					width: 640,
					ph:'e416100f0c85f292fbc2ad198c88ae11',
					m: '<?php echo $midia;?>',
					events:{
						"onListen": eventListener,
						"onFinish": eventListener,
						"onStart": iniciaTempoVisualizacao,
					},
					playerParams:{
						wideScreen: true,
						html5:true,
						startOutput: '480p',
						resume:<?php echo $videoUser;?>,
						cast:true,
						captionTheme:['ffffff','28','pt-BR'],
						autoStart:true,
					}
				};
				var player = new SambaPlayer('player', playerConfig);

				alturaFrame	= jQuery('#player iframe').height();
				alturaTotal	= jQuery(document).height();
				fator = '1.'+parseInt(((alturaFrame * 100)/alturaTotal), 10);
				larguraNova = jQuery(document).height()*fator;

				jQuery('#player iframe').css("height", alturaTotal+'px').css("width", '100%').css('margin','0 auto');
				jQuery('#player').css("text-align", 'center');

				function iniciaTempoVisualizacao (){
					$('.episodios-serie-view').hide();
					jQuery('.player-samba').css('opacity','1');
				}

				var userInicio = 0;
				function eventListener(player) {
					var porcento = player.duration/100;
					if(player.event=='onFinish'){
						var xmlhttp = new XMLHttpRequest();
						xmlhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
								jQuery('.player-samba').css('opacity','0.4');
								jQuery('.video-like-buttom').show();
								jQuery('.icone-voltar-video').css('z-index','9999').css('position','relative');
								jQuery('.episodios-serie-view').append('<h2>Assista tamb&eacute;m</h2>'+this.responseText).fadeIn();
								jQuery('.bxslider-series').bxSlider({
									minSlides: 2,
									maxSlides: 12,
									slideWidth: 270	,
									slideMargin: 10,
									pager:false
								});
							}
						};
						xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/wp-content/plugins/integracao-samba-videos/functions/mostra-titulos-semelhantes.php?pID=$post->ID", true);
						xmlhttp.send();
					}
					if(player.event=='onListen'){
						assitido = parseInt(player.eventParam/porcento);
						if(assitido==0) assitido = '0.01';
						if(userInicio != assitido){
							userInicio = assitido;

							var xmlhttp = new XMLHttpRequest();
							xmlhttp.onreadystatechange = function() {
								if (this.readyState == 4 && this.status == 200) {
									//if(tempoAssistindo==0)
										//finalizaTempoVisualizacao();
								}
							};
//							console.log("https://"+jQuery(location).attr('hostname')+"/api/posts/update_time_video_user/?userID=<?php echo $current_user->data->ID;?>&postID=<?php echo $post->ID;?>&tempo="+player.eventParam+"&porcent="+assitido+"&SessaoVideoTime="+tempoAssistindo+'&totalTime='+player.duration);
							xmlhttp.open("GET", "https://"+jQuery(location).attr('hostname')+"/api/posts/update_time_video_user/?userID=<?php echo $current_user->data->ID;?>&postID=<?php echo $post->ID;?>&tempo="+player.eventParam+"&porcent="+assitido+"&SessaoVideoTime="+tempoAssistindo+'&totalTime='+player.duration, true);
							xmlhttp.send();
						}
					}
				}

			});
		</script>
		<div id='player' class='player-samba' Style='float:left;width:100%;'></div>
		<div class="episodios-serie-view assista-tambem-interna" style="position: absolute;top:10%;float:left;width:100%;margin-bottom:35px;margin-top:-10px;background:url(/wp-content/uploads/fundo-preto-episodios.png) top left repeat;display:none;">
			<div class="video-like-buttom" Style='display:none;text-align: center;padding: 20px;margin-bottom:15px;'>
				<?php echo do_shortcode('[likebtn identifier="'.$post->post_title.'" theme="custom" btn_size="43" icon_l_url="https://www.cinebrasilja.com/wp-content/uploads/icone-aba-aberta-hand-up.png" icon_l_url_v="https://www.cinebrasilja.com/wp-content/uploads/icone-aba-aberta-hand-up-hover.png" icon_d_url="https://www.cinebrasilja.com/wp-content/uploads/icone-aba-aberta-hand-down.png" icon_d_url_v="https://www.cinebrasilja.com/wp-content/uploads/icone-aba-aberta-hand-down-hover.png" bg_c="transparent" brdr_c="transparent" lang="pt_BR" show_like_label="0" icon_like_show="0" icon_dislike_show="0" counter_type="single_number" tooltip_enabled="0" counter_show="0" share_enabled="0" popup_disabled="1"]');?>
			</div>
		</div>
		<style>
		.lb-style-large .lb-voted .lb-dislike-label{
		    color: #555555 !important;
		}
		</style>

	 <?php endwhile; else : ?>
	 	<p><?php _e( 'Sorry, no posts matched your criteria.', 'streamium' ); ?></p>
	 <?php endif; ?>

</div>
<?php
	if (is_user_logged_in()) :
	   update_post_meta($post->ID,'recently_watched',current_time('mysql'));
	   update_post_meta($post->ID,'recently_watched_user_id',get_current_user_id());
	endif;
?>
<?php get_fotter(); ?>

<?php }?>


