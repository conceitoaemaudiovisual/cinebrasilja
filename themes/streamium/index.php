<?php
	session_start();
	ob_start();

	global $current_user;
	get_currentuserinfo();
	if(!isset($current_user->data->ID))
		header('location:/home');
/*
	if(isset($_COOKIE['userloginCookie'])){
		$dadosLogin = explode("|",$_COOKIE['userloginCookie']);
		wp_set_current_user( $dadosLogin[0], $dadosLogin[1] );
		wp_set_auth_cookie( $dadosLogin[0] );
		do_action( 'wp_login', $dadosLogin[1] );
	}else{
		if(isset($current_user->data->ID)){
			$dadosLogin = (array) $current_user->data;
			$dadosLogin = $dadosLogin[ID]."|".$dadosLogin[user_login];
			setcookie( "userloginCookie", "$dadosLogin", time()+(60*60*24*30) );
		}
	}
*/
	$email 		= "operacao@cinebrasil.tv";
	$token 		= "A184D628FD8248B19E408D8F9122DB06";
/*
	$userID 	= $current_user->data->ID;
	$codRetorno = get_user_meta($userID,'codido-retorno-pagamento',true);
	$tracker 	= get_user_meta($userID,'identificador-pagseguro',true);
	if($userID != ""){
		if($codRetorno != ""){
			$retorno = "https://ws.pagseguro.uol.com.br/v2/pre-approvals/$codRetorno?email=$email&token=$token";
			$ch = curl_init($retorno);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$data = utf8_decode(curl_exec($ch));
			curl_close($ch);
			$result = simplexml_load_string ($data, 'SimpleXmlElement', LIBXML_NOERROR+LIBXML_ERR_FATAL+LIBXML_ERR_NONE);
			$xml = new SimpleXmlElement($data, LIBXML_NOCDATA);
			$dadosRetorno = json_decode( json_encode($xml) , 1);
		}

		if($current_user->caps['administrator'] == 1) $dadosRetorno['status'] = "ACTIVE";
		if($current_user->caps['colaborador'] == 1) $dadosRetorno['status'] = "ACTIVE";
		if($dadosRetorno['status'] != "ACTIVE")
			header('location:https://www.cinebrasilja.com/aviso-assinatura/');
	}
*/
	get_header();
?>
	<main class="cd-main-content" id='list-perfil-id' userid='<?php echo $current_user->data->ID;?>'>
		<section class="hero">
			<button class="streamium-prev fa fa-angle-left" aria-hidden="true"></button>
			<div class="hero-slider">
				<?php
					$userID  	= $current_user->data->ID;
					$userList 	= get_user_meta($userID, 'my-list');
					$userList 	= $userList[0];
					$args = array(
						'post_status' 		  => 'publish',
						'posts_per_page'      => 1,
						'post__in'            => get_option( 'sticky_posts' ),
						'ignore_sticky_posts' => 1,
						'post_type' 		  => array( 'post', 'series' ),
						'meta_key' 			  => 'video_destaque',
						'meta_value' 		  => 'checked',
						'orderby' 			  => 'rand',
						'tax_query' => array('taxonomy' => 'category'),
					);
					$loop = new WP_Query( $args );
					$sliderPostCount = 0;
					if($loop->have_posts()):
						while ( $loop->have_posts() ) : $loop->the_post();
							global $post;
							$imgPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-banner_thumbnail_id',true);
						    $image   = wp_get_attachment_image_src( $imgPadraoID, 'large' );
							$title   = wp_trim_words( get_the_title(), $num_words = 10, $more = '... ' );
							$percentage = get_post_meta( get_the_ID(), 'percentage', true );
							$streamiumVideoTrailer = get_post_meta( get_the_ID(), 'streamium_video_trailer_meta_box_text', true );
							$streamiumFeaturedVideo = get_post_meta( get_the_ID(), 'streamium_featured_video_meta_box_text', true );
							$nonce = wp_create_nonce( 'streamium_likes_nonce' );
					        $link = admin_url('admin-ajax.php?action=streamium_likes&post_id='.get_the_ID().'&nonce='.$nonce);
					        $content = (isMobile()) ? get_the_excerpt() : get_the_content();
					        $urlPlay = $post->post_name;
							if(isset($userList)){
								$videoList = array_search($post->ID, $userList);
								if($videoList !== false) $myListClass = "botao-minha-lista-del"; else $myListClass = "botao-minha-lista-home";
							}else{
								$myListClass = "botao-minha-lista-home";
							}

							$serieID = $wpdb->get_var("select post_id from wp_postmeta where meta_key = 'video-serie' and meta_value like '%\"$post->ID\"%'");
							if(isset($serieID))
								$post = get_post($serieID);

							if($post->post_type=='series'){
								$series = get_post_meta($post->ID,'video-serie',true);
								foreach($series as $indice=>$episodios){
									$series[$indice]['pos'] = $wpdb->get_var("select concat(substring(name, LOCATE(' ', name)+1, 10) ,(select substring(name, LOCATE(' ', name)+1, 10) from wp_terms where term_id = $episodios[temporada])) from wp_terms where term_id = $episodios[episodio]");
									if(isset($series[$indice]['pos']))
										$parametro[$indice] = $series[$indice]['pos'];
								}
								$videoSerieAtual = array_search(min($parametro),$parametro);
								$episodio = get_post($series[$videoSerieAtual][video]);
								$urlPlay = $episodio->post_name;
							}
					?>
					<div class="slider-block" style="background-image: url(<?php echo esc_url($image[0]); ?>);">
						<span class='slide-banner'></span>
						<?php if ( ! empty( $streamiumFeaturedVideo ) && !isMobile() && ($sliderPostCount < 1)  && get_theme_mod( 'streamium_enable_premium' ) ) : ?>
							<div class="streamium-featured-background" id="streamium-featured-background-<?php echo get_the_ID(); ?>"></div>
							<script type="text/javascript">
								document.addEventListener("DOMContentLoaded", function(event) {
									S3Bubble.player({
										id : "streamium-featured-background-<?php echo get_the_ID(); ?>",
										codes : "<?php echo $streamiumFeaturedVideo; ?>",
										poster: "<?php echo esc_url($image[0]); ?>",
										fluid: true,
										muted : true,
										loop : true,
										autoplay : true,
										controls: false,
										meta: false
									});
								});
							</script>
						<?php endif; ?>
						<article class="content-overlay">
							<div class="container-fluid rel">
								<div class="row rel">
									<div class="col-sm-5 col-xs-5 rel">
										<div class="synopis-outer">
											<div class="synopis-middle">
												<div class="synopis-inner">
													<div class="synopis content hidden-xs">
														<?php echo $content; ?>
													</div>
													<div class='container-banner-botoes tile' data-id='<?php echo $post->ID;?>'>
														<a href='/<?php echo $urlPlay;?>'><span class='botao-play-home'></span></a>
														<a href='#'><span class='<?php echo $myListClass;?>' attr='<?php echo $post->ID;?>'></span></a>
													</div>
												</div>
											</div>
										<?php echo $logoEmpresa;?>
										</div>
									</div>
								</div>
							</div>
						</article><!--/.content-overlay-->
					</div>
					<?php
					    $sliderPostCount++;
						endwhile;
					else:
					?>
					<div style="background:url(<?php echo esc_url(get_template_directory_uri()); ?>/dist/frontend/assets/tech-2-mobile.jpg);" class="slider-block">
						<article class="content-overlay">
							<h2><?php _e( 'S3Bubble Media Streaming', 'streamium' ); ?></h2>
							<p><?php _e( 'Please replace this by making a post sticky, when you have do this you new sticky post will be displayed here.', 'streamium' ); ?></p>
						</article><!--/.content-overlay-->
					</div>
					<?php
					endif;
					wp_reset_query();
				?>
			</div><!--/.hero-slider-->
			<button class="streamium-next fa fa-angle-right" aria-hidden="true"></button>
		</section><!--/.hero-->
	<?php if(isset($userList)){?>
		<div class="videos-my-list-home"></div>
	  	<section class="videos videos-my-list">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header">
						<h3>Minha Lista</h3>
					</div><!--/.col-sm-12-->
				</div>
				<div class="row my-list-carrousels">
					<div class="col-sm-12">
						<div class="prev_next"></div>
						<div class="carousels my-list-carousels-container">
					  	<?php
							$args = array(
								'post_type' => array( 'post', 'series' ),
								'post__in' => $userList,
								'posts_per_page' => -1
							);
							$loop = new WP_Query( $args );
							if($loop->have_posts()):
								while ( $loop->have_posts() ) : $loop->the_post();
								if ( has_post_thumbnail() ) : // thumbnail check
								$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
								$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
								$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
								$imgMetaHover3ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-3_thumbnail_id',true);
								$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
								$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
								$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
								$imageExpanded3   = wp_get_attachment_image_src( $imgMetaHover3ID, 'large' );
								$nonce = wp_create_nonce( 'streamium_likes_nonce' );
								$linkID = $post->ID;
								if($post->post_type=='series'){
									$imgMetaPadraoID = get_post_meta($post->ID,'series_thumb-padrao_thumbnail_id',true);
									$imgMetaHover1ID = get_post_meta($post->ID,'series_thumb-hover-1_thumbnail_id',true);
									$imgMetaHover2ID = get_post_meta($post->ID,'series_thumb-hover-2_thumbnail_id',true);
									$imgMetaHover3ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-3_thumbnail_id',true);
									$image  		 = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
									$imageExpanded   = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
									$imageExpanded2  = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
									$imageExpanded3  = wp_get_attachment_image_src( $imgMetaHover3ID, 'large' );
									$series = get_post_meta($post->ID,'video-serie',true);
									foreach($series as $indice=>$episodios){
										$series[$indice]['pos'] = $wpdb->get_var("select concat(substring(name, LOCATE(' ', name)+1, 10) ,(select substring(name, LOCATE(' ', name)+1, 10) from wp_terms where term_id = $episodios[temporada])) from wp_terms where term_id = $episodios[episodio]");
										if(isset($series[$indice]['pos']))
											$parametro[$indice] = $series[$indice]['pos'];
									}
									$videoSerieAtual = array_search(min($parametro),$parametro);
									$episodio = get_post($series[$videoSerieAtual][video]);
									$linkID = $episodio->ID;
								}
						?>
							<div class="tile" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="my-list">
								<div class="tile_inner" style="background-image: url(<?php echo esc_url($image[0]); ?>);" attr-image="<?php echo esc_url($image[0]); ?>">
									<?php if($post->premium) : ?>
										<div class="tile_payment_details">
											<div class="tile_payment_details_inner">
												<h2>Available on <?php echo str_replace(array("_"), " ", $post->plans[0]); ?></h2>
											</div>
										</div>
									<?php endif; ?>
									<?php if (function_exists('is_protected_by_s2member')) :
										$check = is_protected_by_s2member(get_the_ID());
										if($check) : ?>
										<div class="tile_payment_details">
											<div class="tile_payment_details_inner">
												<h2>Available on <?php
													$comma_separated = implode(",", $check);
													echo "plan " . $comma_separated;
												?></h2>
											</div>
										</div>
									<?php endif; endif; ?>
									<div class="content">
								      <div class="overlay background-expanded background-img-<?php echo $post->ID;?>" attr0="<?php echo esc_url($imageExpanded[0]); ?>" attr1="<?php echo esc_url($imageExpanded2[0]); ?>" attr2="<?php echo esc_url($imageExpanded3[0]);?>">
								        <div class="overlay-gradient"></div>
								        <a class="play-icon-wrap hidden-xs" href="<?php the_permalink($linkID); ?>">
											<div class="play-icon-wrap-rel">
												<div class="play-icon-wrap-rel-ring"></div>
												<span class="play-icon-wrap-rel-play">
													<i class="fa fa-play fa-1x" aria-hidden="true"></i>
									        	</span>
								        	</div>
							        	</a>
							          	<div class="overlay-meta hidden-xs">
							            	<h4><?php the_title(); ?></h4>
							            	<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="my-list" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
							          	</div>
								      </div>
								    </div>
								</div>
								<?php if(is_user_logged_in() && get_theme_mod( 'streamium_enable_premium' )):
							    		$userId = get_current_user_id();
							    		$percentageWatched = get_post_meta( get_the_ID(), 'user_' . $userId, true );
							    ?>
								    <div class="progress tile_progress">
									  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentageWatched; ?>"
									  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageWatched; ?>%">
									  </div>
									</div>
								<?php endif; ?>

						    </div>
						<?php
							endif;
							endwhile;
							endif;
							wp_reset_query();
						?>
						</div><!--/.carousel-->
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.container-->
		</section><!--/.videos-->
		<section class="s3bubble-details-full my-list">
			<div class="s3bubble-details-full-overlay"></div>
			<div class="container-fluid s3bubble-details-inner-content">
				<div class="row">
					<div class="col-sm-12 col-xs-12 rel">
						<div class="synopis-outer synopis-outer-ajax ">
							<div class="synopis-middle">
								<div class="synopis-inner">
									<h2 class="synopis hidden-xs"></h2>
									<div class="synopis content"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
							<a class="play-icon-wrap synopis" href="#">
								<div class="play-icon-wrap-rel">
									<div class="play-icon-wrap-rel-ring play-ajax"></div>
									<span class="play-icon-wrap-rel-play">
										<i class="fa fa-play fa-3x" aria-hidden="true"></i>
									</span>
								</div>
							</a>
							<a href="#" class="synopis-video-trailer streamium-btns hidden-xs">Watch Trailer</a>
							<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
						</div><!--/.col-sm-12-->
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
		</section><!--/.videos-->
	  	<section class="videos">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header">
						<h3>Sugest&atilde;o para voc&ecirc;</h3>
					</div><!--/.col-sm-12-->
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="prev_next"></div>
						<div class="carousels">
					  	<?php
							foreach($userList as $categorias){
								if(isset($categorias)){
									$catList .= $virgulaCat.$categorias;
									$virgulaCat = ",";
								}
							}

							$listVideoSerie = $userList;
							$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
							foreach($videosSerie as $videoSerie){
								$postsSeries = unserialize($videoSerie->meta_value);
								foreach($postsSeries as $postSeries){
									if($postSeries['video'] != '') {
										$listVideoSerie[] = $postSeries['video'];
									}
								}
							}
					  		$categoryLists = $wpdb->get_results("select term_id
																from wp_term_relationships tr
																inner join wp_term_taxonomy tx on tx.term_taxonomy_id = tr.term_taxonomy_id
																where object_id in ($catList) and object_id not in ($videoSerieList)");
							foreach($categoryLists as $categoryList){
								$categoryID[] = $categoryList->term_id;
							}
							$args = array(
								'post_type' => 'post',
								'category__in' => $categoryID,
								'post__not_in' => $listVideoSerie,
								'posts_per_page' => -1
							);
							$loop = new WP_Query( $args );
							if($loop->have_posts()):
								while ( $loop->have_posts() ) : $loop->the_post();
								if ( has_post_thumbnail() ) : // thumbnail check
								$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
								$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
								$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
								$imgMetaHover3ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-3_thumbnail_id',true);
								$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
								$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
								$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
								$imageExpanded3   = wp_get_attachment_image_src( $imgMetaHover3ID, 'large' );
								$nonce = wp_create_nonce( 'streamium_likes_nonce' );
						?>
							<div class="tile" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="sugestion">
								<div class="tile_inner" style="background-image: url(<?php echo esc_url($image[0]); ?>);" attr-image="<?php echo esc_url($image[0]); ?>">
									<?php if($post->premium) : ?>
										<div class="tile_payment_details">
											<div class="tile_payment_details_inner">
												<h2>Available on <?php echo str_replace(array("_"), " ", $post->plans[0]); ?></h2>
											</div>
										</div>
									<?php endif; ?>
									<?php if (function_exists('is_protected_by_s2member')) :
										$check = is_protected_by_s2member(get_the_ID());
										if($check) : ?>
										<div class="tile_payment_details">
											<div class="tile_payment_details_inner">
												<h2>Available on <?php
													$comma_separated = implode(",", $check);
													echo "plan " . $comma_separated;
												?></h2>
											</div>
										</div>
									<?php endif; endif; ?>
									<div class="content">
								      <div class="overlay background-expanded" attr0="<?php echo esc_url($imageExpanded[0]); ?>" attr1="<?php echo esc_url($imageExpanded2[0]); ?>" attr2="<?php echo esc_url($imageExpanded3[0]);?>">

								        <div class="overlay-gradient"></div>
								        <a class="play-icon-wrap hidden-xs" href="<?php the_permalink(); ?>">
											<div class="play-icon-wrap-rel">
												<div class="play-icon-wrap-rel-ring"></div>
												<span class="play-icon-wrap-rel-play">
													<i class="fa fa-play fa-1x" aria-hidden="true"></i>
									        	</span>
								        	</div>
							        	</a>
							          	<div class="overlay-meta hidden-xs">
							            	<h4><?php the_title(); ?></h4>
							            	<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="sugestion" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
							          	</div>
								      </div>
								    </div>
								</div>
								<?php if(is_user_logged_in() && get_theme_mod( 'streamium_enable_premium' )):
							    		$userId = get_current_user_id();
							    		$percentageWatched = get_post_meta( get_the_ID(), 'user_' . $userId, true );
							    ?>
								    <div class="progress tile_progress">
									  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentageWatched; ?>"
									  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageWatched; ?>%">
									  </div>
									</div>
								<?php endif; ?>

						    </div>
						<?php
							endif;
							endwhile;
							endif;
							wp_reset_query();
						?>
						</div><!--/.carousel-->
					</div><!--/.col-sm-12-->
				</div><!--/.row-->
			</div><!--/.container-->
		</section><!--/.videos-->
	<?php }?>
		<section class="s3bubble-details-full sugestion">
			<div class="s3bubble-details-full-overlay"></div>
			<div class="container-fluid s3bubble-details-inner-content">
				<div class="row">
					<div class="col-sm-12 col-xs-12 rel">
						<div class="synopis-outer synopis-outer-ajax ">
							<div class="synopis-middle">
								<div class="synopis-inner">
									<h2 class="synopis hidden-xs"></h2>
									<div class="synopis content"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
							<a class="play-icon-wrap synopis" href="#">
								<div class="play-icon-wrap-rel">
									<div class="play-icon-wrap-rel-ring play-ajax"></div>
									<span class="play-icon-wrap-rel-play">
										<i class="fa fa-play fa-3x" aria-hidden="true"></i>
									</span>
								</div>
							</a>
							<a href="#" class="synopis-video-trailer streamium-btns hidden-xs">Watch Trailer</a>
							<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
						</div><!--/.col-sm-12-->
					</div>
				</div><!--/.row-->
			</div><!--/.container-->
		</section><!--/.videos-->
		<?php
/*
			$incicePos[1] =  "	<div class='video-titulo-container container-fluid'>
									<span class='video-img'><img src='/wp-content/uploads/icone-filmes.png'></span>
									<span class='video-titulo'>Filmes, S&eacute;ries e Document&aacute;rios</span>
								</div>";
			$incicePos[2] =  "	<div class='video-titulo-container container-fluid'>
									<span class='video-img'><img src='/wp-content/uploads/icone-cursos.png'></span>
									<span class='video-titulo'>Cursos, Palestras e Serm&otilde;es</span>
								</div>
								<br>";
			$incicePos[3] =  "	<div class='video-titulo-container container-fluid'>
									<span class='video-img'><img src='/wp-content/uploads/icone-musica.png'></span>
									<span class='video-titulo'>M&uacute;sica</span>
								</div>";
*/
			for($posicaoH=1;$posicaoH<=3;$posicaoH++){
				echo $incicePos[$posicaoH];
				$categories = $wpdb->get_results("select t.term_id, t.name, t.slug, t.term_group, tx.term_taxonomy_id, tx.taxonomy, tx.description, tx.parent, tx.count,
												 'raw' filter, t.term_id cat_ID, tx.count category_count, tx.description category_description, t.name cat_name,
												 t.name category_nicename, tx.parent category_parent, m.meta_key, m2.meta_value estilo_home, m3.meta_value titulo_home
												 from wp_terms t
												 inner join wp_termmeta m on m.term_id = t.term_id and meta_key = 'posicao_home'
												 inner join wp_term_taxonomy tx on tx.term_id = t.term_id
												 inner join wp_termmeta m1 on m1.term_id = t.term_id and m1.meta_key = 'posicao_home_modulo' and m1.meta_value = $posicaoH
												 inner join wp_termmeta m2 on m2.term_id = t.term_id and m2.meta_key = 'estilo_home'
 												 left join wp_termmeta m3 on m3.term_id = t.term_id and m3.meta_key = 'titulo_home'
												 where m.meta_value <> '' and m.meta_value is not null and m.meta_value > 0
												 order by m.meta_value");
				foreach ($categories as $indice=>$category) :
			?>
			<section class="videos">
				<div class="container-fluid">
					<?php if($category->titulo_home != 1){?>
						<div class="row">
							<div class="col-sm-12 video-header">
								<h3><?php echo ucfirst($category->cat_name);?></h3>
							</div><!--/.col-sm-12-->
						</div>
					<?php }?>
					<div class="row">
						<div class="col-sm-12">
							<div class="prev_next"></div>
							<div class="carousels">
							<?php
/*
								if($indice==3)
									$args = array(
										'post_type' => 'series',
										'tax_query' => array(
											array(
												'taxonomy' => 'Temporada_video',
												'field'    => 'slug',
												'terms'    => 'series',
											),
										),
									);
								else if($indice==4)
									$args = array(
										'post_type' => 'palestrante',
									);
								else{
*/
									unset($listVideoSerie);
									$videosSerie = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = 'video-serie' and meta_value is not null");
									foreach($videosSerie as $videoSerie){
										$postsSeries = unserialize($videoSerie->meta_value);
										foreach($postsSeries as $postSeries){
											if($postSeries['video'] != '') {
												$listVideoSerie[] = $postSeries['video'];
											}
										}
									}
									$args = array(
											'posts_per_page' => (int)get_theme_mod( 'streamium_global_options_homepage_desktop' ),
											'post_type' 	 => array( 'post', 'series','palestrante' ),
											'cat' => $category->cat_ID,
											'post__not_in' => $listVideoSerie,
											'posts_per_page' => -1
									);

//								}
								$loop = new WP_Query( $args );
								if($loop->have_posts()):
									while ( $loop->have_posts() ) : $loop->the_post();
									if ( has_post_thumbnail() ) : // thumbnail check
									$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-padrao_thumbnail_id',true);
									$imgMetaHover1ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-1_thumbnail_id',true);
									$imgMetaHover2ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-2_thumbnail_id',true);
									$imgMetaHover3ID = get_post_meta($post->ID,$post->post_type.'_thumb-hover-3_thumbnail_id',true);
									$image  		  = wp_get_attachment_image_src( $imgMetaPadraoID, 'medium' );
									$imageExpanded    = wp_get_attachment_image_src( $imgMetaHover1ID, 'large' );
									$imageExpanded2   = wp_get_attachment_image_src( $imgMetaHover2ID, 'large' );
									$imageExpanded3   = wp_get_attachment_image_src( $imgMetaHover3ID, 'large' );
									$nonce = wp_create_nonce( 'streamium_likes_nonce' );
									$classAdd = "";
									$linkID = $post->ID;
									$linkPalestraI = $linkPalestraF = "";
									$classAdd = $category->estilo_home;
									if($classAdd=="carrosel-vertical"){
										$imgMetaPadraoID = get_post_meta($post->ID,$post->post_type.'_thumb-vertical_thumbnail_id',true);
										$image  		 = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
										$imageExpanded   = wp_get_attachment_image_src( $imgMetaPadraoID, 'large' );
										$linkPalestraI 	 = "<a href='".get_permalink($linkID)."'>";
										$linkPalestraF 	 = "</a>";
									}
/*
									if($indice==3){
										$series = get_post_meta($post->ID,'video-serie',true);
										foreach($series as $indiceS=>$episodiosS){
											$series[$indiceS]['pos'] = $wpdb->get_var("select concat(substring(name, LOCATE(' ', name)+1, 10) ,(select substring(name, LOCATE(' ', name)+1, 10) from wp_terms where term_id = $episodiosS[temporada])) from wp_terms where term_id = $episodiosS[episodio]");
											if(isset($series[$indiceS]['pos']))
												$parametroS[$indiceS] = $series[$indiceS]['pos'];
										}

										$videoSerieAtualS = array_search(min($parametroS),$parametroS);
										$episodioS = get_post($series[$videoSerieAtualS][video]);
										$linkID = $episodioS->ID;

									}
*/
							?>
								<div class="tile" data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="<?php echo $category->slug; ?>">
									<?php echo $linkPalestraI;?>
									<div class="tile_inner <?php echo $classAdd;?>" style="background-image: url(<?php echo esc_url($image[0]); ?>);" attr-image="<?php echo esc_url($image[0]); ?>">
										<div class="content">
										  <div class="overlay background-expanded" attr0="<?php echo esc_url($imageExpanded[0]);?>" attr1="<?php echo esc_url($imageExpanded2[0]);?>" attr2="<?php echo esc_url($imageExpanded3[0]);?>">

											<div class="overlay-gradient"></div>
											<a class="play-icon-wrap hidden-xs" href="<?php echo get_permalink($linkID); ?>">
												<div class="play-icon-wrap-rel">
													<div class="play-icon-wrap-rel-ring"></div>
													<span class="play-icon-wrap-rel-play">
														<i class="fa fa-play fa-1x" aria-hidden="true"></i>
													</span>
												</div>
											</a>
											<div class="overlay-meta hidden-xs">
												<h4><?php the_title(); ?></h4>
												<a data-id="<?php the_ID(); ?>" data-nonce="<?php echo $nonce; ?>" data-cat="<?php echo $category->slug; ?>" class="tile_meta_more_info hidden-xs"><i class="icon-streamium" aria-hidden="true"></i></a>
											</div>
										  </div>
										</div>
									</div>
									<?php echo $linkPalestraF;?>

									<?php if(is_user_logged_in() && get_theme_mod( 'streamium_enable_premium' )):
											$userId = get_current_user_id();
											$percentageWatched = get_post_meta( get_the_ID(), 'user_' . $userId, true );
									?>
										<div class="progress tile_progress">
										  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentageWatched; ?>"
										  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageWatched; ?>%">
										  </div>
										</div>
									<?php endif; ?>

								</div>
							<?php

								endif;
								endwhile;
								endif;
								wp_reset_query();
							?>
							</div><!--/.carousel-->
						</div><!--/.col-sm-12-->
					</div><!--/.row-->
				</div><!--/.container-->
			</section><!--/.videos-->
			<section class="s3bubble-details-full <?php echo $category->slug; ?>">
				<div class="s3bubble-details-full-overlay"></div>
				<div class="container-fluid s3bubble-details-inner-content">
					<div class="row">
						<div class="col-sm-12 col-xs-12 rel">
							<div class="synopis-outer synopis-outer-ajax ">
								<div class="synopis-middle">
									<div class="synopis-inner">
										<h2 class="synopis hidden-xs"></h2>
										<div class="synopis content"></div>
									</div>
								</div>
							</div>
							<div class="col-sm-7 col-xs-7 rel" Style='float:right;'>
								<a class="play-icon-wrap synopis" href="#">
									<div class="play-icon-wrap-rel">
										<div class="play-icon-wrap-rel-ring play-ajax"></div>
										<span class="play-icon-wrap-rel-play">
											<i class="fa fa-play fa-3x" aria-hidden="true"></i>
										</span>
									</div>
								</a>
								<a href="#" class="synopis-video-trailer streamium-btns hidden-xs">Watch Trailer</a>
								<a href="#" class="s3bubble-details-inner-close"><i class="fa fa-times" aria-hidden="true"></i></a>
							</div><!--/.col-sm-12-->
						</div>
					</div><!--/.row-->
				</div><!--/.container-->
			</section><!--/.videos-->
			<?php
				endforeach;
			}
		?>
		<section class="videos">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 video-header">
						<h3></h3>
					</div><!--/.col-sm-12-->
				</div>
			</div>
		</section><!--/.videos-->
<?php get_footer(); ?>
