package acidhax.cordova.chromecast;

import android.annotation.TargetApi;
import android.os.Build;
import android.graphics.Color;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import java.io.IOException;
import java.util.HashSet;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.RemoteMediaPlayer;
import com.google.android.gms.cast.RemoteMediaPlayer.MediaChannelResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.images.WebImage;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.net.Uri;
import android.util.Log;
import android.widget.ArrayAdapter;

public class ChromecastMediaController {
	private RemoteMediaPlayer remote = null;
	public ChromecastMediaController(RemoteMediaPlayer mRemoteMediaPlayer) {
		this.remote = mRemoteMediaPlayer;
	}
	
	public MediaInfo createLoadUrlRequest(String contentId, String contentType, long duration, String streamType, JSONObject metadata, String legenda) {

		//System.out.println("legenda");
		//System.out.println(legenda);

		List listTracks = new ArrayList();
		if (legenda != ""){
			MediaTrack track = new MediaTrack.Builder(1, MediaTrack.TYPE_TEXT)
			.setName("Portuguese")
			.setSubtype(MediaTrack.SUBTYPE_SUBTITLES)
			.setContentId(legenda)
			.setContentType("text/vtt")
			.setLanguage("pt-BR")
			.build();			
			listTracks.add(track);
		}
	
        // Try creating a GENERIC MediaMetadata obj
		MediaMetadata mediaMetadata = new MediaMetadata();
		try {
		
			int metadataType = metadata.has("metadataType") ? metadata.getInt("metadataType") : MediaMetadata.MEDIA_TYPE_MOVIE;
            
			// GENERIC
			if (metadataType == MediaMetadata.MEDIA_TYPE_GENERIC) {
				mediaMetadata = new MediaMetadata(); // Creates GENERIC MediaMetaData
				mediaMetadata.putString(MediaMetadata.KEY_TITLE, (metadata.has("title")) ? metadata.getString("title") : "" ); // TODO: What should it default to?
				mediaMetadata.putString(MediaMetadata.KEY_SUBTITLE, (metadata.has("subtitle")) ? metadata.getString("subtitle") : "" ); // TODO: What should it default to?
				mediaMetadata = addImages(metadata, mediaMetadata);
			}
			else{
				//if (metadataType == MediaMetadata.MEDIA_TYPE_MOVIE) {
				mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
				mediaMetadata.putString(MediaMetadata.KEY_TITLE, (metadata.has("title")) ? metadata.getString("title") : "" );
				mediaMetadata.putString(MediaMetadata.KEY_SUBTITLE, (metadata.has("subtitle")) ? metadata.getString("subtitle") : "");
			}
			
			
		} catch(Exception e) {
			e.printStackTrace();
			// Fallback
			mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
		}
		

		//mediaMetadata = new MediaMetadata(); // Creates GENERIC MediaMetaData
		//mediaMetadata.putString(MediaMetadata.KEY_TITLE, "teste 111" ); // TODO: What should it default to?
		//mediaMetadata.putString(MediaMetadata.KEY_SUBTITLE, "teste 22222222222" ); // TODO: What should it default to?
		

    	int _streamType = MediaInfo.STREAM_TYPE_BUFFERED;
    	if (streamType.equals("buffered")) {
    		
    	} else if (streamType.equals("live")) {
    		_streamType = MediaInfo.STREAM_TYPE_LIVE;
    	} else if (streamType.equals("other")) {
    		_streamType = MediaInfo.STREAM_TYPE_NONE;
    	}

/*
		TextTrackStyle textTrackStyle = TextTrackStyle.fromSystemSettings(contentId);
		// we need to populate all the fields ourselves
		textTrackStyle.setBackgroundColor("#80FF0000");
		textTrackStyle.setForegroundColor("#EFE700FF");
		//textTrackStyle.setEdgeType(EDGE_TYPE_MAPPING.get(getEdgeType()));
*/
		TextTrackStyle textTrackStyle = new TextTrackStyle();
		textTrackStyle.setForegroundColor(Color.parseColor("#FFEFE700"));
		textTrackStyle.setBackgroundColor(Color.parseColor("#0080FF00"));
		//textTrackStyle.setWindowType(TextTrackStyle.WINDOW_TYPE_NONE);
		textTrackStyle.setEdgeType(2);
		textTrackStyle.setEdgeColor(Color.parseColor("#FF000000"));
		//textTrackStyle.setFontGenericFamily(TextTrackStyle.FONT_FAMILY_SANS_SERIF);

    	MediaInfo mediaInfo = new MediaInfo.Builder(contentId)
    	    .setContentType(contentType)
    	    .setStreamType(_streamType)
    	    .setStreamDuration(duration)
    	    .setMetadata(mediaMetadata)
			.setMediaTracks(listTracks)
			.setTextTrackStyle(textTrackStyle)
    	    .build();
		System.out.println(mediaInfo);
		return mediaInfo;
	}
	

	
	/*AQUI LEO!!!!*/
	/*
	public void editTracksInfo(GoogleApiClient apiClient, ChromecastSessionCallback callback) {
		res.setResultCallback(this.createMediaCallback(callback));
	}
	*/	
	
	public void play(GoogleApiClient apiClient, ChromecastSessionCallback callback) {
		PendingResult<MediaChannelResult> res = this.remote.play(apiClient);
		res.setResultCallback(this.createMediaCallback(callback));
	}
	
	public void pause(GoogleApiClient apiClient, ChromecastSessionCallback callback) {
		PendingResult<MediaChannelResult> res = this.remote.pause(apiClient);
		res.setResultCallback(this.createMediaCallback(callback));
	}
	
	public void stop(GoogleApiClient apiClient, ChromecastSessionCallback callback) {
		PendingResult<MediaChannelResult> res = this.remote.stop(apiClient);
		res.setResultCallback(this.createMediaCallback(callback));
	}
	
	public void seek(long seekPosition, String resumeState, GoogleApiClient apiClient, final ChromecastSessionCallback callback) {
		PendingResult<MediaChannelResult> res = null;
		if (resumeState != null && !resumeState.equals("")) {
			if (resumeState.equals("PLAYBACK_PAUSE")) {
				res = this.remote.seek(apiClient, seekPosition, RemoteMediaPlayer.RESUME_STATE_PAUSE);
			} else if (resumeState.equals("PLAYBACK_START")) {
				res = this.remote.seek(apiClient, seekPosition, RemoteMediaPlayer.RESUME_STATE_PLAY);
			} else {
				res = this.remote.seek(apiClient, seekPosition, RemoteMediaPlayer.RESUME_STATE_UNCHANGED);
			}
		}
		
		if (res == null) {
			res = this.remote.seek(apiClient, seekPosition);
		}
		
		res.setResultCallback(this.createMediaCallback(callback));
	}
	
	public void setVolume(double volume, GoogleApiClient apiClient, final ChromecastSessionCallback callback) {
		PendingResult<MediaChannelResult> res = this.remote.setStreamVolume(apiClient, volume);
		res.setResultCallback(this.createMediaCallback(callback));
	}
	
	public void setMuted(boolean muted, GoogleApiClient apiClient, final ChromecastSessionCallback callback) {
		PendingResult<MediaChannelResult> res = this.remote.setStreamMute(apiClient, muted);
		res.setResultCallback(this.createMediaCallback(callback));
	}
	
	private ResultCallback<RemoteMediaPlayer.MediaChannelResult> createMediaCallback(final ChromecastSessionCallback callback) {
		return new ResultCallback<RemoteMediaPlayer.MediaChannelResult>() {
		    @Override
		    public void onResult(MediaChannelResult result) {
				if (result.getStatus().isSuccess()) {
					callback.onSuccess();
				} else {
					callback.onError("channel_error");
				}
		    }
		};
	}
    
    private MediaMetadata addImages(JSONObject metadata, MediaMetadata mediaMetadata) throws JSONException {
        if (metadata.has("images")) {
            JSONArray imageUrls = metadata.getJSONArray("images");
            for (int i=0; i<imageUrls.length(); i++) {
                JSONObject imageObj = imageUrls.getJSONObject(i); 
                String imageUrl = imageObj.has("url") ? imageObj.getString("url") : "undefined";
                if (imageUrl.indexOf("http://")<0) { continue; } // TODO: don't add image?
                Uri imageURI = Uri.parse( imageUrl );
                WebImage webImage = new WebImage(imageURI);
                mediaMetadata.addImage(webImage);
            }
        }
        return mediaMetadata;
    }
    
}
