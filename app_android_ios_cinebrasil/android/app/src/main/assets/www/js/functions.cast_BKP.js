    initializeCastApi = function() {
      if (ionic.Platform.isAndroid()){
        chrome.cast.initialize(apiConfig, onInitSuccess, onError);
      }
      if (ionic.Platform.isIPad() || ionic.Platform.isIOS()){
          initializeCastApiIOS();
      }
    }


    function verifyCastEnable(){
        //if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false; 
        if(typeof fnCastEnabled !== "undefined"){clearTimeout(fnCastEnabled);}
        if (chrome.cast!=undefined){
            if (sessionRequest==null){
                sessionRequest = new chrome.cast.SessionRequest(chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID);
            }
            if (apiConfig==null){
                apiConfig = new chrome.cast.ApiConfig(sessionRequest, sessionListener, receiverListener);
            }
            //console.log('sessionRequest',sessionRequest,'apiConfig',apiConfig);
            if ((apiConfig==null) || (sessionRequest==null)){
                $('.btn-cast').hide();
                fnCastEnabled = setTimeout(function() { verifyCastEnable(); }, 5000);
            }
            else{
                $('.btn-cast').show();
            }
        }          
    }



/* FUNCTIONS CAST IOS */
    verifyCastEnableIOS = function(){
        cordova.plugins.chromecastios.scanForDevices(defaultAppId).then(function(response){
            setTimeout(function() {                
                devices = cordova.plugins.chromecastios.devices;
                //console.log('devices',devices);
                if (devices.length > 0){
                    $('.btn-cast').show();
                }
                else{
                    $('.btn-cast').hide();
                }                
            }, 1000);      
        }).catch(function(error){
            $('.btn-cast').hide();
        });        
    }

   //Device Status Events
    /*
    document.addEventListener("chromecast-ios-device", function(event) {
        if(event.eventType == "online"){
          console.log("device online");
          //do something
        }
        if(event.eventType == "offline"){
          console.log("device offline")
          //do something
        }
        if(event.eventType == "changed"){
          console.log("device updated");
          //do something
        }
        if(event.eventType == "disconnect"){
          console.log("device disconnected");
          //do something
        }
        console.log("device",event);        
    });
    
    
    //Media Status Events
    document.addEventListener("chromecast-ios-media", function(event) {
        if(event.eventType == "playbackFinished"){
          console.log("playback finished event");
          //do something
        }
        if(event.eventType == "playbackBuffering"){
          console.log("playback paused event")
          //do something
        }
        if(event.eventType == "playbackPlaying"){
          console.log("playback playing event");
          //do something
        }
        if(event.eventType == "playbackPaused"){
          console.log("playback paused event")
          //do something
        }
        console.log("playback",event);        
    });

    //Volume Events

    document.addEventListener("chromecast-ios-volume", function(event) {
        if(event.eventType == "volumeChanged"){
          console.log("volume changed");
          //do something
        }
        if(event.eventType == "volumeMuted"){
          console.log("mute enabled")
          //do something
        }
        if(event.eventType == "volumeUnmuted"){
          console.log("mute disabled");
          //do something
        }
        console.log("volume",event);
    });
    */
        
/******** FUNCTIONS CAST ANDROID *******/

    function receiverListener(e) {
        console.log('receiverListener',e);
    }

    function onVolumeDownKeyDown() {
        if (castSession!=null){
            //$scope.setVolumeCast(-0.1);
            valor = -0.1;
            volumeCalc = (parseFloat(volumeAtual) + parseFloat(valor)).toFixed(2);
            if (!((volumeCalc>1) || (volumeCalc<0))){
                volumeAtual = volumeCalc;
                $('.volume-cast').val(volumeAtual);
                castSession.setReceiverVolumeLevel(parseFloat(volumeAtual), onSucessVolume, onErrorVolume);
            }
        }
    }
    function onVolumeUpKeyDown() {
        if (castSession!=null){
            //$scope.setVolumeCast(+0.1);
            valor = +0.1;
            volumeCalc = (parseFloat(volumeAtual) + parseFloat(valor)).toFixed(2);
            if (!((volumeCalc>1) || (volumeCalc<0))){
                volumeAtual = volumeCalc;
                $('.volume-cast').val(volumeAtual);
                castSession.setReceiverVolumeLevel(parseFloat(volumeAtual), onSucessVolume, onErrorVolume);
            }
        }
    }


    function onRequestSessionSuccess(e) {
        castSession = e;
        $('.btn-cast').hide();
        $('.btn-cast-ativo').show();
        document.addEventListener("volumedownbutton", onVolumeDownKeyDown, false);
        document.addEventListener("volumeupbutton", onVolumeUpKeyDown, false);   
    }


    function onLaunchError(e) {
        setTimeout(function() {
            initializeCastApi();
        }, 1000);
    }

    function sessionListener(e) {
        console.log('sessionListener',e);
    }
    function onLoadPreview(){
    }

    function onErrorPreview(e){
        //console.error("onErrorPreview",e);
        currentMedia = null;
    }

    function onInitSuccess(e) {
        castSession = null;
        chrome.cast.requestSession(onRequestSessionSuccess, onLaunchError);
    }

    function onError(e) {
        castSession = null;
        currentMedia = null;
    }

    /**************************************************************/               
    /******* INICIO - TRATAMENTO SE JA ESTA VENDO O VIDEO *********/
    /**************************************************************/
    function verificaPorcentagemVideo(){ 
        if ((!isNaN(porcentagemVideoAtual)) && (porcentagemVideoAtual!=null) && (porcentagemVideoAtual!=undefined) && (porcentagemVideoAtual!=0) && 
            ((!isNaN(tempoVideoAtual)) && (tempoVideoAtual!=null) && (tempoVideoAtual!=undefined) && (tempoVideoAtual!=0))){
            tempoAtual = tempoVideoAtual;
            $('.tempo-cast').val(parseInt(tempoAtual));
            if (platform=='android'){            
                currentMedia.pause();
                seekRequest = new chrome.cast.media.SeekRequest(); 
                seekRequest.currentTime = parseInt(tempoAtual);
                currentMedia.seek(seekRequest, onSucessSeek, onErrorSeek);
                currentMedia.play();
                if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false;
            }

            if (platform=='ios'){
                cordova.plugins.chromecastios.seekMedia(tempoAtual);
            }
        }
    }
    /**************************************************************/                
    /********* FIM - TRATAMENTO SE JA ESTA VENDO O VIDEO **********/
    /**************************************************************/                


    function onMediaDiscovered(how, media) {
        currentMedia = media;
        /* VOLUME */ 
        if (castSession.receiver.volume!=null){
            volumeAtual = parseFloat(castSession.receiver.volume.level.toFixed(2));
        }
        else{
            volumeAtual = parseFloat(currentMedia.volume.level.toFixed(2));       
        }

        $('.volume-cast').val(volumeAtual);

        /* BARRA DE CONTROLE */
        if (trocaLegenda==0){
            $('#footer-bar-cast').show();
            $("#footer-bar-cast").animate({height: '70px'});
            tempoAtual = (parseInt(currentMedia.currentTime) + 1);
            verificaPorcentagemVideo();
        }
        else{
            currentMedia.pause();
            tempoAtual = parseInt($('.tempo-cast').val());
            seekRequest = new chrome.cast.media.SeekRequest(); 
            seekRequest.currentTime = parseInt(tempoAtual);
            currentMedia.seek(seekRequest, onSucessSeek, onErrorSeek);
            currentMedia.play();
            //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}        
            if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false; 
        }
        /* DADOS MIDIA - BARRA CONTROLE */ 
        $(".nome-titulo-andamento-cast").html(videoCast.post_title);
        $(".imagem-titulo-andamento-cast").attr("src",videoCast.thumb[0]);
        $(".ion-play").hide();
        $(".ion-ios-pause").show();

        /* CONTADOR DE TEMPO */
        tempoTotal = parseInt(currentMedia.media.duration);
        $('.tempo-cast').attr('max',tempoTotal);
        $('.video-duracao').html(toHHMMSS(tempoTotal));
        contadorProgressivo();

    }

    function onMediaDiscoveredIOS(media) {
        currentMedia = media;
        //get the current position of the playing media in seconds
        cordova.plugins.chromecastios.getPosition().then(function(response){
            tempoAtual = response;
             /* VOLUME */ 
            volumeAtual = parseFloat(currentMedia.volume.toFixed(2));
            $('.volume-cast').val(volumeAtual);
            /* BARRA DE CONTROLE */
            if (trocaLegenda==0){
                $('#footer-bar-cast').show();
                $("#footer-bar-cast").animate({height: '70px'});
                tempoAtual = (parseInt(tempoAtual) + 1);
                verificaPorcentagemVideo();                
            }
            else{
                //cordova.plugins.chromecastios.pauseMedia();
                tempoAtual = parseInt($('.tempo-cast').val());
                cordova.plugins.chromecastios.seekMedia(tempoAtual);
            }
            /* DADOS MIDIA - BARRA CONTROLE */ 
            $(".nome-titulo-andamento-cast").html(videoCast.post_title);
            $(".imagem-titulo-andamento-cast").attr("src",videoCast.thumb[0]);
            $(".ion-play").hide();
            $(".ion-ios-pause").show();
            
            /* AQUI FUNCAO PARA CRONOMETRO */
            //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}
            if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false; 
            var a = videoCast.media.duracao.split(':');
            tempoTotal = (parseInt((a[0])) * 3600) + (parseInt(a[1]) * 60) + parseInt(a[2]);
            tempoTotal = parseInt(tempoTotal);
            $('.tempo-cast').attr('max',tempoTotal);
            $('.video-duracao').html(toHHMMSS(tempoTotal));
            contadorProgressivo();                   
        }).catch(function(error){
            //console.log('error',error);
        });
        
    }



    function onErrorSeek(e){
        //console.log('erro ');
        //console.log(e);
    }

    function onSucessSeek(){
        //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}
        //$('.cronometro-cast').html(toHHMMSS(tempoAtual));
        //$('.tempo-cast').val(tempoAtual);        
    }

     function updateTempoUsuarioVideo(tempo,porcentagem){  
        var urlwebservice = localStorage.getItem("com.cinebrasiljaurlwebservice");
        var userID = localStorage.getItem("com.cinebrasiljauserID");
         
        var xmlhttp = new XMLHttpRequest(); 
        xmlhttp.onreadystatechange = function() {  
            if (this.readyState == 4 && this.status == 200) {   
                //console.log(this.responseText);  
            } 
        }; 
        caminho = urlwebservice + "posts/update_time_video_user?userID=" + userID + "&postID=" + videoCast.ID + "&tempo=" + tempo + "&porcent=" + porcentagem;
        //console.log(caminho);
        xmlhttp.open("GET", caminho, true);
        xmlhttp.send();
    }


    function contadorProgressivo() {
        if (tempoAtual<tempoTotal){
            $('.tempo-cast').val(parseInt(tempoAtual));
            $('.cronometro-cast').html(toHHMMSS(parseInt(tempoAtual)));
        
            porcentagemVideoAtual = parseInt((parseFloat(tempoAtual) / parseFloat(tempoTotal)) * 100);
            
            if (porcentagemVideoAtual != porcentagemVideoAnterior){
                updateTempoUsuarioVideo(tempoAtual, porcentagemVideoAtual);
                porcentagemVideoAnterior = porcentagemVideoAtual;
            }

            tempoAtual++;
            contadorAtivo = true;
            fnContProg = setTimeout(function() { contadorProgressivo(); }, 1000);
        }
        else{
            updateTempoUsuarioVideo(tempoTotal);
            tempoAtual = 0;
            $(".ion-play").show();
            $(".ion-ios-pause").hide();
            if (platform=='android'){            
                currentMedia.stop();
            }
            if (platform=='ios'){    
                cordova.plugins.chromecastios.stopMedia();        
            }
            currentMedia = null;
            //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}
            if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false;            
            exibirClassificacaoTitulo();
        }
    }

    function onMediaError(e) {
        //console.error('ERRO AO CARREGAR MIDIA');
        //console.error(e);
        currentMedia = null;
    }

    function disconnectCast(){
        if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false;
        if (platform=='ios'){
            cordova.plugins.chromecastios.disconnect().then(function(response){}).catch(function(error){});
        }
        if (platform=='android'){
            castSession.stop();
            document.removeEventListener("volumedownbutton", onVolumeDownKeyDown, false);
            document.removeEventListener("volumeupbutton", onVolumeUpKeyDown, false);   
            castSession = null;
        }
        currentMedia = null;
        $('.btn-cast').show();
        $('.btn-cast-ativo').hide();
        closeControlCast();
        $('#footer-bar-cast').hide();
        $(".nome-titulo-andamento-cast").html("");
        $(".imagem-titulo-andamento-cast").attr("src","");
    }
    function openControlCast(){
        $(".chamada-footer-cast").hide();
        $("#media-footer-cast").delay("slow").fadeIn();
        $("#footer-bar-cast").animate({height: '100%'});
        /*
        degree = '360';
        $(".ion-chevron-down").animate({
                        '-webkit-transform': 'rotate(' + degree + 'deg)',
                        '-moz-transform': 'rotate(' + degree + 'deg)',
                        '-ms-transform': 'rotate(' + degree + 'deg)',
                        '-o-transform': 'rotate(' + degree + 'deg)',
                        'transform': 'rotate(' + degree + 'deg)',
                        'zoom': 1
            }, 5000);        
        */
        //$(".ion-chevron-down").css('-webkit-transform', 'rotate(360deg)').css('transform','rotate(360deg)');
    }
    function closeControlCast(){
        $(".chamada-footer-cast").show();
        $("#media-footer-cast").hide();
        $("#footer-bar-cast").animate({height: '70px'});   
        if($('.classificar-titulo').css('display')!='none'){
            $("#footer-bar-cast").hide();
            $(".nome-titulo-andamento-cast").html("");
            $(".imagem-titulo-andamento-cast").attr("src","");
            $('.tempo-cast').val("");
            $('.cronometro-cast').html("");
        }     
        esconderClassificacaoTitulo();        
    }

   function exibirClassificacaoTitulo(){
        $('.classificar-titulo').show();
        $('.controles-cast').hide();
    }    
    function esconderClassificacaoTitulo(){
        $('.classificar-titulo').hide();
        $('.controles-cast').show();
    }    
    
    function iniciarControlCast(video){
        //console.log("iniciarControlCast");
        //console.log(video);
        //if(contadorAtivo) clearTimeout(fnContProg);
        //contadorAtivo = false;            
        //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}       
        //if (fnContProg) clearTimeout(fnContProg);
        if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false;         
        $(".nome-titulo-andamento-cast").html(video.post_title);
        $(".imagem-titulo-andamento-cast").attr("src",video.thumb[0]);
        $(".ion-play").hide();        
        $(".ion-ios-pause").show();  
      
    }

    function setTempoCast(tmp){
        if ((tmp!=undefined)&&(tmp!=null)&&(tmp!=0)){
            tempoAtual = parseInt(tmp);
        }
        else{
            tempoAtual = parseInt($('.tempo-cast').val());
        }
        if (platform=='ios'){
            cordova.plugins.chromecastios.seekMedia(tempoAtual);
        }
        if (platform=='android'){
            seekRequest = new chrome.cast.media.SeekRequest(); 
            seekRequest.currentTime = parseInt(tempoAtual);
            currentMedia.seek(seekRequest, onSucessSeek, onErrorSeek);
            //currentMedia.play();
        }
        if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false; 
        //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}
        contadorProgressivo();
    }

    function onErrorVolume(e){

    }

    function onSucessVolume(){
    }


    function setVolumeCastRange(){
        volumeAtual = parseFloat($(".volume-cast").val()).toFixed(2);
        if (platform=='ios'){
            cordova.plugins.chromecastios.setVolumeForMedia(volumeAtual); 
        }
        if (platform=='android'){
            castSession.setReceiverVolumeLevel(parseFloat(volumeAtual), onSucessVolume, onErrorVolume);
        }
    }

    function selecionarTemporada(){
        objClasse = ReplaceAll($('#temporadas').val(),' ', '-')
        $('.opcoesTemporada').hide();
        $('.'+objClasse).show();
    }
    /***************************************/