angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  .state('verificaLogin', {
    cache: false,
    url: '/verificaLogin',
    templateUrl: 'templates/verificaLogin.html',
    controller: 'verificaLoginCtrl'
  })

  .state('login', {
    cache: false,    
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('menu', {
    cache: true,    
    url: '/menu',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('menu.titulo', {
    cache: false,    
    url: '/titulo',
    params: {
      titulo: null
    },
    views: {
      'menu-lateral': {    
      templateUrl: 'templates/titulo.html',
      controller: 'tituloCtrl'
      }
    }
  })

  .state('menu.player', {
    cache: false,    
    url: '/player',
    params: {
      titulo: null,
      video: null,
      videoSelecionado: null
    },
    views: {
      'menu-lateral': {
      templateUrl: 'templates/player.html',
      controller: 'playerCtrl'
      }
    }
  })

  .state('menu.cadastro', {
    cache: false,    
    url: '/cadastro',
    views: {
      'menu-lateral': {    
        templateUrl: 'templates/cadastro.html',
        controller: 'cadastroCtrl'
      }
    }
  })

  .state('esqueciSenha', {
    cache: false,    
    url: '/esqueciSenha',
    templateUrl: 'templates/esqueciSenha.html',
    controller: 'esqueciSenhaCtrl'
  })

  .state('menu.localizarTitulos', {
    cache: false,    
    url: '/localizarTitulos',
    views: {
      'menu-lateral': {
        templateUrl: 'templates/localizarTitulos.html',
        controller: 'localizarTitulosCtrl'
      }
    }
  })
  .state('menu.localizarTitulosCategoria', {
    cache: false,    
    url: '/localizarTitulosCategoria',
    params: {
      category: null
    },    
    views: {
      'menu-lateral': {
        templateUrl: 'templates/localizarTitulosCategoria.html',
        controller: 'localizarTitulosCategoriaCtrl'
      }
    }
  })
  .state('menu.notificacoes', {
    cache: false,    
    url: '/notificacoes',
    views: {
        'menu-lateral': {
        templateUrl: 'templates/notificacoes.html',
        controller: 'notificacoesCtrl'
      }
    }
  })
  .state('menu.home', {
    cache: true,    
    url: '/home',
    views: {
        'menu-lateral': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })
  .state('menu.selecionaPerfil', {
    cache: false,    
    url: '/selecionaPerfil',
    views: {
        'menu-lateral': {
        templateUrl: 'templates/selecionaPerfil.html',
        controller: 'selecionaPerfilCtrl'
      }
    }
  })  

  $urlRouterProvider.otherwise(function($injector, $location){
    var state = $injector.get('$state');
    var chave = localStorage.getItem("com.cinebrasilja.chave");
    var user = localStorage.getItem("com.cinebrasilja.user");
    var userID = localStorage.getItem("com.cinebrasilja.userID");
    var cookie = localStorage.getItem("com.cinebrasilja.cookie");
    var professor = localStorage.getItem("com.cinebrasilja.perfil");
    //console.log(cookie);
    if ((cookie==null) || (cookie=='')  || (cookie=='null')){
      state.go('verificaLogin');
    }
    else{
      state.go('menu.home');
    }
    return $location.path();
  });

  //$urlRouterProvider.otherwise('/login')

});
