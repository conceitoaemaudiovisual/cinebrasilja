angular.module('app.controllers', [])


.controller('verificaLoginCtrl', ['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', 
    function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading) {      
        $ionicLoading.show();
        $ionicNavBarDelegate.showBackButton(false);

        var protocol = 'https://';
        var domain = 'www.cinebrasilja.com';
        //var domain = 'teomidia.com.br';
        var base = '/api/';

        var urlwebservice = protocol + domain + base;
        var urlweb = protocol + domain;

        localStorage.setItem("com.cinebrasilja.urlwebservice", urlwebservice);
        localStorage.setItem("com.cinebrasilja.urlweb", urlweb);
 
        $scope.chave = localStorage.getItem("com.cinebrasilja.chave");
        $scope.user = localStorage.getItem("com.cinebrasilja.user");
        $scope.userID = localStorage.getItem("com.cinebrasilja.userID");
        $scope.cookie = localStorage.getItem("com.cinebrasilja.cookie");
        $scope.perfil = localStorage.getItem("com.cinebrasilja.perfil");
        var origem = '';
        if (($scope.cookie!='') && ($scope.cookie!='null') && ($scope.cookie!=null)){
            caminho = urlwebservice + 'user/get_currentuserinfo/?cookie=' + $scope.cookie;
            $http.get(caminho)
            .success(function(data, status, headers,config){
                $ionicLoading.hide();
                origem = 'login';
                $state.go('menu.home');
            })
            .error(function(data, status, headers,config){
                $ionicLoading.hide();
                localStorage.setItem("com.cinebrasilja.chave", '');
                localStorage.setItem("com.cinebrasilja.userID", '');
                localStorage.setItem("com.cinebrasilja.user", '');
                localStorage.setItem("com.cinebrasilja.cookie", '');
                localStorage.setItem("com.cinebrasilja.perfil", '');
                $state.go('login');
            });
        }
        else{
            $ionicLoading.hide();
            localStorage.setItem("com.cinebrasilja.chave", '');
            localStorage.setItem("com.cinebrasilja.userID", '');
            localStorage.setItem("com.cinebrasilja.user", '');
            localStorage.setItem("com.cinebrasilja.cookie", '');
            localStorage.setItem("com.cinebrasilja.perfil", '');
            $state.go('login');
        }
}])

.controller('loginCtrl',['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup',
function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup) {
    $ionicLoading.hide();
    $ionicNavBarDelegate.showBackButton(false);

    if (ionic.Platform.isAndroid()){           
        window.screen.orientation.unlock();
        window.screen.orientation.lock('portrait');
    }
    if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
        if (screen.orientation){
            screen.orientation.unlock();
            screen.orientation.lock('portrait');
        }
    }


    var usuario = "";
    var senha = "";
    var chave = "";
    var caminho = "";
    var strErro = "";
    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    $scope.login = function(){
        $ionicLoading.show();
        strErro = "";
        usuario = $("#usuario").val();
        senha =  $("#senha").val();
        if(usuario=='')
            strErro += 'Preencha o campo de Usuário <br>';
        if(senha=='')
            strErro += ' Preencha o campo Senha <br>';
        if (strErro!=''){
            alertar(strErro);
        }
        else{
            caminho = urlwebservice + 'get_nonce/?controller=user&method=generate_auth_cookie';
            $http.get(caminho)
            .success(function(data, status, headers,config){
                chave = data.nonce;
                caminho = urlwebservice+'user/generate_auth_cookie/?nonce='+chave+'&username='+usuario+'&password='+senha;
                $http.get(caminho)
                .success(function(data, status, headers,config){
                    if (data.status=='ok'){
                        var userID = data.user.id;
                        var user = JSON.stringify(data.user);
                        var cookie = data.cookie;
                        var config = {
                            headers : {
                                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                            }
                        }             
                        var data = $.param({
                            log: usuario
                        });
                    
                        caminho = urlwebservice+'validaLogin/?acao=1';
                        $http.post(caminho, data ,config)
                        .success(function(data, status, headers,config){
                            if (data.status=='ok'){
                                localStorage.setItem("com.cinebrasilja.chave", chave);
                                localStorage.setItem("com.cinebrasilja.userID", userID);
                                localStorage.setItem("com.cinebrasilja.user", user);
                                localStorage.setItem("com.cinebrasilja.cookie", cookie);
                                localStorage.setItem("com.cinebrasilja.perfil", '');
                                $state.go('menu.home');
                            }
                        })
                        .error(function(data, status, headers, config){
                            //console.log(data, status,headers,config);
                            $ionicLoading.hide();
                            alertar(data.error);
                        });
                    }
                    else{
                        alertar(data.error);
                    }
                });
            })
            .error(function(data, status, headers, config){
                alertar('Erro de comunicação \nVerifique sua conexão de Internet');
            });
        }

        function alertar(strErro){
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Atenção',
                template: strErro,
                okText: 'OK',
                okType: 'btnAlerta'
            });
            alertPopup.then(function(res) {
                return;
            });
        }

    };
}])
      
.controller('menuCtrl', ['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup', '$ionicSideMenuDelegate', 'IonicClosePopupService', '$ionicHistory',
function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup, $ionicSideMenuDelegate, IonicClosePopupService, $ionicHistory) {
    if (ionic.Platform.isAndroid()){           
        window.screen.orientation.unlock();
        window.screen.orientation.lock('portrait');
    }
    if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
        if (screen.orientation){
            screen.orientation.unlock();
            screen.orientation.lock('portrait');
        }
    }    
    if (ionic.Platform.isAndroid()){
        $('.menu-lateral-adaptado').css('margin-top','-44px');
        fnCastEnabled = setTimeout(function() { verifyCastEnable(); }, 2000);
    }
    if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
        fnCastEnabled = setTimeout(function() { verifyCastEnableIOS(); }, 2000);      
    }



    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    var urlweb = localStorage.getItem("com.cinebrasilja.urlweb");
    var userID = localStorage.getItem("com.cinebrasilja.userID");
    var perfil = localStorage.getItem("com.cinebrasilja.perfil");
    if (perfil==null) perfil = '';
    $scope.user = JSON.parse(localStorage.getItem("com.cinebrasilja.user"));
    

    $scope.isMenuOpen = function() {  
        return $ionicSideMenuDelegate.isOpen();  
    };

    if ($scope.user.avatar==null){
        $scope.user.avatar = urlweb + "/wp-content/uploads/icone-usuario.png";
    }
    var carregouCategorias = 0;
    function carregarCategorias(){
        caminho = urlwebservice + 'get_categories/?userID='+userID;
        //console.log(caminho);
        $http.get(caminho)
        .success(function(data, status, headers,config){
            $scope.categorias = data.categorias;
            carregouCategorias++;
            if (carregouCategorias==2){
                setTimeout(function() { exibirCategoriaPerfil();}, 500);
            }
        })
        .error(function(data, status, headers, config){
            //console.error(data);
            //alertar(data.error);
        });
    }
    function carregarCategoriasKids(){
        caminho = urlwebservice + 'get_categories_kids/?userID='+userID;
        $http.get(caminho)
        .success(function(data, status, headers,config){
            $scope.categoriasKids = data.categorias;
            carregouCategorias++;
            if (carregouCategorias==2){
                setTimeout(function() { exibirCategoriaPerfil();}, 500);
            }
        })
        .error(function(data, status, headers, config){
            //alertar(data.error);
        });
    }
    initializeCastApiIOS = function(){
        devices = cordova.plugins.chromecastios.devices;
        if (devices.length > 0){
            buttonsCastIOS = [];
            //console.log(devices);
            var selecionado;
            angular.forEach(devices, function(device, chave){
                buttonsCastIOS[chave] = {
                    text: device.friendlyName,
                    type: 'button button-block btnAlerta',
                    onTap: function(e) {
                        deviceSelecionado = device;
                    }
                }
            });            
            var myPopup = $ionicPopup.show({
                    title: 'Selecione o dispositivo:',        
                    scope: $scope,
                    cssClass: 'popup-head-none',
                    buttons: buttonsCastIOS
            });
            myPopup.then(function(res) {
                cordova.plugins.chromecastios.selectDevice(deviceSelecionado).then(function(response){
                    cordova.plugins.chromecastios.launchApplication().then(function(response){
                        castSessionIOS = response;
                        $('.btn-cast').hide();
                        $('.btn-cast-ativo').show();
                    }).catch(function(error){
                        $('.btn-cast').show();
                        $('.btn-cast-ativo').hide();
                    });
                }).catch(function(error){
                    $('.btn-cast').show();
                    $('.btn-cast-ativo').hide();
                });
            }); 
        }
    }

    carregarCategorias();
    carregarCategoriasKids();

    $scope.showVolumeControls = function(){
        if ($('.controle-volume').css('display')=='none')
            $('.controle-volume').show()
        else
            $('.controle-volume').hide()
    };
    

    function alertar(strErro){
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
            title: 'Atenção',
            template: strErro,
            okText: 'OK',
            okType: 'btnAlerta'
        });
        alertPopup.then(function(res) {
            return;
        });
    }    

    $scope.logout = function(){
        data = JSON.parse(localStorage.getItem('com.cinebrasilja.user'));
        //console.log(data);
        usuario = data.username;
        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }             
        var dados = $.param({
            log: usuario
        });

        caminho = urlwebservice + 'validaLogin?acao=-1';
        $http.post(caminho, dados, config)
        .success(function(data, status, headers, config){
            //console.log(data);
            if (data.status=='ok'){
                localStorage.setItem("com.cinebrasilja.chave", "");
                localStorage.setItem("com.cinebrasilja.userID", "");
                localStorage.setItem("com.cinebrasilja.user", "");
                localStorage.setItem("com.cinebrasilja.cookie", "");
                localStorage.setItem("com.cinebrasilja.urlwebservice", "");
                localStorage.setItem("com.cinebrasilja.urlweb", "");
                localStorage.setItem("com.cinebrasilja.perfil", "");
                $state.go('verificaLogin', {}, { reload: true, inherit: false, notify: true});
            }
        })
    };

    /*
    $scope.expandirCategoria = function(categoria){
        id = categoria.term_id;
        if ($('#menu-list-sub-'+id).css('display')=='none'){
            $('#menu-list-sub-'+id).fadeIn("fast");
            $('#icone-menu-'+id).removeClass("ion-ios-arrow-down").addClass("ion-ios-arrow-up");
        }
        else{
            $('#menu-list-sub-'+id).fadeOut("fast");
            $('#icone-menu-'+id).removeClass("ion-ios-arrow-up").addClass("ion-ios-arrow-down");
        }
    };
    */

    $scope.selecionarCategoria = function(category){
        $ionicHistory.nextViewOptions({
            disableBack: true,
            expire: 300
        });
        $ionicHistory.clearCache().then(function(){
            $state.go('menu.localizarTitulosCategoria', {'category': category}, {reload: true, inherit: false, notify: true});
        });        
    };

    $scope.playCast = function (){
        $(".ion-play").hide();        
        $(".ion-ios-pause").show();
        if (ionic.Platform.isAndroid()){
            currentMedia.play();
            tempoAtual = parseInt(currentMedia.currentTime);
           if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false;
           contadorProgressivo();            
        }
        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            cordova.plugins.chromecastios.playMedia();
            cordova.plugins.chromecastios.getPosition().then(function(response){
                tempoAtual = parseInt(response);
                //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}        
                if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false; 
                contadorProgressivo();
            });            
        }        

    };
    $scope.pauseCast = function (){
        $(".ion-play").show();        
        $(".ion-ios-pause").hide(); 
        if (ionic.Platform.isAndroid()){
            currentMedia.pause();
        }                       
        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            cordova.plugins.chromecastios.pauseMedia();
        }

        //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}        
        if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false; 
    };

    
    $scope.stopCast = function (){
        tempoAtual = 0;
        $(".ion-play").show();
        $(".ion-ios-pause").hide();

        if (ionic.Platform.isAndroid()){
            currentMedia.stop();
        }                       
        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            cordova.plugins.chromecastios.stopMedia();
        }        
        currentMedia = null;
        if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false; 
        //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}
        exibirClassificacaoTitulo();
    };


    $scope.classificarTitulo = function(acao){
        $ionicLoading.show();
        caminho = urlwebservice + 'posts/update_liked/?userID='+userID+"&postID="+videoCast.ID+"&acao="+ acao + "&ip=" + ip;
        $http.get(caminho)
            .success(function(data, status, headers,config){
                $ionicLoading.hide();
                $scope.likes = data.liked;
                $("#footer-bar-cast").hide();
                $(".nome-titulo-andamento-cast").html("");
                $(".imagem-titulo-andamento-cast").attr("src","");
                $('.tempo-cast').val("");
                $('.cronometro-cast').html("");                
                closeControlCast();
            })
            .error(function(data, status, headers, config){
                $ionicLoading.hide();    
                $("#footer-bar-cast").delay("fast").fadeOut();
                $(".nome-titulo-andamento-cast").html("");
                $(".imagem-titulo-andamento-cast").attr("src","");
                $('.tempo-cast').val("");
                $('.cronometro-cast').html("");                
                closeControlCast();
            });        
    }; 

    $scope.seekCast = function(segundos){
        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            cordova.plugins.chromecastios.pauseMedia();
        }
        if (ionic.Platform.isAndroid()){
            currentMedia.pause();
        }
        tempoAtual = parseInt(parseInt($('.tempo-cast').val()) + (segundos));
        if (tempoAtual < 0)
            tempoAtual = 0;
        if (tempoAtual >= tempoTotal)
            tempoAtual = parseInt((tempoTotal) -1);

        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            cordova.plugins.chromecastios.seekMedia(tempoAtual);
            cordova.plugins.chromecastios.playMedia();
        }
        if (ionic.Platform.isAndroid()){
            seekRequest = new chrome.cast.media.SeekRequest(); 
            seekRequest.currentTime = parseInt(tempoAtual);
            currentMedia.seek(seekRequest, onSucessSeek, onErrorSeek);
            currentMedia.play();
        }
        //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}        
        if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false;         
        contadorProgressivo();
    }

    $scope.trocarLegendaIdiomaCast = function(){
        $scope.playTitulo(videoCast,1);
    };

    $scope.setVolumeCast = function(valor){
        volumeCalc = (parseFloat(volumeAtual) + parseFloat(valor)).toFixed(2);
        if (!((volumeCalc>1) || (volumeCalc<0))){
            volumeAtual = volumeCalc;
            $('.volume-cast').val(volumeAtual);
            if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
                cordova.plugins.chromecastios.setVolumeForMedia(volumeAtual); 
            }
            if (ionic.Platform.isAndroid()){
                castSession.setReceiverVolumeLevel(parseFloat(volumeAtual), onSucessVolume, onErrorVolume);
            }
        }
    };

    $scope.voltarNavegacao = function(){
        closeControlCast();
        $("#footer-bar-cast").hide();
        $(".nome-titulo-andamento-cast").html("");
        $(".imagem-titulo-andamento-cast").attr("src","");
        $('.tempo-cast').val("");
        $('.cronometro-cast').html("");
    };

    $scope.playTitulo = function(video, trocaLegendaAux){
        trocaLegenda = trocaLegendaAux;
        $('.btn-legenda').hide();
        if (trocaLegenda==0){
            $('.controle-volume').hide();
        }
        if (video.episodios){
            return false;
        }
        videoCast = video;
        videoSelecionado = null;
        var lingua = 0;
        //console.log(video.media);
        if ((videoCast.media.en.mediakey!="") && (videoCast.media.en.mediakey!=null) && (videoCast.media.en.mediakey!='null') && (videoCast.media.en.mediakey!=undefined)){
            idiomaSelecionado = 'en';
            videoSelecionado = videoCast.media.en;
            videoSelecionado.idioma = 'en';
            currentMediaURL = videoCast.media.en.url;
            lingua++;
        }                
        if ((videoCast.media.pt.mediakey!="") && (videoCast.media.pt.mediakey!=null) && (videoCast.media.en.mediakey!='null') && (videoCast.media.en.mediakey!=undefined)){
            idiomaSelecionado = 'pt';
            videoSelecionado = videoCast.media.pt;
            videoSelecionado.idioma = 'pt';
            currentMediaURL = videoCast.media.pt.url;
            lingua++;
        }
        if (lingua==1){
            playVideo();
        }
        else{
            $('.btn-legenda').show();
            var flagAcaoVideo = '';
            var myPopup = $ionicPopup.show({
                    scope: $scope,
                    cssClass: 'popup-head-none',
                    buttons: [
                        {
                            text: 'Dublado',
                            type: 'button button-block btnAlerta',
                            onTap: function(e) {
                                idiomaSelecionado = 'pt';
                                videoSelecionado = videoCast.media.pt;
                                flagAcaoVideo = 1;
                            }
                        },
                        {
                            text: 'Legendado',
                            type: 'button button-block btnAlerta',
                            onTap: function(e) {
                                idiomaSelecionado = 'en';
                                videoSelecionado = videoCast.media.en;
                                flagAcaoVideo = 1;
                            }
                        }
                    ]
            });
            myPopup.then(function(res) {
                if (!(flagAcaoVideo==''))
                    playVideo();
            });
            IonicClosePopupService.register(myPopup);
            $('.popup-head').hide();
        }
    } 

    function playVideo(){
        /**/
        porcentagemAtual = 0; 
        porcentagemAnterior = 0;
        /**/
        localStorage.setItem("com.cinebrasilja.video",JSON.stringify(videoCast));        
        localStorage.setItem("com.cinebrasilja.videoSelecionado",JSON.stringify(videoSelecionado));        

        if (ionic.Platform.isAndroid()){
            if (castSession==null){
                $state.go('menu.player');
                //$state.go('menu.player', {'video': videoCast, 'videoSelecionado' : videoSelecionado});
            }
            else{
                $ionicLoading.show();            
                /*************************************************************/                
                /************ INICIO - CHROME CAST - ANDROID *****************/
                /*************************************************************/                
                //if(typeof fnContProg !== "undefined"){clearTimeout(fnContProg);}
                if(contadorAtivo) clearTimeout(fnContProg); contadorAtivo = false; 
                currentMediaURL = videoSelecionado.url;
                var activeTrackAux = 0;
                var legenda = "";
                if ((videoSelecionado.legenda) && (videoSelecionado.legenda!==undefined) && (videoSelecionado.legenda!="")){
                    activeTrackAux = 1;
                    legenda = videoSelecionado.legenda;
                }
                var mediaInfo = new chrome.cast.media.MediaInfo(currentMediaURL,"video/mp4", legenda, activeTrackAux);             
                var metadata = new chrome.cast.media.MovieMediaMetadata();  
                metadata.title = "";
                metadata.subtitle = videoCast.post_title;
                mediaInfo.metadataType = 2;
                mediaInfo.metadata = metadata;

                var request = new chrome.cast.media.LoadRequest(mediaInfo);
                
                castSession.loadMedia(request, onMediaDiscovered.bind(this, 'loadMedia'), onMediaError);     
                setTimeout(function() { 
                    $('.like-up-cast').attr('src', $('.like-up').attr('src'));
                    $('.like-down-cast').attr('src', $('.like-down').attr('src'));
                    $ionicLoading.hide(); 
                }, 1200); 
                /*************************************************************/                
                /*************** FIM - CHROME CAST - ANDROID *****************/
                /*************************************************************/                

            }
        }
        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            /*************************************************************/                
            /*************** INICIO - CHROME CAST - IOS ******************/
            /*************************************************************/                   
            //console.log('aqui',castSessionIOS,videoSelecionado);
            if ((castSessionIOS==null)||(castSessionIOS==0)){
                $state.go('menu.player');
                //$state.go('menu.player', {'video': videoCast, 'videoSelecionado' : videoSelecionado});
            }
            else{
                mediaUrl = videoSelecionado.url;
                // metadata.title = null;
                var activeTrackAux = 0;
                var legenda = "";
                if ((videoSelecionado.legenda) && (videoSelecionado.legenda!==undefined) && (videoSelecionado.legenda!="")){
                    activeTrackAux = 1;
                    legenda = videoSelecionado.legenda;
                }                   
                var metadata = {'title': '', 'subtitle': videoCast.post_title, 'legenda': legenda, 'activeTrackAux': activeTrackAux};
                cordova.plugins.chromecastios.loadMedia(mediaUrl, "video/mp4", 1, metadata, 1).then(function(response){
                    //console.log(response);
                    onMediaDiscoveredIOS(response);                   
                    setTimeout(function() { 
                        $('.like-up-cast').attr('src', $('.like-up').attr('src'));
                        $('.like-down-cast').attr('src', $('.like-down').attr('src'));
                        $ionicLoading.hide(); 
                    }, 1200);
                    //successfully loaded media on the device
                    //returns a media Status object on success

                }).catch(function(error){
                    //console.log('error',error);
                    //there was an error loading the media
                    //most likely cause is a disconnection, network issue or time out of the device
                });
            }          
            /*************************************************************/                
            /*************** FIM - CHROME CAST - IOS *********************/
            /*************************************************************/                                   
        }
            
    }  

}])

.controller('selecionaPerfilCtrl', ['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup', '$ionicScrollDelegate', '$ionicHistory',
function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup, $ionicScrollDelegate, $ionicHistory) {
    $ionicNavBarDelegate.showBackButton(false);
    $ionicNavBarDelegate.showBar(true);
    var urlweb = localStorage.getItem("com.cinebrasilja.urlweb");

    if (ionic.Platform.isAndroid()){
        window.screen.orientation.unlock();
        window.screen.orientation.lock('portrait');
    }
    if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
        if (screen.orientation){
            screen.orientation.unlock();
            screen.orientation.lock('portrait');
        }
    }

    $scope.trocarPerfil = function(perfilSelecionado){
        localStorage.setItem("com.cinebrasilja.perfil", perfilSelecionado);
        $ionicHistory.nextViewOptions({
            disableBack: true,
            expire: 300
        });
        $ionicHistory.clearCache().then(function(){
            setTimeout(function() { exibirCategoriaPerfil();}, 500);
            $state.go('menu.home', {}, { reload: true, inherit: false, notify: true});    
        });        
    };
}])

.controller('homeCtrl', ['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup', '$ionicScrollDelegate', '$ionicHistory', '$location', '$anchorScroll',
function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup, $ionicScrollDelegate, $ionicHistory, $location, $anchorScroll) {
    $ionicNavBarDelegate.showBackButton(false);
    $ionicNavBarDelegate.showBar(true);    


    if (ionic.Platform.isAndroid()){
        window.screen.orientation.unlock();
        window.screen.orientation.lock('portrait');
    }
    if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
        if (screen.orientation){
            screen.orientation.unlock();
            screen.orientation.lock('portrait');
        }
    }
    //$scope.$on('$ionicView.enter', function(e) {
    //    $('.botao-menu-geral').removeClass('hide');
    //});
      

    $ionicLoading.show();
    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    var userID = localStorage.getItem("com.cinebrasilja.userID");
    var perfil = localStorage.getItem("com.cinebrasilja.perfil");
    if (perfil==null) perfil = '';
    
    var carregando = 0;

    if(perfil==""){
        $scope.displayBanner = "";
        caminho = urlwebservice + 'posts/get_banners'+perfil+'/?userID='+userID;
        $http.get(caminho)
        .success(function(data, status, headers,config){
            //console.log(data);
            $scope.banners = data.dados;
            carregando++;
            if (carregando==2)
                $ionicLoading.hide();
        })
        .error(function(data, status, headers, config){
            $ionicLoading.hide();
            //console.log(data);
        });
    }
    else{
        $scope.displayBanner = "display:none;";
        carregando++;
        if (carregando==2){
            $ionicLoading.hide();
            //carregouHome();
        }
    }

    caminho = urlwebservice + 'posts/get_videos'+perfil+'/?userID='+userID;
    console.log(caminho);
    $http.get(caminho)
    .success(function(data, status, headers,config){
        $scope.blocos = data;
        //console.log($scope.blocos);
        carregando++;
        if (carregando==2){
            $ionicLoading.hide();
            //carregouHome();            
        }
    })
    .error(function(data, status, headers, config){
        $ionicLoading.hide();
    });

    $scope.swiper = {};
    $scope.onReadySwiper = function (swiper) {
        swiper.initObservers();
		swiper.on('slideChangeStart', function () {
    		//console.log('slide start');
		});
		swiper.on('onSlideChangeEnd', function () {
			//console.log('slide end');
		});		
    };
    $scope.selecionarTitulo = function(titulo, pos1, pos2){
        /*
        if ((pos1!='undefined') && (pos2!='undefined'))
            ancoraHome = 'ancora-'+pos1+'-'+pos2;
        localStorage.setItem("ancoraHome",ancoraHome);
        */
        localStorage.setItem("com.cinebrasilja.titulo",JSON.stringify(titulo));
        $state.go('menu.titulo');
        //$state.go('menu.titulo', {'titulo': titulo});
    };


    /*
    function carregouHome(){
        if ((ancoraHome!='') && (ancoraHome!=null)){
            //setTimeout(function() {
                $location.hash(ancoraHome);
                //$anchorScroll();                
                //$(document).scrollTop( $("#"+ancoraHome).offset().top); 
            //}, 2000);
        }
    }
    */

    /*
    $scope.scrollContentStart = function() {
        $('.bar-header').hide();
        if(typeof fnScroll !== "undefined")
            clearTimeout(fnScroll);
        fnScroll = setTimeout(function() { scrollContentFinish() }, 250);
    };
    function scrollContentFinish () {
        $('.bar-header').show();
    };
    */
}])

.controller('notificacoesCtrl',['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup',
function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup) {
    //window.screen.orientation.unlock();
    $ionicNavBarDelegate.showBackButton(false);
    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    var userID = localStorage.getItem("com.cinebrasilja.userID");
    caminho = urlwebservice + 'user/get_notifications/?userID='+userID;
    $http.get(caminho)
    .success(function(data, status, headers,config){
        //console.log(data);
        //$scope.dados = [{'titulo':'asdasd', data:'01-02-2030', status:0}];
        //console.log($scope.dados);
        $scope.dados = data.dados;
   });

   $scope.visualizarNotificacao = function(notificacao){
        var alertPopup = $ionicPopup.alert({
            title: notificacao.titulo,
            template: notificacao.mensagem,
            buttons: [
                {
                    text: "OK",
                    type: 'button button-block',
                }
            ],
            okText: 'OK'
        });
        alertPopup.then(function(res) {
            return;
        });       
        caminho = urlwebservice + 'user/update_status_mensagem/?userID='+userID+"&mID="+notificacao.id;
        $http.get(caminho)
        .success(function(data, status, headers,config){
            //console.log(data);
        })
        .error(function(data, status, headers, config){

        });       
   };
   
}])

.controller('tituloCtrl',['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup', 'IonicClosePopupService', '$ionicPlatform', '$ionicHistory',
function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup, IonicClosePopupService, $ionicPlatform, $ionicHistory) {
    //window.screen.orientation.unlock();
    tempoVideoAtual = 0;
    porcentagemVideoAtual = 0;
    
    $ionicLoading.show();
    $ionicNavBarDelegate.showBackButton(true);
    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    var userID = localStorage.getItem("com.cinebrasilja.userID");
    var perfil = localStorage.getItem("com.cinebrasilja.perfil");
    if (perfil==null) perfil = '';

    var flagLoading = false;
    var titulo = JSON.parse(localStorage.getItem("com.cinebrasilja.titulo"));
    var acao;
    
    $scope.displayIconePlay = "display:none;";
    $scope.selectTemporadasDisplay = "display:none;";

    if (perfil!=""){
        $scope.displayBtnMinhaLista = "display:none;";
        $ionicLoading.hide();                
    }
    else{
        caminho = urlwebservice + 'posts/my_list/?userID='+userID+"&postID="+titulo.ID+"&acao=-1";
        $scope.displayBtnMinhaLista = "display:none;";
        $http.get(caminho).success(function(data, status, headers,config){
            if (data.dados.time!=null) tempoVideoAtual = data.dados.time;
            if (data.dados.porcent!=null) porcentagemVideoAtual = data.dados.porcent;
            ip = data.dados.ip;
            $scope.classeMinhaLista = data.dados.classe;      
            $scope.displayBtnMinhaLista = "";
            acao = data.dados.acao;
            $scope.likes = data.dados.liked;                    
            $ionicLoading.hide();
        });
    }
    if (titulo==null)
        $state.go('menu.home');

    if ((titulo.episodios) || (titulo.episodios!==null)){
        $scope.displayBtnPlay = 'display:none;';
        $scope.displayBtnCast = 'display:none;';
        $scope.displayEpisodios = '';        

        var i = 0;
        var selTemp = [];
        angular.forEach(titulo.episodios, function(epi, element) {
            selTemp[i++] = epi.temporada;
            titulo.episodios[element]['classeDisplay'] = "opcoesTemporada " + ReplaceAll((epi.temporada),' ', '-');
        });    
        //console.log(selTemp);
        function onlyUnique(value, index, self) { 
            return self.indexOf(value) === index;
        }

        // usage example:
        var selTemp = selTemp.filter(onlyUnique);
        if(selTemp.length==1){
            $scope.temporadas = selTemp;
        }
        else if(selTemp.length>1){
            $scope.temporadas = selTemp;
            $scope.selectTemporadasDisplay = "";
            setTimeout(function() {
                selecionarTemporada();
            }, 500);                  
        } 
    }
    else{
        $scope.displayBtnCast = '';
        $scope.displayBtnPlay = '';
        $scope.displayEpisodios = 'display:none;';
        $scope.displayIconePlay = "";    
    }
    $scope.titulo = titulo; 

    $scope.selecionarSerie = function(titulo){
        localStorage.setItem("com.cinebrasilja.titulo",JSON.stringify(titulo));
        $state.go('menu.titulo', {}, { reload: true, inherit: false, notify: true});
    }
    
    $scope.minhaLista = function(video){
        $ionicHistory.clearCache();
        $ionicLoading.show();
        caminho = urlwebservice + 'posts/my_list/?userID='+userID+"&postID="+video.ID+"&acao="+ acao;
        $http.get(caminho)
            .success(function(data, status, headers,config){
                $(".minha-lista").attr("disabled", true);
                $(".loading-container").css('top', 'auto').css("background-color", "#f8a20c");
                $(".loading").css('top', 'auto').css("background-color", "#f8a20c");
                $ionicLoading.hide();    
                $ionicLoading.show({ template: data.dados.mensagem, animation: 'fade-in', noBackdrop: true, duration: 1900});
                setTimeout(function() {
                    $(".minha-lista").attr("disabled", false);
                    $(".loading").removeAttr('style');
                    $(".loading-container").removeAttr('style');
                }, 1950);
                acao = data.dados.acao;
                $scope.classeMinhaLista = data.dados.classe;
            })
            .error(function(data, status, headers, config){
                $ionicLoading.hide();    
                alertar(data.error);
            });
    };     
    $scope.classificarTitulo = function(acao,titulo){
        $ionicLoading.show();
        caminho = urlwebservice + 'posts/update_liked/?userID='+userID+"&postID="+titulo.ID+"&acao="+ acao + "&ip=" + ip;
        $http.get(caminho)
            .success(function(data, status, headers,config){
                $ionicLoading.hide();
                $scope.likes = data.liked;
            })
            .error(function(data, status, headers, config){
                $ionicLoading.hide();    
            });        
    };    
    function alertar(mensagem){
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
            title: "Atenção",
            template: mensagem,
            buttons: [
                {
                    text: "OK",
                    type: 'button button-block btnAlerta',
                }
            ],
            okText: 'OK',
            okType: 'btnAlerta'
        });
        alertPopup.then(function(res) {
            return;
        });
    }
    
}])


.controller('playerCtrl',['$scope','$rootScope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup',
function ($scope, $rootScope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup) {
    $ionicNavBarDelegate.showBackButton(true);  
    $ionicLoading.show();
    var marginTop,btnCast, btnCastAtivo;
    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    var userID = localStorage.getItem("com.cinebrasilja.userID");

    btnCastAtivo = $('.btn-cast-ativo').css('display');
    btnCast = $('.btn-cast').css('display');
    $('.btn-cast-ativo, .btn-cast').css('display','none');

    $scope.$on("$ionicView.leave", function(){
        $('.btn-cast-ativo').css('display', btnCastAtivo);
        $('.btn-cast').css('display',btnCast);
        if (ionic.Platform.isAndroid()){
            //$('.scroll').css('margin-top','44px');
            window.screen.orientation.unlock();
            window.screen.orientation.lock('portrait');                
            $('.scroll').css('margin-top',marginTop);           
        }
        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            if (screen.orientation){
                screen.orientation.unlock();
                screen.orientation.lock('portrait');                
            }
            $('.scroll').css('margin-top',marginTop);
        }
    });

    //localStorage.setItem("com.cinebrasilja.video",JSON.stringify(videoCast));        
    //localStorage.setItem("com.cinebrasilja.videoSelecionado",JSON.stringify(videoSelecionado));        

    var video = JSON.parse(localStorage.getItem("com.cinebrasilja.video"));
    var videoSelecionado = JSON.parse(localStorage.getItem("com.cinebrasilja.videoSelecionado"));

    /*
    var video = $stateParams.video;
    if (video!=undefined)
        localStorage.setItem("com.cinebrasilja.video",JSON.stringify(video));
    else
        video = JSON.parse(localStorage.getItem("com.cinebrasilja.video"));

    var videoSelecionado = $stateParams.videoSelecionado;
    if (videoSelecionado!=undefined)
        localStorage.setItem("com.cinebrasilja.videoSelecionado",JSON.stringify(videoSelecionado));
    else
        videoSelecionado = JSON.parse(localStorage.getItem("com.cinebrasilja.videoSelecionado"));
    */
    
    if (videoSelecionado.mediakey==""){
        $ionicLoading.hide();    
        $state.go('menu.titulo');
    }
    else{
        if (ionic.Platform.isAndroid()){
            window.screen.orientation.lock('landscape');
        }
        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            if (screen.orientation){
                screen.orientation.lock('landscape');
            }
        }
        var tempoPlayerSamba = false;
        if ((!isNaN(porcentagemVideoAtual)) && (porcentagemVideoAtual!=null) && (porcentagemVideoAtual!=undefined) && (porcentagemVideoAtual!=0) && 
            ((!isNaN(tempoVideoAtual)) && (tempoVideoAtual!=null) && (tempoVideoAtual!=undefined) && (tempoVideoAtual!=0))){
            tempoPlayerSamba = parseInt(tempoVideoAtual);
        }
        //console.log('tempoPlayerSamba',tempoPlayerSamba);
        videoCast = video;
        setTimeout(function() {
            var playerConfig = {
                height: window.innerHeight,
                width: window.innerWidth,
                ph: video.key,
                m: videoSelecionado.mediakey,
                events:{
                    "onListen": eventListener,
                    "onFinish": eventListener
                },
                playerParams:{
                    responsive:true,
                    cast:false,
                    wideScreen: true,
                    autoStart:true,
                    html5:true,
                    resume: tempoPlayerSamba,
                    fsOnStart:true,
                    allowfullscreen:true
               }
               /*                
               playerParams:{
						wideScreen: true,
						html5:true,
						startOutput: '480p',
                        autoStart:true,
						cast:true,
						captionTheme:['ffffff','28','pt-BR'],
			   }
               */
            };
            //if ((videoSelecionado.legenda) && (videoSelecionado.legenda!==undefined) && (videoSelecionado.legenda!="") && (videoSelecionado.legenda!=null)){
            if ((videoSelecionado.idioma) && (videoSelecionado.idioma=='en')){
                playerConfig.playerParams.captionTheme = ['ffffff','25','pt-BR'];
            }
            player = new SambaPlayer('player', playerConfig);
            marginTop = $('.scroll').css('margin-top');
            if (ionic.Platform.isAndroid()){
                $('.scroll').css('margin-top','0px');
            }
            if (ionic.Platform.isIPad() || ionic.Platform.isIOS()){
                $('.scroll').css('margin-top','20px');
            }
            //console.log('player',playerConfig);
        }, 1000);

        var userInicio = 0;
        function eventListener(player) {
            //console.log('player',player);
            var porcento = (parseInt(player.duration)/100);
            if(player.event=='onFinish'){
            }
            if(player.event=='onListen'){
                assitido = parseInt(player.eventParam/parseInt(porcento));
                if(assitido==0){
                     assitido = 0.01;
                }
                if(userInicio != assitido){
                    userInicio = assitido;
                    if (!(isNaN(assitido))){
                        updateTempoUsuarioVideo(parseInt(player.eventParam), parseInt(assitido));
                        porcentagemVideoAtual = parseInt(assitido);
                        tempoVideoAtual = parseInt(player.eventParam);
                    }
                }                
            }
        }


        /*
        if (video.requestFullscreen) {
            video.requestFullscreen();
        } else if (video.msRequestFullscreen) {
            video.msRequestFullscreen();
        } else if (video.mozRequestFullScreen) {
            video.mozRequestFullScreen();
        } else if (video.webkitRequestFullscreen) {
            video.webkitRequestFullscreen();
        } 
        */ 
        $ionicLoading.hide();
    }
    
}])
   
.controller('cadastroCtrl', ['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope, $state, $stateParams, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup) {
    //window.screen.orientation.unlock();
    $ionicNavBarDelegate.showBackButton(false);
    $ionicNavBarDelegate.showBar(true);


    if (ionic.Platform.isAndroid()){           
        window.screen.orientation.unlock();
        window.screen.orientation.lock('portrait');
    }
    if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
        if (screen.orientation){
            screen.orientation.unlock();
            screen.orientation.lock('portrait');
        }
    }    
    
    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    var userID = localStorage.getItem("com.cinebrasilja.userID");
    var perfil = localStorage.getItem("com.cinebrasilja.perfil");
    if (perfil==null) perfil = '';

    
    $scope.displayBtnAtualizar = "";
    if (perfil!='')
        $scope.displayBtnAtualizar = "display:none;";
    
    $scope.alterarAvatar = function(){

        var myPopup = $ionicPopup.show({
                scope: $scope,
                title: '<b>Opções:</b>',
                buttons: [
                    /*
                    {
                        //text: '<b>Camera</b>',
                        type: 'button button-block btnAlerta ion-camera',
                        onTap: function(e) {

                        }
                    },
                    */
                    {
                        //text: '<b>Arquivo</b>',
                        type: 'button button-block btnAlerta ion-folder',
                        onTap: function(e) {
                        }
                    },
                    { 
                        //text: '<b>Cancelar</b>',
                        type: 'button button-block btnAlerta ion-close',    
                    },
                ]
        });
        myPopup.then(function(res) {
            //console.log('Tapped!', res);
        });
    };
        
    $scope.salvarCadastro = function(){
        //alert("salvando em desenvolvimento");
    };
}])
   
.controller('esqueciSenhaCtrl', ['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup',
function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup) {
    //$ionicNavBarDelegate.showBackButton(true);
    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    var usuario = "";

    if (ionic.Platform.isAndroid()){           
        window.screen.orientation.unlock();
        window.screen.orientation.lock('portrait');
    }
    if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
        if (screen.orientation){
            screen.orientation.unlock();
            screen.orientation.lock('portrait');
        }
    }    

    $scope.voltarLogin = function(){
        $state.go('login');
    };
    $scope.esqueciSenhaSubmeter = function(){
        usuario = $("#usuario").val();
        if(usuario===''){
            alertar('Preencha o usuário');
            return;
        }
        $ionicLoading.show();
        caminho = urlwebservice+'user/retrieve_password/?user_login='+usuario;
        $http.get(caminho)
        .success(function(data, status, headers,config){
            $ionicLoading.hide();
            var dados = JSON.stringify(data);
            alertar(data.msg);
            $state.go('login');
        })
        .error(function(data, status, headers, config){
            $ionicLoading.hide();
            if (data!=null){
                alertar(data.error);
            }
            return;
        });
    };
    function alertar(strErro){
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
            title: 'Atenção',
            template: strErro,
            okText: 'OK',
            okType: 'btnAlerta'
        });
        alertPopup.then(function(res) {
            return;
        });
    }

}])
   
.controller('localizarTitulosCtrl', ['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup) {
    //window.screen.orientation.unlock();
    $ionicNavBarDelegate.showBackButton(false);
    $ionicNavBarDelegate.showBar(true);

    if (ionic.Platform.isAndroid()){           
        window.screen.orientation.unlock();
        window.screen.orientation.lock('portrait');
    }
    if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
        if (screen.orientation){
            screen.orientation.unlock();
            screen.orientation.lock('portrait');
        }
    }    
    
    var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
    var userID = localStorage.getItem("com.cinebrasilja.userID");
    var perfil = localStorage.getItem("com.cinebrasilja.perfil");
    if (perfil==null) perfil = '';

    $scope.selecionarTitulo = function(titulo){
        localStorage.setItem("com.cinebrasilja.titulo",JSON.stringify(titulo));
        $state.go('menu.titulo');        
        //$state.go('menu.titulo', {'titulo': titulo});
    };

    $scope.pesquisar = function(){
        $ionicLoading.show();        
        caminho = urlwebservice + 'posts/search_videos'+perfil+'/?userID='+userID+"&search=" + $("#localizarTitulos").val();
        $http.get(caminho)
        .success(function(data, status, headers,config){
            $scope.dados = data.dados;
            //console.log(data);
            $ionicLoading.hide();
        })
        .error(function(data, status, headers, config){
            //console.log(data.error);
            var alertPopup = $ionicPopup.alert({ title: 'Atenção', template: data.error, okText: 'OK', okType: 'btnAlerta'});
            alertPopup.then(function(res) { return; });
            $ionicLoading.hide();
        });
    };
}])
.controller('localizarTitulosCategoriaCtrl',  ['$scope', '$stateParams', '$state', '$http', '$ionicNavBarDelegate', '$ionicLoading', '$ionicPopup',
    function ($scope, $stateParams, $state, $http, $ionicNavBarDelegate, $ionicLoading, $ionicPopup) {
        $ionicNavBarDelegate.showBackButton(false);
        $ionicNavBarDelegate.showBar(true);

        if (ionic.Platform.isAndroid()){           
            window.screen.orientation.unlock();
            window.screen.orientation.lock('portrait');
        }
        if ((ionic.Platform.isIPad()) || (ionic.Platform.isIOS())){
            if (screen.orientation){
                screen.orientation.unlock();
                screen.orientation.lock('portrait');
            }
        }        

        var urlwebservice = localStorage.getItem("com.cinebrasilja.urlwebservice");
        var userID = localStorage.getItem("com.cinebrasilja.userID");
        var category = $stateParams.category;
        var perfil = localStorage.getItem("com.cinebrasilja.perfil");
        if (perfil==null) perfil = '';
        
        $scope.displaySearch = "display:none;";
        if (category==null)
            category = JSON.parse(localStorage.getItem("com.cinebrasilja.category"));
        else
            localStorage.setItem("com.cinebrasilja.category",JSON.stringify(category)); 
        if (category==null)
            $state.go('menu.home');            


        $ionicLoading.show();        
        caminho = urlwebservice + 'posts/get_videos_category'+perfil+'/?userID='+userID + "&categoryID="+category.term_id;
        $http.get(caminho)
        .success(function(data, status, headers,config){
            $scope.listas = data.listas;
            $scope.total = data.total;
            $scope.categoria = category.name;
            $ionicLoading.hide();
        })
        .error(function(data, status, headers, config){
            var alertPopup = $ionicPopup.alert({ title: 'Atenção', template: data.error, okText: 'OK', okType: 'btnAlerta'});
            alertPopup.then(function(res) { return; });
            $ionicLoading.hide();
        });

        $scope.selecionarTitulo = function(titulo){
            localStorage.setItem("com.cinebrasilja.titulo",JSON.stringify(titulo));
            $state.go('menu.titulo');
            //$state.go('menu.titulo', {'titulo': titulo});
        };
}])